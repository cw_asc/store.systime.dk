<?php

$proxyUrl = 'http://vpn.systime.dk:3000/authorinfo/SalesInfo.ashx';
// $proxyUrl = 'http://192.168.3.3/authorinfo/SalesInfo.ashx';
// $proxyUrl = 'http://192.168.10.13/authorinfo/SalesInfo.ashx';
// $proxyUrl = 'http://192.168.10.13/authorinfo/Hmm.ashx';

$allowedIPs = array('127.0.0.1',
										'86.58.159.10',
										'89.221.168.220');

$allowAccess = false;
foreach (array('REMOTE_ADDR', 'HTTP_X_FORWARDED_FOR') as $key) {
	if (isset($_SERVER[$key])) {
		if (in_array($_SERVER[$key], $allowedIPs)) {
			$allowAccess = true;
		}
	}
}

if (false)
if (!$allowAccess) {
	header('Content-type: text/plain; charset=utf-8');
	die('Invalid request');
}

$query = $_SERVER['QUERY_STRING'];

$url = $proxyUrl;
if ($query) {
	$url .= (strpos($proxyUrl, '?') === false ? '?' : '&').$query;
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, false);
//  curl_setopt($ch, CURLOPT_HEADERFUNCTION, 'copy_header');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$data = curl_exec($ch);
curl_close($ch);

// header('Content-Type: text/xml');
echo $data;
die();
?>
