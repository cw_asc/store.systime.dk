<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<?php
ini_set('display_errors', 1);
$actionUrl = $_SERVER['REQUEST_URI'];
$lang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : '';
$showEAN = isset($_REQUEST['showean']) && $_REQUEST['showean'];
$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;

$query = isset($_POST['q']) ? $_POST['q'] : '';

$locale = array();
switch ($lang) {
case 'da_DK':
$locale = array(
'Search' => 'Søg',
'Cancel' => 'Annuller',
'Close' => 'Luk',
'Select' => 'Vælg',
'Company name' => 'Skole-/institutionsnavn',
'Enter company name' => 'Indtast firmanavn',
'No matches' => 'Ingen resultater.',
'Showing %d of %d' => 'Viser %d ud af %d',
'EAN' => 'EAN-nr.',
'TAX/VAT' => 'Momsnr.',
);
break;
}

function __($label) {
 global $locale;
 return isset($locale[$label]) ? $locale[$label] : $label;
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
<style type="text/css">
body {
color:#444444;
font-family:arial,helvetica,sans-serif;
	font-size: 11px;
}
div.address {
cursor: pointer;
padding: 1px 0;
}
div.address:hover {
background: #fff7b7;
}
.search-result-info {
 margin: 0 0 5px 0;
	font-style: italic;
 }
.search-result {
 margin: 0 0 5px 0;
 }
.search-result .address .name {
  font-weight: bolder;
}
.search-result button {
border: solid 1px #aaa;
font-size:smaller;
	/* cursor: pointer; */
}
</style>
		<title>Find Company</title>
<script type="text/javascript">/*<![CDATA[*/
var setAddress = function(address) {
<?php
$callbackFunctionName = isset($_GET['callback']) ? $_GET['callback'] : null;
if ($callbackFunctionName): ?>
if (parent && parent.<?php echo $callbackFunctionName ?>) {
	parent.<?php echo $callbackFunctionName ?>(address);
}
<?php endif ?>
}
/*]]>*/</script>

	</head>
	<body>
<div class="container">
		<form action="<?php echo htmlspecialchars($actionUrl) ?>" method="post">
		<div>
			<label><?php echo __('Company name') ?>
			<input type="text" name="q" value="<?php echo htmlspecialchars($query) ?>"/>
		</label>
			<input type="submit" name="search" value="<?php echo __('Search') ?>"/>
			<input type="button" name="cancel" value="<?php echo __('Cancel') ?>" onclick="setAddress()"/>

			<?php if (false): ?>
<br/>
	<div class="message"><?php echo __('Enter company name') ?></div>
	<?php endif ?>

		</div>
	</form>
<?php

function substitute($template, $data) {
	$text = $template;
	foreach ($data as $field => $value) {
		if (!is_array($value)) {
			$text = str_replace('{'.$field.'}', $value, $text);
		}
	}
	return $text;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	// Render search result
	$url = 'https://konto.systime.dk/?eID=tx_systimecrm_search';
	$url .= (strpos('?', $url) === false ? '&' : '?').'type=company';
	$url .= '&q='.urlencode($query);
	$url .= '&limit='.urlencode($limit);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_exec($ch);
	curl_close($ch);

	$content = __('No matches');

	if ($response) {
		try {
			$result = json_decode($response, true);
			if (isset($result, $result['ResultSet'], $result['ResultSet']['Result']) && $result['ResultSet']['Result'] && is_array($result['ResultSet']['Result'])) {
				$resultSet = $result['ResultSet'];
				$matches = $resultSet['Result'];

				$template = '
<div class="search-result">
<div class="address" ###onclick_setAddress###>
		<div class="name">{name}</div>
<div class="street">{street}</div>
<div class="city"><div class="zip">{zip}</div> <div class="city">{city}</div></div>
<div class="country">{country}</div>
'.($showEAN ? '<div class="ean">EAN: {ean}</div>': '').'
</div>
<div class="button">
<button type="button" ###onclick_setAddress###>'.__('Select').'</button>
</div>
</div>
';

				$template = '
<div class="search-result">
<div class="address" ###onclick_setAddress###>
		<span class="name">{name}</span>;
<span class="street">{street}</span>;
<span class="city"><span class="zip">{zip}</span> <span class="city">{city}</span></span>;
<span class="country">{country}</span>
###select_button###
###taxvat###
###ean_number###
</div>
';

$template .= '</div>
';

				$content = '';

				$content .= '<div class="search-result-info">'
					.'('.sprintf(__('Showing %d of %d'), $resultSet['totalResultsReturned'], $resultSet['totalResultsAvailable']).')'
					.'</div>';

				foreach ($matches as $company) {
					$item = $template;
					$item = str_replace('###select_button###', '<button type="button" ###onclick_setAddress###>'.__('Select').'</button>', $item);
					$item = str_replace('###onclick_setAddress###', 'onclick="setAddress('.htmlspecialchars(json_encode($company)).')"', $item);

					$taxvat = $company['taxvat'] ? '<div class="taxvat">'.__('TAX/VAT').': {taxvat}</div>' : '';
					$item = str_replace('###taxvat###', $taxvat, $item);

					$ean_number = ($showEAN && $company['ean']) ? '<div class="ean">'.__('EAN').': {ean}</div>' : '';
					$item = str_replace('###ean_number###', $ean_number, $item);

					$content .= substitute($item, $company);
				}

// 				echo '<pre>';
// 				print_r($result);
// 				// print_r($result['ResultSet']['Result']);
// 				echo '</pre>';

			}
		} catch (Exception $ex) {}
	}
	echo $content;
}
?>

		</div>
	</body>
</html>
