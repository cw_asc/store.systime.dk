<?php
require_once 'init.php';

header('Content-type: text/plain; charset=utf-8');

$code = isset($_REQUEST['code']) ? trim($_REQUEST['code']) : 0;
if (!$code) {
	$code = isset($_REQUEST['ajour']) ? trim($_REQUEST['ajour']) : 0;
}
if ($code) {
	$product = Mage::getModel('catalog/product')->loadByAttribute('ajour_code', $code);
	if ($product) {
		$url = $product->getProductUrl();
		if ($url) {
			header('Location: '.$url);
		}
	}
}

echo 'Missing or invalid code: '.$code;


exit;
