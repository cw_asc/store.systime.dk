<?php
require_once 'init.php';

$productUrl = '/';
$productSku = isset($_REQUEST['sku']) ? trim($_REQUEST['sku']) : '';
if ($productSku) {
	$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
	if ($product) {
		$productUrl = $product->getProductUrl();
	}
}

header('Location: '.$productUrl);
exit;
?>
