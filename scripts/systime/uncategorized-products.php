<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<?php require_once 'init.php' ?>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
		<title>Uncategorized Products</title>
	</head>
	<body>
		<h1>Uncategorized Products</h1>

		<ul>
<?php
$editProductUrl = '/boglokalet/catalog_product/edit/id/%id%/';
// $editProductUrl = '/boglokalet/catalog_product/categories/id/%id%/';

$read = Mage::getSingleton('core/resource')->getConnection('core_read');
if ($res = $read->query('
select
entity_id, sku,
count(catalog_category_product.category_id) categoryCount
from
catalog_product_entity
left outer join catalog_category_product on catalog_category_product.product_id = catalog_product_entity.entity_id
group by
entity_id, sku
')) {
while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
if ($row['categoryCount'] == 0) {
echo '<li>';
echo '<a target="editcategories" href="'.htmlspecialchars(str_replace('%id%', $row['entity_id'], $editProductUrl)).'">'
.$row['sku']
.'</a>';
echo '</li>';
}
$productsIds[] = $row['entity_id'];
}
}
?>
		</ul>
	</body>
</html>
