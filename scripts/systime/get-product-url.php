<?php
require_once 'init.php';

$productUrl = null;
$superProductUrl = null;
$productSku = isset($_REQUEST['sku']) ? trim($_REQUEST['sku']) : '';
if ($productSku) {
	$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);

	// Check whether a parent product exists (super product)
	if ($product) {
		$res = Mage::getSingleton('core/resource');
		$read = $res->getConnection('core_read');
		$select = $read
			->select()
			->from($res->getTableName('catalog/product_super_link'), 'parent_id')
			->where('product_id = ?', $product->getId())
			;

		$superProductId = $read->fetchOne($select);
		if ($superProductId) {
			$superProduct = Mage::getModel('catalog/product')->load($superProductId);
			$superProductUrl = $superProduct->getProductUrl();
		}

		$productUrl = $product->getProductUrl();
	}
}

$redirect = isset($_REQUEST['redirect']) ? trim($_REQUEST['redirect']) : '';
if ($redirect) {
	if ($superProductUrl) {
		header('Location: '.$superProductUrl);
		exit;
	}
	if ($productUrl) {
		header('Location: '.$productUrl);
		exit;
	}
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
		<title>Get Product Url</title>
	</head>
	<body>
		<h1>Get Product Url</h1>

		<form action="#" method="get">
			<div>
				<label>sku <input type="text" name="sku" value="<?php echo htmlspecialchars($productSku) ?>"/></label>
				<button type="submit">OK</button>
			</div>
		</form>

	<div>
<?php
if ($productUrl) {
	echo '<p>Product url: <a href="'.htmlspecialchars($productUrl).'">'.htmlspecialchars($productUrl).'</a></p>';
	if ($superProductUrl) {
		echo '<p>Super product url: <a href="'.htmlspecialchars($superProductUrl).'">'.htmlspecialchars($superProductUrl).'</a></p>';
	}
} else if ($productSku) {
	echo 'Invalid sku: '.$productSku;
}
?>
	</div>
	</body>
</html>
