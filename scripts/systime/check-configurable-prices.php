<?php
require_once 'init.php';

header('Content-type: text/plain; charset=utf-8');

// $res = Mage::getSingleton('core/resource');
// $read = $res->getConnection('core_read');
// $select = $read
// 	->select()
// 	->from($res->getTableName('catalog/product_super_link'), 'parent_id')
// 	->where('product_id = ?', $product->getId())
// 	;

$licenseTypeAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'licens_type');

$options = Mage::getModel('eav/entity_attribute_source_table')->setAttribute($licenseTypeAttribute)->getAllOptions(false);

$licenseTypeLabels = array();
foreach ($options as $option) {
	$licenseTypeLabels[$option['value']] = $option['label'];
}

// print_r($licenseTypeLabels);

$resource = Mage::getSingleton('core/resource');
$db = $resource->getConnection('core_read');

// Get configurable products and the configured prices
$select = $db->select()
	->from(array('product' => $resource->getTableName('catalog/product')), 'sku')
	->join(array('product_super_attribute' => $resource->getTableName('catalog/product_super_attribute')), 'product_super_attribute.product_id = product.entity_id')
	->join(array('product_super_attribute_pricing' => $resource->getTableName('catalog/product_super_attribute_pricing')), 'product_super_attribute_pricing.product_super_attribute_id = product_super_attribute.product_super_attribute_id', array('price' => 'pricing_value', 'license_type' => 'value_index'))
	->where('product_super_attribute.attribute_id = ?', $licenseTypeAttribute->getId())
	// ->where('product_super_attribute.product_id = ?', 2592)
	->order('price', 'license_type')
	;

// echo $select->assemble(), "\n";

// Map from product_id => array(<attribute value> => <price>)
$superProducts = array();
$rows = $db->fetchAll($select);
foreach ($rows as $row) {
	$sku = $row['sku'];
	$licenseType = $row['license_type'];
	if (!isset($superProducts[$sku])) {
		$superProducts[$sku] = array();
	}
	if (isset($superProducts[$sku][$licenseType])) {
		echo 'Duplicate license type: '.$licenseTypeLabels[$licenseType], "\n";
	} else {
		$superProducts[$sku][$licenseType] = array(
																									// 'sku' => $row['sku'],
																									// 'license_type' => $row['license_type'],
																									'price' => $row['price'],
																									);
	}
}

// print_r($superProducts);

$priceAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'price');
// var_dump($priceAttribute);

// Get parent (super) and child products
$select = $db->select()
	->from(array('super_link' => $resource->getTableName('catalog/product_super_link')))
	->join(array('parent' => $resource->getTableName('catalog/product')),
				 'parent.entity_id = super_link.parent_id',
				 array('parent_sku' => 'parent.sku'))
	->join(array('child' => $resource->getTableName('catalog/product')),
				 'child.entity_id = super_link.product_id',
				 array('child_sku' => 'child.sku'))
	// join with child price
	->join(array('license_type' => $resource->getTableName('catalog_product_entity_'.$licenseTypeAttribute->getBackendType())),
				 'license_type.attribute_id = '.$licenseTypeAttribute->getAttributeId().' and license_type.entity_id = child.entity_id',
				 array('license_type' => 'license_type.value'))
	// join with child price
	->join(array('price' => $resource->getTableName('catalog_product_entity_'.$priceAttribute->getBackendType())),
				 'price.attribute_id = '.$priceAttribute->getAttributeId().' and price.entity_id = child.entity_id',
				 array('price' => 'price.value'))
	->order('price', 'license_type')
	;

// var_dump($select->assemble());

$rows = $db->fetchAll($select);

// var_dump($rows);
// exit;

$childProducts = array();
foreach ($rows as $row) {
	$sku = $row['parent_sku'];
	$licenseType = $row['license_type'];
	if (!isset($childProducts[$sku])) {
		$childProducts[$sku] = array();
	}
	if (isset($childProducts[$sku][$licenseType])) {
		echo 'Duplicate license type: '.$licenseTypeLabels[$licenseType], "\n";
	} else {
		$childProducts[$sku][$licenseType] = array(
																							'sku' => $row['child_sku'],
																							// 'license_type' => $licenseType,
																							'price' => $row['price'],
																							);
	}
}

// print_r($childProducts);

$errors = array();

foreach ($childProducts as $sku => $expectedPrices) {
	$actualPrices = isset($superProducts[$sku]) ? $superProducts[$sku] : null;
	$error = false;
	if (!$actualPrices) {
		$error = sprintf('Missing configured prices for sku: %s', $sku);
	} else if (count($actualPrices) < count($expectedPrices)) {
		$error = sprintf('Too few configured prices for sku: %s', $sku);
	} else if (count($actualPrices) > count($expectedPrices)) {
		$error = sprintf('Too many configured prices for sku: %s', $sku);
	}

	if (!$error) {
		// Compare configured prices to simple product prices
		foreach ($expectedPrices as $licenseType => $expectedPrice) {
			$actualPrice = isset($actualPrices[$licenseType]) ? $actualPrices[$licenseType] : null;
			if (!$actualPrice) {
				$error = sprintf('Missing configured price: %s (%s)', $sku, $licenseTypeLabels[$licenseType]);
			}
			foreach (array('price') as $field) {
				if ($actualPrice[$field] != $expectedPrice[$field]) {
					$error = sprintf('Incorrect configured price: %s (%s) %0.2f (actual price: %0.2f)', $sku, $licenseTypeLabels[$licenseType], $expectedPrice[$field], $actualPrice[$field]);
				}
			}
			// if (!isset
		}
	}

	if ($error) {
		$errors[$sku] = $error;

// 		echo 'Error: ', $error, "\n";
// 		print_r(array('sku' => $sku,
// 									'expected' => $expectedPrices,
// 									'actual' => $actualPrices,
// 									));
	}
}

var_dump($errors);


exit;

$parentIds = array();
$query = $db->query($select);
while ($row = $query->fetch()) {
	print_r($row);
	$parentIds[$row['parent_id']] = $row['parent_id'];
}

$parentIds = array_keys($parentIds);

var_dump($parentIds);
