-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eav_attribute_option_value`
--

DROP TABLE IF EXISTS `eav_attribute_option_value`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `eav_attribute_option_value` (
  `value_id` int(10) unsigned NOT NULL auto_increment,
  `option_id` int(10) unsigned NOT NULL default '0',
  `store_id` smallint(5) unsigned NOT NULL default '0',
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`value_id`),
  KEY `FK_ATTRIBUTE_OPTION_VALUE_OPTION` (`option_id`),
  KEY `FK_ATTRIBUTE_OPTION_VALUE_STORE` (`store_id`),
  CONSTRAINT `FK_ATTRIBUTE_OPTION_VALUE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ATTRIBUTE_OPTION_VALUE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=642 DEFAULT CHARSET=utf8 COMMENT='Attribute option values per store';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `eav_attribute_option_value`
--

LOCK TABLES `eav_attribute_option_value` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option_value` DISABLE KEYS */;
INSERT INTO `eav_attribute_option_value` VALUES (58,12,0,'Linux'),(59,13,0,'Windows 98'),(60,14,0,'Windows NT'),(61,15,0,'Mac'),(62,16,0,'Windows Vista'),(63,17,0,'Windows XP'),(477,25,0,'Systime'),(478,25,2,'Systime'),(479,25,3,'Systime'),(480,24,0,'ViaSystime'),(481,24,2,'ViaSystime'),(482,24,3,'ViaSystime'),(562,23,0,'Book'),(563,23,2,'Bog'),(564,23,3,'Bog'),(565,19,0,'CD-audio'),(566,19,2,'CD-audio'),(567,19,3,'CD-audio'),(568,20,0,'CD-rom'),(569,20,2,'CD-rom'),(570,20,3,'CD-rom'),(571,18,0,'DVD'),(572,18,2,'DVD (film)'),(573,18,3,'DVD (film)'),(574,21,0,'E-book'),(575,21,2,'E-bog'),(576,21,3,'E-bog'),(577,30,0,'Miscellaneous'),(578,30,2,'Diverse'),(579,30,3,'Diverse'),(580,22,0,'Website'),(581,22,2,'Website'),(582,22,3,'Website'),(618,1,0,'lærer/klasselicens'),(619,1,2,'Lærer/klasselicens pr. bruger'),(620,2,0,'skolelicens'),(621,2,2,'Skolelicens pr. bruger'),(622,26,0,'studie/privatlicens'),(623,26,2,'Studie/privatlicens'),(624,26,3,'Studie/privatlicens'),(625,35,0,'Private/studerende/virksomheder (indland/udland)'),(626,36,0,'Offentlige institutioner og andre uddannelsesinstitutioner (indland/udland)'),(632,10,0,'Grundskolen'),(633,11,0,'Ungdomsuddannelserne'),(634,9,0,'Videregående udd.'),(635,39,0,'book_backorder_2'),(636,38,0,'book_backorder'),(637,37,0,'book_general'),(638,34,0,'cdrom_general'),(639,32,0,'ebook_general'),(640,33,0,'website_general'),(641,40,0,'OK');
/*!40000 ALTER TABLE `eav_attribute_option_value` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:33
