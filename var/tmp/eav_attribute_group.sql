-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eav_attribute_group`
--

DROP TABLE IF EXISTS `eav_attribute_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `eav_attribute_group` (
  `attribute_group_id` smallint(5) unsigned NOT NULL auto_increment,
  `attribute_set_id` smallint(5) unsigned NOT NULL default '0',
  `attribute_group_name` varchar(255) NOT NULL default '',
  `sort_order` smallint(6) NOT NULL default '0',
  `default_id` smallint(5) unsigned default '0',
  PRIMARY KEY  (`attribute_group_id`),
  UNIQUE KEY `attribute_set_id` (`attribute_set_id`,`attribute_group_name`),
  KEY `attribute_set_id_2` (`attribute_set_id`,`sort_order`),
  CONSTRAINT `FK_eav_attribute_group` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `eav_attribute_group`
--

LOCK TABLES `eav_attribute_group` WRITE;
/*!40000 ALTER TABLE `eav_attribute_group` DISABLE KEYS */;
INSERT INTO `eav_attribute_group` VALUES (1,1,'General',1,1),(2,2,'General',1,1),(3,3,'General Information',10,1),(4,4,'General',2,1),(5,4,'Prices',3,0),(6,4,'Meta Information',4,0),(7,4,'Images',5,0),(8,4,'Design',6,0),(9,5,'General',1,1),(10,6,'General',1,1),(11,7,'General',1,1),(12,8,'General',1,1),(13,9,'General',1,1),(14,10,'General',1,1),(15,11,'General',1,1),(16,12,'General',1,1),(17,13,'General',1,1),(18,14,'General',1,1),(19,15,'General',1,1),(20,16,'General',1,1),(21,17,'General',1,1),(22,18,'General',1,1),(23,19,'General',1,1),(24,20,'General',1,1),(25,21,'General',1,1),(26,22,'General',1,1),(27,23,'General',1,1),(28,24,'General',1,1),(29,25,'General',1,1),(30,26,'General',2,1),(31,26,'Prices',3,0),(32,26,'Meta Information',4,0),(33,26,'Images',5,0),(34,26,'Design',6,0),(35,27,'General',2,1),(36,27,'Prices',3,0),(37,27,'Meta Information',4,0),(38,27,'Images',5,0),(39,27,'Design',6,0),(40,28,'General',2,1),(41,28,'Prices',3,0),(42,28,'Meta Information',4,0),(43,28,'Images',5,0),(44,28,'Design',6,0),(45,29,'General',2,1),(46,29,'Prices',3,0),(47,29,'Meta Information',4,0),(48,29,'Images',5,0),(49,29,'Design',6,0),(50,30,'General',2,1),(51,30,'Prices',3,0),(52,30,'Meta Information',4,0),(53,30,'Images',5,0),(54,30,'Design',6,0),(55,31,'General',2,1),(56,31,'Ajourtilbud',3,0),(57,31,'Meta Information',4,0),(58,31,'Images',5,0),(59,31,'Design',6,0),(65,3,'Display Settings',20,0),(66,3,'Custom Design',30,0),(67,27,'Stamdata (Marketing Manager)',1,0),(68,29,'Stamdata (Marketing Manager)',1,0),(69,4,'Stamdata (Marketing Manager)',1,0),(70,30,'Stamdata (Marketing Manager)',1,0),(71,31,'Stamdata',1,0),(72,28,'Stamdata',1,0),(73,26,'Stamdata',1,0);
/*!40000 ALTER TABLE `eav_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:33
