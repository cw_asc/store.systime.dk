-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cms_block`
--

DROP TABLE IF EXISTS `cms_block`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cms_block` (
  `block_id` smallint(6) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `identifier` varchar(255) NOT NULL default '',
  `content` text,
  `creation_time` datetime default NULL,
  `update_time` datetime default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='CMS Blocks';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cms_block`
--

LOCK TABLES `cms_block` WRITE;
/*!40000 ALTER TABLE `cms_block` DISABLE KEYS */;
INSERT INTO `cms_block` VALUES (5,'Footer Links','footer_links','<ul>\r\n<!--\r\n<li><a href=\"{{store url=\" mce_href=\"/index.php/boglokalet/cms_block/edit/block_id/5/key/764c22829aba8fbbd31c1b7213a921e3/{{store url=\"\"}}about-magento-demo-store\">About Us</a></li>\r\n-->\r\n<li><a href=\"http://info.systime.dk/\" target=\"_blank\">Information</a></li>\r\n<li class=\"last\"><a href=\"http://info.systime.dk/kontakt\" target=\"_blank\">Kontakt</a></li>\r\n</ul>','2008-08-25 09:44:34','2009-05-14 13:32:24',1),(6,'Gymnasiet - Dansk - landing page','dansk_landing_gymnasium','<form action=\"http://systime.createsend.com/t/1/s/pjihr/\" method=\"post\">\r\n<div>\r\n<label for=\"mb-name\">Name:</label><br /><input type=\"text\" name=\"mb-name\" id=\"mb-name\" /><br /><label for=\"mb-pjihr-pjihr\">Email:</label><br /><input type=\"text\" name=\"mb-pjihr-pjihr\" id=\"mb-pjihr-pjihr\" /><br />\r\n<br /><input type=\"submit\" value=\"Subscribe\" />\r\n</div>\r\n</form>','2008-11-05 14:08:13','2008-11-25 12:13:34',1),(7,'Frontpage Widget','widget','<div id=\"slider\">\r\n<ul>\r\n<li><a href=\"/\"><img src=\"{{skin url=slider/images/01.jpg}}\" alt=\"\" /></a></li>\r\n<li><a href=\"/\"><img src=\"{{skin url=slider/images/02.jpg}}\" alt=\"\" /></a></li>\r\n<li><a href=\"/\"><img src=\"{{skin url=slider/images/03.jpg}}\" alt=\"\" /></a></li>\r\n</ul>\r\n</div>\r\n','2009-02-18 13:52:25','2009-02-25 19:52:04',1),(8,'ebook_general','ebook_general','Du kan bruge e-bogen med det samme efter køb. \r\n<br>\r\nVær opmærksom på, at du ikke kan udskrive, kopiere eller copy/paste fra e-bogen. <a href=\"http://info.systime.dk/undervisning-online#TOC-Hvad-er-en-e-bog-\">Læs mere om e-bøger.</a>\r\n<br>','2009-03-10 09:49:32','2009-04-30 08:13:12',1),(9,'website_general','website_general','<p>Er du i tvivl om, hvad du skal k&oslash;be? <a href=\"http://info.systime.dk/licenstyper\">L&aelig;s om licenstyperne</a>.</p>','2009-03-11 08:38:59','2009-05-15 07:30:41',1),(10,'cdrom_general','cdrom_general','Vær opmærksom på, at vores cd-rommer ikke virker på Windows Vista.','2009-03-12 08:07:30','2009-04-30 08:13:59',1),(11,'create_systime_account','create_systime_account','<h4><strong>Opret en MinKonto for at gennemf&oslash;re k&oslash;bet</strong></h4>\r\n<ul>\r\n<li>Har du en Mit Systime konto skal du ikke oprette dig, men bare logge ind</li>\r\n<li>Med en konto har du altid overblik over dine bestillinger og undervisningsmaterialer</li>\r\n<li>Mit Systime login er det samme som MinKonto!</li>\r\n</ul>\r\n<ul>\r\n</ul>','2009-04-20 06:41:47','2009-05-22 09:20:59',1),(12,'frontpage_catalog','frontpage_catalog','<p><a href=\"http://info.systime.dk/katalog-2\" target=\"_self\">Hent kataloget</a> - helt eller delvist.</p>','2009-05-06 15:00:47','2009-05-20 12:36:14',1),(13,'frontpage_magasinet_systimes','frontpage_magasinet_systimes','<p><img style=\"float: right;\" src=\"/skin/frontend/default/systime_hellosleek_red/images/magasin_bill.png\" alt=\"\" width=\"87\" height=\"111\" />L&aelig;s om de nye mediers rolle og deltag i debatten.</p>\r\n<p>L&aelig;s <a href=\"http://magasin.systime.dk\">Magasinet Systimes</a></p>','2009-05-06 15:01:10','2009-05-07 07:05:52',1),(14,'frontpage_newsletter','frontpage_newsletter','<p>Tilmeld dig Nyhedsbrevet <a href=\"http://konto.systime.dk/?id=497\" target=\"_self\"><strong><br />Systime Ajour</strong></a>.</p>','2009-05-06 15:01:34','2009-05-20 13:18:35',1),(15,'Footer Links','footer_links','<ul>\r\n<li><a href=\"http://info.via.systime.dk/\" target=\"_blank\">Information</a></li>\r\n<li class=\"last\"><a href=\"http://info.systime.dk/kontakt\" target=\"_blank\">Kontakt</a></li>\r\n</ul>','2009-05-06 20:46:24','2009-06-12 11:50:39',1),(16,'Footer Contact Info','footer_contactinfo','<p><span class=\"phone\">70 12 11 00</span> &middot; <a class=\"mailto\" href=\"mailto:systime@systime.dk\">systime@systime.dk</a> &middot; <span class=\"address\">Skt. Pauls Gade 25, 8000 &Aring;rhus C</span></p>','2009-05-07 13:38:48','2009-05-07 13:45:54',1),(17,'frontpage_newsletter_via','frontpage_newsletter_via','<p>F&aring; gode tilbud og nyheder direkte i din mailbox.</p>\r\n<form action=\"http://systime.createsend.com/t/r/s/jyiduj/\" method=\"post\">\r\n<div><label for=\"name\">Navn:</label><br /><input id=\"name\" name=\"cm-name\" type=\"text\" /><label for=\"jyiduj-jyiduj\"><br />Email:</label><br /><input id=\"jyiduj-jyiduj\" name=\"cm-jyiduj-jyiduj\" type=\"text\" /> <input type=\"submit\" value=\"Tilmeld\" /></div>\r\n</form>&nbsp;','2009-05-11 09:15:16','2009-06-17 08:54:12',1),(18,'ajour_info','ajour_info','<p><strong>Dette er et Ajour-tilbud</strong></p>\r\n<ul>\r\n<li>Du skal v&aelig;re logget ind for at k&oslash;be bogen.</li>\r\n<li>Du kan k&oslash;be et eksemplar til dig selv.</li>\r\n<li>Hvis du er faggruppeleder, kan du k&oslash;be til hele faggruppen.</li>\r\n</ul>\r\n<p><br /><a title=\"L&aelig;s mere om Systime Ajour\" href=\"http://info.systime.dk/om-systime-ajour\" target=\"_blank\">L&aelig;s mere om Systime Ajour</a></p>\r\n<p><a title=\"Tilbage til oversigten over dine tilbud!\" href=\"http://konto.systime.dk/index.php?id=502\"><img src=\"/skin/frontend/default/systime_hellosleek_red/images/ajour_tilbud_tilbage.png\" alt=\"\" /></a></p>','2009-05-12 13:00:24','2009-08-18 09:25:09',1),(19,'frontpage_account','frontpage_account','<p><strong>Log ind her for at bruge din undervisningsmaterialer</strong></p>\r\n<p><strong><a href=\"http://konto.systime.dk/\"><img src=\"/skin/frontend/default/systime_hellosleek_red/images/minkonto_button.png\" alt=\"\" /></a><br /></strong></p>','2009-05-13 12:05:23','2009-08-25 10:14:07',1),(20,'frontpage_important','frontpage_important','<p><strong>VIGTIGT:</strong>&nbsp;DENNE BUTIK ER UNDER UDVIKLING. Hvis du vil k&oslash;be nogle af vores produkter, g&aring; til <a href=\"http://www.systime.dk/\">systime.dk</a></p>','2009-05-13 12:28:51','2009-05-21 19:14:44',0),(21,'book_general','book_general','<p>Levering: 2-3 dage</p>','2009-05-15 10:19:12','2009-06-11 12:59:39',1),(22,'frontpage_new_author_via','frontpage_new_author_via','<p><img style=\"float: right;\" src=\"/skin/frontend/default/systime_hellosleek_via/images/ill01.jpg\" alt=\"En forfatter i maven\" width=\"80\" height=\"113\" /><a href=\"http://info.via.systime.dk/bliv-forfatter\"><strong>ViaSystime s&oslash;ger forfattere</strong><br /></a>Har du formen til at blive forfatter med udgangspunkt i dit fag eller din profession?</p>','2009-06-11 07:35:58','2009-06-12 12:20:41',1),(23,'frontpage_account_via','frontpage_account_via','<p><a href=\"#\"><strong>MinKonto i ViaSystime<br /></strong></a>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>','2009-06-11 08:37:36','2009-06-11 08:40:53',1),(24,'frontpage_who-are-we','frontpage_who-are-we','<p>ViaSystime producerer fag- og&nbsp;undervisningsmateriale til professionsuddannelserne. Vi satser fokuseret p&aring; at ligge forrest i udviklingen... <a href=\"http://info.via.systime.dk/\" target=\"_self\"><strong>L&aelig;s mere</strong></a></p>','2009-06-12 07:52:27','2009-06-12 07:52:27',1),(25,'via_systime_ingen_produkter_i_kategori','via_systime_ingen_produkter_i_kategori','<p><strong>Vi har nye udgivelser p&aring; vej i denne kategori.</strong><br /><br /><strong>Tilmeld dig vores nyhedsbrev:</strong></p>\r\n<form action=\"http://systime.createsend.com/t/r/s/jyiduj/\" method=\"post\">\r\n<div><label for=\"name\">Navn:</label><br /><input id=\"name\" name=\"cm-name\" type=\"text\" /><label for=\"jyiduj-jyiduj\"><br />Email:</label><br /><input id=\"jyiduj-jyiduj\" name=\"cm-jyiduj-jyiduj\" type=\"text\" /> <input type=\"submit\" value=\"Tilmeld\" /></div>\r\n</form>&nbsp;','2009-06-19 11:09:17','2009-06-26 10:08:33',1),(26,'book_backorder','book_backorder','<p><strong>Under genoptryk. P&aring; lager igen primo september.<br /></strong></p>','2009-06-26 07:08:18','2009-08-19 08:45:43',1),(27,'ajour_tilbud_tilbage_info','ajour_tilbud_tilbage_info','<p><img src=\"/skin/frontend/default/systime_hellosleek_red/images/ajour_tilbud_tilbage.png\" alt=\"\" /></p>','2009-08-18 09:18:02','2009-08-18 09:20:31',1),(28,'book_backorder_2','book_backorder_2','<p><strong>Under genoptryk. P&aring; lager igen medio september.</strong></p>','2009-08-19 08:50:03','2009-08-19 08:50:03',1);
/*!40000 ALTER TABLE `cms_block` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:32
