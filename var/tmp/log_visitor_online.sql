-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `log_visitor_online`
--

DROP TABLE IF EXISTS `log_visitor_online`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `log_visitor_online` (
  `visitor_id` bigint(20) unsigned NOT NULL auto_increment,
  `visitor_type` char(1) NOT NULL,
  `remote_addr` bigint(20) NOT NULL,
  `first_visit_at` datetime default NULL,
  `last_visit_at` datetime default NULL,
  `customer_id` int(10) unsigned default NULL,
  `last_url` varchar(255) default NULL,
  PRIMARY KEY  (`visitor_id`),
  KEY `IDX_VISITOR_TYPE` (`visitor_type`),
  KEY `IDX_VISIT_TIME` (`first_visit_at`,`last_visit_at`),
  KEY `IDX_CUSTOMER` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=357303 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `log_visitor_online`
--

LOCK TABLES `log_visitor_online` WRITE;
/*!40000 ALTER TABLE `log_visitor_online` DISABLE KEYS */;
INSERT INTO `log_visitor_online` VALUES (356911,'v',1507698901,'2009-07-20 06:02:52','2009-07-20 07:30:24',NULL,'http://www.systime.dk/catalog/category/view/id/71?SID=ec4pujj5dhv3khn3vvs8ln8sm0&order=publication_year&dir=desc&mediatype=23'),(357202,'c',1507698901,'2009-07-20 07:11:00','2009-07-20 07:17:48',3619,'http://www.systime.dk/'),(357231,'v',1507698901,'2009-07-20 07:17:31','2009-07-20 07:17:31',NULL,'http://www.systime.dk/catalog/product/view/id/2030?gclid=CLKW_PLe45sCFRIM3godHTvB_w'),(357232,'c',1507698901,'2009-07-20 07:17:34','2009-07-20 07:18:47',71,'http://www.systime.dk/'),(357233,'v',1507698901,'2009-07-20 07:17:34','2009-07-20 07:17:35',NULL,'http://www.systime.dk/catalogsearch/advanced/result/?name=grammatik&description=&sku=&manufacturer=&isbn_13=&publication_year=&subject=&mediatype%5B%5D=23&authors=&x=39&y=18&gclid=CKPey_Te45sCFU0A4wod4SDztg'),(357234,'v',1507698901,'2009-07-20 07:17:50','2009-07-20 07:17:50',NULL,'http://www.systime.dk/catalog/category/view/id/193?mediatype=19'),(357235,'v',1507698901,'2009-07-20 07:18:03','2009-07-20 07:18:04',NULL,'http://www.systime.dk/'),(357236,'v',1507698901,'2009-07-20 07:18:03','2009-07-20 07:18:04',NULL,'http://www.systime.dk/'),(357237,'v',1507698901,'2009-07-20 07:18:33','2009-07-20 07:18:33',NULL,'http://www.systime.dk/catalog/product/view/id/1809'),(357238,'v',1507698901,'2009-07-20 07:18:37','2009-07-20 07:18:38',NULL,'http://www.systime.dk/catalog/category/view/id/51?p=2'),(357239,'v',1507698901,'2009-07-20 07:19:03','2009-07-20 07:19:03',NULL,'http://www.systime.dk/'),(357240,'v',1507698901,'2009-07-20 07:19:04','2009-07-20 07:19:04',NULL,'http://www.systime.dk/'),(357241,'v',1507698901,'2009-07-20 07:20:04','2009-07-20 07:20:04',NULL,'http://www.systime.dk/'),(357242,'v',1507698901,'2009-07-20 07:20:04','2009-07-20 07:20:04',NULL,'http://www.systime.dk/'),(357243,'v',1507698901,'2009-07-20 07:20:22','2009-07-20 07:20:23',NULL,'http://www.systime.dk/catalog/product/view/id/2255?gclid=CPr-58Tf45sCFVUA4wodxkDl_w'),(357244,'v',1507698901,'2009-07-20 07:20:58','2009-07-20 07:20:59',NULL,'http://www.systime.dk/catalog/product/view/id/1961/category/11'),(357245,'v',1507698901,'2009-07-20 07:21:04','2009-07-20 07:21:04',NULL,'http://www.systime.dk/'),(357246,'v',1507698901,'2009-07-20 07:21:04','2009-07-20 07:21:05',NULL,'http://www.systime.dk/catalog/product/view/id/1786?gclid=CIfv1Njf45sCFZeD3god23j2-g'),(357247,'v',1507698901,'2009-07-20 07:21:04','2009-07-20 07:21:05',NULL,'http://www.systime.dk/'),(357248,'v',1507698901,'2009-07-20 07:21:16','2009-07-20 07:21:16',NULL,'http://www.systime.dk/'),(357249,'v',1507698901,'2009-07-20 07:21:44','2009-07-20 07:21:45',NULL,'http://www.systime.dk/catalog/category/view/id/83?mediatype=21'),(357250,'v',1507698901,'2009-07-20 07:21:51','2009-07-20 07:21:52',NULL,'http://www.systime.dk/catalog/category/view/id/46?order=position&dir=asc&p=1'),(357251,'v',1507698901,'2009-07-20 07:22:03','2009-07-20 07:22:03',NULL,'http://www.systime.dk/'),(357252,'v',1507698901,'2009-07-20 07:22:03','2009-07-20 07:22:04',NULL,'http://www.systime.dk/'),(357253,'v',1507698901,'2009-07-20 07:22:11','2009-07-20 07:22:11',NULL,'http://www.systime.dk/uploads/pics/ulla.jpg'),(357254,'v',1507698901,'2009-07-20 07:22:31','2009-07-20 07:22:32',NULL,'http://www.systime.dk/catalog/category/view/id/49?mediatype=22'),(357255,'v',1507698901,'2009-07-20 07:22:31','2009-07-20 07:22:32',NULL,'http://www.systime.dk/catalog/category/view/id/8?cat=11'),(357256,'v',1507698901,'2009-07-20 07:22:51','2009-07-20 07:22:52',NULL,'http://www.systime.dk/catalog/category/view/id/66?p=3'),(357257,'v',1507698901,'2009-07-20 07:23:03','2009-07-20 07:23:03',NULL,'http://www.systime.dk/'),(357258,'v',1507698901,'2009-07-20 07:23:03','2009-07-20 07:23:03',NULL,'http://www.systime.dk/'),(357259,'v',1507698901,'2009-07-20 07:23:05','2009-07-20 07:23:06',NULL,'http://www.systime.dk/catalog/category/view/id/178?uddannelsesniveau=10&order=release_date_estimated&dir=asc&p=1'),(357260,'v',1507698901,'2009-07-20 07:23:14','2009-07-20 07:23:24',NULL,'http://www.systime.dk/bogwebs/fraordtilsaetning/index.htm?tid=16%3A14%3A52&x=2'),(357261,'v',1507698901,'2009-07-20 07:23:18','2009-07-20 07:23:19',NULL,'http://www.systime.dk/catalog/category/view/id/41?mediatype=20'),(357262,'v',1507698901,'2009-07-20 07:23:39','2009-07-20 07:23:39',NULL,'http://www.systime.dk/'),(357263,'v',1507698901,'2009-07-20 07:24:03','2009-07-20 07:24:03',NULL,'http://www.systime.dk/'),(357264,'v',1507698901,'2009-07-20 07:24:03','2009-07-20 07:24:03',NULL,'http://www.systime.dk/'),(357265,'v',1507698901,'2009-07-20 07:24:05','2009-07-20 07:24:05',NULL,'http://www.systime.dk/fagbank/engelsk/Beatgen/milstein.htm'),(357266,'v',1507698901,'2009-07-20 07:24:16','2009-07-20 07:24:17',NULL,'http://www.systime.dk/catalog/category/view/id/115?uddannelsesniveau=9&order=release_date_estimated&dir=asc&p=1'),(357267,'v',1507698901,'2009-07-20 07:24:42','2009-07-20 07:24:42',NULL,'http://www.systime.dk/'),(357268,'v',1507698901,'2009-07-20 07:24:52','2009-07-20 07:24:53',NULL,'http://www.systime.dk/catalog/product/view/id/2641/category/173'),(357269,'v',1507698901,'2009-07-20 07:24:56','2009-07-20 07:24:57',NULL,'http://www.systime.dk/catalog/product/view/id/2019?gclid=CLaNjMfg45sCFUR_3god4CKL_w'),(357270,'v',1507698901,'2009-07-20 07:25:04','2009-07-20 07:25:04',NULL,'http://www.systime.dk/'),(357271,'v',1507698901,'2009-07-20 07:25:04','2009-07-20 07:25:04',NULL,'http://www.systime.dk/'),(357272,'v',1507698901,'2009-07-20 07:25:31','2009-07-20 07:25:32',NULL,'http://www.systime.dk/catalog/category/view/id/73?uddannelsesniveau=9&order=price&dir=asc&p=2'),(357273,'v',1507698901,'2009-07-20 07:25:39','2009-07-20 07:25:40',NULL,'http://www.systime.dk/catalogsearch/advanced/result/?authors=Ole+Thorup'),(357274,'v',1507698901,'2009-07-20 07:26:02','2009-07-20 07:26:03',NULL,'http://www.systime.dk/'),(357275,'v',1507698901,'2009-07-20 07:26:02','2009-07-20 07:26:03',NULL,'http://www.systime.dk/'),(357276,'v',1507698901,'2009-07-20 07:26:26','2009-07-20 07:26:27',NULL,'http://www.systime.dk/catalog/product/view/id/2265'),(357277,'v',1507698901,'2009-07-20 07:26:41','2009-07-20 07:26:42',NULL,'http://www.systime.dk/catalog/category/view/id/73?uddannelsesniveau=9&mediatype=23&order=price&dir=asc&p=1'),(357278,'v',1507698901,'2009-07-20 07:26:52','2009-07-20 07:26:52',NULL,'http://www.systime.dk/catalog/product/view/id/1745?gclid=CJbpwP7g45sCFcE63godE2YS_A'),(357279,'v',1507698901,'2009-07-20 07:27:06','2009-07-20 07:27:07',NULL,'http://www.systime.dk/'),(357280,'v',1507698901,'2009-07-20 07:27:07','2009-07-20 07:27:07',NULL,'http://www.systime.dk/'),(357281,'v',1507698901,'2009-07-20 07:27:13','2009-07-20 07:27:14',NULL,'http://www.systime.dk/catalog/product/view/id/2433'),(357282,'c',1507698901,'2009-07-20 07:27:37','2009-07-20 07:29:41',17,'http://www.systime.dk/catalog/category/view/id/49'),(357283,'v',1507698901,'2009-07-20 07:27:47','2009-07-20 07:27:48',NULL,'http://www.systime.dk/catalog/product/view/id/1912?gclid=CPfpg5nh45sCFZgU4wodvl7s-A'),(357284,'v',1507698901,'2009-07-20 07:28:03','2009-07-20 07:28:03',NULL,'http://www.systime.dk/'),(357285,'v',1507698901,'2009-07-20 07:28:03','2009-07-20 07:28:03',NULL,'http://www.systime.dk/'),(357286,'v',1507698901,'2009-07-20 07:28:47','2009-07-20 07:28:47',NULL,'http://www.systime.dk/catalogsearch/advanced/result/?authors=Arne+J%C3%B8rgensen'),(357287,'v',1507698901,'2009-07-20 07:28:58','2009-07-20 07:28:58',NULL,'http://www.systime.dk/'),(357288,'v',1507698901,'2009-07-20 07:29:02','2009-07-20 07:29:03',NULL,'http://www.systime.dk/'),(357289,'v',1507698901,'2009-07-20 07:29:03','2009-07-20 07:29:03',NULL,'http://www.systime.dk/'),(357290,'v',1507698901,'2009-07-20 07:29:30','2009-07-20 07:29:30',NULL,'http://www.systime.dk/robots.txt'),(357291,'v',1507698901,'2009-07-20 07:29:34','2009-07-20 07:29:35',NULL,'http://www.systime.dk/catalogsearch/advanced/result/?authors=Henrik+Juhl+Andersen'),(357292,'v',1507698901,'2009-07-20 07:29:50','2009-07-20 07:29:51',NULL,'http://www.systime.dk/catalog/category/view/id/78'),(357293,'v',1507698901,'2009-07-20 07:30:05','2009-07-20 07:30:06',NULL,'http://www.systime.dk/'),(357294,'v',1507698901,'2009-07-20 07:30:06','2009-07-20 07:30:06',NULL,'http://www.systime.dk/'),(357295,'v',1507698901,'2009-07-20 07:30:14','2009-07-20 07:30:14',NULL,'http://www.systime.dk/robots.txt'),(357296,'v',1507698901,'2009-07-20 07:30:21','2009-07-20 07:30:21',NULL,'http://www.systime.dk/catalogsearch/advanced/result/?authors=S%C3%B8ren+Munthe'),(357297,'v',1507698901,'2009-07-20 07:31:03','2009-07-20 07:31:03',NULL,'http://www.systime.dk/'),(357298,'v',1507698901,'2009-07-20 07:31:03','2009-07-20 07:31:04',NULL,'http://www.systime.dk/'),(357299,'v',1507698901,'2009-07-20 07:31:08','2009-07-20 07:31:09',NULL,'http://www.systime.dk/catalog/product/view/id/1946/category/51'),(357300,'v',1507698901,'2009-07-20 07:31:33','2009-07-20 07:31:34',NULL,'http://www.systime.dk/catalog/category/view/id/45?mediatype=21&order=release_date_confirmed&dir=asc&p=1'),(357301,'v',1507698901,'2009-07-20 07:32:03','2009-07-20 07:32:03',NULL,'http://www.systime.dk/'),(357302,'v',1507698901,'2009-07-20 07:32:03','2009-07-20 07:32:04',NULL,'http://www.systime.dk/');
/*!40000 ALTER TABLE `log_visitor_online` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:42
