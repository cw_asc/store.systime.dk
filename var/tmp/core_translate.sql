-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `core_translate`
--

DROP TABLE IF EXISTS `core_translate`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `core_translate` (
  `key_id` int(10) unsigned NOT NULL auto_increment,
  `string` varchar(255) NOT NULL default '',
  `store_id` smallint(5) unsigned NOT NULL default '0',
  `translate` varchar(255) NOT NULL default '',
  `locale` varchar(20) NOT NULL default 'en_US',
  PRIMARY KEY  (`key_id`),
  UNIQUE KEY `IDX_CODE` (`store_id`,`locale`,`string`),
  KEY `FK_CORE_TRANSLATE_STORE` (`store_id`),
  CONSTRAINT `FK_CORE_TRANSLATE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COMMENT='Translation data';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `core_translate`
--

LOCK TABLES `core_translate` WRITE;
/*!40000 ALTER TABLE `core_translate` DISABLE KEYS */;
INSERT INTO `core_translate` VALUES (1,'isbn',0,'isbn','en_US'),(2,'licens type',0,'licens type','en_US'),(4,'Mage_Catalog::Author',0,'Author','en_US'),(5,'Mage_Catalog::Number of pages',0,'Number of pages','en_US'),(7,'Mage_Catalog::Isbn 10',0,'Isbn 10','en_US'),(8,'Mage_Catalog::Isbn 13',0,'Isbn 13','en_US'),(9,'Mage_Catalog::Projectmanager',0,'Projectmanager','en_US'),(10,'Mage_Catalog::Publication year',0,'Publication year','en_US'),(11,'Mage_Catalog::website_adress',0,'website_adress','en_US'),(12,'Mage_Catalog::Edition',0,'Edition','en_US'),(13,'Mage_Catalog::Support URL',0,'Support URL','en_US'),(14,'Mage_Catalog::Google Books URL',0,'Google Books URL','en_US'),(15,'Mage_Catalog::website url',0,'website url','en_US'),(16,'Mage_Catalog::E-book format',0,'E-book format','en_US'),(17,'Mage_Catalog::Book format',0,'Book format','en_US'),(18,'Mage_Catalog::Google Books embed',0,'Google Books embed','en_US'),(19,'Mage_Catalog::Titel',0,'Titel','en_US'),(20,'Mage_Catalog::Medietype',0,'Medietype','en_US'),(21,'Mage_Catalog::Tax Class',0,'Tax Class','en_US'),(22,'Mage_Catalog::Price',0,'Price','en_US'),(23,'Mage_Catalog::Description',0,'Description','en_US'),(24,'Mage_Catalog::Short Description',0,'Short Description','en_US'),(25,'Mage_Catalog::Ajourtilbud',0,'Ajourtilbud','en_US'),(26,'Mage_Catalog::Lærereksemplar',0,'Lærereksemplar','en_US'),(27,'Mage_Catalog::Tilbud til lærere',0,'Tilbud til lærere','en_US'),(29,'Mage_Catalog::Lab url',0,'Lab url','en_US'),(31,'Mage_Catalog::Porjectnumber',0,'Porjectnumber','en_US'),(32,'Mage_Catalog::Projectnumber',0,'Projectnumber','en_US'),(33,'Mage_Catalog::Projectnumber',2,'Projeknummer','en_US'),(34,'Mage_Catalog::Licens Type',0,'Licens Type','en_US'),(35,'Mage_Catalog::Licens Type',2,'Vælg licensform','en_US'),(37,'Mage_Catalog::Project Manager E-mail',0,'Project Manager E-mail','en_US'),(38,'Mage_Catalog::Project Manager E-mail',2,'Projektlederens e-mail adresse:','en_US'),(39,'Mage_Catalog::Subject',0,'Subject','en_US'),(40,'Mage_Catalog::Subject',2,'Fag','en_US'),(42,'Mage_Catalog::Publisher',0,'Publisher','en_US'),(43,'Mage_Catalog::Publisher',2,'Udgiver','en_US'),(44,'Mage_Catalog::Publisher',3,'Udgiver','en_US'),(45,'Mage_Catalog::Varedeklaration',0,'Varedeklaration','en_US'),(46,'Mage_Catalog::Varedeklaration',2,'Sitet indeholder','en_US'),(47,'Mage_Catalog::Varedeklaration',3,'Sitet indeholder','en_US'),(48,'Mage_Catalog::Uddannelsesniveau',0,'Uddannelsesniveau','en_US'),(49,'Mage_Catalog::Uddannelsesniveau',2,'Niveau','en_US'),(50,'Mage_Catalog::Printable',0,'Printable','en_US'),(51,'Mage_Catalog::Printable',2,'Udskrivning','en_US'),(52,'Mage_Catalog::Printable',3,'Udskrivning','en_US'),(53,'Mage_Catalog::Copy/paste',0,'Copy/paste','en_US'),(54,'Mage_Catalog::Copy/paste',2,'Kopier / indsæt','en_US'),(55,'Mage_Catalog::Copy/paste',3,'Kopier / indsæt','en_US'),(56,'Mage_Catalog::Platform',0,'Platform','en_US'),(57,'Mage_Catalog::Platform',2,'Styresystem','en_US'),(58,'Mage_Catalog::Platform',3,'Styresystem','en_US'),(59,'Mage_Catalog::Mediatype',0,'Mediatype','en_US'),(60,'Mage_Catalog::Mediatype',2,'Medietype','en_US'),(61,'Mage_Catalog::Mediatype',3,'Medietype','en_US'),(62,'Mage_Catalog::mm_mediatype',0,'mm_mediatype','en_US'),(63,'Mage_Catalog::Number of pages',2,'Antal sider','en_US'),(64,'Mage_Catalog::Number of pages',3,'Antal sider','en_US'),(65,'Mage_Catalog::Author',2,'Forfatter(e)','en_US'),(66,'Mage_Catalog::Author',3,'Forfatter(e)','en_US'),(67,'Mage_Catalog::Edition',2,'Udgave','en_US'),(68,'Mage_Catalog::Edition',3,'Udgave','en_US'),(69,'Mage_Catalog::E-bøger leveres med det samme!',0,'E-bøger leveres med det samme!','en_US'),(70,'Mage_Catalog::E-bøger leveres med det samme!',2,'E-bøger leveres med det samme!','en_US'),(71,'Mage_Catalog::E-bøger leveres med det samme!',3,'E-bøger leveres med det samme!','en_US'),(72,'Mage_Catalog::Manufacturer',0,'Manufacturer','en_US'),(73,'Mage_Catalog::Array',0,'Array','en_US'),(74,'Mage_Catalog::Array',2,'Vælg licensform','en_US'),(75,'Mage_Catalog::License type',0,'License type','en_US'),(76,'Mage_Catalog::License type',2,'Vælg licensform','en_US'),(77,'Mage_Catalog::License type',3,'Vælg licensform','en_US'),(78,'Mage_Catalog::Additional Url',0,'Additional Url','en_US'),(79,'Mage_Catalog::Additional Url',2,'Supplerende Url','en_US'),(80,'Mage_Catalog::Additional Url',3,'Supplerende Url','en_US'),(81,'Mage_Catalog::Manufacturer',2,'Forlag','en_US'),(82,'Mage_Catalog::License Type (*)',0,'License Type (*)','en_US'),(83,'Mage_Catalog::License Type (*)',2,'Vælg licensform','en_US'),(84,'Mage_Catalog::License Type (*)',3,'Vælg licensform','en_US'),(85,'Mage_Catalog::Vælg licensform',0,'Vælg licensform','en_US'),(86,'Mage_Catalog::Vælg licensform',2,'Vælg licensform','en_US'),(87,'Mage_Catalog::Vælg licensform',3,'Vælg licensform','en_US'),(88,'Mage_Catalog::Media Type Info',0,'Media Type Info','en_US'),(89,'Mage_Catalog::Media Type Info',2,'Medietype-info','en_US'),(90,'Mage_Catalog::Media Type Info',3,'Medietype-info','en_US'),(91,'Mage_Catalog::Google Books ISBN/URL',0,'Google Books ISBN/URL','en_US'),(92,'Mage_Catalog::Google Books ISBN',0,'Google Books ISBN','en_US'),(93,'Mage_Catalog::Forlag',0,'Forlag','en_US'),(94,'Mage_Catalog::Forlag',2,'Forlag','en_US'),(95,'Mage_Catalog::Forlag',3,'Forlag','en_US'),(96,'Mage_Catalog::Bestillingstype',0,'Bestillingstype','en_US'),(97,'Mage_Catalog::Is product available for purchase with Google Checkout',0,'Is product available for purchase with Google Checkout','en_US'),(109,'Mage_Catalog::bla',0,'bla','en_US'),(110,'Mage_Catalog::blaw',0,'blaw','en_US'),(112,'Mage_Catalog::EAN-nummer',0,'EAN-nummer','en_US'),(113,'Mage_Catalog::bla2',0,'bla2','en_US'),(114,'Mage_Catalog::Ajour Activity No.',0,'Ajour Activity No.','en_US'),(115,'Mage_Catalog::Ajour Activity No.',2,'Ajour-aktivitetsnr.','en_US'),(116,'Mage_Catalog::Ajour Activity No.',3,'Ajour-aktivitetsnr.','en_US'),(117,'Mage_Catalog::Version (printing)',0,'Version (printing)','en_US'),(118,'Mage_Catalog::Version (printing)',2,'Version (oplag)','en_US'),(119,'Mage_Catalog::Version (printing)',3,'Version (oplag)','en_US'),(120,'Mage_Catalog::Version (oplag)',0,'Version (oplag)','en_US'),(121,'Mage_Catalog::Ajour-kampagnenr.',0,'Ajour-kampagnenr.','en_US'),(122,'Mage_Catalog::Version (aktivt oplag)',0,'Version (aktivt oplag)','en_US'),(123,'Mage_Catalog::Estimated publication date',0,'Estimated publication date','en_US'),(124,'Mage_Catalog::Estimated publication date',2,'Forventet udgivelse','en_US'),(125,'Mage_Catalog::Release date confirmed',0,'Release date confirmed','en_US'),(126,'Mage_Catalog::Release date confirmed',2,'Udkommer','en_US'),(127,'Mage_Catalog::Licenstype',0,'Licenstype','en_US'),(128,'Mage_Catalog::Licenstype',2,'Vælg licensform','en_US'),(129,'Mage_Catalog::Licenstype',3,'Vælg licensform','en_US'),(130,'Mage_Catalog::Licens Type',3,'Vælg licensform','en_US'),(131,'Mage_Catalog::Publication year',2,'Udgivelsesår','en_US'),(132,'Mage_Catalog::Forfattere',0,'Forfattere','en_US'),(133,'Mage_Catalog::Exclude product from Free Shipping',0,'Exclude product from Free Shipping','en_US'),(134,'Mage_Catalog::Description',2,'Beskrivelse','en_US'),(135,'Mage_Catalog::Description',3,'Beskrivelse','en_US'),(136,'Mage_Catalog::Price',2,'Pris','en_US'),(137,'Mage_Catalog::Price',3,'Pris','en_US'),(138,'Mage_Catalog::Medietype',2,'Medietype','en_US'),(139,'Mage_Catalog::Medietype',3,'Medietype','en_US'),(140,'Mage_Catalog::Forfatter(e)',0,'Forfatter(e)','en_US'),(141,'Mage_Catalog::Kort beskrivelse',0,'Kort beskrivelse','en_US'),(142,'Mage_Catalog::Udgivelsesår',0,'Udgivelsesår','en_US'),(143,'Mage_Catalog::Supplerende URL',0,'Supplerende URL','en_US'),(144,'Mage_Catalog::Uddannelsesniveau',3,'Niveau','en_US'),(145,'Mage_Catalog::customer_type',0,'customer_type','en_US'),(146,'Mage_Catalog::Lab-url',0,'Lab-url','en_US'),(147,'Mage_Catalog::Ajour-aktivitetsnr.',0,'Ajour-aktivitetsnr.','en_US'),(148,'Mage_Catalog::Release date confirmed',3,'Udkommer','en_US'),(149,'Mage_Catalog::Estimated publication date',3,'Forventet udgivelse','en_US'),(150,'Mage_Catalog::Publication year',3,'Udgivelsesår','en_US'),(151,'Mage_Catalog::Udgivelsesstatus',0,'Udgivelsesstatus','en_US');
/*!40000 ALTER TABLE `core_translate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:32
