-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalogindex_aggregation_tag`
--

DROP TABLE IF EXISTS `catalogindex_aggregation_tag`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `catalogindex_aggregation_tag` (
  `tag_id` int(10) unsigned NOT NULL auto_increment,
  `tag_code` varchar(255) NOT NULL,
  PRIMARY KEY  (`tag_id`),
  UNIQUE KEY `IDX_CODE` (`tag_code`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `catalogindex_aggregation_tag`
--

LOCK TABLES `catalogindex_aggregation_tag` WRITE;
/*!40000 ALTER TABLE `catalogindex_aggregation_tag` DISABLE KEYS */;
INSERT INTO `catalogindex_aggregation_tag` VALUES (58,'catalog_category'),(2,'catalog_category1'),(14,'catalog_category10'),(90,'catalog_category100'),(91,'catalog_category101'),(92,'catalog_category102'),(93,'catalog_category103'),(94,'catalog_category104'),(95,'catalog_category105'),(96,'catalog_category106'),(97,'catalog_category107'),(98,'catalog_category108'),(99,'catalog_category109'),(168,'catalog_category11'),(100,'catalog_category110'),(101,'catalog_category111'),(102,'catalog_category112'),(103,'catalog_category113'),(104,'catalog_category114'),(105,'catalog_category115'),(106,'catalog_category116'),(107,'catalog_category117'),(108,'catalog_category118'),(109,'catalog_category119'),(79,'catalog_category12'),(110,'catalog_category120'),(111,'catalog_category121'),(112,'catalog_category122'),(113,'catalog_category123'),(114,'catalog_category124'),(115,'catalog_category125'),(116,'catalog_category126'),(117,'catalog_category127'),(118,'catalog_category128'),(119,'catalog_category129'),(121,'catalog_category130'),(123,'catalog_category131'),(124,'catalog_category132'),(125,'catalog_category133'),(126,'catalog_category134'),(129,'catalog_category135'),(130,'catalog_category136'),(131,'catalog_category137'),(132,'catalog_category138'),(133,'catalog_category139'),(77,'catalog_category14'),(134,'catalog_category140'),(135,'catalog_category141'),(136,'catalog_category142'),(137,'catalog_category143'),(138,'catalog_category144'),(139,'catalog_category145'),(140,'catalog_category146'),(141,'catalog_category147'),(142,'catalog_category148'),(143,'catalog_category149'),(127,'catalog_category15'),(144,'catalog_category150'),(145,'catalog_category151'),(146,'catalog_category152'),(147,'catalog_category153'),(148,'catalog_category154'),(149,'catalog_category155'),(150,'catalog_category156'),(151,'catalog_category157'),(152,'catalog_category158'),(153,'catalog_category159'),(154,'catalog_category160'),(155,'catalog_category161'),(156,'catalog_category162'),(157,'catalog_category163'),(158,'catalog_category164'),(159,'catalog_category165'),(160,'catalog_category166'),(161,'catalog_category167'),(162,'catalog_category168'),(163,'catalog_category169'),(165,'catalog_category170'),(166,'catalog_category171'),(173,'catalog_category172'),(174,'catalog_category173'),(175,'catalog_category174'),(176,'catalog_category175'),(177,'catalog_category176'),(178,'catalog_category177'),(179,'catalog_category178'),(180,'catalog_category179'),(181,'catalog_category180'),(182,'catalog_category181'),(183,'catalog_category182'),(184,'catalog_category183'),(185,'catalog_category184'),(186,'catalog_category185'),(187,'catalog_category186'),(188,'catalog_category187'),(189,'catalog_category188'),(190,'catalog_category189'),(191,'catalog_category190'),(192,'catalog_category191'),(193,'catalog_category192'),(194,'catalog_category193'),(195,'catalog_category194'),(196,'catalog_category195'),(197,'catalog_category196'),(198,'catalog_category197'),(199,'catalog_category198'),(200,'catalog_category199'),(3,'catalog_category2'),(201,'catalog_category200'),(202,'catalog_category201'),(203,'catalog_category202'),(204,'catalog_category203'),(205,'catalog_category204'),(206,'catalog_category205'),(207,'catalog_category206'),(208,'catalog_category207'),(209,'catalog_category208'),(210,'catalog_category209'),(212,'catalog_category210'),(213,'catalog_category211'),(214,'catalog_category212'),(215,'catalog_category213'),(216,'catalog_category214'),(217,'catalog_category215'),(218,'catalog_category216'),(219,'catalog_category217'),(220,'catalog_category218'),(221,'catalog_category219'),(4,'catalog_category41'),(17,'catalog_category42'),(16,'catalog_category43'),(18,'catalog_category44'),(19,'catalog_category45'),(20,'catalog_category46'),(21,'catalog_category47'),(6,'catalog_category48'),(22,'catalog_category49'),(23,'catalog_category50'),(24,'catalog_category51'),(25,'catalog_category52'),(27,'catalog_category53'),(28,'catalog_category54'),(29,'catalog_category55'),(30,'catalog_category56'),(31,'catalog_category57'),(32,'catalog_category58'),(33,'catalog_category59'),(34,'catalog_category60'),(35,'catalog_category61'),(36,'catalog_category62'),(37,'catalog_category63'),(52,'catalog_category64'),(38,'catalog_category65'),(15,'catalog_category66'),(39,'catalog_category67'),(40,'catalog_category68'),(41,'catalog_category69'),(42,'catalog_category70'),(43,'catalog_category71'),(44,'catalog_category72'),(5,'catalog_category73'),(45,'catalog_category74'),(46,'catalog_category75'),(47,'catalog_category76'),(48,'catalog_category77'),(49,'catalog_category78'),(50,'catalog_category79'),(11,'catalog_category8'),(51,'catalog_category80'),(128,'catalog_category81'),(26,'catalog_category82'),(55,'catalog_category83'),(56,'catalog_category84'),(57,'catalog_category85'),(59,'catalog_category86'),(60,'catalog_category87'),(76,'catalog_category88'),(78,'catalog_category89'),(12,'catalog_category9'),(80,'catalog_category90'),(81,'catalog_category91'),(82,'catalog_category92'),(83,'catalog_category93'),(84,'catalog_category94'),(85,'catalog_category95'),(86,'catalog_category96'),(87,'catalog_category97'),(88,'catalog_category98'),(89,'catalog_category99'),(61,'EAV_ATTRIBUTE461'),(67,'EAV_ATTRIBUTE474'),(73,'EAV_ATTRIBUTE475'),(74,'EAV_ATTRIBUTE476'),(64,'EAV_ATTRIBUTE492'),(68,'EAV_ATTRIBUTE493'),(71,'EAV_ATTRIBUTE494'),(69,'EAV_ATTRIBUTE495'),(75,'EAV_ATTRIBUTE514'),(7,'EAV_ATTRIBUTE515'),(66,'EAV_ATTRIBUTE516'),(65,'EAV_ATTRIBUTE517'),(8,'EAV_ATTRIBUTE518'),(9,'EAV_ATTRIBUTE519'),(70,'EAV_ATTRIBUTE520'),(72,'EAV_ATTRIBUTE521'),(54,'EAV_ATTRIBUTE522'),(63,'EAV_ATTRIBUTE523'),(120,'EAV_ATTRIBUTE524'),(122,'EAV_ATTRIBUTE525'),(164,'EAV_ATTRIBUTE534'),(167,'EAV_ATTRIBUTE545'),(169,'EAV_ATTRIBUTE548'),(170,'EAV_ATTRIBUTE549'),(172,'EAV_ATTRIBUTE559'),(171,'EAV_ATTRIBUTE560'),(211,'EAV_ATTRIBUTE561'),(222,'EAV_ATTRIBUTE574'),(13,'EAV_ATTRIBUTE59'),(53,'EAV_ATTRIBUTE65'),(62,'EAV_ATTRIBUTE75'),(10,'PRODUCT_PRICE'),(1,'SEARCH_QUERY');
/*!40000 ALTER TABLE `catalogindex_aggregation_tag` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:31
