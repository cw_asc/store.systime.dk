-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eav_entity_type`
--

DROP TABLE IF EXISTS `eav_entity_type`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `eav_entity_type` (
  `entity_type_id` smallint(5) unsigned NOT NULL auto_increment,
  `entity_type_code` varchar(50) NOT NULL default '',
  `entity_model` varchar(255) NOT NULL,
  `attribute_model` varchar(255) NOT NULL,
  `entity_table` varchar(255) NOT NULL default '',
  `value_table_prefix` varchar(255) NOT NULL default '',
  `entity_id_field` varchar(255) NOT NULL default '',
  `is_data_sharing` tinyint(4) unsigned NOT NULL default '1',
  `data_sharing_key` varchar(100) default 'default',
  `default_attribute_set_id` smallint(5) unsigned NOT NULL default '0',
  `increment_model` varchar(255) NOT NULL default '',
  `increment_per_store` tinyint(1) unsigned NOT NULL default '0',
  `increment_pad_length` tinyint(8) unsigned NOT NULL default '8',
  `increment_pad_char` char(1) NOT NULL default '0',
  PRIMARY KEY  (`entity_type_id`),
  KEY `entity_name` (`entity_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `eav_entity_type`
--

LOCK TABLES `eav_entity_type` WRITE;
/*!40000 ALTER TABLE `eav_entity_type` DISABLE KEYS */;
INSERT INTO `eav_entity_type` VALUES (1,'customer','customer/customer','','customer/entity','','',1,'default',1,'eav/entity_increment_numeric',0,8,'0'),(2,'customer_address','customer/customer_address','','customer/address_entity','','',1,'default',2,'',0,8,'0'),(3,'catalog_category','catalog/category','catalog/resource_eav_attribute','catalog/category','','',1,'default',3,'',0,8,'0'),(4,'catalog_product','catalog/product','catalog/resource_eav_attribute','catalog/product','','',1,'default',4,'',0,8,'0'),(5,'quote','sales/quote','','sales/quote','','',1,'default',5,'',0,8,'0'),(6,'quote_item','sales/quote_item','','sales/quote_item','','',1,'default',6,'',0,8,'0'),(7,'quote_address','sales/quote_address','','sales/quote_address','','',1,'default',7,'',0,8,'0'),(8,'quote_address_item','sales/quote_address_item','','sales/quote_entity','','',1,'default',8,'',0,8,'0'),(9,'quote_address_rate','sales/quote_address_rate','','sales/quote_entity','','',1,'default',9,'',0,8,'0'),(10,'quote_payment','sales/quote_payment','','sales/quote_entity','','',1,'default',10,'',0,8,'0'),(11,'order','sales/order','','sales/order','','',1,'default',11,'eav/entity_increment_numeric',0,7,'0'),(12,'order_address','sales/order_address','','sales/order_entity','','',1,'default',12,'',0,8,'0'),(13,'order_item','sales/order_item','','sales/order_entity','','',1,'default',13,'',0,8,'0'),(14,'order_payment','sales/order_payment','','sales/order_entity','','',1,'default',14,'',0,8,'0'),(15,'order_status_history','sales/order_status_history','','sales/order_entity','','',1,'default',15,'',0,8,'0'),(16,'invoice','sales/order_invoice','','sales/order_entity','','',1,'default',16,'eav/entity_increment_numeric',1,8,'0'),(17,'invoice_item','sales/order_invoice_item','','sales/order_entity','','',1,'default',17,'',0,8,'0'),(18,'invoice_comment','sales/order_invoice_comment','','sales/order_entity','','',1,'default',18,'',0,8,'0'),(19,'shipment','sales/order_shipment','','sales/order_entity','','',1,'default',19,'eav/entity_increment_numeric',1,8,'0'),(20,'shipment_item','sales/order_shipment_item','','sales/order_entity','','',1,'default',20,'',0,8,'0'),(21,'shipment_comment','sales/order_shipment_comment','','sales/order_entity','','',1,'default',21,'',0,8,'0'),(22,'shipment_track','sales/order_shipment_track','','sales/order_entity','','',1,'default',22,'',0,8,'0'),(23,'creditmemo','sales/order_creditmemo','','sales/order_entity','','',1,'default',23,'eav/entity_increment_numeric',1,8,'0'),(24,'creditmemo_item','sales/order_creditmemo_item','','sales/order_entity','','',1,'default',24,'',0,8,'0'),(25,'creditmemo_comment','sales/order_creditmemo_comment','','sales/order_entity','','',1,'default',25,'',0,8,'0'),(26,'aitoc_checkout','','','','','',1,'default',0,'',0,8,'0');
/*!40000 ALTER TABLE `eav_entity_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:33
