-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tax_calculation_rate_title`
--

DROP TABLE IF EXISTS `tax_calculation_rate_title`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL auto_increment,
  `tax_calculation_rate_id` int(11) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`tax_calculation_rate_title_id`),
  KEY `IDX_TAX_CALCULATION_RATE_TITLE` (`tax_calculation_rate_id`,`store_id`),
  KEY `FK_TAX_CALCULATION_RATE_TITLE_RATE` (`tax_calculation_rate_id`),
  KEY `FK_TAX_CALCULATION_RATE_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_TAX_CALCULATION_RATE_TITLE_RATE` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALCULATION_RATE_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tax_calculation_rate_title`
--

LOCK TABLES `tax_calculation_rate_title` WRITE;
/*!40000 ALTER TABLE `tax_calculation_rate_title` DISABLE KEYS */;
INSERT INTO `tax_calculation_rate_title` VALUES (7,6,2,'Dansk moms'),(8,6,3,'Dansk moms'),(9,7,2,'Dansk moms'),(10,7,3,'Dansk moms'),(11,8,2,'Dansk moms'),(12,8,3,'Dansk moms'),(13,9,2,'Dansk moms'),(14,9,3,'Dansk moms'),(15,10,2,'Dansk moms'),(16,10,3,'Dansk moms'),(17,11,2,'Dansk moms'),(18,11,3,'Dansk moms'),(19,12,2,'Dansk moms'),(20,12,3,'Dansk moms'),(21,13,2,'Dansk moms'),(22,13,3,'Dansk moms'),(23,14,2,'Dansk moms'),(24,14,3,'Dansk moms'),(25,15,2,'Dansk moms'),(26,15,3,'Dansk moms'),(27,16,2,'Dansk moms'),(28,16,3,'Dansk moms'),(29,17,2,'Dansk moms'),(30,17,3,'Dansk moms'),(31,18,2,'Dansk moms'),(32,18,3,'Dansk moms'),(33,19,2,'Dansk moms'),(34,19,3,'Dansk moms'),(35,20,2,'Dansk moms'),(36,20,3,'Dansk moms'),(37,21,2,'Dansk moms'),(38,21,3,'Dansk moms'),(39,22,2,'Dansk moms'),(40,22,3,'Dansk moms'),(41,23,2,'Dansk moms'),(42,23,3,'Dansk moms'),(43,24,2,'Dansk moms'),(44,24,3,'Dansk moms'),(45,25,2,'Dansk moms'),(46,25,3,'Dansk moms'),(47,26,2,'Dansk moms'),(48,26,3,'Dansk moms'),(49,27,2,'Dansk moms'),(50,27,3,'Dansk moms'),(51,28,2,'Dansk moms'),(52,28,3,'Dansk moms'),(53,29,2,'Dansk moms'),(54,29,3,'Dansk moms');
/*!40000 ALTER TABLE `tax_calculation_rate_title` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:44
