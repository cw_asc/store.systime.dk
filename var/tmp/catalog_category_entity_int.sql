-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalog_category_entity_int`
--

DROP TABLE IF EXISTS `catalog_category_entity_int`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `catalog_category_entity_int` (
  `value_id` int(11) NOT NULL auto_increment,
  `entity_type_id` smallint(5) unsigned NOT NULL default '0',
  `attribute_id` smallint(5) unsigned NOT NULL default '0',
  `store_id` smallint(5) unsigned NOT NULL default '0',
  `entity_id` int(10) unsigned NOT NULL default '0',
  `value` int(11) NOT NULL default '0',
  PRIMARY KEY  (`value_id`),
  UNIQUE KEY `IDX_BASE` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_INT_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_CATEGORY_EMTITY_INT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_CATEGORY_EMTITY_INT_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_EMTITY_INT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_EMTITY_INT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_EMTITY_INT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `catalog_category_entity_int`
--

LOCK TABLES `catalog_category_entity_int` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_int` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_int` VALUES (1,3,32,0,2,1),(19,3,32,0,8,1),(20,3,41,0,8,1),(21,3,49,0,8,1),(25,3,32,0,10,0),(26,3,41,0,10,1),(27,3,49,0,10,1),(28,3,32,0,11,1),(29,3,41,0,11,1),(30,3,49,0,11,1),(33,3,32,0,12,1),(34,3,41,0,12,1),(35,3,49,0,12,1),(39,3,32,0,14,1),(40,3,41,0,14,1),(41,3,49,0,14,1),(42,3,32,0,15,1),(43,3,41,0,15,1),(44,3,49,0,15,1),(45,3,41,0,2,1),(46,3,49,0,2,1),(117,3,32,0,40,1),(118,3,41,0,40,1),(119,3,49,0,40,1),(120,3,32,0,41,1),(121,3,41,0,41,1),(122,3,49,0,41,1),(126,3,32,0,43,1),(127,3,41,0,43,1),(128,3,49,0,43,1),(129,3,32,0,44,1),(130,3,41,0,44,1),(131,3,49,0,44,1),(132,3,32,0,45,1),(133,3,41,0,45,1),(134,3,49,0,45,1),(135,3,32,0,46,1),(136,3,41,0,46,1),(137,3,49,0,46,1),(138,3,32,0,47,1),(139,3,41,0,47,1),(140,3,49,0,47,1),(141,3,32,0,48,1),(142,3,41,0,48,1),(143,3,49,0,48,1),(144,3,32,0,49,1),(145,3,41,0,49,1),(146,3,49,0,49,1),(147,3,32,0,50,1),(148,3,41,0,50,1),(149,3,49,0,50,1),(150,3,32,0,51,1),(151,3,41,0,51,1),(152,3,49,0,51,1),(153,3,32,0,52,1),(154,3,41,0,52,1),(155,3,49,0,52,1),(156,3,32,0,53,1),(157,3,41,0,53,1),(158,3,49,0,53,1),(159,3,32,0,54,1),(160,3,41,0,54,1),(161,3,49,0,54,1),(162,3,32,0,55,1),(163,3,41,0,55,1),(164,3,49,0,55,1),(165,3,32,0,56,1),(166,3,41,0,56,1),(167,3,49,0,56,1),(168,3,32,0,57,1),(169,3,41,0,57,1),(170,3,49,0,57,1),(171,3,32,0,58,1),(172,3,41,0,58,1),(173,3,49,0,58,1),(174,3,32,0,59,1),(175,3,41,0,59,1),(176,3,49,0,59,1),(177,3,32,0,60,1),(178,3,41,0,60,1),(179,3,49,0,60,1),(180,3,32,0,61,1),(181,3,41,0,61,1),(182,3,49,0,61,1),(183,3,32,0,62,1),(184,3,41,0,62,1),(185,3,49,0,62,1),(186,3,32,0,63,1),(187,3,41,0,63,1),(188,3,49,0,63,1),(189,3,32,0,64,1),(190,3,41,0,64,1),(191,3,49,0,64,1),(192,3,32,0,65,1),(193,3,41,0,65,1),(194,3,49,0,65,1),(195,3,32,0,66,1),(196,3,41,0,66,1),(197,3,49,0,66,1),(198,3,32,0,67,1),(199,3,41,0,67,1),(200,3,49,0,67,1),(201,3,32,0,68,1),(202,3,41,0,68,1),(203,3,49,0,68,1),(204,3,32,0,69,1),(205,3,41,0,69,1),(206,3,49,0,69,1),(207,3,32,0,70,1),(208,3,41,0,70,1),(209,3,49,0,70,1),(210,3,32,0,71,1),(211,3,41,0,71,1),(212,3,49,0,71,1),(213,3,32,0,72,1),(214,3,41,0,72,1),(215,3,49,0,72,1),(216,3,32,0,73,1),(217,3,41,0,73,1),(218,3,49,0,73,1),(219,3,32,0,74,1),(220,3,41,0,74,1),(221,3,49,0,74,1),(222,3,32,0,75,1),(223,3,41,0,75,1),(224,3,49,0,75,1),(225,3,32,0,76,1),(226,3,41,0,76,1),(227,3,49,0,76,1),(228,3,32,0,77,1),(229,3,41,0,77,1),(230,3,49,0,77,1),(231,3,32,0,78,1),(232,3,41,0,78,1),(233,3,49,0,78,1),(234,3,32,0,79,1),(235,3,41,0,79,1),(236,3,49,0,79,1),(237,3,32,0,80,1),(238,3,41,0,80,1),(239,3,49,0,80,1),(240,3,40,0,48,6),(241,3,32,0,81,1),(242,3,41,0,81,1),(243,3,49,0,81,1),(247,3,32,0,83,1),(248,3,41,0,83,1),(249,3,49,0,83,1),(250,3,32,0,84,1),(251,3,41,0,84,1),(252,3,49,0,84,1),(253,3,32,0,85,1),(254,3,41,0,85,1),(255,3,49,0,85,1),(261,3,32,0,89,1),(262,3,41,0,89,1),(263,3,49,0,89,1),(264,3,32,0,91,1),(265,3,41,0,91,1),(266,3,49,0,91,1),(270,3,32,0,94,1),(271,3,41,0,94,1),(272,3,49,0,94,1),(273,3,32,0,95,0),(274,3,41,0,95,1),(275,3,49,0,95,1),(276,3,32,0,98,1),(277,3,41,0,98,1),(278,3,49,0,98,1),(279,3,32,0,100,0),(280,3,41,0,100,1),(281,3,49,0,100,1),(282,3,32,0,101,1),(283,3,41,0,101,1),(284,3,49,0,101,1),(285,3,32,0,103,0),(286,3,41,0,103,1),(287,3,49,0,103,1),(288,3,32,0,104,1),(289,3,41,0,104,1),(290,3,49,0,104,1),(291,3,32,0,105,1),(292,3,41,0,105,1),(293,3,49,0,105,1),(294,3,32,0,106,1),(295,3,41,0,106,1),(296,3,49,0,106,1),(297,3,32,0,108,1),(298,3,41,0,108,1),(299,3,49,0,108,1),(300,3,32,0,109,1),(301,3,41,0,109,1),(302,3,49,0,109,1),(303,3,32,0,111,1),(304,3,41,0,111,1),(305,3,49,0,111,1),(306,3,32,0,112,1),(307,3,41,0,112,1),(308,3,49,0,112,1),(309,3,32,0,113,0),(310,3,41,0,113,1),(311,3,49,0,113,1),(312,3,32,0,114,0),(313,3,41,0,114,1),(314,3,49,0,114,1),(315,3,32,0,115,1),(316,3,41,0,115,1),(317,3,49,0,115,1),(318,3,32,0,117,0),(319,3,41,0,117,1),(320,3,49,0,117,1),(321,3,32,0,120,0),(322,3,41,0,120,1),(323,3,49,0,120,1),(324,3,32,0,121,0),(325,3,41,0,121,1),(326,3,49,0,121,1),(327,3,32,0,122,0),(328,3,41,0,122,1),(329,3,49,0,122,1),(330,3,32,0,123,1),(331,3,41,0,123,1),(332,3,49,0,123,1),(333,3,32,0,124,0),(334,3,41,0,124,1),(335,3,49,0,124,1),(336,3,32,0,125,1),(337,3,41,0,125,1),(338,3,49,0,125,1),(339,3,32,0,126,1),(340,3,41,0,126,1),(341,3,49,0,126,1),(342,3,32,0,127,0),(343,3,41,0,127,1),(344,3,49,0,127,1),(345,3,32,0,128,1),(346,3,41,0,128,1),(347,3,49,0,128,1),(357,3,32,0,133,1),(358,3,41,0,133,1),(359,3,49,0,133,1),(360,3,32,0,134,0),(361,3,41,0,134,1),(362,3,49,0,134,1),(363,3,32,0,135,1),(364,3,41,0,135,1),(365,3,49,0,135,1),(366,3,32,0,136,0),(367,3,41,0,136,1),(368,3,49,0,136,1),(369,3,32,0,137,0),(370,3,41,0,137,1),(371,3,49,0,137,1),(372,3,32,0,138,1),(373,3,41,0,138,1),(374,3,49,0,138,1),(375,3,32,0,139,1),(376,3,41,0,139,1),(377,3,49,0,139,1),(381,3,32,0,141,1),(382,3,41,0,141,1),(383,3,49,0,141,1),(393,3,32,0,145,1),(394,3,41,0,145,1),(395,3,49,0,145,1),(396,3,32,0,146,1),(397,3,41,0,146,1),(398,3,49,0,146,1),(399,3,32,0,147,1),(400,3,41,0,147,1),(401,3,49,0,147,1),(402,3,32,0,148,1),(403,3,41,0,148,1),(404,3,49,0,148,1),(411,3,32,0,152,0),(412,3,41,0,152,1),(413,3,49,0,152,1),(414,3,32,0,153,0),(415,3,41,0,153,1),(416,3,49,0,153,1),(417,3,32,0,154,0),(418,3,41,0,154,1),(419,3,49,0,154,1),(420,3,32,0,155,0),(421,3,41,0,155,1),(422,3,49,0,155,1),(423,3,32,0,156,0),(424,3,41,0,156,1),(425,3,49,0,156,1),(426,3,32,0,157,0),(427,3,41,0,157,1),(428,3,49,0,157,1),(429,3,32,0,158,0),(430,3,41,0,158,1),(431,3,49,0,158,1),(432,3,32,0,159,0),(433,3,41,0,159,1),(434,3,49,0,159,1),(435,3,32,0,160,0),(436,3,41,0,160,1),(437,3,49,0,160,1),(438,3,32,0,161,0),(439,3,41,0,161,1),(440,3,49,0,161,1),(441,3,32,0,162,0),(442,3,41,0,162,1),(443,3,49,0,162,1),(444,3,32,0,163,0),(445,3,41,0,163,1),(446,3,49,0,163,1),(447,3,32,0,164,0),(448,3,41,0,164,1),(449,3,49,0,164,1),(450,3,32,0,165,0),(451,3,41,0,165,1),(452,3,49,0,165,1),(453,3,32,0,166,1),(454,3,41,0,166,1),(455,3,49,0,166,1),(456,3,32,0,167,0),(457,3,41,0,167,1),(458,3,49,0,167,1),(459,3,32,0,168,0),(460,3,41,0,168,1),(461,3,49,0,168,1),(465,3,32,0,170,1),(466,3,41,0,170,1),(467,3,49,0,170,1),(468,3,32,0,171,1),(469,3,41,0,171,1),(470,3,49,0,171,1),(471,3,32,0,172,1),(472,3,41,0,172,1),(473,3,49,0,172,1),(474,3,32,0,173,1),(475,3,41,0,173,1),(476,3,49,0,173,1),(477,3,32,0,174,1),(478,3,41,0,174,1),(479,3,49,0,174,1),(480,3,32,0,175,1),(481,3,41,0,175,1),(482,3,49,0,175,1),(483,3,32,0,176,1),(484,3,41,0,176,1),(485,3,49,0,176,1),(489,3,32,0,178,1),(490,3,41,0,178,1),(491,3,49,0,178,1),(492,3,32,0,179,1),(493,3,41,0,179,1),(494,3,49,0,179,1),(498,3,32,0,181,1),(499,3,41,0,181,1),(500,3,49,0,181,1),(501,3,32,0,183,1),(502,3,41,0,183,1),(503,3,49,0,183,1),(504,3,32,0,184,1),(505,3,41,0,184,1),(506,3,49,0,184,1),(507,3,32,0,185,1),(508,3,41,0,185,1),(509,3,49,0,185,1),(513,3,32,0,187,1),(514,3,41,0,187,1),(515,3,49,0,187,1),(516,3,32,0,189,1),(517,3,41,0,189,1),(518,3,49,0,189,1),(519,3,32,0,191,1),(520,3,41,0,191,1),(521,3,49,0,191,1),(522,3,32,0,192,1),(523,3,41,0,192,1),(524,3,49,0,192,1),(525,3,32,0,193,1),(526,3,41,0,193,1),(527,3,49,0,193,1),(528,3,32,0,195,1),(529,3,41,0,195,1),(530,3,49,0,195,1),(531,3,32,0,197,1),(532,3,41,0,197,1),(533,3,49,0,197,1),(534,3,32,0,198,1),(535,3,41,0,198,1),(536,3,49,0,198,1),(537,3,32,0,199,1),(538,3,41,0,199,1),(539,3,49,0,199,1),(540,3,32,0,200,1),(541,3,41,0,200,1),(542,3,49,0,200,1),(543,3,32,0,202,1),(544,3,41,0,202,1),(545,3,49,0,202,1),(546,3,32,0,203,1),(547,3,41,0,203,1),(548,3,49,0,203,1),(549,3,32,0,204,1),(550,3,41,0,204,1),(551,3,49,0,204,1),(552,3,32,0,205,1),(553,3,41,0,205,1),(554,3,49,0,205,1),(555,3,32,0,206,1),(556,3,41,0,206,1),(557,3,49,0,206,1),(558,3,32,0,207,1),(559,3,41,0,207,1),(560,3,49,0,207,1),(561,3,32,0,208,1),(562,3,41,0,208,1),(563,3,49,0,208,1),(564,3,32,0,210,1),(565,3,41,0,210,0),(566,3,49,0,210,1),(567,3,32,0,212,1),(568,3,41,0,212,0),(569,3,49,0,212,1),(576,3,32,0,217,1),(577,3,41,0,217,0),(578,3,49,0,217,1),(579,3,32,0,218,1),(580,3,41,0,218,0),(581,3,49,0,218,1),(582,3,32,0,219,1),(583,3,41,0,219,0),(584,3,49,0,219,1),(585,3,40,0,170,25),(586,3,40,0,212,25),(587,3,40,0,141,25),(588,3,40,0,171,25);
/*!40000 ALTER TABLE `catalog_category_entity_int` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:31
