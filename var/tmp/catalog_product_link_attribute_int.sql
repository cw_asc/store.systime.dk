-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalog_product_link_attribute_int`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_int`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `catalog_product_link_attribute_int` (
  `value_id` int(11) unsigned NOT NULL auto_increment,
  `product_link_attribute_id` smallint(6) unsigned default NULL,
  `link_id` int(11) unsigned default NULL,
  `value` int(11) NOT NULL default '0',
  PRIMARY KEY  (`value_id`),
  KEY `FK_INT_PRODUCT_LINK_ATTRIBUTE` (`product_link_attribute_id`),
  KEY `FK_INT_PRODUCT_LINK` (`link_id`),
  CONSTRAINT `FK_INT_PRODUCT_LINK` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_INT_PRODUCT_LINK_ATTRIBUTE` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=366 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `catalog_product_link_attribute_int`
--

LOCK TABLES `catalog_product_link_attribute_int` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute_int` DISABLE KEYS */;
INSERT INTO `catalog_product_link_attribute_int` VALUES (37,4,51,0),(38,4,52,0),(39,4,53,0),(176,2,1270,0),(177,2,1271,0),(178,2,1272,0),(199,2,1293,0),(200,2,1294,0),(201,2,1295,0),(202,2,1296,0),(238,2,2659,0),(239,2,2660,0),(240,2,2661,0),(241,2,2662,0),(242,2,2663,0),(247,2,2699,0),(248,2,2851,0),(249,2,2852,0),(250,2,2853,0),(253,2,2910,0),(254,2,2913,0),(255,2,2914,0),(256,2,2915,0),(257,2,2928,0),(258,2,2929,0),(259,2,2930,0),(260,2,2931,0),(261,2,2932,0),(262,2,2933,0),(263,2,2934,0),(264,2,2935,0),(265,2,2936,0),(266,2,2937,0),(268,2,2939,0),(269,2,2940,0),(270,2,2941,0),(272,2,2943,0),(273,2,2944,0),(274,2,2945,0),(275,2,2946,0),(276,2,2947,0),(277,2,2948,0),(278,2,2949,0),(279,2,2950,0),(280,2,2951,0),(281,2,2952,0),(282,2,2953,0),(283,2,2954,0),(284,2,2955,0),(298,2,2969,0),(299,2,2970,0),(300,2,2971,0),(301,2,2972,0),(302,2,2973,0),(303,2,2974,0),(304,2,2975,0),(305,2,2976,0),(306,2,2977,0),(307,2,2978,0),(308,2,2979,0),(309,2,2980,0),(310,2,2981,0),(311,2,2982,0),(312,2,2983,0),(313,2,2984,0),(314,2,2985,0),(315,2,2986,0),(316,2,2987,0),(317,2,2988,0),(318,2,2989,0),(319,2,2990,0),(320,2,2991,0),(321,2,2992,0),(322,2,2993,0),(323,2,3002,0),(324,2,3003,0),(325,2,3004,0),(326,2,3005,0),(327,2,3006,0),(328,2,3007,0),(329,2,3008,0),(330,2,3009,0),(331,2,3010,0),(332,2,3011,0),(333,2,3012,0),(334,2,3013,0),(335,2,3014,0),(336,2,3015,0),(337,2,3016,0),(338,2,3017,0),(339,2,3018,0),(340,2,3021,0),(341,2,3022,0),(342,2,3023,0),(343,2,3024,0),(344,2,3025,0),(345,2,3026,0),(346,2,3027,0),(347,2,3028,0),(348,2,3029,0),(349,2,3030,0),(350,2,3032,0),(351,2,3039,0),(352,2,3043,0),(353,2,3044,0),(354,2,3045,0),(355,2,3046,0),(356,2,3047,0),(357,2,3048,0),(358,2,3049,0),(359,2,3050,0),(360,2,3051,0),(361,2,3052,0),(362,2,3053,0),(363,2,3054,0),(365,2,3056,0);
/*!40000 ALTER TABLE `catalog_product_link_attribute_int` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:31
