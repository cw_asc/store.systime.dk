-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalog_product_super_attribute_label`
--

DROP TABLE IF EXISTS `catalog_product_super_attribute_label`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `catalog_product_super_attribute_label` (
  `value_id` int(10) unsigned NOT NULL auto_increment,
  `product_super_attribute_id` int(10) unsigned NOT NULL default '0',
  `store_id` smallint(5) unsigned NOT NULL default '0',
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`value_id`),
  KEY `FK_SUPER_PRODUCT_ATTRIBUTE_LABEL` (`product_super_attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_SUPER_ATTRIBUTE_STORE_PSAI_SI` (`product_super_attribute_id`,`store_id`),
  CONSTRAINT `FK_SUPER_PRODUCT_ATTRIBUTE_LABEL` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `catalog_product_super_attribute_label`
--

LOCK TABLES `catalog_product_super_attribute_label` WRITE;
/*!40000 ALTER TABLE `catalog_product_super_attribute_label` DISABLE KEYS */;
INSERT INTO `catalog_product_super_attribute_label` VALUES (8,7,0,'Licenstype'),(9,8,0,'Licenstype'),(10,9,0,'Licenstype'),(11,10,0,'Licenstype'),(12,11,0,'Licenstype'),(13,12,0,'Licenstype'),(14,13,0,'Licenstype'),(15,14,0,'Licenstype'),(16,15,0,'Licenstype'),(17,16,0,'Licenstype'),(18,17,0,'Licenstype'),(19,18,0,'Licenstype'),(20,19,0,'Licenstype'),(21,20,0,'Licenstype'),(22,21,0,'Licenstype'),(23,22,0,'Licenstype'),(24,23,0,'Licenstype'),(25,24,0,'Licenstype'),(26,25,0,'Licenstype'),(27,26,0,'Licenstype'),(28,27,0,'Licenstype'),(29,28,0,'Licenstype'),(30,29,0,'Licenstype'),(31,30,0,'Licenstype'),(32,31,0,'Licenstype'),(33,32,0,'Licenstype'),(34,33,0,'Licenstype'),(35,34,0,'Licenstype'),(36,35,0,'Licenstype'),(37,36,0,'Licenstype'),(38,37,0,'Licenstype'),(39,38,0,'Licenstype'),(40,39,0,'Licenstype'),(41,40,0,'Licenstype'),(42,41,0,'Licenstype'),(43,42,0,'Licenstype'),(44,43,0,'Licenstype'),(45,44,0,'Licenstype'),(46,45,0,'Licenstype'),(47,46,0,'Licenstype'),(48,47,0,'Licenstype'),(49,48,0,'Licenstype'),(50,49,0,'Licenstype'),(51,50,0,'Licenstype'),(52,51,0,'Licenstype'),(53,52,0,'Licenstype'),(54,53,0,'Licenstype'),(55,54,0,'Licenstype'),(56,55,0,'Licenstype'),(57,56,0,'Licenstype'),(58,57,0,'Licenstype'),(59,58,0,'Licenstype'),(60,59,0,'Licenstype'),(61,60,0,'Licenstype'),(62,61,0,'Licenstype'),(63,62,0,'Licenstype'),(64,63,0,'Licenstype'),(65,64,0,'Licenstype'),(66,65,0,'Licenstype'),(67,66,0,'Licenstype'),(68,67,0,'Licenstype'),(69,68,0,'Licenstype'),(70,69,0,'Licenstype'),(71,70,0,'Licenstype'),(72,71,0,'Licenstype'),(73,72,0,'Licenstype'),(74,73,0,'Licenstype'),(75,74,0,'Licenstype'),(76,75,0,'Licenstype'),(77,76,0,'Licenstype'),(78,77,0,'Licenstype'),(79,78,0,'Licenstype'),(80,79,0,'Licenstype'),(81,80,0,'Licenstype'),(82,81,0,'Licenstype'),(83,82,0,'Licenstype'),(84,83,0,'Licenstype'),(85,84,0,'Licenstype'),(86,85,0,'Licenstype'),(87,86,0,'Licenstype'),(88,87,0,'Licenstype'),(89,88,0,'Licenstype'),(90,89,0,'Licenstype'),(91,90,0,'Licenstype'),(92,91,0,'Licenstype'),(93,92,0,'Licenstype'),(94,93,0,'Licenstype'),(95,94,0,'Licenstype'),(96,95,0,'Licenstype'),(97,96,0,'Licenstype'),(98,97,0,'Licenstype'),(99,98,0,'Licenstype'),(100,99,0,'Licenstype'),(101,100,0,'Licenstype'),(102,101,0,'Licenstype'),(103,102,0,'Licenstype'),(104,103,0,'Licenstype'),(105,104,0,'Licenstype'),(106,105,0,'Licenstype'),(107,106,0,'Licenstype'),(108,107,0,'Licenstype'),(110,109,0,'Licenstype');
/*!40000 ALTER TABLE `catalog_product_super_attribute_label` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:31
