-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `core_resource`
--

DROP TABLE IF EXISTS `core_resource`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `core_resource` (
  `code` varchar(50) NOT NULL default '',
  `version` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Resource version registry';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `core_resource`
--

LOCK TABLES `core_resource` WRITE;
/*!40000 ALTER TABLE `core_resource` DISABLE KEYS */;
INSERT INTO `core_resource` VALUES ('adminnotification_setup','1.0.0'),('admin_setup','0.7.1'),('amazonpayments_setup','0.1.2'),('api_setup','0.8.1'),('backup_setup','0.7.0'),('bundle_setup','0.1.7'),('catalogindex_setup','0.7.10'),('cataloginventory_setup','0.7.5'),('catalogrule_setup','0.7.7'),('catalogsearch_setup','0.7.6'),('catalog_setup','0.7.69'),('checkout_setup','0.9.3'),('cms_setup','0.7.8'),('contacts_setup','0.8.0'),('core_setup','0.8.13'),('cron_setup','0.7.1'),('customer_setup','0.8.11'),('dataflow_setup','0.7.4'),('dibs_setup','0.9.8'),('directory_setup','0.8.5'),('downloadable_setup','0.1.14'),('eav_setup','0.7.13'),('export_setup','0.1.0'),('Freeoptionalshipping_setup','1.0.5'),('giftmessage_setup','0.7.2'),('googlebase_setup','0.1.1'),('googlecheckout_setup','0.7.3'),('googleoptimizer_setup','0.1.2'),('log_setup','0.7.6'),('matrixrate_setup','2.0.1'),('newsletter_setup','0.8.0'),('paygate_setup','0.7.0'),('payment_setup','0.7.0'),('paypaluk_setup','0.7.0'),('paypal_setup','0.7.2'),('poll_setup','0.7.2'),('productalert_setup','0.7.2'),('rating_setup','0.7.2'),('reports_setup','0.7.7'),('review_setup','0.7.4'),('salesrule_setup','0.7.7'),('sales_setup','0.9.38'),('sendfriend_setup','0.7.2'),('shipping_setup','0.7.0'),('sitemap_setup','0.7.2'),('sphinx_setup','1.1.2'),('systimepayment_setup','0.1.0'),('systimesales_setup','0.1.0'),('systimesearch_setup','0.1.0'),('systimesso_setup','0.1.0'),('tag_setup','0.7.2'),('tax_setup','0.7.8'),('usa_setup','0.7.0'),('weee_setup','0.13'),('wishlist_setup','0.7.4');
/*!40000 ALTER TABLE `core_resource` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:32
