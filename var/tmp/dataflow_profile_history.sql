-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dataflow_profile_history`
--

DROP TABLE IF EXISTS `dataflow_profile_history`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `dataflow_profile_history` (
  `history_id` int(10) unsigned NOT NULL auto_increment,
  `profile_id` int(10) unsigned NOT NULL default '0',
  `action_code` varchar(64) default NULL,
  `user_id` int(10) unsigned NOT NULL default '0',
  `performed_at` datetime default NULL,
  PRIMARY KEY  (`history_id`),
  KEY `FK_dataflow_profile_history` (`profile_id`),
  CONSTRAINT `FK_dataflow_profile_history` FOREIGN KEY (`profile_id`) REFERENCES `dataflow_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `dataflow_profile_history`
--

LOCK TABLES `dataflow_profile_history` WRITE;
/*!40000 ALTER TABLE `dataflow_profile_history` DISABLE KEYS */;
INSERT INTO `dataflow_profile_history` VALUES (1,1,'run',2,'2008-09-15 12:47:34'),(2,1,'run',2,'2008-10-02 12:58:21'),(3,2,'run',4,'2008-10-02 13:35:06'),(4,1,'run',4,'2008-11-13 12:47:22'),(6,8,'create',4,'2008-11-17 13:24:25'),(7,8,'update',4,'2008-11-17 13:25:13'),(8,8,'run',4,'2008-11-17 13:26:32'),(9,8,'run',4,'2008-11-17 15:23:08'),(10,8,'run',4,'2008-11-17 15:26:27'),(11,1,'run',4,'2008-11-17 15:30:28'),(12,8,'run',4,'2008-11-18 07:46:14'),(13,8,'run',4,'2008-11-18 08:43:30'),(14,8,'update',4,'2008-11-18 09:19:31'),(15,9,'create',4,'2008-11-18 09:20:10'),(16,9,'run',4,'2008-11-18 09:21:31'),(17,9,'run',4,'2008-11-18 09:57:08'),(18,9,'run',4,'2008-11-18 10:05:15'),(19,9,'run',4,'2008-11-18 10:09:45'),(20,9,'run',4,'2008-11-18 15:04:31'),(21,9,'run',4,'2008-11-18 15:44:14'),(22,9,'run',4,'2008-11-28 11:55:29'),(23,8,'run',4,'2008-12-02 09:02:19'),(24,8,'run',4,'2008-12-02 13:09:32'),(25,8,'run',4,'2008-12-02 13:39:18'),(26,8,'run',4,'2008-12-02 13:45:01'),(27,8,'run',4,'2008-12-10 07:59:02'),(28,8,'run',4,'2008-12-10 08:14:01'),(29,1,'run',2,'2009-01-19 10:03:49'),(30,8,'run',4,'2009-01-19 11:43:56'),(31,1,'run',4,'2009-01-19 11:56:30'),(32,8,'run',4,'2009-01-19 13:09:31'),(33,8,'run',4,'2009-01-26 09:25:41'),(34,8,'run',4,'2009-01-26 10:07:00'),(35,8,'run',4,'2009-01-27 07:50:43'),(36,8,'run',4,'2009-01-27 08:33:00'),(37,8,'run',4,'2009-02-11 12:47:55'),(38,8,'run',4,'2009-02-11 13:07:46'),(39,8,'run',4,'2009-02-11 13:11:54'),(40,8,'run',4,'2009-02-12 10:07:21'),(41,8,'run',4,'2009-02-13 12:32:09'),(42,10,'create',4,'2009-03-02 09:10:07'),(43,10,'run',4,'2009-03-02 09:10:21'),(44,8,'run',4,'2009-03-02 10:22:04'),(45,11,'create',2,'2009-03-16 14:58:02'),(46,11,'run',2,'2009-03-16 14:58:55'),(47,11,'update',2,'2009-03-16 15:02:40'),(48,11,'run',2,'2009-03-16 15:02:53'),(49,11,'update',2,'2009-03-16 15:07:56'),(50,11,'run',2,'2009-03-16 15:08:02'),(51,12,'create',2,'2009-03-16 15:12:09'),(52,12,'update',2,'2009-03-16 15:13:14'),(53,12,'update',2,'2009-03-16 15:15:10'),(54,12,'run',2,'2009-03-16 15:15:50'),(55,13,'create',4,'2009-03-19 12:26:53'),(56,13,'run',4,'2009-03-19 12:27:10'),(57,13,'update',4,'2009-03-19 12:51:04'),(58,14,'create',4,'2009-03-19 12:52:38'),(59,14,'update',4,'2009-03-19 12:54:47'),(60,14,'run',4,'2009-03-19 12:55:01'),(61,14,'run',4,'2009-03-19 13:34:20'),(62,8,'run',4,'2009-03-31 11:44:19'),(63,10,'run',4,'2009-04-03 07:13:31'),(64,10,'run',4,'2009-04-03 07:15:22'),(65,10,'run',4,'2009-04-03 07:16:21'),(66,8,'run',4,'2009-04-03 11:42:18'),(67,15,'create',4,'2009-04-13 10:23:05'),(68,15,'run',4,'2009-04-13 10:26:31'),(69,16,'create',4,'2009-04-14 18:25:47'),(70,16,'run',4,'2009-04-14 18:25:54'),(71,8,'run',4,'2009-04-15 08:47:30'),(72,8,'run',4,'2009-04-15 09:55:08'),(73,8,'run',4,'2009-04-15 12:32:53'),(74,16,'run',4,'2009-04-15 14:08:36'),(75,17,'create',4,'2009-04-15 16:43:33'),(76,17,'run',4,'2009-04-15 16:43:44'),(77,18,'create',4,'2009-04-15 16:58:18'),(78,18,'run',4,'2009-04-15 16:58:46'),(79,18,'update',4,'2009-04-15 17:30:53'),(80,8,'run',4,'2009-04-17 06:45:00'),(81,16,'run',4,'2009-04-21 12:24:52'),(82,15,'update',4,'2009-04-21 14:23:58'),(83,15,'run',4,'2009-04-21 14:24:08'),(84,8,'run',4,'2009-05-05 14:26:33'),(85,8,'run',4,'2009-05-07 08:28:43'),(86,19,'create',4,'2009-05-13 11:24:58'),(87,19,'run',4,'2009-05-13 11:25:23'),(88,19,'run',4,'2009-05-13 13:21:17'),(89,8,'run',4,'2009-05-14 09:37:53'),(90,20,'create',2,'2009-05-15 07:13:01'),(91,20,'run',2,'2009-05-15 07:14:08'),(92,8,'run',4,'2009-05-18 12:54:08'),(93,21,'create',4,'2009-05-20 13:59:05'),(94,21,'run',4,'2009-05-20 14:03:15'),(95,22,'create',4,'2009-05-22 20:42:37'),(96,22,'run',4,'2009-05-22 20:44:55'),(97,23,'create',4,'2009-05-23 09:56:39'),(98,23,'run',4,'2009-05-23 09:56:44'),(99,23,'update',4,'2009-05-23 09:57:43'),(100,23,'update',4,'2009-05-23 11:23:13'),(101,23,'run',4,'2009-05-23 11:24:06'),(102,23,'update',4,'2009-05-23 11:30:50'),(103,23,'update',4,'2009-05-23 11:30:50'),(104,23,'update',4,'2009-05-23 11:30:54'),(105,23,'run',4,'2009-05-23 11:31:01'),(106,23,'update',4,'2009-05-23 11:35:07'),(107,23,'run',4,'2009-05-23 11:35:21'),(108,22,'update',4,'2009-05-23 12:25:49'),(109,22,'run',4,'2009-05-23 12:26:14'),(110,24,'create',4,'2009-06-12 10:37:08'),(111,24,'run',4,'2009-06-12 10:37:15');
/*!40000 ALTER TABLE `dataflow_profile_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:33
