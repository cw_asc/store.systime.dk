-- MySQL dump 10.11
--
-- Host: localhost    Database: store_systime_dk
-- ------------------------------------------------------
-- Server version	5.0.51a-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalog_product_link_attribute_decimal`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_decimal`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `catalog_product_link_attribute_decimal` (
  `value_id` int(11) unsigned NOT NULL auto_increment,
  `product_link_attribute_id` smallint(6) unsigned default NULL,
  `link_id` int(11) unsigned default NULL,
  `value` decimal(12,4) NOT NULL default '0.0000',
  PRIMARY KEY  (`value_id`),
  KEY `FK_DECIMAL_PRODUCT_LINK_ATTRIBUTE` (`product_link_attribute_id`),
  KEY `FK_DECIMAL_LINK` (`link_id`),
  CONSTRAINT `FK_DECIMAL_LINK` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DECIMAL_PRODUCT_LINK_ATTRIBUTE` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COMMENT='Decimal attributes values';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `catalog_product_link_attribute_decimal`
--

LOCK TABLES `catalog_product_link_attribute_decimal` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute_decimal` DISABLE KEYS */;
INSERT INTO `catalog_product_link_attribute_decimal` VALUES (26,6,1293,'0.0000'),(27,6,1294,'0.0000'),(28,6,1295,'0.0000'),(50,6,2659,'0.0000'),(51,6,2660,'0.0000'),(52,6,2661,'0.0000'),(53,6,2662,'0.0000'),(54,6,2663,'0.0000'),(57,6,2699,'0.0000'),(58,6,2851,'0.0000'),(59,6,2852,'0.0000'),(60,6,2853,'0.0000'),(62,6,2910,'0.0000'),(63,6,2913,'0.0000'),(64,6,2914,'0.0000'),(65,6,2915,'0.0000'),(66,6,2939,'0.0000'),(67,6,2969,'0.0000'),(68,6,2970,'0.0000'),(69,6,2971,'0.0000'),(70,6,2972,'0.0000'),(71,6,2973,'0.0000'),(72,6,2974,'0.0000'),(73,6,2975,'0.0000'),(74,6,2976,'0.0000'),(75,6,2977,'0.0000'),(76,6,2978,'0.0000'),(77,6,2979,'0.0000'),(78,6,2980,'0.0000'),(79,6,2981,'0.0000'),(80,6,2982,'0.0000'),(81,6,2983,'0.0000'),(82,6,2984,'0.0000'),(83,6,2985,'0.0000'),(84,6,2986,'0.0000'),(85,6,2987,'0.0000'),(86,6,2988,'0.0000'),(87,6,2989,'0.0000'),(88,6,2990,'0.0000'),(89,6,2991,'0.0000'),(90,6,2992,'0.0000'),(91,6,2993,'0.0000'),(92,6,3002,'0.0000'),(93,6,3003,'0.0000'),(94,6,3004,'0.0000'),(95,6,3005,'0.0000'),(96,6,3006,'0.0000'),(97,6,3007,'0.0000'),(98,6,3008,'0.0000'),(99,6,3009,'0.0000'),(100,6,3010,'0.0000'),(101,6,3011,'0.0000'),(102,6,3012,'0.0000'),(103,6,3013,'0.0000'),(104,6,3014,'0.0000'),(105,6,3015,'0.0000'),(106,6,3016,'0.0000'),(107,6,3017,'0.0000'),(108,6,3018,'0.0000'),(109,6,3021,'0.0000'),(110,6,3022,'0.0000'),(111,6,3023,'0.0000'),(112,6,3024,'0.0000'),(113,6,3025,'0.0000'),(114,6,3026,'0.0000'),(115,6,3027,'0.0000'),(116,6,3028,'0.0000'),(117,6,3029,'0.0000'),(118,6,3030,'0.0000'),(119,6,3039,'0.0000'),(120,6,3056,'0.0000');
/*!40000 ALTER TABLE `catalog_product_link_attribute_decimal` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-08-28 15:06:31
