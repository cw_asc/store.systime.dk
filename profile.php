<?php
/**
 * Error reporting
 */
#error_reporting(E_ALL | E_STRICT);

/**
 * Compilation includes configuration file
 */
$compilerConfig = 'includes/config.php';
if (file_exists($compilerConfig)) {
    include($compilerConfig);
}

$mageFilename = 'app/Mage.php';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

require_once $mageFilename;



Varien_Profiler::enable();
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);

umask(0);


//Varien_Profiler::start(__FILE__);

//die(__FILE__);

//Mage::run();
require_once 'selectstore.php';


//Varien_Profiler::stop(__FILE__);

// Write profiling info
$timers = Varien_Profiler::getTimers();
$filename = 'var/tmp/profiler-data.csv';

$fp = fopen($filename, 'w');

$first = true;
foreach ($timers as $timerName => $timer) {
	if ($first) {
		$values = array('timer');
		$values = array_merge($values, array_keys($timer));
		fputcsv($fp, $values);
	}
	$values = array($timerName);
	$values = array_merge($values, array_map(create_function('$value', 'return is_int($value) ? $value : sprintf("%f", $value);'), $timer));
	fputcsv($fp, $values);
	$first = false;
}

fclose($fp);

echo '<!-- PROFILING -->';
echo phpversion();

exit;

