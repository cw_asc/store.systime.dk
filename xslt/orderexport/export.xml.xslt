<?xml version="1.0" encoding="UTF-8"?>
<files>
	<file filename="copy-20%y%-%m%-%d%-T%h%-%i%-%s%.xml"
				path="/export" active="true" ftp="false" encoding="utf-8" escaping="false">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
			<xsl:output method="xml" encoding="utf-8"/>

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

		</xsl:stylesheet>
	</file>
</files>
