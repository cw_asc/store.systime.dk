<?php
if (count($argv) < 3) {
	echo sprintf('Usage: %s xsl-file orders-file', $argv[0]), "\n";
	exit;
}

$xslFilename = $argv[1];
$ordersFilename = $argv[2];

$xsl = new SimpleXMLElement($xslFilename, null, true);
$transformations = $xsl->xpath('//files/file');

echo sprintf('#transformations: %d', count($transformations)), "\n";

foreach ($transformations as $transformation) {
	$content = $transformation->xpath('*');

	$xsl = new XSLTProcessor();
	$xsl->registerPHPFunctions();
	$doc = new DOMDocument();
	$doc->loadXML($content[0]->asXML());
	$xsl->importStyleSheet($doc);
	$doc->load($ordersFilename);

	$result = $xsl->transformToXML($doc);

	echo '================================================================================', "\n";
	echo $transformation->attributes()->filename, "\n";
	echo '--------------------------------------------------------------------------------', "\n";
	echo $result, "\n";
	echo '================================================================================', "\n";
}
