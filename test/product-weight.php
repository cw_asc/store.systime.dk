<?php
require_once 'init.php';

ini_set('html_errors', 'Off');
error_reporting(E_ALL & ~E_DEPRECATED);

header('Content-type: text/plain; charset=utf-8');

// ob_end_flush();

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$updateCount = 0;
$rowNumber = 0;

$csvFilename = 'product-weight.csv';
if (($handle = fopen($csvFilename, 'r')) !== false) {
	while (($data = fgetcsv($handle)) !== false) {
		$sku = $data[0];
		$weight = $data[1];
		$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
		echo sprintf("% 4d\t", $rowNumber);
		if (!$product) {
			echo 'Invalid product ', $sku;
		} else {
			if ($product->getWeight() != $weight) {
				echo 'Update     ', $sku, ' ', $weight, ' (was ', $product->getWeight(), ')';
				$product->setWeight($weight);
				$product->save();
				echo ' done';
				$updateCount++;
			} else {
				echo 'Up to date ', $sku, ' ', $product->getWeight();
			}
		}
		echo "\n";

		flush();

		// if ($updateCount >= 100) {
			// echo $updateCount, ' updates - stopping.', "\n";
			// break;
		// }

		$rowNumber++;
	}
	fclose($handle);
}

echo $updateCount, ' updates.', "\n";

