<?php

if (version_compare(phpversion(), '5.2.0', '<')===true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;"><div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;"><h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">Whoops, it looks like you have an invalid PHP version.</h3></div><p>Magento supports PHP 5.2.0 or newer. <a href="http://www.magentocommerce.com/install" target="">Find out</a> how to install</a> Magento using PHP-CGI as a work-around.</p></div>';
    exit;
}

$mageFilename = '../app/Mage.php';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

require_once $mageFilename;

#Varien_Profiler::enable();

#Mage::setIsDeveloperMode(true);

#ini_set('display_errors', 1);

umask(0);
Mage::app();

function debug($value, $header = null) {
	if (!$header || is_string($header)) {
		$trace = debug_backtrace();
		$frame = $trace[0];
		$location = $frame['file'].':'.$frame['line'];
		if (isset($trace[1])) {
			$location .= ' ('.$trace[1]['function'].')';
		}
		$header = is_string($header) ? $location.': '.$header : $location;
	}

	if ($header) {
		echo "\n", "--------------------------------------------------------------------------------", "\n", $header, "\n";
	}
	print_r($value);
	echo "\n";
}

?>