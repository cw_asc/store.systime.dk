<?php
require_once 'init.php';

header('Content-type: text/plain; charset=utf-8');

$skus = array(
							'61623676',
							'textanalyse-website',
							'61621862',
							'61616338',
							'61611875',
							);

foreach ($skus as $sku) {
	$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

	$helper = Mage::helper('systimemisc');

	$virtualMediaTypes = $helper->getConfig('virtual_mediatypes') ? explode(',', $helper->getConfig('virtual_mediatypes')) : array();
	$productMediaTypes = $product->getData('mediatype') ? explode(',', $product->getData('mediatype')) : array();


	$isVirtual = array_diff($productMediaTypes, $virtualMediaTypes) ? false : true;

	var_dump(array(
								 // $virtualMediaTypes,
								 // $productMediaTypes,
								 // array_diff($productMediaTypes, $virtualMediaTypes),
								 $product->getSku(),
								 $product->getName(),
								 $helper->isVirtualProduct($product),
								 ));
}
