<?php
require_once 'init.php';

header('Content-type: text/plain; charset: utf-8');

// $stores = Mage::getModel('core/store')
// 	->getResourceCollection()
// 	->setLoadDefault(true)
// 	->load();
// print_r(array('#stores' => count($stores), __FILE__));

$csvFilename = 'Oversigt over alle forfattere med aktive projekter.csv';

$authors = array();
$row = 1;

($handle = @fopen($csvFilename, 'r')) || die('Unable to open file '.$csvFilename);

while (($data = fgetcsv($handle, 1000, ',')) !== false) {
	if ($row > 1) {
		$authors[] = array(
											 'name' => $data[0],
											 'id' => $data[1],
											 );
	}
	$row++;
}
fclose($handle);

// print_r($authors);
// die(__FILE__);


$entityTypeId = Mage::getModel('catalog/product')->getResource()->getEntityType()->getId();
// @see http://snippi.net/magento-add-optionvalue-dropdownselectmultiselect-attribute
$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'author_brands');
$attribute->setStoreId(0);

/* @var $_options Array My array of current ['Option Name'] = 'option_id' pairs for my $_attribute */
$attributeOptions = Mage::getModel('eav/entity_attribute_source_table')->setAttribute($attribute)->getAllOptions(false);
$existingOptions = array();
foreach ($attributeOptions as $option) {
	$value = $option['label'];
	$id = $option['value'];
	$existingOptions[$value] = $id;
}

// var_dump(array('existingOptions' => $existingOptions));

// $existingOptions['Mikkel Ricky'] =>
$optionArray = array('value' => array(),
 										 'order' => array(),
 										 'delete' => array());
foreach ($existingOptions as $value => $id) {
	$optionArray['value'][$id] = array($value);
}

// $authors = array(
// 								 array('Tommy Gjøe', 10813),
// 								 );

// $newOptions = array();
// foreach ($authors as $author) {

// }


foreach ($authors as $author) {
	$id = $author['id'];
	$name = $author['name'];
	$value = $id.' '.$name;

	$key = isset($existingOptions[$value]) ? $existingOptions[$value] : 'option_'.$id;
	$optionArray['value'][$key] = array(0 => $value,
																			2 => $name,
																			3 => $name);
}

// $_optionArr['value']['option_1'] = array($name); # This is our new option.

// var_dump(array('optionArray' => $optionArray));

// var_dump(get_class($attribute));

$attribute->setOption($optionArray);

echo 'Saving ...';
$attribute->save();
echo 'done!', "\n";

$attributeOptions = Mage::getModel('eav/entity_attribute_source_table')->setAttribute($attribute)->getAllOptions(false);
$existingOptions = array();
foreach ($attributeOptions as $option) {
	$value = $option['label'];
	$id = $option['value'];
	$existingOptions[$value] = $id;
}

print_r(array('|existingOptions|' => count($existingOptions),
							'existingOptions' => $existingOptions,
							));

// var_dump(get_class(Mage::getModel('eav/entity_attribute_source_table')->setAttribute($licenseTypeAttribute)));
// var_dump(get_class($options = Mage::getModel('eav/entity_attribute_source_table')->setAttribute($licenseTypeAttribute)));

// $options = Mage::getModel('eav/entity_attribute_source_table')->setAttribute($licenseTypeAttribute)->getAllOptions(false);
// foreach ($options as $option) {
// 	var_dump($option);
// }

die(__FILE__);

$attributesInfo = Mage::getResourceModel('eav/entity_attribute_collection')
	->setEntityTypeFilter(4)
	// ->setCodeFilter('author_brands')
	// ->addSetInfo()
	// ->getData()
;

foreach ($attributesInfo as $attribute) {
	var_dump(get_class($attribute));
	var_dump($attribute->getAttributeCode());
	var_dump($attribute->usesSource());
	var_dump($attribute->getSourceModel());

	// var_dump(get_class_methods($attribute));

	// foreach ($attribute->getSource()->getAllOptions(false) as $option) {
	// 	var_dump($option);
	// }
}
