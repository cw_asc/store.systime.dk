<?php
require_once 'init.php';

header('Content-type: text/plain; charset: utf-8');

$stores = Mage::getModel('core/store')
	->getResourceCollection()
	->setLoadDefault(true)
	->load();

var_dump(array('stores' => count($stores), __METHOD__));

