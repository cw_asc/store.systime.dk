<?php
require_once 'init.php';

header('Content-type: text/plain; charset=utf-8');

$products = Mage::getModel('catalog/product')->getCollection()
	->addAttributeToSelect('options_container')
->load()
;

$info = array();

foreach ($products as $product) {
$container = $product->getOptionsContainer();
//echo $product->getSku(), "\t", $product->getOptionsContainer(), "\n";
if (!isset($info[$container])) {
$info[$container] = array();
}
$info[$container][] = $product->getSku();
}

print_r($info);
