<?php

require_once 'init.php';

header('Content-type: text/plain; charset=utf-8');

//Mage::log(__FILE__);
//Mage::helper('systimesso')->log(__FILE__);

Mage::logException(new Exception(__FILE__));

$orderId = 12935;
$order = Mage::getModel('sales/order')->load($orderId);

echo $order->getIncrementId();

$contents = $order->getSystimeekeysContents();
$info = @unserialize($contents);

//var_dump($info);

				if ($info) {
					$successes = isset($info['successes']) ? $info['successes'] : null;
					$failures = isset($info['failures']) ? $info['failures'] : null;
					$helper = Mage::helper('systimeekeys');

					$translate = Mage::getSingleton('core/translate');
					$translate->setTranslateInline(false);

					$mailTemplate = Mage::getModel('core/email_template');

//var_dump(array('mailTemplate' => $mailTemplate));

					$sendTo = array(
													array(
																'name'  => $order->getCustomerName(),
																'email' => $order->getCustomerEmail(),
																),
													);

					$bcc = $helper->getConfig('ekeys_email_bcc', $order->getStoreId());
					if ($bcc) {
						foreach (explode(',', $bcc) as $email) {
							$mailTemplate->addBcc($email);
						}
					}

					if ($successes) {
						foreach ($successes as $type => $data) {
							foreach ($sendTo as $recipient) {
								$variables = array(
																	 'order'  => $order,
																	 'ekeys'=> $data,
																	 );

								$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $order->getStoreId()))
									->sendTransactional($helper->getConfig('email_template_'.$type),
																			$helper->getConfig('email_identity', $order->getStoreId()),
																			$recipient['email'],
																			$recipient['name'],
																			$variables
																			);

var_dump(array( $mailTemplate->getId(), $mailTemplate->getSentSuccess(),



$helper->getConfig('email_template_'.$type),
$helper->getConfig('email_identity', $order->getStoreId()),
                                                                                                                                                        $recipient['email'],
                                                                                                                                                        $recipient['name'],
                                                                                                                                                        $variables

));

							}
						}
					}

					if ($failures) {
						foreach ($failures as $failure) {
							$failures_html = '';
							$failures_html .= '<dl>';
							$failures_html .= '<dt>'.htmlspecialchars(sprintf('%s (%s)', $failure->product->getName(), $failure->product->getSku())).'</dt>';
							$failures_html .= "\n";
							$failures_html .= '<dd>'.$failure->error.'</dd>';
							$failures_html .= "\n";
							$failures_html .= '</dl>';

							if ($bcc) {
								foreach (explode(',', $bcc) as $email) {
									$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $order->getStoreId()))
										->sendTransactional($helper->getConfig('email_failure_template'),
																				$helper->getConfig('email_identity', $order->getStoreId()),
																				$email,
																				$email,
																				array(
																							'order'  => $order,
																							'failures'=> $failures,
																							'failures_html'=> $failures_html,
																							)
																				);
								}
							}
						}
					}

					$translate->setTranslateInline(true);
				}
?>
