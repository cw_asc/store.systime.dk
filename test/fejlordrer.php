<?php
require_once 'init.php';

 // ini_set ('display_errors', 1);
 //    error_reporting (E_ALL | E_STRICT);

header('Content-type: text/plain; charset: utf-8');

define('TEACHER_LICENSE', 1);
define('SCHOOL_LICENSE', 2);

$minQtyOrdered = 20;
$minCreatedAt = strftime('%Y-%m-%d %H:%M:%S', strtotime('-12 months'));
$minCreatedAt = 0;

$products = Mage::getModel('catalog/product')
	->getCollection()
	->addAttributeToSelect(array('name'))
	->addFieldToFilter('licens_type', array(SCHOOL_LICENSE));

// echo $products->getSelect(), "\n";
// echo '#products: ', count($products), "\n";

$orderIds = array();

foreach ($products as $product) {
	$select = '*';
	$from = 'sales_flat_order_item';
	$where = 'product_id = '.intval($product->getId());

	$read = Mage::getSingleton('core/resource')->getConnection('core_read');
	$select = $read->select()
		->from('sales_flat_order_item', array('order_id', 'qty_ordered'))
		->where('product_id = ?', $product->getId())
		->where('qty_ordered < ?', $minQtyOrdered)
		->where('created_at > ?', $minCreatedAt)
		;

	// echo $select, "\n";

	$rows = $read->fetchAll($select);
	if (is_array($rows) && (count($rows) > 0)) {
		// print_r($product->toArray(array('entity_id', 'sku', 'licens_type')));
		// echo implode("\t", $product->toArray(array('sku', 'name', 'licens_type'))), "\n";

		foreach ($rows as $row) {
			$orderIds[] = $row['order_id'];
		}
	}
}

$orders = Mage::getModel('sales/order')
	->getCollection()
	// ->addAttributeToSelect(array('name'))
	->addFieldToFilter('entity_id', $orderIds)
	;

// echo $orders->getSelect();

// $orderIds = array_unique($orderIds);

echo '#orders: ', count($orders), "\n";

// print_r($orderIds);

foreach ($orders as $order) {
	echo $order->getIncrementId(), "\n";
}