<?php
require_once 'init.php';

header('Content-type: text/plain; charset=utf-8');

// echo Mage::helper('export')->getBaseDir()."/export/export.log";

Mage::helper('export')->errorlog(Mage::helper('export')->__('Observer: Export type not found.'));

$observer = Mage::getModel('export/observer');

echo get_class($observer);

$observer->cronjob();

