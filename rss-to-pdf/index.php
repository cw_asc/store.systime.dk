﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>RSS to PDF Newspaper | fivefilters.org</title>
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>

	<style>
	body {
		background: #fff url(images/background.png) no-repeat top left;
		font-family: sans-serif;
		margin-left: 3em;
		margin-right: 8em;
		font-size: 100%;
		line-height: 1.4em;
	}
	h1#title { margin-bottom: 0; }
	h1 a { border-bottom: none !important;}
	#content h1 { margin-left: .1em; }
	#menu { margin-left: 10px; }
	#menu a {
		background-color: #000;
		color: #fff;
		padding: 5px;
		margin-right: 1px;
		border-bottom: none;
	}
	a:link {
		color: #000;
		border-bottom: 1px dotted #888;
		text-decoration: none;
	}
	a:visited {
		color: #000;
		border-bottom: 1px dotted #888;
		text-decoration: none;
	}
	a:hover {
		color: #000;
		border-bottom: 1px solid #333;
	}
	h1#title a {
		background: transparent url(images/title.png) no-repeat top left;
		text-indent: -9999px;
		height: 70px;
		display: block;
	}
	p { line-height: 1.5em; }
	h2 { font-size: 1.5em; margin-bottom: .5em; margin-top: 1.5em; }
	h4 { margin-bottom: 0; font-weight: normal; }
	#pdf_form { margin-bottom: 3em; }
	#content { width: 700px; margin-left: 2em; margin-top: 2em; }
	#feed_label { margin-bottom: .3em; }
	label { display: block; margin: .2em; cursor:pointer; }
    #feed { font-size: 1.2em; padding: 5px; width: 503px; display: block; background-color: #C8E6A6; margin: 0; }
    #submit { 
		font-size: 1em;
		cursor:pointer; 
		background: #405927 url(images/icon_pdf.png) no-repeat scroll 5px center;
		padding: 0.5em .5em .5em 36px;
		font-weight: bold; 
		-moz-border-radius: 0 0 15px 15px; 
		width: 516px; 
		text-align: left; 
		color: #fff; 
		border: 0; 
		margin: 0;
		-webkit-border-top-left-radius: 0;
		-webkit-border-top-right-radius: 0;
		-webkit-border-bottom-left-radius: 15px;
		-webkit-border-bottom-right-radius: 15px;
	}
    /* #submit:hover { border: 1px solid #999; color: #fff; background-color: #666; } */
	#footer { font-size: .8em; margin-top: 2em; }
	table#compare { font-size: .8em; border-spacing: 1px; width: 850px; }
	#compare thead { background-color: #ddd; }
	#compare th, #compare td { padding: .5em; text-align: left; }
	img { border: 0; }
	.donate th { font-size: .8em; text-align: right; }
    .donate td, .donate th { padding-bottom: .8em; padding-right: .2em; }
	.donate-button {
		background-color: #eee;
		border: 1px solid #666;
		font-size: .8em;
		-moz-border-radius: .2em;
		padding: .2em;
		cursor: pointer;
	}
	.donate-button:hover {
		background-color: lightyellow;
		border: 1px solid red;
	}
	kbd {
		border-style: solid;
		border-width: 1px 2px 2px 1px;
		padding: 0 3px;
	}
	#advanced {
		width: 506px;
		padding: 2px 5px;
		margin: 0;
		border-top: 0;
		border-bottom: 0;
		background-color: #7AA61C;
	}
	#toggle-advanced {
		cursor: pointer;
		width: 506px;
		padding: 2px 5px;
		margin: 0;
		border-top: 0;
		border-bottom: 0;
		background-color: #7AA61C;
		dispdlay: none;
	}
	.alt-feeds { margin-top: .3em; }
	.alt-feeds a {
		background-color: #eee;
		padding: .2em;
	}
	</style>
	<script type="text/javascript">
	$(document).ready(function() {
		$('#advanced').hide();
		$('#toggle-advanced').click(function() {
			if (!$('#advanced').is(':visible')) {
				$('#toggle-advanced label').text('hide options');
				$('#advanced').slideToggle('fast');
			} else {
				$('#toggle-advanced label').text('show more options...');
				$('#advanced').slideToggle('fast', function() {
				});
			}
		});
	});
	</script>
  </head>
  <body>
	<div id="header">
		<h1 id="title"><a href="http://fivefilters.org">fivefilters.org</a></h1>
		<div id="menu">
		<a href="../">explore independent media</a><a href="../pdf-newspaper">pdf newspaper</a><a href="../content-only">full-text rss</a>
		</div>
	</div>
	<div id="content">
	<h1>RSS to PDF Newspaper</h1>
    <form method="GET" action="makepdf.php" id="pdf_form">
	<label id="feed_label" for="feed">Enter feed or OPML URL</label>
	<input type="text" id="feed" name="feed" />
	<div id="toggle-advanced" class="advanced-bottom"><label>show more options...</label></div>
	<div id="advanced">
		<label>Title: <input type="text" name="title" value="Your Personal Newspaper" style="width:300px; padding: 2px;" /></label>
		<label>Show stories in <select name="order"><option value="desc">descending</option><option value="asc">ascending</option></select> date order</label>
		<label>Include images (slow and experimental): <input type="checkbox" id="images" name="images" value="true" /></label>
	</div>
	<input type="submit" id="submit" name="submit" value="Create PDF" /> 
	<h4>Or select from one of the feeds below...</h4>
	<div class="alt-feeds">
		<a href="makepdf.php?title=UK+Indymedia&feed=<?php echo urlencode('http://indymedia.org.uk/en/features.rss'); ?>">UK Indymedia</a>
		<a href="makepdf.php?title=Mark+Curtis&feed=<?php echo urlencode('http://markcurtis.wordpress.com/'); ?>">Mark Curtis</a>
		<a href="makepdf.php?title=Schnews&feed=<?php echo urlencode('fivefilters.org/content-only/makefulltextfeed.php?url='.urlencode('http://www.schnews.org.uk/feed.xml')); ?>">Schnews</a>
		<a href="makepdf.php?title=Medialens&feed=<?php echo urlencode('fivefilters.org/content-only/makefulltextfeed.php?url='.urlencode('http://www.myantiwar.org/feeds/rss/channel_8.xml')); ?>">Medialens</a>
	</div>
	</form>
	<h2>About</h2>
	<p>This is a free software project to help people create printable PDFs from content found on the web. It is a <a href="http://www.gnu.org/philosophy/free-sw.html" title="free as in freedom">free</a> alternative to HP's <a href="http://www.tabbloid.com" rel="nofollow">Tabbloid</a> service. It is being developed as part of the <a href="http://fivefilters.org">Five Filters</a> project to promote alternative, non-corporate media.</p>
	
<!-- AddThis Button BEGIN -->
<script type="text/javascript">var addthis_pub="k1mk1m";</script>
<a style="border-bottom: none;" href="http://www.addthis.com/bookmark.php?v=20" onmouseover="return addthis_open(this, '', '[URL]', '[TITLE]')" onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s7.addthis.com/static/btn/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/200/addthis_widget.js"></script>
<!-- AddThis Button END -->

	<h2>Bookmarklet</h2>
	<p>To easily convert a webpage you're reading to PDF, drag the link below to your browser's bookmarks toolbar.
	Then whenever you'd like a PDF, click the bookmarklet.</p>
	<p>Drag this: <a href="javascript:location.href='http://fivefilters.org/pdf-newspaper/makepdf.php?title=Your+Personal+Newspaper&feed='+escape('http://fivefilters.org/content-only/makefulltextfeed.php?html=true&url='+escape(document.location.href));">Create PDF</a></p>
	
	<h2>Compare</h2>
	<table id="compare">
		<thead>
			<tr><th></th><th>FiveFilters.org</th><th>Tabbloid.com</th><th>FeedJournal.com</th><th>Feedbooks.com</th><th>Zinepal.com</th></tr>
		</thead>
		<tbody>
			<tr>
				<th>PDF library</th>
				<td style="background-color: #C8E6A6"><a href="http://www.tcpdf.org">TCPDF</a> (<a href="http://www.gnu.org/philosophy/free-sw.html">free</a>)</td>
				<td style="background-color: #f1a1a1"><a href="http://www.pdflib.com/" rel="nofollow">PDFLib</a> (not <a href="http://www.gnu.org/philosophy/free-sw.html">free</a>)</td>
				<td style="background-color: #C8E6A6"><a href="http://sourceforge.net/projects/itextsharp/">iTextSharp</a> (<a href="http://www.gnu.org/philosophy/free-sw.html">free</a>)</td>
				<td style="background-color: #f1a1a1"><a href="http://princexml.com" rel="nofollow">PrinceXML</a> (not <a href="http://www.gnu.org/philosophy/free-sw.html">free</a>)</td>
				<td style="background-color: #C8E6A6"><a href="http://xmlgraphics.apache.org/fop/">Apache FOP</a> (<a href="http://www.gnu.org/philosophy/free-sw.html">free</a>)</td>
			</tr>
			<tr>
				<th>Free software (<acronym title="free/libre/open source software">FLOSS</acronym>)</th>
				<td style="background-color: #C8E6A6">Yes (see below)</td>
				<td style="background-color: #f1a1a1">No</td>
				<td style="background-color: #f1a1a1">No</td>
				<td style="background-color: #f1a1a1">No</td>
				<td style="background-color: #f1a1a1">No</td>
			</tr>
			<tr>
				<th>Output #1: <a href="samples/lasthours/feed.xml" style="font-weight: normal;">Last Hours</th>
				<td><a href="samples/lasthours/fivefilters.pdf">PDF</a> (<a href="samples/lasthours/fivefilters_textonly.pdf">text only</a>)</td>
				<td><a href="samples/lasthours/tabbloid.pdf">PDF</a></td>
				<td><a href="samples/lasthours/feedjournal.pdf">PDF</a> (<a href="samples/lasthours/feedjournal_textonly.pdf">text only</a>)</td>
				<td><a href="samples/lasthours/feedbooks.pdf">PDF</a></td>
				<td><a href="samples/lasthours/zinepal.pdf">PDF</a></td>
			</tr>
			<tr>
				<th>Output #2: <a href="samples/crimethink/feed.xml" style="font-weight: normal;">CrimethInc.</th>
				<td><a href="samples/crimethink/fivefilters.pdf">PDF</a> (<a href="samples/crimethink/fivefilters_textonly.pdf">text only</a>)</td>
				<td><a href="samples/crimethink/tabbloid.pdf">PDF</a></td>
				<td><a href="samples/crimethink/feedjournal.pdf">PDF</a> (<a href="samples/crimethink/feedjournal_textonly.pdf">text only</a>)</td>
				<td><a href="samples/crimethink/feedbooks.pdf">PDF</a></td>
				<td><a href="samples/crimethink/zinepal.pdf">PDF</a></td>
			</tr>
			<tr>
				<th>Output #3: <a href="samples/ceasefire/feed.xml" style="font-weight: normal;">Ceasefire</th>
				<td><a href="samples/ceasefire/fivefilters.pdf">PDF</a> (<a href="samples/ceasefire/fivefilters_textonly.pdf">text only</a>)</td>
				<td><a href="samples/ceasefire/tabbloid.pdf">PDF</a></td>
				<td><a href="samples/ceasefire/feedjournal.pdf">PDF</a> (<a href="samples/ceasefire/feedjournal_textonly.pdf">text only</a>)</td>
				<td><a href="samples/ceasefire/feedbooks.pdf">PDF</a></td>
				<td><a href="samples/ceasefire/zinepal.pdf">PDF</a></td>
			</tr>
		</tbody>
	</table>
	<p>Note: Many of these services allow you to customize the output. The PDFs above were generated with the default settings in each app.</p>
	
	
	<h2>Source Code and Technologies</h2>
	<p><a href="https://code.launchpad.net/~keyvan-k1m/fivefilters/pdf-newspaper">Source code available</a>.<br />The application uses PHP, <a href="http://www.tcpdf.org">TCPDF</a>, <a href="http://tidy.sourceforge.net/">HTML Tidy</a>, <a href="http://htmlpurifier.org">HTML Purifier</a>, <a href="http://simplepie.org/">SimplePie</a>, <a href="http://michelf.com/projects/php-smartypants/">SmartyPants</a> and <a href="http://freshmeat.net/projects/opml-parser-class/">OPML Parser</a>.</p>

	<h2>Installation and System Requirements</h2>
	<p>This code should run on most hosts running PHP5 with Tidy enabled. I have it running on <a href="https://www.nearlyfreespeech.net/">NearlyFreeSpeech.NET</a> (a great host with a smart pricing model) but I've also tested it on Windows using <a href="http://www.wampserver.com/en/index.php">WampServer</a>. The instructions below will install the code in a folder called 'rss-to-pdf'. The instructions have been tested on a NearlyFreeSpeech account but should work on other hosts which offer shell access and have the <a href="http://bazaar-vcs.org/">Bazaar</a> client installed.</p>
	
	<ol>
		<li>Log in to your host using SSH</li>
		<li>Change to the directory where you want RSS to PDF Newspaper installed</li>
		<li>Enter <kbd>bzr branch lp:fivefilters rss-to-pdf</kbd><br />
			It should finish by saying something like <samp>Branched x revision(s)</samp></li>
		<li>Now enter <kbd>chmod 777 rss-to-pdf/cache/</kbd></li>
		<li>That's it! Try accessing the rss-to-pdf folder through your web browser, you should see the form asking for a feed URL.</li>
	</ol>
	
	<p>If you'd like to generate PDFs without going through the form first, you can simply pass the feed URL in the query string to makepdf.php. For example:<br /><tt>http://example.org/rss-to-pdf/makepdf.php?feed=http://schnews.org.uk/feed.xml</tt></p>
	
	<p>If you'd like to change the title image, replace images/five_filters.jpg with an image of your own (keeping the same filename).</p>
	
	<h2>Todo</h2>
	<ul>
	<li><del>Image support</del> (partial)</li>
	<li><del>Prevent headlines wrapping to next column/page</del></li>
	<li><del>Display date</del></li>
	<li><del>Custom title</del></li>
	<li>Compact, ink/toner saving template</li>
	<li>Display source</li>
	<li>Test support for other languages</li>
	<li>More testing</li>
	<li>Develop web app
		<ul>
			<li>multiple feeds (see tabbloid/feedbooks' newspaper feature)</li>
			<li>multiple output formats (for ebook use)</li>
		</ul></li>
	</ul>
	
	<h2>Support</h2>
	<p>I'm happy to help activists/anarchists/progressives set this up for their own content. I can either help you set it up on your own server, or create a customised look (e.g. different title image) and host it here. If you fall in this category, get in touch.</p>
	<p>If you don't fall in this category, I offer paid support.</p>
	<p><a href="https://bugs.launchpad.net/fivefilters">Bug reports</a> and <a href="https://answers.launchpad.net/fivefilters">questions</a> welcome.</p>
	
	<h2>License</h2>
	<p><a href="http://en.wikipedia.org/wiki/Affero_General_Public_License" style="border-bottom: none;"><img src="images/agplv3.png" /></a><br />This web application is licensed under the <a href="http://en.wikipedia.org/wiki/Affero_General_Public_License">AGPL version 3</a> (<a href="http://www.clipperz.com/users/marco/blog/2008/05/30/freedom_and_privacy_cloud_call_action">find out why</a>). The bulk of the work, however, is carried out by libraries which are licensed as follows...</p>
	<ul>
		<li>TCPDF: <a href="http://www.fsf.org/licensing/licenses/lgpl.html">LGPL</a></li>
		<li>HTML Tidy: <a href="http://tidy.sourceforge.net/#license">MIT-like</a></li>
		<li>HTML Purifier: <a href="http://www.fsf.org/licensing/licenses/lgpl.html">LGPL</a></li>
		<li>SimplePie: <a href="http://en.wikipedia.org/wiki/BSD_license">BSD</a></li>
		<li>SmartyPants: <a href="http://en.wikipedia.org/wiki/BSD_license">BSD</a></li>
		<li>OPML Parser: Freeware</li>
	</ul>
	
	<h2>Donate</h2>
	<p>I'm a student and I work on this in my spare time. Donations very welcome...</p>
	<table class="donate" style="margin-bottom: .5em;">
		<tr><th>£ (UK Pounds)</th>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="1.00">
<input type="hidden" name="currency_code" value="GBP">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate £1" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="2.00">
<input type="hidden" name="currency_code" value="GBP">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate £2" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="5.00">
<input type="hidden" name="currency_code" value="GBP">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate £5" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="currency_code" value="GBP">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate £?" border="0" name="submit" class="donate-button">
</form>
</td>
		</tr>
		<tr><th>€ (Euros)</th>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="1.00">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate €1" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="2.00">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate €2" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="5.00">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate €5" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate €?" border="0" name="submit" class="donate-button">
</form>
</td>
		</tr>
		<tr><th>$ (US Dollars)</th>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="1.00">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate $1" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="2.00">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate $2" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="amount" value="5.00">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate $5" border="0" name="submit" class="donate-button">
</form>
</td>
			<td><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="keyvan@k1m.com">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="RSS to PDF">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="cn" value="Comments?">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="submit" value="Donate $?" border="0" name="submit" class="donate-button">
</form>
</td>
		</tr>
	</table>
	
	<h2>Author</h2>
	<p>Created by <a href="http://www.keyvan.net">Keyvan Minoukadeh</a> for the <a href="http://fivefilters.org">Five Filters</a> project.<br />
	Email: keyvan (at) keyvan.net</p>
	
	</div>
  </body>
</html>
