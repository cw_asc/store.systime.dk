<?php
/**
 * Meat Media AS 
 * Dibs Payment Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Meatmedia
 * @package    Meatmedia_Dibs
 * @author     Meat Media AS
 * @author     Chris Magnussen
 * @copyright  Copyright (c) 2008 Meat Media AS. (http://www.meatmedia.no)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * DIBS Payment Block
 *
 * @category   Meatmedia
 * @package    Meatmedia_Dibs
 */
class Meatmedia_Dibs_Model_Dibs extends Mage_Payment_Model_Method_Abstract
{
    const CGI_URL = 'https://payment.architrade.com/payment/start.pml';
    const CGI_URL_TEST = 'https://payment.architrade.com/payment/start.pml';
    const REQUEST_AMOUNT_EDITABLE = 'N';

    protected $_code  = 'dibs';
    protected $_formBlockType = 'meatmedia_dibs_block_form';
    protected $_allowCurrencyCode = array('EUR', 'USD', 'GBP', 'SEK', 'DKK', 'NOK', 'AUD', 'CAD', 'ISK', 'JPY', 'NZD', 'CHF', 'TRY');
    
    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Fontis_Australia_Model_Payment_Paymate
     */
    public function assignData($data)
    {
        $details = array();
        if ($this->getUsername())
        {
            $details['username'] = $this->getUsername();
        }
        if (!empty($details)) 
        {
            $this->getInfoInstance()->setAdditionalData(serialize($details));
        }
        return $this;
    }
    
    public function isTest()
	{
	    $conn = Mage::getModel('Core/Mysql4_Config')->getReadConnection();
		$result = $conn->fetchAll("SELECT value FROM core_config_data WHERE `path`='payment/dibs/api_test'");
		
		$isTest = $result[0];
		
		return $isTest;	
	}

    public function getUsername()
    {
        return $this->getConfigData('username');
    }
    
    protected function getFailureURL ()
    {
        $url = $this->getConfigData('url_failure');
    	
    	return $url;
    }
    
    protected function getAcceptURL ()
    {
        $url = $this->getConfigData('url_accept');
    	
    	return $url;
    }
    
    protected function getCallbackURL ()
    {
        $url = $this->getConfigData('url_callback');
    	
    	return $url;
    }
    
    public function getUrl()
    {
    	$url = $this->getConfigData('cgi_url');
    	
    	if(!$url)
    	{
    		$url = self::CGI_URL_TEST;
    	}
    	
    	return $url;
    }
    
    public function convertToDibsCurrency($curr)
    {
      switch ($curr)
      {
        case "DKK": return "208";
        case "EUR": return "978";
        case "USD": return "840";
        case "GBP": return "826";
        case "SEK": return "752";
        case "AUD": return "036";
        case "CAD": return "124";
        case "ISK": return "352";
        case "JPY": return "392";
        case "NZD": return "554";
        case "NOK": return "578";
        case "CHF": return "756";
        case "TRY": return "949";
      }
      
      return "";
    }
    
    /**
     * Get session namespace
     *
     * @return Fontis_Australia_Model_Payment_Paymate_Session
     */
    public function getSession()
    {
        return Mage::getSingleton('dibs/dibs_session');
    }

    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }
    
	public function getCheckoutFormFields()
	{
		$a = $this->getQuote()->getShippingAddress();
		$b = $this->getQuote()->getBillingAddress();
		$currency_code = $this->getQuote()->getBaseCurrencyCode();
		$cost = $a->getBaseSubtotal() - $a->getBaseDiscountAmount();
		$shipping = $a->getBaseShippingAmount();
		$language = $b->getCountry();
		$order = Mage::getModel('sales/order');
        $order->loadByIncrementId($this->getCheckout()->getLastRealOrderId());
        $convertor = Mage::getModel('sales/convert_order');
        $invoice = $convertor->toInvoice($order); 
        $check_test = $this->isTest();
        if ($check_test = 1) {
        	$check_test = true;
        } else {
        	$check_test = false;
        }
		
		$fields = array(
			'amount'					=> $order->getTotalDue() * 100,
			'currency'					=> $this->convertToDibsCurrency($order->getOrderCurrency()),
			'lang'						=> $b->getCountry(),
			'delivery1.Navn'			=> $b->getFirstname() ." ". $b->getLastname(),
			'delivery2.Adresse'			=> $b->getStreet(1),
			'priceinfo1.shippingcosts'	=> $shipping,
			'ordline0-1'				=> $this->getQuote()->getProductId(),
			'merchant'					=> $this->getConfigData('account_number'),
			'accepturl'         		=> Mage::getUrl('dibs/dibs/success'),
			'cancelurl'					=> Mage::getUrl($this->getFailureURL()),
			'callbackurl'      			=> Mage::getUrl('dibs/dibs/callback'),
			'orderid'					=> $order->getRealOrderId(),
			'test'						=> $check_test,
			
		);

		$filtered_fields = array();
        foreach ($fields as $k=>$v) {
            $value = str_replace("&","and",$v);
            $filtered_fields[$k] =  $value;
        }
        
        return $filtered_fields;
	}

    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('dibs/dibs_form', $name)
            ->setMethod('dibs')
            ->setPayment($this->getPayment())
            ->setTemplate('meatmedia/dibs/form.phtml');

        return $block;
    }

    public function validate()
    {
        parent::validate();
        $currency_code = $this->getQuote()->getBaseCurrencyCode();
        if (!in_array($currency_code,$this->_allowCurrencyCode)) {
            Mage::throwException(Mage::helper('dibs')->__('Selected currency code ('.$currency_code.') is not compatabile with Dibs'));
        }
        return $this;
    }

    public function onOrderValidate(Mage_Sales_Model_Order_Payment $payment)
    {
       return $this;
    }

    public function onInvoiceCreate(Mage_Sales_Model_Invoice_Payment $payment)
    {

    }

    public function canCapture()
    {
        return true;
    }

    public function getOrderPlaceRedirectUrl()
    {
          return Mage::getUrl('dibs/dibs/redirect');
    }
}
