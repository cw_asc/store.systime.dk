<?php
class MailChimp_Ecomm360_Model_Sale_Observer{

    var $system = "magento";
    var $version = "1.0";
    
    var $debug = true;

    var $valid_files = false;
    var $valid_cfg = false;
    var $install_dir = '';
    var $cfg_file = 'mc360.ini.php';
    var $apikey = '';
    var $key_valid = false;
    var $store_id = '';


    public function __construct(){

        if ($this->debug){
            Mage::log(date("Y-m-d H:i:s").": construct...");
        }
        
        $this->install_dir = dirname(__FILE__).'/../../';
        require_once($this->install_dir.'../class.iniparser.php');
        require_once($this->install_dir.'../MCAPI.class.php');

        $this->cfg_file = $this->install_dir.$this->cfg_file;
        
        $this->validate_files();
        if ($this->valid_files){
            $cfg = new iniParser($this->cfg_file);
            $this->apikey = $cfg->get("user_settings","apikey");
            $this->store_id = $cfg->get("do_not_edit","store_id");
            $this->key_valid = $cfg->get("do_not_edit","key_valid");
        }
        $this->validate_cfg();
    
    }
    
    function startup(){
        //runs on every page load.
        $thirty_days = time()+60*60*24*30;
        if (isset($_REQUEST['mc_cid'])){
            setcookie('mailchimp_campaign_id',trim($_REQUEST['mc_cid']), $thirty_days);
        }
        if (isset($_REQUEST['mc_eid'])){
            setcookie('mailchimp_email_id',trim($_REQUEST['mc_eid']), $thirty_days);
        }
        if ($this->debug){
            Mage::log( '[startup] current ids:');
            if (isset($_COOKIE['mailchimp_email_id']))
                Mage::log( ' eid ='.$_COOKIE['mailchimp_email_id'] );
            if (isset($_COOKIE['mailchimp_campaign_id']))
                Mage::log( ' cid ='.$_COOKIE['mailchimp_campaign_id'] );
        }
        return;
    }
    
    function complain($msg){
            echo '<div style="position:absolute;left:0;top:0;width:100%;font-size:24px;text-align:center;background:#CCCCCC;color:#660000;z-index:100;">MC360 Module: '.$msg.'</div><br/>';
    }
    function validate_files(){
        $this->valid_files = false;
        if (!file_exists($this->cfg_file)){
            $this->complain('<strong>'.$this->cfg_file.'</strong> must EXIST and be writable');
        }elseif (!is_writable($this->cfg_file)){
            $this->complain('<strong>'.$this->cfg_file.'</strong> must BE WRITABLE');
        } else {
            $this->valid_files = true;
        }
    }
    
    function validate_cfg(){
        $this->valid_cfg = false;
        if (!$this->valid_files){
            return;
        }elseif (!$this->apikey){
            $this->complain('You have not entered your API key. Please read the installation instructions.');
            return;
        }
        
        if (!$this->key_valid){
            $GLOBALS["mc_api_key"] = $this->apikey;
            $api = new MCAPI('notused','notused');
            $res = $api->ping();
            if ($api->errorMessage!=''){
                $this->complain('Server said: "'.$api->errorMessage.'". Your API key is likely invalid. Please read the installation instructions.');
                return;
            } else {
                $cfg = new iniParser($this->cfg_file);
                $this->key_valid = true;
                $cfg->setValue("do_not_edit","key_valid", $this->key_valid);
                
                if (!$this->store_id){
                    $this->store_id = md5(uniqid(rand(), true));
                    $cfg->setValue("do_not_edit","store_id",$this->store_id);   
                }
                $cfg->save();
            }
        }
        
        if (!$this->store_id){
            $this->complain('Your Store ID has not been set. This is not good. Contact support.');
        } else {
            $this->valid_cfg = true;
        }
    }


    /**
    * Applies the special price percentage discount
    * @param   Varien_Event_Observer $observer
    * @return  nothing
    */
    public function logsale($observer)
    {
        if (!$this->valid_cfg){
            return;
        }

        $event = $observer->getEvent();
        
        $order = $event->getOrder();

        if ($this->debug){
            Mage::log( '[logsale] current ids:');
            Mage::log( ' eid ='.$_COOKIE['mailchimp_email_id'] );
            Mage::log( ' cid ='.$_COOKIE['mailchimp_campaign_id'] );
        }
        
        if (!$_COOKIE['mailchimp_campaign_id'] || !$_COOKIE['mailchimp_email_id']){
            return;
        }

        $mcorder = array(
                'id' => $order->getRealOrderId(),
                'total'=>$order->getSubtotal(),
                'shipping'=>$order->getShippingAmount(),
                'tax'  =>$order->getTaxAmount(),
                'items'=>array(),
                'store_id'=>$this->store_id,
                'store_name' => $_SERVER['SERVER_NAME'],
                'campaign_id'=>$_COOKIE['mailchimp_campaign_id'],
                'email_id'=>$_COOKIE['mailchimp_email_id'],
                'plugin_id'=>1215
                );
        $prod_ids = array();
        foreach ($order->getAllItems() as $item) {
            
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
//                $category = Mage::getModel('catalog/category')->load($product->getCategoryId());

            $mcitem = array();
            $mcitem['product_id'] = $product->getId();
            if (!in_array($product->getId(), $prod_ids) || $item->getPrice()>0){
                $prod_ids[] = $product->getId();
                $mcitem['product_name'] = $product->getName();
                $mcitem['sku'] = $product->getSku();
                $mcitem['qty'] = $item->getQtyOrdered();
                $mcitem['cost'] = $item->getPrice();

                $i = 0;
                $names = array();
                $cat_ids = $product->getCategoryIds();
                if ($cat_ids[0]){
                    $category = Mage::getModel('catalog/category')->load($cat_ids[0]);
                    $mcitem['category_id'] = $cat_ids[0];
                    $names[] = $category->getName();
                    while ($category->getParentId() && $category->getParentId()!=0){
                        $category = Mage::getModel('catalog/category')->load($category->getParentId());
                        $names[] = $category->getName();
                        Mage::log( 'catid = '.$category->getId().'  | name = '.$category->getName());
                    }                
                }
                //this is h-a-c-k-y
                $names = array_reverse($names);
                if (sizeof($names)<=2){
                    $mcitem['category_name'] = 'None';
                } else {
                    //get rid of the 1st 2...
                    array_shift($names);
                    array_shift($names);
                    $mcitem['category_name'] = implode(" - ",$names);
                }
                    
                $mcorder['items'][] = $mcitem;

                if ($this->debug){
                    //Mage::log( ' cat info:');
                    //Mage::log( ' cat id ='.print_r($product->getCategoryCollection(), true));
                    //Mage::log( ' cat name ='.print_r($category,true));
                }
            }
         }//foreach
        
        $GLOBALS["mc_api_key"] = $this->apikey;
        $api = new MCAPI('notused','notused');
        $res = $api->campaignEcommAddOrder($mcorder);
        if ($api->errorMessage!=''){
            Mage::log( '[MC360]:ERROR: ' . $api->errorMessage );
            //it failed... but what to do? nothing?
        } else {
            Mage::log( '[MC360]:SUCCESS: Submitted Order #'.$order->getRealOrderId());
            //nothing
        }
        
    }//logsale()
    
}//class MailChimp_Ecomm360_Model_Sale_Observer
