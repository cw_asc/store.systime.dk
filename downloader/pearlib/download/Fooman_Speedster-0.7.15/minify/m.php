<?php

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);
define('BP', dirname(dirname(dirname(__FILE__))));

// Prepends include_path. You could alternately do this via .htaccess or php.ini
set_include_path( BP.DS.'lib'.DS.'minify'.DS.'lib'. PS . get_include_path());

/**
 * Set $minifyCachePath to a PHP-writeable path to enable server-side caching
 */
$minifyCachePath = BP.DS.'var'.DS.'minifycache';

/**
 * The Files controller only "knows" CSS, and JS files. 
 */
$serveExtensions = array('css', 'js');

// serve
if (isset($_GET['f'])) {
    $filenames =  explode(",", $_GET['f']) ;
    $filenamePattern = '/[^\'"\\/\\\\]+\\.(?:' 
        .implode('|', $serveExtensions).   ')$/';

    require 'Minify.php';

    if ($minifyCachePath) {
        Minify::setCache($minifyCachePath);
    }

    if (0 === stripos(PHP_OS, 'win')) {
        Minify::setDocRoot(); // we may be on IIS
    }

    $servefiles = array();
    foreach ($filenames as $filename) {
            if (preg_match($filenamePattern, $filename)
                && file_exists(BP .  $filename)) {
                //Minify can't handle the regex for the email addresses - change to file with less demanding regex for email validation
                $filename = str_replace("validation.js", "validation-4min.js", $filename);
                $servefiles[]=BP . $filename;
            }
    }

    $serveOptions = array(
         'files' => $servefiles
        ,'maxAge' => 31536000 // now + 1 yr
    );
    //var_dump($serveOptions);
    Minify::serve('Files', $serveOptions);

    exit();

}

header("HTTP/1.0 404 Not Found");
echo "HTTP/1.0 404 Not Found";
