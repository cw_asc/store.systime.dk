<?php

/**
 * Kit Hy
 * Enhanced Search module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   hyteckit 
 * @package    hyteckit_EnhancedSearch
 * @copyright  Copyright (c) 2008 Kit Hy
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class hyteckit_EnhancedSearch_Model_Mysql4_Search_Collection
    extends Mage_CatalogSearch_Model_Mysql4_Search_Collection
{
    protected $_attributesCollection;
    protected $_searchQuery;

 	protected function search_split_terms($terms)
	{
		$exclude = array('for','and');

		$terms = preg_replace("/\"(.*?)\"/e", "\$this->search_transform_term('\$1')", $terms);
		$terms = preg_split("/\s+|,/", $terms);

		$out = array();

		foreach($terms as $term)
		{

			$term = preg_replace("/\{WHITESPACE-([0-9]+)\}/e", "chr(\$1)", $term);
			$term = preg_replace("/\{COMMA\}/", ",", $term);

			if (in_array($term, $exclude)) { continue; }

			
			// SYNONIM ENHANCEMENT
			#echo "TERM: $term\n";
            $enhanced_search = Mage::getModel("catalogsearch/query")
                ->load($term)
                ->setQueryText($term);
			$synonim = $enhanced_search->getSynonimFor();
			$term = $synonim?$synonim:$term;
			#echo "SYNONIM: $term\n";
			
			$out[] = $term;
		}

		return $out;
	}
	
	protected function search_db_escape_terms($terms)
	{
		$out = array();
		foreach($terms as $term){
			$out[] = '[[:<:]]'.AddSlashes($this->search_escape_rlike($term)).'[[:>:]]';
		}
		return $out;
	}

	protected function search_transform_term($term)
	{
		$term = preg_replace("/(\s)/e", "'{WHITESPACE-'.ord('\$1').'}'", $term);
		$term = preg_replace("/,/", "{COMMA}", $term);
		return $term;
	}

	protected function search_escape_rlike($string)
	{
		return preg_replace("/([.\[\]*^\$\(\)])/", '\\\$1', $string);
	}

    /**
     * Add search query filter
     *
     * @param   string $query
     * @return  Mage_CatalogSearch_Model_Mysql4_Search_Collection
     */
    public function addSearchFilter($query)
    {
		#echo "ENHANCED: $query";
        $this->_searchQuery = '%'.$query.'%';
        $this->addFieldToFilter('entity_id', array('in'=>new Zend_Db_Expr($this->_getSearchEntityIdsSql($query))));
        return $this;
    }

    /**
     * Retrieve collection of all attributes
     *
     * @return Varien_Data_Collection_Db
     */
    protected function _getAttributesCollection()
    {
        if (!$this->_attributesCollection) {
            $this->_attributesCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($this->getEntity()->getTypeId())
                ->load();

            foreach ($this->_attributesCollection as $attribute) {
                $attribute->setEntity($this->getEntity());
            }
        }
        return $this->_attributesCollection;
    }

    protected function _isAttributeTextAndSearchable($attribute)
    {
        if (($attribute->getIsSearchable() && $attribute->getFrontendInput() != 'select')
            && (in_array($attribute->getBackendType(), array('varchar', 'text')) || $attribute->getBackendType() == 'static')) {
            return true;
        }
        return false;
    }

    protected function _hasAttributeOptionsAndSearchable($attribute)
    {
        if ($attribute->getIsSearchable() && $attribute->getFrontendInput() == 'select') {
            return true;
        }

        return false;
    }

    protected function _getSearchEntityIdsSql($query)
    {
        $tables = array();
        $selects = array();
        /**
         * Collect tables and attribute ids of attributes with string values
         */
        //echo "<pre>";
		$terms = $this->search_split_terms($query);
		#print_r($terms);
		$terms_db = $this->search_db_escape_terms($terms);
		#print_r($terms_db);
		$parts = array();
		foreach($terms_db as $term_db){
			$parts[] = "IFNULL(t2.value, t1.value) RLIKE '$term_db'";
		}
		$parts = implode(' AND ', $parts);	
		#echo $parts;
		#$query = implode(' ', $terms_db);
        foreach ($this->_getAttributesCollection() as $attribute) {
            if ($this->_isAttributeTextAndSearchable($attribute)) {
                #echo $attribute->getAttributeCode()."\n";
                $table = $attribute->getBackend()->getTable();
                if (!isset($tables[$table]) && $attribute->getBackendType() != 'static') {
                    $tables[$table] = array();
                }

                if ($attribute->getBackendType() == 'static') {
                    $param = $attribute->getAttributeCode().'_search_query';
                    $selects[] = $this->getConnection()->select()
                        ->from($table, 'entity_id')
                        ->where($attribute->getAttributeCode().' LIKE :'.$param);
                    $this->addBindParam($param, $this->_searchQuery);
                } else {
                    $tables[$table][] = $attribute->getId();
                }
            }
        }

        foreach ($tables as $table => $attributeIds) {
            $param = $table.'_search_query';
			#echo "$param\n";
            $selects[] = $this->getConnection()->select()
                ->from(array('t1' => $table), 'entity_id')
                ->joinLeft(
                    array('t2' => $table),
                    $this->getConnection()->quoteInto('t1.entity_id = t2.entity_id AND t1.attribute_id = t2.attribute_id AND t2.store_id=?', $this->getStoreId()),
                    array()
                )
                ->where('t1.attribute_id IN (?)', $attributeIds)
                ->where('t1.store_id = ?', 0)
                ->where($parts);
                #->where('IFNULL(t2.value, t1.value) LIKE :'.$param);
                #$this->addBindParam($param, $this->_searchQuery);
        }

        if ($sql = $this->_getSearchInOptionSql($query)) {
            $selects[] = $sql;
        }
        //die(print_r($selects));
        $sql = implode(' UNION ', $selects);
		#echo "$sql\n";
        return $sql;
    }

    /**
     * Retrieve SQL for search entities by option
     *
     * @param unknown_type $query
     * @return unknown
     */
    protected function _getSearchInOptionSql($query)
    {
        $attributeIds = array();
        $table = '';

        /**
         * Collect attributes with options
         */
        foreach ($this->_getAttributesCollection() as $attribute) {
            if ($this->_hasAttributeOptionsAndSearchable($attribute)) {
                $table = $attribute->getBackend()->getTable();
                $attributeIds[] = $attribute->getId();
            }
        }
        if (empty($attributeIds)) {
            return false;
        }

        $optionTable = Mage::getSingleton('core/resource')->getTableName('eav/attribute_option');
        $optionValueTable = Mage::getSingleton('core/resource')->getTableName('eav/attribute_option_value');

        /**
         * Select option Ids
         */
        $select = $this->getConnection()->select()
            ->from(array('default'=>$optionValueTable), array('option_id','option.attribute_id', 'store_id'=>'IFNULL(store.store_id, default.store_id)'))
            ->joinLeft(array('store'=>$optionValueTable),
                $this->getConnection()->quoteInto('store.option_id=default.option_id AND store.store_id=?', $this->getStoreId()),
                array())
            ->join(array('option'=>$optionTable),
                'option.option_id=default.option_id',
                array())
            ->where('default.store_id=0')
            ->where('option.attribute_id IN (?)', $attributeIds);

        $searchCondition = 'IFNULL(store.value, default.value) LIKE :search_query';
        $select->where($searchCondition);

        $options = $this->getConnection()->fetchAll($select, array('search_query'=>$this->_searchQuery));

        if (empty($options)) {
            return false;
        }

        $cond = array();
        foreach ($options as $option) {
            $cond[] = "attribute_id = '{$option['attribute_id']}' AND value = '{$option['option_id']}' AND store_id = '{$option['store_id']}'";
        }

        return $this->getConnection()->select()
            ->from($table, 'entity_id')
//            ->where('store_id=?', $this->getStoreId())
            ->where(implode(' OR ', $cond));
    }
}