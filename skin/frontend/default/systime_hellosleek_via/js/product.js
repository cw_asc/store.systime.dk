var SystimeProductConfig = Class.create(Product.Config, {
	initialize: function($super, config) {
		$super(config);
		this.minimalPriceExcTax = $('price-excluding-tax-'+this.config.productId).innerHTML;
		this.minimalPriceIncTax = $('price-including-tax-'+this.config.productId).innerHTML;
	},
	
	// overridden to not change any dropdown labels, and to hide/show prices
	reloadOptionLabels: function(element){
		if (element.selectedIndex==0) {
			$$('.product-options-bottom .price-box').first().setStyle({display: 'none'});
		} else {
			$$('.product-options-bottom .price-box').first().setStyle({display: 'block'});
		}
    },

	getAllowPriceViewChange: function() {
		return this.allowPriceViewChange;
	},

	// overridden to remove signs on prices
	getOptionLabel: function($super, option, price) {
		var price = parseFloat(price);
        if (this.taxConfig.includeTax) {
            var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
            var excl = price - tax;
            var incl = excl*(1+(this.taxConfig.currentTax/100));
        } else {
            var tax = price * (this.taxConfig.currentTax / 100);
            var excl = price;
            var incl = excl + tax;
        }

        if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
            price = incl;
        } else {
            price = excl;
        }

        var str = option.label;
        if(price){
            if (this.taxConfig.showBothPrices) {
                str+= ' ' + this.formatPrice(excl, false) + ' (' + this.formatPrice(price, false) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str+= ' ' + this.formatPrice(price, false);
            }
        }
        return str;
	}
});