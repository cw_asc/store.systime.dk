if (typeof(systime) == "undefined") {
	systime = {};
}

(function() {
	var debug = function() {
		if ((typeof(console) != "undefined") && (typeof(console.debug) == "function")) {
			console.debug.apply(console, arguments);
		}
	},

	today, yesterday,

	zeroPad = function(value, length) {
		value = value.toString();
		while (value.length < length) {
			value = '0'+value;
		}
		return value;
	},

	formatPostDate = function(pubDate) {
		if (!today) {
			today = new Date();
			today.setHours(0);
			today.setMinutes(0);
			today.setSeconds(0);

			yesterday = new Date(today.getTime()-24*60*60*1000);
		}

		if (typeof(pubDate) == "string") {
			pubDate = new Date(pubDate);
		}

		if (today.getTime() < pubDate.getTime()) {
			return [ zeroPad(pubDate.getHours(), 2), zeroPad(pubDate.getMinutes(), 2) ].join(':');
 		} else if (yesterday.getTime() < pubDate.getTime()) {
			return "I g&aring;r";
			// pubDate = YAHOO.util.Date.format(pubDate, { format: "%T"}, "da");
		} else {
			return [ zeroPad(pubDate.getDate(), 2), zeroPad(pubDate.getMonth()+1, 2) ].join('/');
		}
	},

	getExcerpt = function(text, length) {
		if (length < text.length) {
			while ((length > 0) && !/\s/.test(text.charAt(length))) {
				length--;
			}
			if (length > 0) {
				text = text.substring(0, length)+" &#x2026;";
			}
		}
		return text;
	}

	renderItems = function(el, items, config) {
		var i,
			itemsTemplate = new Template("<ol class='items'>#{items}<\/ol>"),
			itemTemplate = new Template([
				"<li class='item'>",
				"<a href='#{link}'>",
				"<span class='time'>#{pubDate}<\/span>",

				" ", "<span class='subjects'>#{lab-subjects}<\/span>", " ",
				// " ", "<span class='mediatype'>#{lab-mediatype}<\/span>", " ",

				"<span class='title'>#{title}<\/span>",
				" ",
				"<span class='description'>#{excerpt}<\/span>",
				"<\/a>",
				"<\/li>"
			].join("")),
			itemsContent = "";
		if (el) {
			el.innerHTML = "";
			if (items) {
				for (i = 0; i < items.length; i++) {
					var item = items[i];
					item['pubDate'] = formatPostDate(item.pubDate);
					item['lab-subjects'] = item['lab-subjects'] ? '('+item['lab-subjects']+')' : '';
					item.excerpt = getExcerpt(item.description, config.excerptLength);
					itemsContent += itemTemplate.evaluate(item);
				}
				el.innerHTML = itemsTemplate.evaluate({ items: itemsContent });
			}
		}
	},

	systime.LabFeed = function(el, config) {
		var el = $(el);
		if (el) {
			if (!config.excerptLength) {
				config.excerptLength = 60;
			}

			if (config.proxyUrl) {
				// Systimes server redirect setup is f... up! Thus, we cannot rely on certain server variables ...
				var match = new RegExp("([a-z]+://)([^.]+\.)([^.]+\.systime\.dk/)").exec(document.location);
				if (match) {
					config.proxyUrl = config.proxyUrl.replace(match[1]+match[3], match[0]);
				}
			}

			try {
				new Ajax.Request(config.proxyUrl+"?url="+config.url, {
					method: "get",
					onSuccess: function(transport) {
						try {
						// ;;; debug("transport", transport);
							var i,
								item,
								items = [],
								maxPostCount = config.numberOfPosts ? parseInt(config.numberOfPosts) : 10,
								nodes, node, xml;// = transport.responseXML;

							if (!xml) {
								if (window.DOMParser) {
									xml = (new DOMParser()).parseFromString(transport.responseText, "text/xml");
								} else { // Internet Explorer
									xml = new ActiveXObject("Microsoft.XMLDOM");
									xml.async = false;
									xml.loadXML(transport.responseText);
								}
							}

							try {
								nodes = xml.getElementsByTagName("item");
							} catch (ex) {}

							if (nodes) {
								for (i = 0; i < nodes.length; i++) {
									item = {};
									node = nodes[i].firstChild;
									while (node) {
										//ELEMENT_NODE
										while (node && (node.nodeType != 1)) {
											node = node.nextSibling;
										}
										if (node) {
											item[node.nodeName] = node.firstChild.nodeValue;
											node = node.nextSibling;
										}
									}
									if (items.length < maxPostCount) {
										items.push(item);
									} else {
										break;
									}

								}
							}

							// ;;; debug("items", items);

							renderItems(el, items, config);
						} catch (ex) {
							;;; debug("ex", ex);
						}
					}
				});

			} catch(ex) {
				;;; debug(ex);
			}
		}
	}
}());
