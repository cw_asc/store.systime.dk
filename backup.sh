#!/usr/bin/env bash
filename=app-$(hostname)-$(date +%Y%m%d%H%M).tgz
tar zcf $filename app/
echo 'Done ('$filename')'
