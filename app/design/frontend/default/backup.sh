#!/usr/bin/env bash
filename=systime_hellosleek-$(hostname)-$(date +%Y%m%d%H%M).tgz
tar zcf $filename systime_hellosleek/
echo 'Done ('$filename')'
