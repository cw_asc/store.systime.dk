<?php
$_salesHelper = Mage::helper('systimesales');
$_product = $_item->getProduct();
if ($_releaseDateInfo = $_salesHelper->getReleaseDateInfo($_product)):
?>
<div class="release-date">
	 <?php echo $_salesHelper->__($_product->getAttributeText('publication_status')) ?>,
	 <?php echo $_salesHelper->__($_releaseDateInfo['label']) ?>: <?php echo $_releaseDateInfo['value'] ?>
	 </div>
<?php endif ?>
	 <?php if ($_product->isSaleable() && $_salesHelper->isSchoolOnlyProduct($_product, $_item)) {
		if ($_releaseDateInfo) {
			$_blockId = $_salesHelper->getConfig('school_only_items_block_catalog', null, 'checkout');
		} else {
			$_blockId = $_salesHelper->getConfig('school_only_items_block_cart_item', null, 'checkout');
		}
		echo Mage::helper('systimemisc')->renderBlock($_blockId, 'systime-school-only-item shopping-cart-item-message');
} ?>
