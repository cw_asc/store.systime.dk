<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Rss
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Review form block
 *
 * @category   Mage
 * @package    Mage_Rss
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Rss_Block_Catalog_Category extends Mage_Rss_Block_Abstract
{
    protected function _construct()
    {
        /*
        * setting cache to save the rss for 10 minutes
        */
        $this->setCacheKey('rss_catalog_category_'
            .$this->getRequest()->getParam('cid').'_'
            .$this->getRequest()->getParam('store_id')
        );
        $this->setCacheLifetime(600);
    }

    protected function _toHtml()
    {
	$_salesHelper = Mage::helper('systimesales');

	$categoryId = $this->getRequest()->getParam('cid');
        $storeId = $this->_getStoreId();
        $rssObj = Mage::getModel('rss/rss');
        if ($categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            if ($category && $category->getId()) {
                $layer = Mage::getSingleton('catalog/layer')->setStore($storeId);
                //want to load all products no matter anchor or not
                $category->setIsAnchor(true);
                $newurl = $category->getUrl();
                $title = $category->getName();
                $data = array('title' => $title,
                        'description' => $title,
                        'link'        => $newurl,
                        'charset'     => 'UTF-8',
                        );
//echo "<pre>";
                $rssObj->_addHeader($data);

                $_collection = $category->getCollection();
                $_collection->addAttributeToSelect('url_key')
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('is_anchor')
                    ->addAttributeToFilter('is_active',1)
                    ->addIdFilter($category->getChildren())
                    ->load()
                ;
                $productCollection = Mage::getModel('catalog/product')->getCollection();

                $currentyCateogry = $layer->setCurrentCategory($category);
                $layer->prepareProductCollection($productCollection);
                $productCollection->addCountToCategories($_collection);

                /*if ($_collection->count()) {
                    foreach ($_collection as $_category){
                         $data = array(
                                    'title'         => $_category->getName(),
                                    'link'          => $_category->getCategoryUrl(),
                                    'description'   => $this->helper('rss')->__('Total Products: %s', $_category->getProductCount()),
                                    );

                        $rssObj->_addEntry($data);
                    }
                }
                */
                $category->getProductCollection()->setStoreId($storeId);
                /*
                only load latest 50 products
                */
                $_productCollection = $currentyCateogry
                    ->getProductCollection()
					          ->addAttributeToSort('name', 'asc')
					          ->addAttributeToSelect('isbn_13')
					          ->addAttributeToSelect('authors')
					          ->addAttributeToSelect('edition')
					          ->addAttributeToSelect('publication_year')
						  ->addAttributeToSelect('updated_at')
						//  ->addAttributeToSort('updated_at','desc')                    
                    ->setCurPage(1)
                ;

                #->addAttributeToSort('updated_at','desc')

		if ($_productCollection->getSize()>0) {
			foreach ($_productCollection as $_product) {

#		    if ($_product->isSaleable()) {
  				unset($prices);
  				if ($_product->isConfigurable()) {
  					$childProducts = $_product->getTypeInstance(true)->getUsedProducts(null, $_product);
  					if ($childProducts) {
  						$prices = '<div>';
  						foreach ($childProducts as $child) {

  								$prices .= $child->getAttributeText('licens_type') . ': ' . Mage::helper('core')->currency($child->getFinalPrice()/1.25, true, true) . '<br />';

  					 	}
  						$prices .= '</div>';
  					}
  				}

          $mediatypes = $_product->getMediatype();
          if ($mediatypes) {
          	$mediatypes = explode(',', $mediatypes);
          	$prefix = 'mediatype-';
          	$mediatypeClassNames = $prefix.implode(' '.$prefix, $mediatypes);
          }

    			$mediatype = $_product->getAttributeText('mediatype');
    			$mediatypestring = (is_array($mediatype)) ? implode('/', $mediatype) : $mediatype;

  				// prepare authors
  				$authors = $_product->getData('authors');
  				$authors = rtrim(trim($authors), ',');

  				$final_price = $_product->getFinalPrice();

          $release_date = '';
          $_salesHelper = Mage::helper('systimesales');
          if ($_releaseDateInfo = $_salesHelper->getReleaseDateInfo($_product)) {
            $release_date = '
              <p>
            	  '.$_salesHelper->__($_product->getAttributeText('publication_status')) .',
            	  '.$_salesHelper->__($_releaseDateInfo['label']).': '.$_releaseDateInfo['value'];

            if ($_product->isSaleable() && $_salesHelper->isSchoolOnlyProduct($_product)) {
                $_blockId = $_salesHelper->getConfig('school_only_items_block_catalog', null, 'checkout');
            		$release_date .= Mage::helper('systimemisc')->renderBlock($_blockId, 'systime-school-only-item shopping-cart-item-message');
            }

            $release_date .= '</p>';
            
          }

  					$release = $_product->getAttributeText('publication_status') . $_salesHelper->__($_product->getAttributeText('publication_status'));

                          $description = '<table><tr>'.
                              '<td><a href="'.$_product->getProductUrl().'"><img src="' . $this->helper('catalog/image')->init($_product, 'thumbnail')->resize(75, 75)
                              .'" border="0" align="left" height="75" width="75"></a></td>'.
                              '<td  style="text-decoration:none;">' . $_product->getDescription();
                              
                          if (!empty($release_date)) {
                            $description .= $release_date;
                          }

                          if (isset($prices)) {
                            $description .= '<p>'.$prices.'</p>';
                          } elseif($_product->getPrice() > 0) {
                            $description .= '
                              <p>
                                Pris: '.Mage::helper('core')->currency($_product->getPrice()/1.25).
                                ($_product->getPrice() != $final_price  ? ' Special Pris: '. Mage::helper('core')->currency($final_price) : '').
                              '<p>';
                          }
                          
                $description .= '
                  <p>'.($_product->getData('isbn_13') == '' ? '' : 'ISBN: '.$_product->getData('isbn_13').'<br />').
  							  (!empty($authors) ? 'Forfatter(e): ' . $authors : '').
  							  '</p>
  							      </td>
                    </tr>
                  </table>
                ';
                          $data = array(
                                      'title'         => $_product->getName() . ' ' . ($_product->getEdition() ? $this->__('Edition %s', $_product->getEdition()) . ' ' . $_product->getPublicationYear() : '') . ' (' . $mediatypestring . ')',
                                      'link'          => $_product->getProductUrl(),
                                      'description'   => $description,
                                      'lastUpdate'    => strtotime($_product->getUpdatedAt()) //  strtotime($args['row']['updated_at'])
                                      );

                          $rssObj->_addEntry($data);
                      }
                  }
          #    }
          }
        }
        return $rssObj->createRssXml();
    }
}
