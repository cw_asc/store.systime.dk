<?php
/**
 * Meat Media AS 
 * Dibs Payment Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Meatmedia
 * @package    Meatmedia_Dibs
 * @author     Meat Media AS
 * @author     Chris Magnussen
 * @copyright  Copyright (c) 2008 Meat Media AS. (http://www.meatmedia.no)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Dibs Checkout Controller
 *
 */
class Meatmedia_Dibs_DibsController extends Mage_Core_Controller_Front_Action
{
	protected $_order;
	protected $_callbackAction = false;
	
    protected function _expireAjax()
    {
        if (!Mage::getSingleton('checkout/session')->getQuote()->hasItems()) {
            $this->getResponse()->setHeader('HTTP/1.1','403 Session Expired');
            exit;
        }
    }

    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setDibsQuoteId($session->getQuoteId());
        $this->getResponse()->setBody($this->getLayout()->createBlock('dibs/redirect')->toHtml());
        $session->unsQuoteId();
    }
    
    public function getOrder ()
    {
        if ($this->_order == null) {
            $session = Mage::getSingleton('checkout/session');
            $this->_order = Mage::getModel('sales/order');
            $this->_order->loadByIncrementId($session->getLastRealOrderId());
        }
        return $this->_order;
    }

    public function cancelAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getDibsQuoteId(true));
        $this->_redirect('checkout/cart');
     }

    public function  successAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getDibsQuoteId(true));
        
        Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
        
        $order = Mage::getModel('sales/order');
        
        $order->load(Mage::getSingleton('checkout/session')->getLastOrderId());

    	$order->sendNewOrderEmail();
        
        $msg = $this->__("Order paid") . " <br>". $this->__("OrderID"). ": <b>" . $_REQUEST["transact"] . "</b>";

        $order->addStatusToHistory($order->getStatus(),$msg);
        $order->save();
		
        if (!$this->_callbackAction) {
          $this->_redirect('checkout/onepage/success');
        } else {
          echo "OK";
          exit();
     	}
    }
    public function callbackAction()
    {
      $this->_callbackAction = true;
      $this->successAction();
    }

}
