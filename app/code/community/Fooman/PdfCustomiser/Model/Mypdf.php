<?php

//load the tcpdf library
require_once(BP. DS .'lib'. DS .'tcpdf'. DS .'tcpdf.php');

/*
 *  Extend the TCPDF class
 */

class Fooman_PdfCustomiser_Model_Mypdf extends TCPDF {


    /**
     * keep track if we have output
     * @access protected
     */
    protected $_PdfAnyOutput=false;

   /**
     * do we have output?
     * @return  bool
     * @access public
     */
    public function getPdfAnyOutput(){
        return $this->_PdfAnyOutput;
    }

   /**
     * set _PdfAnyOutput
     * @return  void
     * @access public
     */
    public function setPdfAnyOutput($flag){
        $this->_PdfAnyOutput = $flag;
    }

    /**
     * retrieve line items
     * @param
     * @return void
     * @access public
     */
    public function prepareLineItems($helper,$items,&$pdfItems,&$pdfBundleItems){
        foreach ($items as $item){
            //check if we are printing an order
            if(!$item instanceof Mage_Sales_Model_Order_Item) {
                //we generallly don't want to display subitems of configurable products etc but we do for bundled
                $type = $item->getOrderItem()->getProductType();
                $itemId = $item->getOrderItem()->getItemId();
                $parentType = 'none';
                $parentItemId = $item->getOrderItem()->getParentItemId();

                if($parentItemId){
                    $parentItem = Mage::getModel('sales/order_item')->load($parentItemId);
                    $parentType = $parentItem->getProductType();
                }

                //Get item Details
                $pdfTemp['itemId'] = $itemId;
                $pdfTemp['productId'] = $item->getOrderItem()->getProductId();
                $pdfTemp['type'] = $type;
                $pdfTemp['parentType'] = $parentType;
                $pdfTemp['parentItemId'] = $parentItemId;
                $pdfTemp['productDetails'] = $this->getItemNameAndSku($item);
                $pdfTemp['productOptions'] = $item->getOrderItem()->getProductOptions();
                $pdfTemp['price'] = $item->getPrice();
                $pdfTemp['discountAmount'] = $item->getDiscountAmount();
                $pdfTemp['qty'] = $helper->getPdfQtyAsInt()?(int)$item->getQty():$item->getQty();
                $pdfTemp['taxAmount'] = $item->getTaxAmount();
                $pdfTemp['rowTotal'] = $item->getRowTotal()+$item->getTaxAmount();
                $pdfTemp['basePrice'] = $item->getBasePrice();
                $pdfTemp['baseDiscountAmount'] = $item->getBaseDiscountAmount();
                $pdfTemp['baseTaxAmount'] = $item->getBaseTaxAmount();
                $pdfTemp['baseRowTotal'] = $item->getBaseRowTotal()+$item->getBaseTaxAmount();

                //collect bundle subitems separately
                if($parentType == 'bundle'){
                    //ugly workaround for bug in Magento on some bundles
                    //only needed for items and bundle sub products with no individual price
                    //for shipments
                    if( $item instanceof Mage_Sales_Model_Order_Shipment_Item){
                        if(!Mage::getModel('bundle/sales_order_pdf_items_shipment')->isShipmentSeparately($item)) {
                            $bundleSelection = unserialize($pdfTemp['productOptions']['bundle_selection_attributes']);
                            $pdfTemp['qty'] = $helper->getPdfQtyAsInt()?(int)$bundleSelection['qty']:$bundleSelection['qty'];
                        }
                    }
                    //invoices and creditmemos
                    else {
                        if(!Mage::getModel('bundle/sales_order_pdf_items_invoice')->isChildCalculated($item)) {
                            $bundleSelection = unserialize($pdfTemp['productOptions']['bundle_selection_attributes']);
                            $pdfTemp['qty'] = $helper->getPdfQtyAsInt()?(int)$bundleSelection['qty']:$bundleSelection['qty'];
                        }
                    }

                    $pdfBundleItems[$parentItemId][]=$pdfTemp;
                }else{
                    $pdfItems[$itemId]=$pdfTemp;
                }

            }else {
                //we generallly don't want to display subitems of configurable products etc but we do for bundled
                $type = $item->getProductType();
                $itemId = $item->getItemId();
                $parentType = 'none';
                $parentItemId = $item->getParentItemId();

                if($parentItemId){
                    $parentType = Mage::getModel('sales/order_item')->load($parentItemId)->getProductType();
                }

                //Get item Details
                $pdfTemp['itemId'] = $itemId;
                $pdfTemp['productId'] = $item->getProductId();
                $pdfTemp['type'] = $type;
                $pdfTemp['parentType'] = $parentType;
                $pdfTemp['parentItemId'] = $parentItemId;
                $pdfTemp['productDetails'] = $this->getItemNameAndSku($item);
                $pdfTemp['productOptions'] = $item->getProductOptions();
                $pdfTemp['price'] = $item->getPrice();
                $pdfTemp['discountAmount'] = $item->getDiscountAmount();
                $pdfTemp['qty'] = $helper->getPdfQtyAsInt()?(int)$item->getQtyOrdered():$item->getQtyOrdered();
                $pdfTemp['taxAmount'] = $item->getTaxAmount();
                $pdfTemp['rowTotal'] = $item->getRowTotal()+$item->getTaxAmount();
                $pdfTemp['basePrice'] = $item->getBasePrice();
                $pdfTemp['baseDiscountAmount'] = $item->getBaseDiscountAmount();
                $pdfTemp['baseTaxAmount'] = $item->getBaseTaxAmount();
                $pdfTemp['baseRowTotal'] = $item->getBaseRowTotal()+$item->getBaseTaxAmount();

                //collect bundle subitems separately
                if($parentType == 'bundle'){
                    if(!Mage::getModel('bundle/sales_order_pdf_items_invoice')->isChildCalculated($item)) {
                        $bundleSelection = unserialize($pdfTemp['productOptions']['bundle_selection_attributes']);
                        $pdfTemp['qty'] = $helper->getPdfQtyAsInt()?(int)$bundleSelection['qty']:$bundleSelection['qty'];
                    }
                    $pdfBundleItems[$parentItemId][]=$pdfTemp;
                }else{
                    $pdfItems[$itemId]=$pdfTemp;
                }
            }
        }
    }

    /*
     * Page header
     * return float height of logo
     */

    public function printHeader($helper,$title) {

        $maxLogoHeight = 25;
        //add title
        $this->SetFont($helper->getPdfFont(), 'B', $helper->getPdfFontsize('large'));
        $this->Cell($helper->getPageWidth() / 2 - $helper->getPdfMargins('sides'), 0, $title, 0, 2, 'L',null,null,1);
        $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());

        // Place Logo
        if($helper->getPdfLogo()){
            //Figure out if logo is too wide - half the page width minus margins
            /*$maxWidth = ($helper->getPageWidth()/2) - $helper->getPdfMargins('sides');
            if($helper->getPdfLogoDimensions('w') > $maxWidth ){
                $logoWidth = $maxWidth;
            }else{
                $logoWidth = $helper->getPdfLogoDimensions('w');
            }*/
			// anders@crius.dk 2010-03-04 - hack to scale logo down
			$logoWidth = $helper->getPdfLogoDimensions('w')/4;
			$maxLogoHeight = $helper->getPdfLogoDimensions('h')/4;
            $this->Image($helper->getPdfLogo(), $this->getPageWidth() - $helper->getPdfMargins('sides') - $logoWidth, $helper->getPdfMargins('top'), $logoWidth, $maxLogoHeight, null, null, null, null, null, null, null, null, null, true);
        }

        // Line break
        $this->SetY($helper->getPdfMargins('top')+min($helper->getPdfLogoDimensions('h-scaled'),$maxLogoHeight));
        $this->Ln(6);
    }

    /*
     *  set some standards for all pdf pages
     */
    public function SetStandard($helper){

        // set document information
        $this->SetCreator('Magento');

        //set margins
        $this->SetMargins($helper->getPdfMargins('sides'), $helper->getPdfMargins('top'));

        // set header and footer
        $this->setPrintFooter($helper->hasFooter());
        $this->setPrintHeader(true);

        $this->setHeaderMargin(0);
        $this->setFooterMargin($helper->getPdfMargins('bottom'));

        // set default monospaced font
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set auto page breaks
        $this->SetAutoPageBreak(true, $helper->getPdfMargins('bottom')+10);

        //set image scale factor 1 pixel = 1mm
        $this->setImageScale(1);

        // set font
        $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());

        // set fillcolor black
        $this->SetFillColor(0);

        // see if we need to sign
        if(Mage::getStoreConfig('sales_pdf/all/allsign',$helper->getStoreId())){
            $certificate = Mage::helper('core')->decrypt(Mage::getStoreConfig('sales_pdf/all/allsigncertificate',$helper->getStoreId()));
            $certpassword = Mage::helper('core')->decrypt(Mage::getStoreConfig('sales_pdf/all/allsignpassword',$helper->getStoreId()));

            // set document signature
            $this->setSignature($certificate, $certificate, $certpassword, '', 2, null);
        }

        //set Right to Left Language
        if(Mage::app()->getLocale()->getLocaleCode() == 'he_IL'){
            $this->setRTL(true);
        }else{
            $this->setRTL(false);
        }

    }

    public function Header() {
        // Line break
        $this->Ln();
    }

    public function Footer() {
        $helper = Mage::helper('pdfcustomiser/pdf');
        $footers = $helper->getFooters();

        if($footers[0]>0){
            $marginBetween=5;
            $width = ($this->getPageWidth() - 2* $helper->getPdfMargins('sides') - ($footers[0]-1)*$marginBetween) / $footers[0];
            $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize('small'));
            foreach ($footers as $key =>$footer){
                //don't display first element
                if($key > 0){
                    if($key < $footers[0]){
                        //not last element
                        $this->MultiCell($width, $this->getLastH(), $footer, 0, 'L', 0, 0);
                        $this->SetX($this->GetX()+$marginBetween);
                    }elseif($key == $footers[0]) {
                        //last element
                        if(!empty($footer)){
                            $this->MultiCell($width, $this->getLastH(), $footer, 0, 'L', 0, 1);
    }
                    }
                }
            }
            $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize(''));
        }
    }

    public function Line2($space=1) {
        $this->SetY($this->GetY()+$space);
        $margins =$this->getMargins();
        $this->Line($margins['left'],$this->GetY(),$this->getPageWidth()-$margins['right'],$this->GetY());
        $this->SetY($this->GetY()+$space);

    }

    /*
     *  get product name and Sku, take into consideration configurable products and product options
     */
    public function getItemNameAndSku($item){
        $return = array();
        $return['Name'] = $item->getName();
        $return['Sku'] = $item->getSku();

        //check if we are printing an order - doesn't have method getOrderItem
        if(method_exists($item,'getOrderItem')){
            if ($options = $item->getOrderItem()->getProductOptions()) {
                if (isset($options['options'])) {
                    foreach ($options['options'] as $option){
                       $return['Name'] .= "<br/>&nbsp;&nbsp;".$option['label'].": ".$option['value'];
                    }
                    $return['Name'] .= "<br/>";
                }
                if (isset($options['additional_options'])) {
                    foreach ($options['additional_options'] as $additionalOption){
                       $return['Name'] .= "<br/>&nbsp;&nbsp;".$additionalOption['label'].": ".$additionalOption['value'];
                    }
                    $return['Name'] .= "<br/>";
                }
                if (isset($options['attributes_info'])) {
                    foreach ($options['attributes_info'] as $attribute){
                       $return['Name'] .= "<br/>&nbsp;&nbsp;".$attribute['label'].": ".$attribute['value'];
                    }
                }
                if($item->getOrderItem()->getProductOptionByCode('simple_sku')){
                    $return['Sku'] = $item->getOrderItem()->getProductOptionByCode('simple_sku');
                }
            }
        }else{
            if ($options = $item->getProductOptions()) {
                if (isset($options['options'])) {
                    foreach ($options['options'] as $option){
                       $return['Name'] .= "<br/>&nbsp;&nbsp;".$option['label'].": ".$option['value'];
                    }
                    $return['Name'] .= "<br/>";
                }
                if (isset($options['additional_options'])) {
                    foreach ($options['additional_options'] as $additionalOption){
                       $return['Name'] .= "<br/>&nbsp;&nbsp;".$additionalOption['label'].": ".$additionalOption['value'];
                    }
                    $return['Name'] .= "<br/>";
                }
                if (isset($options['attributes_info'])) {
                    foreach ($options['attributes_info'] as $attribute){
                       $return['Name'] .= "<br/>&nbsp;&nbsp;".$attribute['label'].": ".$attribute['value'];
                    }
                }
                if($item->getProductOptionByCode('simple_sku')){
                    $return['Sku'] = $item->getProductOptionByCode('simple_sku');
                }
            }
        }
        /*
        //Uncomment this block: delete /* and * / and enter your attribute code below
        $attributeCode ='attribute_code_from_Magento_backend';
        $productAttribute = Mage::getModel('catalog/product')->load($item->getProductId())->getData($attributeCode);
        if(!empty($productAttribute)){
            $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attributeCode);
            $return['Name'] .= "<br/><br/>".$attribute->getFrontendLabel().": ".$productAttribute;
        }
         */
        return $return;
    }

    /*
     *  output customer addresses
     */
    public function OutputCustomerAddresses($helper, $order, $which){

        //$format = Mage::getStoreConfig('sales_pdf/all/alladdressformat',$helper->getStoreId());
		$format = 'html';
        if($order->getCustomerTaxvat()){
            $billingAddress = $this->_fixAddress($order->getBillingAddress()->format($format)."<br/>".Mage::helper('sales')->__('TAX/VAT Number').": ".$order->getCustomerTaxvat()."<br/>".$order->getCustomerEmail());
        }else{
            $billingAddress = $this->_fixAddress($order->getBillingAddress()->format($format)."<br/>".$order->getCustomerEmail());
        }
		$shippingAddress = '';
		// anders@crius.dk 2010-03-09 - check added to support orders without shipping address
		if ($order->getShippingAddress()) {
        	$shippingAddress = $this->_fixAddress($order->getShippingAddress()->format($format));
		}
        //uncomment the next line (delete the //) to show the email address underneath the billing address
        //$billingAddress.="<br/>".$order->getCustomerEmail();

        //which addresses are we supposed to show
        switch($which){
            case 'both':
                //swap order for Packing Slips - shipping on the left
                if(get_class($helper) =='Fooman_PdfCustomiser_Shipment'){
                    $this->SetX($helper->getPdfMargins('sides') + 5);
                    $this->Cell($this->getPageWidth() / 2 - $helper->getPdfMargins('sides'), 0, Mage::helper('sales')->__('SHIP TO:'), 0, 0, 'L');
                    if(!$order->getIsVirtual()){
                        $this->Cell(0, 0, Mage::helper('pdfcustomiser')->__('Faktureringsinformation'), 0, 1, 'L');
                    }else{
                        $this->Cell(0, $this->getLastH(), '', 0, 1, 'L');
                    }
                    $this->SetX($helper->getPdfMargins('sides') + 10);
                    $this->writeHTMLCell($this->getPageWidth() / 2 - $helper->getPdfMargins('sides'), 0, null, null,$shippingAddress,null,0);
                    if(!$order->getIsVirtual()){
                        $this->writeHTMLCell(0, $this->getLastH(), null, null, $billingAddress,null,1);
                    }else{
                        $this->Cell(0, $this->getLastH(), '', 0, 1, 'L');
                    }
                    break;
                }else{
					$this->SetFont($helper->getPdfFont(), 'B', $helper->getPdfFontsize());
                    $this->SetX($helper->getPdfMargins('sides'));
                    $this->Cell($this->getPageWidth() / 2 - $helper->getPdfMargins('sides'), 0, Mage::helper('pdfcustomiser')->__('Faktureringsinformation'), 0, 0, 'L');
                    if(!$order->getIsVirtual()){
                        $this->Cell(0, 0, Mage::helper('pdfcustomiser')->__('Leveringsinformation'), 0, 1, 'L');
                    }else{
                        $this->Cell(0, $this->getLastH(), '', 0, 1, 'L');
                    }
					$this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());
                    $this->SetX($helper->getPdfMargins('sides'));
                    $this->writeHTMLCell($this->getPageWidth() / 2 - $helper->getPdfMargins('sides'), 0, null, null, $billingAddress,null,0);
                    if(!$order->getIsVirtual()){
                        $this->writeHTMLCell(0, $this->getLastH(), null, null, $shippingAddress,null,1);
                    }else{
                        $this->Cell(0, $this->getLastH(), '', 0, 1, 'L');
                    }
                    break;
            }

            case 'billing':
                $this->SetX($helper->getPdfMargins('sides') + 5);
                $this->writeHTMLCell(0, 0, null, null, $billingAddress,null,1);
                break;
            case 'shipping':
                $this->SetX($helper->getPdfMargins('sides') + 5);
                if(!$order->getIsVirtual()){
                    $this->writeHTMLCell(0, 0, null, null, $shippingAddress,null,1);
                }
                break;
            case 'singlebilling':
                $this->SetAutoPageBreak(false, 85);
                $this->SetXY(-180, -67);
                $this->writeHTMLCell(75, 0, null, null, $billingAddress,null,0);
                $this->SetAutoPageBreak(true, 85);
                break;
            case 'singleshipping':
                $this->SetAutoPageBreak(false, 85);
                $this->SetXY(-180, -67);
                if(!$order->getIsVirtual()){
                    $this->writeHTMLCell(75, $this->getLastH(), null, null, $shippingAddress,null,1);
                }
                $this->SetAutoPageBreak(true, 85);
                break;
            case 'double':
                $this->SetAutoPageBreak(false, 85);
                $this->SetXY(-180, -67);
                $this->writeHTMLCell(75, 0, null, null, $billingAddress,null,0);
                $this->SetXY(-95, -67);
                if(!$order->getIsVirtual()){
                    $this->writeHTMLCell(75, $this->getLastH(), null, null, $shippingAddress,null,1);
                }
                $this->SetAutoPageBreak(true, 85);
                break;
            case 'doublereturn':
                $this->SetAutoPageBreak(false, 85);
                $this->SetXY(-180, -67);
                $this->MultiCell(75, 0, Mage::helper('pdfcustomiser')->__('Return Address').":\n\n".$helper->getPdfOwnerAddresss(), 0, 'L', 0, 1);
                $this->SetXY(-95, -67);
                if(!$order->getIsVirtual()){
                    $this->writeHTMLCell(75, $this->getLastH(), null, null, $shippingAddress,null,1);
                }
                $this->SetAutoPageBreak(true, 85);
                break;
            default:
                $this->SetX($helper->getPdfMargins('sides') + 5);
                $this->writeHTMLCell(0, 0, null, null, $billingAddress,null,1);
        }
        $this->Ln(10);
    }


    /*
     *  output payment and shipping blocks
     */
    public function OutputPaymentAndShipping($helper, $order){

        $paymentInfo = Mage::helper('payment')->getInfoBlock($order->getPayment())
                        ->setIsSecureMode(true)
                        ->toHtml();

        $this->SetFont($helper->getPdfFont(), 'B', $helper->getPdfFontsize());
        $this->Cell(0.5*($this->getPageWidth() - 2*$helper->getPdfMargins('sides')), 0, Mage::helper('pdfcustomiser')->__('Betalingsmetode'), 0, 0, 'L');
        if(!$order->getIsVirtual()){
            $this->Cell(0, 0,Mage::helper('pdfcustomiser')->__('Leveringsmetode'), 0, 1, 'L');
        }else{
            $this->Cell(0, 0, '', 0, 1, 'L');
        }

        $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());
        $this->writeHTMLCell(0.5*($this->getPageWidth() - 2*$helper->getPdfMargins('sides')), 0, null, null, $paymentInfo,0,0);

        if(!$order->getIsVirtual()){
            $trackingInfo ="";
            $tracks = $order->getTracksCollection();
            if (count($tracks)) {
                $trackingInfo ="\n";
                foreach ($tracks as $track) {
                    $trackingInfo .="\n".$track->getTitle().": ".$track->getNumber();
                }
            }

            //display depending on if Total Weight should be displayed or not
            if($helper->displayWeight()){
                //calculate weight
            $totalWeight = 0;
            foreach ($order->getAllItems() as $item){
                $totalWeight +=$item->getQty()*$item->getWeight();
            }
                //Output Shipping description with tracking info and Total Weight
                $this->MultiCell(0, $this->getLastH(), $order->getShippingDescription().$trackingInfo."\n\n".Mage::helper('pdfcustomiser')->__('Total Weight').': '.$totalWeight.Mage::helper('pdfcustomiser')->__('kg'), 0, 'L', 0, 1);
        }else{
                //Output Shipping description with tracking info
                $this->MultiCell(0, $this->getLastH(), $order->getShippingDescription().$trackingInfo, 0, 'L', 0, 1);
            }
        }else{
            $this->Cell(0, $this->getLastH(), '', 0, 1, 'L');
        }
        $this->Cell(0, 0, '', 0, 1, 'L');
    }

    /*
     *  output totals for invoice and creditmemo
     */
    public function OutputTotals($helper, $order,$item){

        //Display both currencies if flag is set and order is in a different currency
        $displayBoth = $helper->getDisplayBoth() && $order->isCurrencyDifferent();

        $widthTextTotals = $displayBoth ? $this->getPageWidth() - 2*$helper->getPdfMargins('sides') - 4.5*$helper->getPdfFontsize():
                                          $this->getPageWidth() - 2*$helper->getPdfMargins('sides') - 2.5*$helper->getPdfFontsize();
        $this->MultiCell($widthTextTotals, 0, Mage::helper('sales')->__('Order Subtotal').':', 0, 'R', 0, 0);
        $this->OutputTotalPrice($item->getSubtotal(), $item->getBaseSubtotal(),$displayBoth,$order);

        if ((float)$item->getDiscountAmount() > 0){
            $this->MultiCell($widthTextTotals, 0, Mage::helper('sales')->__('Discount').':', 0, 'R', 0, 0);
            $this->OutputTotalPrice($item->getDiscountAmount(), $item->getBaseDiscountAmount(),$displayBoth,$order);
        }

        if ((float)$item->getTaxAmount() > 0){
            if (Mage::helper('tax')->displayFullSummary()){
                $filteredTaxrates = array();
                //need to filter out doubled up taxrates on edited/reordered items -> Magento bug
                foreach ($order->getFullTaxInfo() as $taxrate){
                    foreach ($taxrate['rates'] as $rate){
                        $taxId= $rate['code'];
                        if(!isset($rate['title'])){
                            $rate['title']=$taxId;
                        }
                        $filteredTaxrates[$taxId]= array('id'=>$rate['code'],'percent'=>$rate['percent'],'amount'=>$taxrate['amount'],'baseAmount'=>$taxrate['base_amount'],'title'=>$rate['title']);
                    }
                }
                foreach ($filteredTaxrates as $filteredTaxrate){
                    $this->MultiCell($widthTextTotals, 0, $filteredTaxrate['title']." :", 0, 'R', 0, 0);
                    $this->OutputTotalPrice($filteredTaxrate['amount'], $filteredTaxrate['baseAmount'],$displayBoth,$order);
                }
            }else{
                $this->MultiCell($widthTextTotals, 0, Mage::helper('sales')->__('Tax').":", 0, 'R', 0, 0);
                $this->OutputTotalPrice($item->getTaxAmount(), $item->getBaseTaxAmount(),$displayBoth,$order);
            }
        }

        if ((float)$item->getShippingAmount() > 0){
            $this->MultiCell($widthTextTotals, 0, Mage::helper('sales')->__('Shipping & Handling').':', 0, 'R', 0, 0);
            $this->OutputTotalPrice($item->getShippingAmount(), $item->getBaseShippingAmount(),$displayBoth,$order);
        }

        if ($item->getAdjustmentPositive()){
            $this->MultiCell($widthTextTotals, 0, Mage::helper('sales')->__('Adjustment Refund').':', 0, 'R', 0, 0);
            $this->OutputTotalPrice($item->getAdjustmentPositive(), $item->getBaseAdjustmentPositive(),$displayBoth,$order);
        }

        if ((float) $item->getAdjustmentNegative()){
            $this->MultiCell($widthTextTotals, 0, Mage::helper('sales')->__('Adjustment Fee').':', 0, 'R', 0, 0);
            $this->OutputTotalPrice($item->getAdjustmentNegative(), $item->getBaseAdjustmentNegative(),$displayBoth,$order);
        }

        if ((float)$order->getFoomanSurchargeAmount() > 0){
            $this->MultiCell($widthTextTotals, 0, $order->getFoomanSurchargeDescription().':', 0, 'R', 0, 0);
            $this->OutputTotalPrice($order->getFoomanSurchargeAmount(), $order->getBaseFoomanSurchargeAmount(),$displayBoth,$order);
        }

        //Total separated with line plus bolded
        $this->Ln(5);
        $this->Cell($this->getPageWidth()/2 - $helper->getPdfMargins('sides'), 5, '', 0, 0, 'C');
        $this->Cell(0, 5, '', 'T', 1, 'C');
        $this->SetFont($helper->getPdfFont(), 'B', $helper->getPdfFontsize());
        $this->MultiCell($widthTextTotals, 0, Mage::helper('sales')->__('Grand Total').':', 0, 'R', 0, 0);
        $this->OutputTotalPrice($item->getGrandTotal(), $item->getBaseGrandTotal(),$displayBoth,$order);
        $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());
    }

    /*
     *  output Gift Message for Order / Should work for Item but seems to be a bug in Magento (getGiftMessageId = null)
     */
    public function OutputGiftMessage($helper, $order){

       if ($order->getGiftMessageId() && $giftMessage = Mage::helper('giftmessage/message')->getGiftMessage($order->getGiftMessageId())){
            $this->SetFont($helper->getPdfFont(), 'B', $helper->getPdfFontsize());
            $this->Cell(0, 0, Mage::helper('giftmessage')->__('Gift Message'), 0, 1, 'L',null,null,1);
            $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());

            $message = "<b>".Mage::helper('giftmessage')->__('From:')."</b> ".htmlspecialchars($giftMessage->getSender())."<br/>";
            $message .= "<b>".Mage::helper('giftmessage')->__('To:')."</b> ".htmlspecialchars($giftMessage->getRecipient())."<br/>";
            $message .= "<b>".Mage::helper('giftmessage')->__('Message:')."</b> ".htmlspecialchars($giftMessage->getMessage())."<br/>";
            $this->writeHTMLCell(0, 0, null, null,$message, null,1);
        }
    }

    /*
     *  output Comments on item - complete comment history
     *
     */
    public function OutputComment($helper, $item){
        if($helper->getPrintComments()){
            $comments ='';
            if (get_class($item) == 'Fooman_EmailAttachments_Model_Order'){
                foreach ($item->getAllStatusHistory() as $history){
                    $comments .=Mage::helper('core')->formatDate($history->getCreatedAt(), 'medium') ." | ".$history->getStatusLabel()."  ".$history->getComment()."\n";
                }
            }else{
                if ($item->getCommentsCollection()){
                    foreach($item->getCommentsCollection() as $comment){
                        $comments .=Mage::helper('core')->formatDate($comment->getCreatedAt(), 'medium') ." | ".$comment->getComment()."\n";
                    }

                }
            }
            if(!empty($comments)){
                $this->SetFont($helper->getPdfFont(), 'B', $helper->getPdfFontsize());
                $this->Cell(0, 0, Mage::helper('sales')->__('Comments'), 0, 1, 'L',null,null,1);
                $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());
                $this->MultiCell(0, 0, $comments, 0, 'L', 0, 1);
            }
        }
    }

    /*
     *  output prices for invoice and creditmemo
     */
    public function OutputPrice($price, $basePrice,$displayBoth,$order)
    {

        return $displayBoth ? (strip_tags($order->formatBasePrice($basePrice)).'<br/>'.strip_tags($order->formatPrice($price)))
                        : $order->formatPriceTxt($price);
    }

    /*
     *  output total prices for invoice and creditmemo
     */
    public function OutputTotalPrice($price, $basePrice,$displayBoth,$order)
    {
        if($displayBoth){
            $this->MultiCell(2.25*$this->getFontSizePt(), 0, strip_tags($order->formatBasePrice($basePrice)), 0, 'R', 0, 0);
        }
        $this->MultiCell(0, 0, $order->formatPriceTxt($price), 0, 'R', 0, 1);
    }

    public function write1DBarcode($code, $type, $x='', $y='', $w='', $h='', $xres=0.4, $style='', $align='T') {
        $style =array(
        'position' => 'S',
        'border' => false,
        'padding' => 1,
        'fgcolor' => array(0,0,0),
        'bgcolor' => false,
        'text' => true,
        'font' => 'helvetica',
        'fontsize' => 8,
        'stretchtext' => 4
        );
        parent::write1DBarcode($code,$type, $x, $y, $w, $h, $xres, $style, $align);
    }

    /**
     * replace any htmlspecialchars from input address except <br/>
     *
     * @param string $address
     * @return string
     */
    private function _fixAddress($address) {
        $address = htmlspecialchars($address);
        $pattern = array('&lt;br/&gt;','&lt;br /&gt;');
        $replacement = array('<br/>','<br/>');
        return str_replace($pattern, $replacement, $address);
    }

	public function OutputCheckoutAttributes($helper, $order)
	{

		$aCustomAtrrList = $this->getOrderCustomData($order);
		$text = '';
		foreach ($aCustomAtrrList as $aItem) {
			$text .= '<b>' . $aItem['label'] . ':</b> ' . $aItem['value'] . '<br/>';
		}
        $this->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());
		$this->writeHTMLCell(0, 0, null, null,$text, null,1);
	}
	
	public function getOrderCustomData($order)
    {
        $iStoreId = $order->getStoreId();
        
        $oAitcheckoutfields  = Mage::getModel('aitcheckoutfields/aitcheckoutfields');

        $aCustomAtrrList = $oAitcheckoutfields->getOrderCustomData($order->getId(), $iStoreId, true);
        
        return $aCustomAtrrList;
    }
}
