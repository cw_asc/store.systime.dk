<?php

class TBT_Bss_Block_Catalog_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{

    /**
     * Init Toolbar
     *
     */
    protected function _construct()
    {
        parent::_construct();
    }
    /**
     * Retrieve current order field
     *
     * @return string
     */
    public function getCurrentOrder()
    {
        $orders = $this->getAvailableOrders();
        $order = $this->getRequest()->getParam($this->getOrderVarName());
        if ($order && isset($orders[$order])) {
            Mage::getSingleton('catalog/session')->setSortOrder($order);
        }
        else {
            $order = Mage::getSingleton('catalog/session')->getSortOrder();
        }

        // validate session value
        if (!isset($orders[$order])) {
            $order = $this->_orderField;
        }

        // validate has order value
        if (!isset($orders[$order])) {
            $keys = array_keys($orders);
            $order = $keys[0];
        }

        return $order;
    }

    /**
     * Retrieve current direction
     *
     * @return string
     */
    public function getCurrentDirection()
   	{
		//@nelkaake Tuesday April 27, 2010 :
        $directions = array('desc', 'asc');
        $dir = strtolower($this->getRequest()->getParam($this->getDirectionVarName()));
        if ($dir && in_array($dir, $directions)) {
            Mage::getSingleton('catalog/session')->setSortDirection($dir);
        }
        else {
            $dir = Mage::getSingleton('catalog/session')->getSortDirection();
			//@nelkaake Tuesday April 27, 2010 :
			if($this->getCurrentOrder() == 'relevance') {
				$dir = empty($dir) ? 'desc' : $dir;
			}
        }

        // validate direction
        if (!$dir || !in_array($dir, $directions)) {
            $dir = $this->_direction;
        }

        return $dir;
    }

    /**
     * Set default Order field
     *
     * @param string $field
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setDefaultOrder($field)
    {
        if (isset($this->_availableOrder[$field])) {
            $this->_orderField = $field;
        }
        return $this;
    }

    /**
     * Set default sort direction
     *
     * @param string $dir
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setDefaultDirection($dir)
    {
        if (in_array(strtolower($dir), array('asc', 'desc'))) {
            $this->_direction = strtolower($dir);
        }
        return $this;
    }


}
