<?php

class TBT_Bss_TestController extends Mage_Adminhtml_Controller_action
{

    protected function _initAction()
    {
        $this->loadLayout()
        ->_addBreadcrumb(Mage::helper('bss')->__('Referrals'), Mage::helper('bss')->__('Referrals'));

        return $this;
    }
    /*
    	public function indexAction() {
            $this->_initAction()
                ->renderLayout();
    	}
    */
    
    public function getversionAction() {
        echo  Mage::getConfig()->getModuleConfig('TBT_Bss')->version;
        die();
    }

    public function indexAction()
    {

        try
        {
            die("do not run me please.");
            
            //@nelkaake Added on Wednesday June 2, 2010: If the model exists, delete it.
            $type_model = Mage::getModel('eav/entity_type')->loadByCode('catalog_product');
            $model = Mage::getModel('catalog/entity_attribute');
            $model->loadByCode($type_model->getId(), 'bss_weight');
            if($model->getId()) {
                $model->delete();
            }
            
            //@nelkaake Added on Wednesday June 2, 2010: Create a new one
            $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
            $setup->addAttribute('catalog_product', 'bss_weight', array(
            	'label'		        => 'Search Weight Modifier +/-',
            	'type'		        => 'int',
            	'visible'	        => true,
            	'required'	        => false,
            	'position'         	=> 1,
                'searchable'        => false,
                'filterable'        => false,
                'comparable'        => false,
                'visible_on_front'  => false,
                'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            ));
        }
        catch (Exception $e)
        {
            Mage::helper('bss')->log("Exception occured while trying to create search_weight model:");
            Mage::helper('bss')->log($e);
            die("err: $e");
        }
        die("done: ");
    }


}