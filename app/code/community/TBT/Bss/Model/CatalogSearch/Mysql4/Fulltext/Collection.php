<?php
class TBT_Bss_Model_CatalogSearch_Mysql4_Fulltext_Collection
    extends Mage_CatalogSearch_Model_Mysql4_Fulltext_Collection
{

    /**
     * Add search query filter
     *
     * @param   Mage_CatalogSearch_Model_Query $query
     * @return  Mage_CatalogSearch_Model_Mysql4_Search_Collection
     */
    public function addSearchFilter($query)
    {
    try {
        Mage::getSingleton('catalogsearch/fulltext')->prepareResult();
        $this->getSelect()->joinInner(
            array('search_result' => $this->getTable('catalogsearch/result')),
            $this->getConnection()->quoteInto(
                'search_result.product_id=e.entity_id AND search_result.query_id=?',
                $this->_getQuery()->getId()
            ),
            array('relevance2' => 'relevance') //@nelkaake WDCA : changed to relevance 2
        );
        //@nelkaake WDCA : next few lines were added by WDCA
        $eav_name = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'name');
        $rel_likes = array();
        $terms = explode(" ", trim($query));
        foreach($terms as $t) {
            //@nelkaake Added on Wednesday June 2, 2010: If we've specified an attribute weighting, use that instead
            if($aw_name = (float)Mage::getStoreConfig('bss/aw/name')) {
                $rel_likes[] = $this->getConnection()->quoteInto("(product_name.value LIKE ?)*{$aw_name}", '%'.$t.'%');
            }
        }
        //@nelkaake Added on Wednesday June 2, 2010: Try weighting the name with the term as a whole.
        if($aw_name = (float)Mage::getStoreConfig('bss/aw/name')) {
            $rel_likes[] = $this->getConnection()->quoteInto("(product_name.value LIKE ?)*{$aw_name}", '%'.trim($query).'%');
        }
        
        //@nelkaake Added on Wednesday June 2, 2010: If we've specified an attribute weighting, use that instead
        //@nelkaake Added on Wednesday June 2, 2010:  With the sku, we dont need to break up the digits.
        if($aw_sku = (float)Mage::getStoreConfig('bss/aw/sku')) {
            $rel_likes[] = $this->getConnection()->quoteInto("(`e`.sku = ?)*{$aw_sku}", '%'.trim($query).'%');
        }
            
        $manual_modifiers = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('bss_weight')
            ->addFieldToFilter('bss_weight', array('notnull' => 1));
            foreach($manual_modifiers as $mp) {
                Mage::helper('bss')->log("Product id={$mp->getSku()} will be modified with {$mp->getBssWeight()} weighting");
                $weight_m = (float)($mp->getBssWeight());
                if(!empty($weight_m)) {
                    $rel_likes[] = $this->getConnection()->quoteInto("(`e`.entity_id = ?)*{$weight_m}", $mp->getId());
                }
            }
            
    	// Added 2011-05-06 by anders@crius.dk
		// Select publication year
		$eav_publication_year = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'publication_year');
		$thisyear = date('Y');
        $this->getSelect()->joinLeft(
			array('product_publication_year' => 'catalog_product_entity_varchar'),
	        $this->getConnection()->quoteInto(
	            'product_publication_year.entity_id=e.entity_id AND product_publication_year.attribute_id = ?',
	            $eav_publication_year->getId()
	        ),
			array('product_publication_year' => 'value')
		);
		// Add weights for recent publication year. Newer publication = higher weight.
		// This year = 1, last year = 0.8, two years ago = 0.6 ...
		$noOfYears = 5;
		$newWeight = 1.0;
		for ($age = 0; $age < $noOfYears; $age++) {
			$ageWeight = $newWeight - $age*(1/$noOfYears);
			$year = $thisyear - $age;
			$rel_likes[] = "(product_publication_year.value = '$year')*$ageWeight";
		}
            
        $rel_added_statement = implode(" + ", $rel_likes);
        
        
        $this->getSelect()->joinLeft(
            array('product_name' => 'catalog_product_entity_varchar'),
            $this->getConnection()->quoteInto(
                'product_name.entity_id=e.entity_id AND product_name.attribute_id = ?',
                $eav_name->getId()
            ),
            array('product_name' => 'value', 'relevance' => "(  ({$rel_added_statement}) + search_result.relevance  )")
        );
        
         //echo((string)$this->getSelect());
    } catch(Exception $e) {
         //@nelkaake Added on Wednesday June 2, 2010: If we have any errors always revert back to the old way
         Mage::helper('bss')->log("Got an error trying to run so reverting back to old search method..");
         Mage::helper('bss')->log($e);
         return parent::addSearchFilter($query);
    }
        return $this;
    }
    
    

    /**
     * Set Order field
     *
     * @param string $attribute
     * @param string $dir
     * @return Mage_CatalogSearch_Model_Mysql4_Fulltext_Collection
     */
    public function setOrder($attribute, $dir='desc')
    {
        if ($attribute == 'relevance') {
            //$this->getSelect()->order("relevance2 {$dir}");
            $this->getSelect()->order("relevance {$dir}");
        }
        else {
            parent::setOrder($attribute, $dir);
        }
        return $this;
    }
}
