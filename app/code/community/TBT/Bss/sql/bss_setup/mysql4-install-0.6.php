<?php

$installer = $this;

$installer->startSetup();


//@nelkaake Added on Wednesday June 2, 2010: If the model exists, delete it.
$type_model = Mage::getModel('eav/entity_type')->loadByCode('catalog_product');
$model = Mage::getModel('catalog/entity_attribute');
$model->loadByCode($type_model->getId(), 'bss_weight');
if($model->getId()) {
    $model->delete();
}

//@nelkaake Added on Wednesday June 2, 2010: Create a new one
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->addAttribute('catalog_product', 'bss_weight', array(
	'label'		        => 'Search Weight Modifier +/-',
	'type'		        => 'int',
	'visible'	        => true,
	'required'	        => false,
	'position'         	=> 1,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
            
            
$installer->endSetup(); 