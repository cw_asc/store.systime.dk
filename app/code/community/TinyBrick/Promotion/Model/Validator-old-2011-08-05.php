<?php
class TinyBrick_Promotion_Model_Validator extends Mage_SalesRule_Model_Validator
{	
	public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
    	if(version_compare('1.4.0', Mage::getVersion(), '<=')) {
	        $item->setDiscountAmount(0);
	        $item->setBaseDiscountAmount(0);
	        $item->setDiscountPercent(0);
	
			//creates new quote if one is not found
			//needed for buy one get one (auto add to cart)
	        $quote = $item->getQuote();
	        if(!$quote->getId()) {
	    		$quote = Mage::getModel('sales/quote')
	    			->setId(null)
	    			->setStoreId(1)
	    			->setCustomerId('NULL')
	    			->setCustomerTaxClassId(1);
	    		$quote->save();
				Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
			}
	
	        $address    = $this->_getAddress($item);
	        
			//set all custom prices to null
			//this prevents after removing an promo item getting an item free
			$quoteItems = $quote->getAllItems();
			foreach($quoteItems as $quoteItem) {
				if($quoteItem->getQuoteId()) {
					$quoteItem->setCustomPrice(NULL);
					$quoteItem->setOriginalCustomPrice(NULL);
					$quoteItem->save();
				}
			}
			
	        //Clearing applied rule ids for quote and address
	        if ($this->_isFirstTimeProcessRun !== true){
	            $this->_isFirstTimeProcessRun = true;
	            $quote->setAppliedRuleIds('');
	            $address->setAppliedRuleIds('');
	        }
	
	        $itemPrice  = $item->getDiscountCalculationPrice();
	        if ($itemPrice !== null) {
	            $baseItemPrice = $item->getBaseDiscountCalculationPrice();
	        } else {
	            $itemPrice = $item->getCalculationPrice();
	            $baseItemPrice = $item->getBaseCalculationPrice();
	        }
	
	        $appliedRuleIds = array();
			
	        foreach ($this->_getRules() as $rule) {
	            /* @var $rule Mage_SalesRule_Model_Rule */
	            if (!$this->_canProcessRule($rule, $address)) {
	                continue;
	            }
	
	            //if (!$rule->getActions()->validate($item)) {
	              //  continue;
	            //}
				
	            $qty = $item->getTotalQty();
	            $qty = $rule->getDiscountQty() ? min($qty, $rule->getDiscountQty()) : $qty;
	            $rulePercent = min(100, $rule->getDiscountAmount());
	
	            $discountAmount = 0;
	            $baseDiscountAmount = 0;
				
				
	            switch ($rule->getSimpleAction()) {
	                case 'to_percent':
	                    $rulePercent = max(0, 100-$rule->getDiscountAmount());
	                    //no break;
	                case 'by_percent':
	                    $step = $rule->getDiscountStep();
	                    if ($step) {
	                        $qty = floor($qty/$step)*$step;
	                    }
	                    $discountAmount    = ($qty*$itemPrice - $item->getDiscountAmount()) * $rulePercent/100;
	                    $baseDiscountAmount= ($qty*$baseItemPrice - $item->getBaseDiscountAmount()) * $rulePercent/100;
	
	                    if (!$rule->getDiscountQty() || $rule->getDiscountQty()>$qty) {
	                        $discountPercent = min(100, $item->getDiscountPercent()+$rulePercent);
	                        $item->setDiscountPercent($discountPercent);
	                    }
	                    break;
	                case 'to_fixed':
	                    $quoteAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount());
	                    $discountAmount    = $qty*($itemPrice-$quoteAmount);
	                    $baseDiscountAmount= $qty*($baseItemPrice-$rule->getDiscountAmount());
	                    break;
	
	                case 'by_fixed':
	                    $step = $rule->getDiscountStep();
	                    if ($step) {
	                        $qty = floor($qty/$step)*$step;
	                    }
	                    $quoteAmount        = $quote->getStore()->convertPrice($rule->getDiscountAmount());
	                    $discountAmount     = $qty*$quoteAmount;
	                    $baseDiscountAmount = $qty*$rule->getDiscountAmount();
	                    break;
	
	                case 'cart_fixed':
	                    $cartRules = $address->getCartFixedRules();
	                    if (!isset($cartRules[$rule->getId()])) {
	                        $cartRules[$rule->getId()] = $rule->getDiscountAmount();
	                    }
	                    if ($cartRules[$rule->getId()] > 0) {
	                        $quoteAmount        = $quote->getStore()->convertPrice($cartRules[$rule->getId()]);
	                        /**
	                         * We can't use row total here because row total not include tax
	                         */
	                        $discountAmount     = min($itemPrice*$qty - $item->getDiscountAmount(), $quoteAmount);
	                        $baseDiscountAmount = min($baseItemPrice*$qty - $item->getBaseDiscountAmount(), $cartRules[$rule->getId()]);
	                        $cartRules[$rule->getId()] -= $baseDiscountAmount;
	                    }
	                    $address->setCartFixedRules($cartRules);
	                    break;
	
	                case 'buy_x_get_y':
	                    $x = $rule->getDiscountStep();
	                    $y = $rule->getDiscountAmount();
	                    if (!$x || $y>=$x) {
	                        break;
	                    }
	                    $buy = 0; $free = 0;
	                    while ($buy+$free<$qty) {
	                        $buy += $x;
	                        if ($buy+$free>=$qty) {
	                            break;
	                        }
	                        $free += min($y, $qty-$buy-$free);
	                        if ($buy+$free>=$qty) {
	                            break;
	                        }
	                    }
	                    $discountAmount    = $free*$itemPrice;
	                    $baseDiscountAmount= $free*$baseItemPrice;
	                    break;
	                    
	                case 'buy_itemx_get_itemy':			
	                	$conditionsArr = unserialize($rule->getActionsSerialized());
	                	foreach($conditionsArr['conditions'] as $condition) {
	                		$product = Mage::getModel('catalog/product')->getCollection()
								->addAttributeToFilter('sku', $condition['value'])
								->addAttributeToSelect('*')
								->getFirstItem();
							
							$stockItem = Mage::getModel('cataloginventory/stock_item');
							$stockItem->assignProduct($product);
							if(!$stockItem->getUseConfigManageStock()) {
								$stockItem->setData('is_in_stock', 1);
								$stockItem->setData('stock_id', 1);
								$stockItem->setData('store_id', 1);
								$stockItem->setData('manage_stock', 0);
								$stockItem->setData('use_config_manage_stock', 0);
								$stockItem->setData('min_sale_qty', 0);
								$stockItem->setData('use_config_min_sale_qty', 0);
								$stockItem->setData('max_sale_qty', 1000);
								$stockItem->setData('use_config_max_sale_qty', 0);
								$stockItem->save();
							}

				            $newitem = $quote->addProduct($product);
				            if($rulePercent == 100) {
				            	$newPrice = 0;
				            } else {
				            	$discountPercent = min(100, $rulePercent);
								$discountItemAmount = ($discountPercent/100) * $product->getPrice();
								$newPrice = round($product->getPrice() - $discountItemAmount, 2);
								$newitem->setBaseRowTotal($qty * $newPrice);
								$newitem->setRowTotal($qty * $newPrice);
				            }
				            $newitem->setCustomPrice($newPrice);
				            $newitem->setOriginalCustomPrice($newPrice);
							$newitem->setQty($qty);
							$newitem->save();
	                	}
	                	break;
	                	
	            }
	            $result = new Varien_Object(array(
	                'discount_amount'      => $discountAmount,
	                'base_discount_amount' => $baseDiscountAmount,
	            ));
	            Mage::dispatchEvent('salesrule_validator_process', array(
	                'rule'    => $rule,
	                'item'    => $item,
	                'address' => $address,
	                'quote'   => $quote,
	                'qty'     => $qty,
	                'result'  => $result,
	            ));
	
	            $discountAmount = $result->getDiscountAmount();
	            $baseDiscountAmount = $result->getBaseDiscountAmount();
	
	            $percentKey = $item->getDiscountPercent();
	            /**
	             * Process "delta" rounding
	             */
	            if ($percentKey) {
	                $delta      = isset($this->_roundingDeltas[$percentKey]) ? $this->_roundingDeltas[$percentKey] : 0;
	                $baseDelta  = isset($this->_baseRoundingDeltas[$percentKey]) ? $this->_baseRoundingDeltas[$percentKey] : 0;
	                $discountAmount+= $delta;
	                $baseDiscountAmount+=$baseDelta;
	
	                $this->_roundingDeltas[$percentKey]     = $discountAmount - $quote->getStore()->roundPrice($discountAmount);
	                $this->_baseRoundingDeltas[$percentKey] = $baseDiscountAmount - $quote->getStore()->roundPrice($baseDiscountAmount);
	                $discountAmount = $quote->getStore()->roundPrice($discountAmount);
	                $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);
	            } else {
	                $discountAmount     = $quote->getStore()->roundPrice($discountAmount);
	                $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);
	            }
	
	            /**
	             * We can't use row total here because row total not include tax
	             * Discount can be applied on price included tax
	             */
	            $discountAmount     = min($item->getDiscountAmount()+$discountAmount, $itemPrice*$qty);
	            $baseDiscountAmount = min($item->getBaseDiscountAmount()+$baseDiscountAmount, $baseItemPrice*$qty);
	
	            $item->setDiscountAmount($discountAmount);
	            $item->setBaseDiscountAmount($baseDiscountAmount);
	
	            $appliedRuleIds[$rule->getRuleId()] = $rule->getRuleId();
	
	            if ($rule->getCouponCode() && ( strtolower($rule->getCouponCode()) == strtolower($this->getCouponCode()))) {
	                $address->setCouponCode($this->getCouponCode());
	            }
	            $this->_addDiscountDescription($address, $rule);
	            if ($rule->getStopRulesProcessing()) {
	                break;
	            }
	        }
	        $item->setAppliedRuleIds(join(',',$appliedRuleIds));
	        $address->setAppliedRuleIds($this->mergeIds($address->getAppliedRuleIds(), $appliedRuleIds));
	        $quote->setAppliedRuleIds($this->mergeIds($quote->getAppliedRuleIds(), $appliedRuleIds));
	        return $this;
		} else {
			$item->setFreeShipping(false);
	        $item->setDiscountAmount(0);
	        $item->setBaseDiscountAmount(0);
	        $item->setDiscountPercent(0);
	
	        //$quote = $item->getQuote();
	        //creates new quote if one is not found
			//needed for buy one get one (auto add to cart)
	        $quote = $item->getQuote();
	        if(!$quote->getId()) {
	    		$quote = Mage::getModel('sales/quote')
	    			->setId(null)
	    			//->setStoreId(1) // anders@crius.dk 2010-12-07: Store ID 1 is wrong
	    			->setCustomerId('NULL')
	    			->setCustomerTaxClassId(1);
	    		$quote->save();
				Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
			}
	        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
	            $address = $item->getAddress();
	        } elseif ($quote->isVirtual()) {
	            $address = $quote->getBillingAddress();
	        } else {
	            $address = $quote->getShippingAddress();
	        }
			
			//set all custom prices to null
			//this prevents after removing an promo item getting an item free
			$quoteItems = $quote->getAllItems();
			foreach($quoteItems as $quoteItem) {
				if($quoteItem->getQuoteId()) {
					$quoteItem->setCustomPrice(NULL);
					$quoteItem->setOriginalCustomPrice(NULL);
					$quoteItem->save();
				}
			}
			
	        $customerId = $quote->getCustomerId();
	        $ruleCustomer = Mage::getModel('salesrule/rule_customer');
	        $appliedRuleIds = array();
	
	        foreach ($this->_rules as $rule) {
	            /* @var $rule Mage_SalesRule_Model_Rule */
	            /**
	             * already tried to validate and failed
	             */
	            if ($rule->getIsValid() === false) {
	                continue;
	            }
				
	            if ($rule->getIsValid() !== true) {
	                /**
	                 * too many times used in general
	                 */
	                if ($rule->getUsesPerCoupon() && ($rule->getTimesUsed() >= $rule->getUsesPerCoupon())) {
	                    $rule->setIsValid(false);
	                    continue;
	                }
	                /**
	                 * too many times used for this customer
	                 */
	                $ruleId = $rule->getId();
	                if ($ruleId && $rule->getUsesPerCustomer()) {
	                    $ruleCustomer->loadByCustomerRule($customerId, $ruleId);
	                    if ($ruleCustomer->getId()) {
	                        if ($ruleCustomer->getTimesUsed() >= $rule->getUsesPerCustomer()) {
	                            continue;
	                        }
	                    }
	                }
	                $rule->afterLoad();
	                /**
	                 * quote does not meet rule's conditions
	                 */
	                if (!$rule->validate($address)) {
	                    $rule->setIsValid(false);
	                    continue;
	                }
	                /**
	                 * passed all validations, remember to be valid
	                 */
	                $rule->setIsValid(true);
	            }
	
	            /**
	             * although the rule is valid, this item is not marked for action
	             */
	            //if (!$rule->getActions()->validate($item)) {
	            //    continue;
	            //}
	            // anders@crius.dk 2011-05-27 Action validation needs to be disabled for the promobot rule to work, but we need it for other rules, so let's run it just for those:
	            if ($rule->getSimpleAction() != 'buy_itemx_get_itemy' && !$rule->getActions()->validate($item)) {
	                continue;
	            }
	            
	            $qty = $item->getQty();
	            if ($item->getParentItem()) {
	                $qty*= $item->getParentItem()->getQty();
	            }
	            $qty = $rule->getDiscountQty() ? min($qty, $rule->getDiscountQty()) : $qty;
	            $rulePercent = min(100, $rule->getDiscountAmount());
	            $discountAmount = 0;
	            $baseDiscountAmount = 0;
	
	            switch ($rule->getSimpleAction()) {
	                case 'to_percent':
	                    $rulePercent = max(0, 100-$rule->getDiscountAmount());
	                    //no break;
	
	                case 'by_percent':
	                    if ($step = $rule->getDiscountStep()) {
	                        $qty = floor($qty/$step)*$step;
	                    }
	                    $discountAmount    = ($qty*$item->getCalculationPrice() - $item->getDiscountAmount()) * $rulePercent/100;
	                    $baseDiscountAmount= ($qty*$item->getBaseCalculationPrice() - $item->getBaseDiscountAmount()) * $rulePercent/100;
	
	                    if (!$rule->getDiscountQty() || $rule->getDiscountQty()>$qty) {
	                        $discountPercent = min(100, $item->getDiscountPercent()+$rulePercent);
	                        $item->setDiscountPercent($discountPercent);
	                    }
	                    break;
	
	                case 'to_fixed':
	                    $quoteAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount());
	                    $discountAmount    = $qty*($item->getCalculationPrice()-$quoteAmount);
	                    $baseDiscountAmount= $qty*($item->getBaseCalculationPrice()-$rule->getDiscountAmount());
	                    break;
	
	                case 'by_fixed':
	                    if ($step = $rule->getDiscountStep()) {
	                        $qty = floor($qty/$step)*$step;
	                    }
	                    $quoteAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount());
	                    $discountAmount    = $qty*$quoteAmount;
	                    $baseDiscountAmount= $qty*$rule->getDiscountAmount();
	                    break;
	
	                case 'cart_fixed':
	                    $cartRules = $address->getCartFixedRules();
	                    if (!isset($cartRules[$rule->getId()])) {
	                        $cartRules[$rule->getId()] = $rule->getDiscountAmount();
	                    }
	                    if ($cartRules[$rule->getId()] > 0) {
	                        $quoteAmount = $quote->getStore()->convertPrice($cartRules[$rule->getId()]);
	                        $discountAmount = min($item->getRowTotal(), $quoteAmount);
	                        $baseDiscountAmount = min($item->getBaseRowTotal(), $cartRules[$rule->getId()]);
	                        $cartRules[$rule->getId()] -= $baseDiscountAmount;
	                    }
	                    $address->setCartFixedRules($cartRules);
	                    break;
	
	                case 'buy_x_get_y':
	                    $x = $rule->getDiscountStep();
	                    $y = $rule->getDiscountAmount();
	                    if (!$x || $y>=$x) {
	                        break;
	                    }
	                    $buy = 0; $free = 0;
	                    while ($buy+$free<$qty) {
	                        $buy += $x;
	                        if ($buy+$free>=$qty) {
	                            break;
	                        }
	                        $free += min($y, $qty-$buy-$free);
	                        if ($buy+$free>=$qty) {
	                            break;
	                        }
	                    }
	                    $discountAmount    = $free*$item->getCalculationPrice();
	                    $baseDiscountAmount= $free*$item->getBaseCalculationPrice();
	                    break;
	                    
	                case 'buy_itemx_get_itemy':		
	                	$conditionsArr = unserialize($rule->getActionsSerialized());
	                	foreach($conditionsArr['conditions'] as $condition) {
	                		$product = Mage::getModel('catalog/product')->getCollection()
								->addAttributeToFilter('sku', $condition['value'])
								->addAttributeToSelect('*')
								->getFirstItem();
							
							$stockItem = Mage::getModel('cataloginventory/stock_item');
							$stockItem->assignProduct($product);
							if(!$stockItem->getUseConfigManageStock()) {
								$stockItem->setData('is_in_stock', 1);
								$stockItem->setData('stock_id', 1);
								$stockItem->setData('store_id', 1);
								$stockItem->setData('manage_stock', 0);
								$stockItem->setData('use_config_manage_stock', 0);
								$stockItem->setData('min_sale_qty', 0);
								$stockItem->setData('use_config_min_sale_qty', 0);
								$stockItem->setData('max_sale_qty', 1000);
								$stockItem->setData('use_config_max_sale_qty', 0);
								$stockItem->save();
							}
	
				            $newitem = $quote->addProduct($product);
				            if($rulePercent == 100) {
				            	$newPrice = 0;
				            } else {
				            	$discountPercent = min(100, $rulePercent);
								$discountItemAmount = ($discountPercent/100) * $product->getPrice();
								$newPrice = round($product->getPrice() - $discountItemAmount, 2);
								$newitem->setBaseRowTotal($qty * $newPrice);
								$newitem->setRowTotal($qty * $newPrice);
				            }
				            $newitem->setCustomPrice($newPrice);
				            $newitem->setOriginalCustomPrice($newPrice);
							$newitem->setQty($qty);
							$newitem->save();
	                	}
	                	break;
	
	            }
	
	            $result = new Varien_Object(array(
	                'discount_amount'      => $discountAmount,
	                'base_discount_amount' => $baseDiscountAmount,
	            ));
	            Mage::dispatchEvent('salesrule_validator_process', array(
	                'rule'    => $rule,
	                'item'    => $item,
	                'address' => $address,
	                'quote'   => $quote,
	                'qty'     => $qty,
	                'result'  => $result,
	            ));
	
	            $discountAmount = $result->getDiscountAmount();
	            $baseDiscountAmount = $result->getBaseDiscountAmount();
	
	            $discountAmount     = $quote->getStore()->roundPrice($discountAmount);
	            $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);
	            $discountAmount     = min($item->getDiscountAmount()+$discountAmount, $item->getRowTotal());
	            $baseDiscountAmount = min($item->getBaseDiscountAmount()+$baseDiscountAmount, $item->getBaseRowTotal());
	
	            $item->setDiscountAmount($discountAmount);
	            $item->setBaseDiscountAmount($baseDiscountAmount);
	
				if ($rule->getActions()->validate($item)) { // anders@crius.dk 2010-12-16 - action validation inserted - otherwise FREE_SHIPPING_ITEM will apply to all items
		            switch ($rule->getSimpleFreeShipping()) {
		                case Mage_SalesRule_Model_Rule::FREE_SHIPPING_ITEM:
		                    $item->setFreeShipping($rule->getDiscountQty() ? $rule->getDiscountQty() : true);
		                    break;
	
		                case Mage_SalesRule_Model_Rule::FREE_SHIPPING_ADDRESS:
		                    $address->setFreeShipping(true);
		                    break;
		            }
				}
	
	            $appliedRuleIds[$rule->getRuleId()] = $rule->getRuleId();
	
	            if ($rule->getCouponCode() && ( strtolower($rule->getCouponCode()) == strtolower($this->getCouponCode()))) {
	                $address->setCouponCode($this->getCouponCode());
	            }
	
	            if ($rule->getStopRulesProcessing()) {
	                break;
	            }
	        }
	        $item->setAppliedRuleIds(join(',',$appliedRuleIds));
	        $address->setAppliedRuleIds($this->mergeIds($address->getAppliedRuleIds(), $appliedRuleIds));
	        $quote->setAppliedRuleIds($this->mergeIds($quote->getAppliedRuleIds(), $appliedRuleIds));

	        return $this;

		}
    }

	// anders@crius.dk 2010-12-08 - fix wrong totals by updating an extra time after add to cart
	public function updateTotals($observer)
	{
		$session = Mage::getSingleton('checkout/session');
		$quote = $session->getQuote();
		$quote->collectTotals();
		$quote->save();
	}
}