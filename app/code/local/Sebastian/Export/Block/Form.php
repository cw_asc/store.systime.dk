<?php

/**
 *
 * Copyright Sebastian Enzinger <sebastian@enzinger.de> www.sebastian-enzinger.de
 *
 * All rights reserved.
 *
**/

class Sebastian_Export_Block_Form extends Mage_Adminhtml_Block_Template
{
    public function getCalendarHtml($id)
    {
        $calendar = $this->getLayout()
            ->createBlock('core/html_date')
            ->setId($id)
            ->setName($id)
            ->setClass('input-text')
            ->setImage(Mage::getDesign()->getSkinUrl('images/grid-cal.gif'))
            ->setFormat(Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT));

        return $calendar->getHtml();
    }

    public function getGrid() {
      return $this->getLayout()->createBlock('export/grid_grid', 'export.grid')->getHtml();
    }
}