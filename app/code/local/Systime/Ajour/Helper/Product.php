<?php
class Systime_Ajour_Helper_Product extends Mage_Catalog_Helper_Product {
	/**
	 *  Overwrite method to allow showing Ajour products eventhough they're visible "Nowhere"
	 */
	public function canShow($product, $where = 'catalog') {
		if (is_int($product)) {
			$product = Mage::getModel('catalog/product')->load($product);
		}

		/* @var $product Mage_Catalog_Model_Product */

		if (!$product->getId()) {
			return false;
		}

		if ($product->isVisibleInCatalog() && trim($product->getAjourCode())) {
			return true;
		}

		return parent::canShow($product, $where);
	}
}