<?php
class Systime_Ajour_Helper_Data extends Mage_Core_Helper_Abstract {
	public function getConfig($key, $storeId=null) {
		return Mage::getStoreConfig('systimeajour/general/'.$key, $storeId);
	}

	public function buildUrl($url, $params) {
		$query = '';

		foreach ($params as $name => $value) {
			if ($query) {
				$query .= '&';
			}
			$query .= urlencode($name).'='.urlencode($value);
		}

		return $url.((strpos($url, '?') !== false) ? '&' : '?').$query;
	}

	private $cache = array();

	/**
	 * return int
	 */
	public function getAjourOfferMaxQuantity($product) {
		$userid = $_COOKIE['ekeys-userid'];
		$code = $product->getAjourCode();

		$response = 0;
		if (isset($this->cache[$userid], $this->cache[$userid][$code])) {
			$response = $this->cache[$userid][$code];
		}

		if ($userid && $code) {
			$url = $this->getConfig('ajour_offer_url');
			$url = $this->buildUrl($url, array(
																				 'userid' => $userid,
																				 'code' => $code,
																				 ));

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);

			if (!isset($this->cache[$userid])) {
				$this->cache[$userid] = array();
			}
			$this->cache[$userid][$code] = $response;

 			//error_log(print_r(array(
 															//$url,
 															//$response,
 															//), true));
		}

// 		error_log(print_r(array(
// 														$this->cache,
// 														$response,
// 														), true));

		return intval($response);
	}

	public function saveAjourOrder($product, $quantity) {
		$userid = $_COOKIE['ekeys-userid'];
		$code = $product->getAjourCode();

		$url = $this->getConfig('ajour_offer_url');
		$url = $this->buildUrl($url, array(
																			 'userid' => $userid,
																			 'code' => $code,
																			 'quantity' => $quantity,
																			 'action' => 'save_order',
																			 ));

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);

// 			error_log(print_r(array(
// 															$url,
// 															$response,
// 															), true));

		return intval($response);
	}

	public function log($message, $level=null) {
		// if ((bool)$this->getConfig('write_log'))
			{
			Mage::log($message, $level, 'systime_ajour');
		}
	}
}
