<?php

class Systime_Ajour_Model_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item {
	public function checkQuoteItemQty($qty, $summaryQty, $origQty = 0) {
		$result = parent::checkQuoteItemQty($qty, $summaryQty, $origQty);

		// Check Ajour Offer
		if (!$result->getHasError()) {
			$product = Mage::getModel('catalog/product')->load($this->getProductId());
			$ajourCode = $product ? $product->getAjourCode() : null;

			if ($ajourCode) {
				$helper = Mage::helper('systimeajour');
				$errorMsg = null;

				if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
					// $maxQuantity = 1;
					$errorMsg = $helper->__('Du skal være logget ind for at bestille dette Ajour-tilbud');
				} else {
					$maxQuantity = $helper->getAjourOfferMaxQuantity($product);

					if ($qty > $maxQuantity) {
						if ($maxQuantity <= 0) {
							$errorMsg = $helper->__('Du kan ikke bestille dette Ajour-tilbud');
						} else {
							$errorMsg = $helper->__('Du kan maksimalt bestille %d styk af dette Ajour-tilbud', $maxQuantity);
						}
					}
				}

				if ($errorMsg) {
					$result->setHasError(true)
						->setMessage($helper->__($errorMsg))
						->setQuoteMessage(Mage::helper('cataloginventory')->__('Some of the products can not be ordered in requested quantity'))
						->setQuoteMessageIndex('qty');
				}
			}
		}

		// Minimum company e-key quantity hack!
		if (!$result->getHasError()) {
			$product = Mage::getModel('catalog/product')->load($this->getProductId());

			$helper = Mage::helper('systimeekeys');
			$ekeyType = $helper->getEkeyType($product);

			if ($ekeyType == 'company') {
				$minQuantity = intval($helper->getConfig('min_quantity_company'));
				$errorMsg = null;

// 				error_log(__METHOD__.': '.print_r(array(
// 																								'ekeyType' => $ekeyType,
// 																								'minQuantity' => $minQuantity,
// 																								), true));

				if ($qty <= $minQuantity) {
					$errorMsg = $helper->getConfig('min_quantity_message_company');

// 				error_log(__METHOD__.': '.print_r(array(
// 																								'ekeyType' => $ekeyType,
// 																								'minQuantity' => $minQuantity,
// 																								'errorMsg' => $errorMsg,
// 																								), true));

					if ($errorMsg) {
						$result->setHasError(true)
							->setMessage($errorMsg)
							// Only "Quote Message" is not shown for configurable products!?
							// ->setQuoteMessage(Mage::helper('cataloginventory')->__('Some of the products can not be ordered in requested quantity'))
							->setQuoteMessage($errorMsg)
							->setQuoteMessageIndex('qty');
					}
				}
			}
		}

		return $result;

		// return parent::checkQuoteItemQty($qty, $summaryQty, $origQty);
	}
}