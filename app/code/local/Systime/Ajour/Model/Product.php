<?php

class Systime_Ajour_Model_Product extends Mage_Catalog_Model_Product {

	public function checkQuoteItemQty($qty, $summaryQty, $origQty = 0) {
		$result = parent::checkQuoteItemQty($qty, $summaryQty, $origQty);

		if (!$result->getHasError()) {
			// Check Ajour Offer
			$product = Mage::getModel('catalog/product')->load($this->getProductId());
			$ajourCode = $product ? $product->getAjourCode() : null;

			if ($ajourCode) {
				$helper = Mage::helper('systimeajour');
				$errorMsg = null;

				if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
					// $maxQuantity = 1;
					$errorMsg = $helper->__('Du skal være logget ind for at bestille dette Ajour-tilbud!');
				} else {
					$maxQuantity = $helper->getAjourOfferMaxQuantity($product);

					if ($qty > $maxQuantity) {
						if ($maxQuantity <= 0) {
							$errorMsg = $helper->__('Du kan ikke bestille dette Ajour-tilbud!');
						} else {
							$errorMsg = $helper->__('Du kan maksimalt bestille %d styk af dette Ajour-tilbud!', $maxQuantity);
						}
					}
				}

				if ($errorMsg) {
					// $result = new Varien_Object();
					$result->setHasError(true)
						->setMessage($helper->__($errorMsg))
						->setQuoteMessage(Mage::helper('cataloginventory')->__('Some of the products can not be ordered in requested quantity'))
						->setQuoteMessageIndex('qty');
				}
			}
		}

		return $result;

		// return parent::checkQuoteItemQty($qty, $summaryQty, $origQty);
	}
}