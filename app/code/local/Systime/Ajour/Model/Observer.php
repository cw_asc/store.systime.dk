<?php
	/**
	 * Generate E-keys module observer
	 *
	 * @category   Mage
	 * @package    Mage_GenerateEKeys
	 */
class Systime_Ajour_Model_Observer {
	public function checkout_onepage_controller_success_action($observer) {
		try {
			$order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
			$this->saveAjourOrders($order);
		} catch (Exception $ex) {
			Mage::logException($ex);
		}
	}

	/**
	 * Report ordered ajour offers back to CRM system
	 */
	/*private*/ function saveAjourOrders($order) {
		try {
			$helper = Mage::helper('systimeajour');
			$items = $order->getAllItems();
			foreach ($items as $item) {
				$product = Mage::getModel('catalog/product')->load($item->getProductId());
				$ajourCode = $product ? $product->getAjourCode() : null;

				if ($ajourCode) {
					$quantity  = intval($item->getQtyOrdered());
					$helper->saveAjourOrder($product, $quantity);
				}
			}
		} catch (Exception $ex) {
			Mage::logException($ex);
		}
	}

	private function log($value, $level=null) {
		Mage::helper('systimeajour')->log($value, $level);
	}
}
