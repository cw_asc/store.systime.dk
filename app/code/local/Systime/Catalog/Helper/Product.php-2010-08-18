<?php
class Systime_Catalog_Helper_Product extends Mage_Core_Helper_Abstract
{
	public function getCategoryNamesForProduct(Mage_Catalog_Model_Product $product)
	{
		$categorynames = array();
		$categories = $product->getCategoryCollection();
		$categories->addAttributeToSelect('name');
		// Add level 2 categories
		foreach ($categories as $category) {
			if ($category->getLevel() == 2) {
				$categorynames[$category->getId()] = array("name" => $category->getName(), "children" => array());
			}
		}
		// Add level 3 categories
		foreach ($categories as $category) {
			if ($category->getLevel() == 3) {
				// Add parent category if missing
				if (!array_key_exists($category->getParentId(), $categorynames)) {
					$parentcategory = $category->getParentCategory();
					$categorynames[$parentcategory->getId()] = array("name" => $parentcategory->getName(), "children" => array());
				}
				// Add level 3 category
				$categorynames[$category->getParentId()]["children"][] = $category->getName();
			}
		}
		return $categorynames;
	}

	/**
	 * Get formatted release date and label.
	 *
	 * @param Mage_Catalog_Model_Product $product
	 *
	 * @return null|array()
	 */
	public function getReleaseDateInfo(Mage_Catalog_Model_Product $product) {
		//$product->load($product->getId());

		$release_date_estimated = $product->getData('release_date_estimated');
		$release_date_confirmed = $product->getData('release_date_confirmed');

		$label = null;
		$formattedValue = null;

		$now = Mage::getModel('core/date')->Timestamp(time());

		if ($release_date_confirmed) {
			$release_date_confirmed = new Zend_Date($release_date_confirmed, Zend_Date::ISO_8601);
			if ($release_date_confirmed->isLater($now)) {
				// Show confirmed release date
				$attribute = $product->getResource()->getAttribute('release_date_confirmed');

				// Why doesn't this format using the locale?!
				// $formattedValue = Mage::helper('core/data')->formatDate($release_date_confirmed, Mage_Core_Model_Locale::FORMAT_TYPE_LONG);
				$locale = Mage::app()->getLocale();
				$formattedValue = $release_date_confirmed->toString('d. MMMM YYYY', $locale->getLocale());

				return array(
										 'label' => $attribute->getFrontend()->getLabel(),
										 'value' => $formattedValue,
										 );
			}
		} else if ($release_date_estimated) {
			$release_date_estimated = new Zend_Date($release_date_estimated, Zend_Date::ISO_8601);
			if ($release_date_estimated->isLater($now)) {
				// Show estimated release date
				$attribute = $product->getResource()->getAttribute('release_date_estimated');
				$month = intval($release_date_estimated->get(Zend_Date::MONTH_SHORT));
				$quarter = intval(($month-1)/3)+1;
				$formattedValue = sprintf($this->__('Quarter %1$d, %2$d'), $quarter, $release_date_estimated->get(Zend_Date::YEAR));

				return array(
										 'label' => $attribute->getFrontend()->getLabel(),
										 'value' => $formattedValue,
										 );
			}
		}
	}

}
