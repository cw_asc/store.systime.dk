<?php
class Systime_CatalogSearch_Model_Layer extends Mage_CatalogSearch_Model_Layer
{
	public function prepareProductCollection($collection)
	{
		parent::prepareProductCollection($collection);
		$collection->addAttributeToSelect('edition')
			->addAttributeToSelect('publication_year')
			->addAttributeToSelect('mediatype');
		return $this;
	}
}