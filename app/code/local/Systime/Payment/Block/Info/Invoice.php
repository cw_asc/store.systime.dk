<?php
class Systime_Payment_Block_Info_Invoice extends Mage_Payment_Block_Info {
	protected function _construct() {
		parent::_construct();
		$this->setTemplate('systimepayment/info/invoice.phtml');
	}
}
