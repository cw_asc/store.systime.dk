<?php
class Systime_Payment_Block_Form_Invoice extends Mage_Payment_Block_Form {
	protected function _construct() {
		parent::_construct();
		$this->setTemplate('systimepayment/form/invoice.phtml');
	}
}
