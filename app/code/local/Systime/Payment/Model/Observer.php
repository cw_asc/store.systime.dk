<?php
class Systime_Payment_Model_Observer {
	public function sales_convert_quote_payment_to_order_payment($observer) {
		$order = $observer->getEvent()->getOrderPayment();
    $quote = $observer->getEvent()->getQuotePayment();

		$order->setSystimepaymentInvoiceEan($quote->getSystimepaymentInvoiceEan());
		$order->setSystimepaymentInvoiceAttention($quote->getSystimepaymentInvoiceAttention());
		$order->setSystimepaymentInvoiceOrderref($quote->getSystimepaymentInvoiceOrderref());
		$order->setSystimepaymentInvoiceIko($quote->getSystimepaymentInvoiceIko());

// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceEan()));
// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceAttention()));
// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceOrderref()));
// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceIko()));
	}
}
