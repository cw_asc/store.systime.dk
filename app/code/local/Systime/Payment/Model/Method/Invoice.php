<?php
class Systime_Payment_Model_Method_Invoice extends Mage_Payment_Model_Method_Abstract {
	/**
	 * unique internal payment method identifier
	 *
	 * @var string [a-z0-9_]
	 */
	protected $_code = 'systimepayment_invoice';

	protected $_formBlockType = 'systimepayment/form_invoice';
	protected $_infoBlockType = 'systimepayment/info_invoice';

	public function assignData($data) {
		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		$info = $this->getInfoInstance();
		$info
			->setSystimepaymentInvoiceEan($data->getSystimepaymentInvoiceEan())
			->setSystimepaymentInvoiceAttention($data->getSystimepaymentInvoiceAttention())
			->setSystimepaymentInvoiceOrderref($data->getSystimepaymentInvoiceOrderref())
			->setSystimepaymentInvoiceIko($data->getSystimepaymentInvoiceIko())
			;
		return $this;
	}

	public function getCustomText() {
		return $this->getConfigData('customtext');
	}

	public function getEan() {
		$info = $this->getInfoInstance();
		return $info->getSystimepaymentInvoiceEan();
	}

	public function getAttention() {
		$info = $this->getInfoInstance();
		return $info->getSystimepaymentInvoiceAttention();
	}

	public function getOrderref() {
		$info = $this->getInfoInstance();
		return $info->getSystimepaymentInvoiceOrderref();
	}

	public function getIko() {
		$info = $this->getInfoInstance();
		return $info->getSystimepaymentInvoiceIko();
	}
}
