<?php
class Systime_Sanity_Block_Index extends Mage_Adminhtml_Block_Template {
	private $allErrors = null;
	private $licenseTypeAttribute;
	private $licenseTypeLabels;
	private $superProducts;
	private $childProducts;

	public function load() {
		// $res = Mage::getSingleton('core/resource');
		// $read = $res->getConnection('core_read');
		// $select = $read
		// 	->select()
		// 	->from($res->getTableName('catalog/product_super_link'), 'parent_id')
		// 	->where('product_id = ?', $product->getId())
		// 	;

		$this->licenseTypeAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'licens_type');

		$this->licenseTypeLabels = array();

		$options = Mage::getModel('eav/entity_attribute_source_table')->setAttribute($this->licenseTypeAttribute)->getAllOptions(false);
		foreach ($options as $option) {
			$this->licenseTypeLabels[$option['value']] = $option['label'];
		}

		// print_r($this->licenseTypeLabels);

		$resource = Mage::getSingleton('core/resource');
		$db = $resource->getConnection('core_read');

		// Get configurable products and the configured prices
		$select = $db->select()
			->from(array('product' => $resource->getTableName('catalog/product')), array('sku', 'product_id' => 'product.entity_id'))
			->join(array('product_super_attribute' => $resource->getTableName('catalog/product_super_attribute')), 'product_super_attribute.product_id = product.entity_id')
			->join(array('product_super_attribute_pricing' => $resource->getTableName('catalog/product_super_attribute_pricing')), 'product_super_attribute_pricing.product_super_attribute_id = product_super_attribute.product_super_attribute_id', array('price' => 'pricing_value', 'license_type' => 'value_index'))
			->where('product_super_attribute.attribute_id = ?', $this->licenseTypeAttribute->getId())
			// ->where('product_super_attribute.product_id = ?', 2592)
			->order('price', 'license_type')
			;

		// echo $select->assemble(), "\n";

		// Map from product_id => array(<attribute value> => <price>)
		$this->superProducts = array();
		$rows = $db->fetchAll($select);
		foreach ($rows as $row) {
			$sku = $row['sku'];
			$licenseType = $row['license_type'];
			if (!isset($this->superProducts[$sku])) {
				$this->superProducts[$sku] = array('sku' => $row['sku'],
																					 'product_id' => $row['product_id'],
																					 'licenses' => array());
			}
			if (isset($this->superProducts[$sku]['licenses'][$licenseType])) {
				echo 'Duplicate license type: '.$this->licenseTypeLabels[$licenseType], "\n";
			} else {
				$this->superProducts[$sku]['licenses'][$licenseType] = array(
																																		 // 'sku' => $row['sku'],
																																		 // 'license_type' => $row['license_type'],
																																		 'price' => $row['price'],
																																		 );
			}
		}

// 		echo '<pre>';
// 		print_r($this->superProducts);
// 		echo '</pre>';

		$priceAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'price');
		// var_dump($priceAttribute);

		// Get parent (super) and child products
		$select = $db->select()
			->from(array('super_link' => $resource->getTableName('catalog/product_super_link')))
			->join(array('parent' => $resource->getTableName('catalog/product')),
						 'parent.entity_id = super_link.parent_id',
						 array('parent_sku' => 'parent.sku'))
			->join(array('child' => $resource->getTableName('catalog/product')),
						 'child.entity_id = super_link.product_id',
						 array('child_sku' => 'child.sku'))
			// join with child price
			->join(array('license_type' => $resource->getTableName('catalog_product_entity_'.$this->licenseTypeAttribute->getBackendType())),
						 'license_type.attribute_id = '.$this->licenseTypeAttribute->getAttributeId().' and license_type.entity_id = child.entity_id',
						 array('license_type' => 'license_type.value'))
			// join with child price
			->join(array('price' => $resource->getTableName('catalog_product_entity_'.$priceAttribute->getBackendType())),
						 'price.attribute_id = '.$priceAttribute->getAttributeId().' and price.entity_id = child.entity_id',
						 array('price' => 'price.value'))
			->order('price', 'license_type')
			;

		// var_dump($select->assemble());

		$rows = $db->fetchAll($select);

		// var_dump($rows);
		// exit;

		$this->childProducts = array();
		foreach ($rows as $row) {
			$sku = $row['parent_sku'];
			$licenseType = $row['license_type'];
			if (!isset($this->childProducts[$sku])) {
				$this->childProducts[$sku] = array('licenses' => array());
			}
			if (isset($this->childProducts[$sku]['licenses'][$licenseType])) {
				echo 'Duplicate license type: '.$this->licenseTypeLabels[$licenseType], "\n";
			} else {
				$this->childProducts[$sku]['licenses'][$licenseType] = array(
																																		 'sku' => $row['child_sku'],
																																		 // 'license_type' => $licenseType,
																																		 'price' => $row['price'],
																																		 );
			}
		}

// 		echo '<pre>';
// 		// print_r($this->superProducts);
// 		print_r($this->childProducts);
// 		echo '</pre>';
		// exit;

		$this->allErrors = array();

		foreach ($this->childProducts as $sku => $info) {
			$errors = array();

			$licenses = $info['licenses'];
			$superLicenses = isset($this->superProducts[$sku], $this->superProducts[$sku]['licenses']) ? $this->superProducts[$sku]['licenses'] : null;
			if (!$superLicenses) {
				$errors[] = sprintf('Missing configured prices');
			} else if (count($superLicenses) < count($licenses)) {
				$errors[] = sprintf('Too few configured prices');
			} else if (count($superLicenses) > count($licenses)) {
				$errors[] = sprintf('Too many configured prices');
			}

			if (!$errors) {
				// Compare configured prices to simple product prices
				foreach ($licenses as $licenseType => $expectedPrice) {
					$actualPrice = isset($superLicenses[$licenseType]) ? $superLicenses[$licenseType] : null;
					if (!$actualPrice) {
						$errors[] = sprintf('Missing configured price for "%s"', $this->licenseTypeLabels[$licenseType]);
					}
					foreach (array('price') as $field) {
						if ($actualPrice[$field] != $expectedPrice[$field]) {
							$errors[] = sprintf('Incorrect configured price: "%s" %0.2f (should be: %0.2f)', $this->licenseTypeLabels[$licenseType], $actualPrice[$field], $expectedPrice[$field]);
						}
					}
					// if (!isset
				}
			}

			if ($errors) {
				$this->allErrors[$sku] = $errors;

// 				echo '<pre>';
// 				print_r(array('errors' => $errors,
// 											'sku' => $sku,
// 											'licenses' => $licenses,
// 											'superLicenses' => $superLicenses,
// 											));
// 				echo '</pre>';

// 				exit;
			}
		}

// 		if ($this->errors) {
// 			$message = $this->__('Something\'s wrong ...');
// 			Mage::getSingleton('adminhtml/session')->addError($message);
// 		} else {
// 			$message = $this->__('Everything seems fine');
// 			Mage::getSingleton('adminhtml/session')->addSuccess($message);
// 		}

		// var_dump($errors);
	}

	public function formatPrice($price) {
		return $price;
	}

	public function getLicenseTypeLabel($licenseType) {
		return isset($this->licenseTypeLabels[$licenseType]) ? $this->licenseTypeLabels[$licenseType] : $licenseType;
	}

	public function getSuperProducts() {
		return $this->superProducts;
	}

	public function getProduct($sku) {
		return Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
	}

	public function getProductEditUrl($id) {
		return $this->getUrl('adminhtml/catalog_product/edit', array('id' => $id));
	}

	public function getAllErrors() {
		return $this->allErrors;
	}
}