<?php
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE {$this->getTable('cms_page')} ADD COLUMN systime_use_wysiwyg TINYINT(1) NOT NULL DEFAULT '0';
");

$installer->endSetup();