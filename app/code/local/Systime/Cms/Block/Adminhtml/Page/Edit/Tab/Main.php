<?php
class Systime_Cms_Block_Adminhtml_Page_Edit_Tab_Main extends Mage_Adminhtml_Block_Cms_Page_Edit_Tab_Main
{
	protected function _prepareForm()
	{
		parent::_prepareForm();
		
		$form = $this->getForm();
		$elements = $form->getElements();
		$fieldset = $elements[0];
		$model = Mage::registry('cms_page');
		
		$fieldset->addField('systime_use_wysiwyg', 'select', array(
            'label'     => Mage::helper('cms')->__('Rich text editor'),
            'title'     => Mage::helper('cms')->__('Rich text editor'),
            'name'      => 'systime_use_wysiwyg',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('cms')->__('Enabled'),
                '0' => Mage::helper('cms')->__('Disabled'),
            ),
			'after_element_html' => '<p class="nm"><small>' . Mage::helper('cms')->__('Aktivering/inaktivering træder i kraft når du gemmer siden') . '</small></p>',
        ));
		
		$form->setValues($model->getData());
		
		return $this;
	}
}