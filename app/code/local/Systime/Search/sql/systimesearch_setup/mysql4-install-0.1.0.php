<?php
$installer = $this;

$installer->startSetup();

$db = $installer->getConnection();

$tableName = 'catalogsearch_fulltext';
$fieldName = 'systimesearch_data_index';
$definition = 'longtext not null';

$actualTableName = $installer->getTable($tableName);
if ($db->tableColumnExists($actualTableName, $fieldName)) {
	$sql = 'ALTER TABLE `'.$actualTableName.'` CHANGE `'.$fieldName.'` `'.$fieldName.'` '.$definition;
	$installer->run($sql);
} else {
	$db->addColumn($actualTableName, $fieldName, $definition);
}

$installer->endSetup();
