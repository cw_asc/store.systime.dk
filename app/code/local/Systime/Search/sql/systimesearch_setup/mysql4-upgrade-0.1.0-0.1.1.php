<?php
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE {$this->getTable('catalogsearch_query')} ADD COLUMN systime_link VARCHAR(255) NOT NULL AFTER `updated_at`;
	ALTER TABLE {$this->getTable('catalogsearch_query')} ADD COLUMN systime_description MEDIUMTEXT NOT NULL AFTER `systime_link`;
");

$installer->endSetup();