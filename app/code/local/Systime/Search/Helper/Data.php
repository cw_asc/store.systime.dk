<?php
class Systime_Search_Helper_Data extends Mage_CatalogSearch_Helper_Data {
	protected $_rawQueryText;
	
	public function getQueryText() {
//echo __METHOD__, "\n";
		if (is_null($this->_queryText)) {
			$queryText = parent::getQueryText();

			// mri@systime.dk 2010-03-10
			// Trim ISBN formatting stuff from query
			$substitutions = array(// pattern => replacement
														 // ISBN 13
														 '@\b(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)\b@' => '\1\2\3\4\5\6\7\8\9\10\11\12\13',
														 // ISBN 10
														 '@\b(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*([\dxX])\b@' => '\1\2\3\4\5\6\7\8\9\10',
														 );
			$patterns = array_keys($substitutions);
			$replacements = array_values($substitutions);

			$this->_queryText = preg_replace($patterns, $replacements, $queryText);

			// anders@crius.dk 2010-04-26
			// Force quotes around query
			$this->_rawQueryText = $this->_queryText; // Save query without quotes for text field
			if ($this->_queryText && substr($this->_queryText,0,1) != '"' && substr($this->_queryText,-1) != '"') {
				$this->_queryText = '"'.$this->_queryText.'"';
			}
		}
		return $this->_queryText;
	}
	
	// anders@crius.dk 2010-04-26
	// Get query text for search field - use raw query without quotes
	public function getEscapedQueryText()
	{
		if (!$this->_rawQueryText) {
			$this->getQueryText();
		}
		return $this->htmlEscape($this->_rawQueryText);
	}
}
