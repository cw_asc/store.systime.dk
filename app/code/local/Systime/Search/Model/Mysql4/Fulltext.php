<?php
class Systime_Search_Model_Mysql4_Fulltext extends Mage_CatalogSearch_Model_Mysql4_Fulltext {
	protected function _rebuildStoreIndex($storeId, $productIds = null) {
		$this->cleanIndex($storeId, $productIds);

		// preparesearchable attributes
		$staticFields   = array();
		foreach ($this->_getSearchableAttributes('static') as $attribute) {
			$staticFields[] = $attribute->getAttributeCode();
		}
		$dynamicFields  = array(
														'int'       => array_keys($this->_getSearchableAttributes('int')),
														'varchar'   => array_keys($this->_getSearchableAttributes('varchar')),
														'text'      => array_keys($this->_getSearchableAttributes('text')),
														);

		// status and visibility filter
		$visibility     = $this->_getSearchableAttribute('visibility');
		$status         = $this->_getSearchableAttribute('status');
		$visibilityVals = Mage::getSingleton('catalog/product_visibility')->getVisibleInSearchIds();
		$statusVals     = Mage::getSingleton('catalog/product_status')->getVisibleStatusIds();

		$lastProductId = 0;
		while (true) {
			$products = $this->_getSearchableProducts($storeId, $staticFields, $productIds, $lastProductId);
			if (!$products) {
				break;
			}

			$productAttributes  = array();
			$productRelations   = array();
			foreach ($products as $productData) {
				$lastProductId = $productData['entity_id'];
				$productAttributes[$productData['entity_id']] = $productData['entity_id'];
				$productChilds = $this->_getProductChildIds($productData['entity_id'], $productData['type_id']);
				$productRelations[$productData['entity_id']] = $productChilds;
				if ($productChilds) {
					foreach ($productChilds as $productChildId) {
						$productAttributes[$productChildId] = $productChildId;
					}
				}
			}

			$productIndexes     = array();
			$productIndexesPriority = array();
			$productAttributes  = $this->_getProductAttributes($storeId, $productAttributes, $dynamicFields);
			foreach ($products as $productData) {
				if (!isset($productAttributes[$productData['entity_id']])) {
					continue;
				}
				$protductAttr = $productAttributes[$productData['entity_id']];
				if (!isset($protductAttr[$visibility->getId()]) || !in_array($protductAttr[$visibility->getId()], $visibilityVals)) {
					continue;
				}
				if (!isset($protductAttr[$status->getId()]) || !in_array($protductAttr[$status->getId()], $statusVals)) {
					continue;
				}

				$productIndex = array(
															$productData['entity_id'] => $protductAttr
															);
				if ($productChilds = $productRelations[$productData['entity_id']]) {
					foreach ($productChilds as $productChildId) {
						if (isset($productAttributes[$productChildId])) {
							$productIndex[$productChildId] = $productAttributes[$productChildId];
						}
					}
				}

				$index = $this->_prepareProductIndex($productIndex, $productData, $storeId);
				$indexPriority = $this->_prepareProductIndexPriority($productIndex, $productData, $storeId);

				$productIndexes[$productData['entity_id']] = $index;
				$productIndexesPriority[$productData['entity_id']] = $indexPriority;
				//$this->_saveProductIndex($productData['entity_id'], $storeId, $index);
			}
			$this->_saveProductIndexesPriority($storeId, $productIndexes, $productIndexesPriority);
		}

		$this->resetSearchResults();

		return $this;
	}

	public function prepareResult($object, $queryText, $query) {
		if (!$query->getIsProcessed()) {
			// mri@systime.dk 2010-03-10
			$substitutions = array(// pattern => replacement
														 // ISBN 13
														 '@\b(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)\b@' => '\1\2\3\4\5\6\7\8\9\10\11\12\13',
														 // ISBN 10
														 '@\b(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*(\d)[-\s]*([\dxX])\b@' => '\1\2\3\4\5\6\7\8\9\10',
														 );
			$patterns = array_keys($substitutions);
			$replacements = array_values($substitutions);

			$queryText = preg_replace($patterns, $replacements, $queryText);


			$searchType = $object->getSearchType($query->getStoreId());

			$stringHelper = Mage::helper('core/string');
			/* @var $stringHelper Mage_Core_Helper_String */

			$bind = array(
										':query'     => $queryText
										);
			$like = array();

			$fulltextCond   = '';
			$likeCond       = '';
			$separateCond   = '';

			if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_LIKE
					|| $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE) {
				$words = $stringHelper->splitWords($queryText, true, $query->getMaxQueryWords());
				$likeI = 0;
				foreach ($words as $word) {
					$like[] = '`s`.`data_index` LIKE :likew' . $likeI;
					$bind[':likew' . $likeI] = '%' . $word . '%';
					$likeI ++;
				}
				if ($like) {
					$likeCond = '(' . join(' AND ', $like) . ')';
				}
			}
			if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_FULLTEXT
					|| $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE) {
				$fulltextCond = 'MATCH (`s`.`data_index`) AGAINST (:query IN BOOLEAN MODE)';
			}
			if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE && $likeCond) {
				$separateCond = ' OR ';
			}

			$priorityWeight = $this->_getPriorityWeight();
			$relevanceScale = $this->_getNegateRelevance() ? -1 : 1;

			$sql = sprintf("REPLACE INTO `{$this->getTable('catalogsearch/result')}` "
										 . "(SELECT '%d', `s`.`product_id`, ".$relevanceScale."*(".intval($priorityWeight)."*(MATCH (`s`.`systimesearch_data_index`) AGAINST (:query IN BOOLEAN MODE)) + MATCH (`s`.`data_index`) AGAINST (:query IN BOOLEAN MODE)) "
										 . "FROM `{$this->getMainTable()}` AS `s` INNER JOIN `{$this->getTable('catalog/product')}` AS `e`"
										 . "ON `e`.`entity_id`=`s`.`product_id` WHERE (%s%s%s) AND `s`.`store_id`='%d')",
										 $query->getId(),
										 $fulltextCond,
										 $separateCond,
										 $likeCond,
										 $query->getStoreId()
										 );

			$this->_getWriteAdapter()->query($sql, $bind);

			$query->setIsProcessed(1);
		}

		return $this;
	}

	protected function _getPriorityWeight() {
		$value = Mage::getStoreConfig('catalog/systimesearch/priority_weight');
		return intval($value);
	}

	protected function _getNegateRelevance() {
		$value = Mage::getStoreConfig('catalog/systimesearch/negate_relevance');
		return (bool)$value;
	}

	protected $_priorityAttributes = null;

	protected function _getPriorityAttributes() {
		if (is_null($this->_priorityAttributes)) {
			$this->_priorityAttributes = array();
			$entityType = $this->getEavConfig()->getEntityType('catalog_product');

			$attributeCodes = explode(',', Mage::getStoreConfig('catalog/systimesearch/priority_attributes'));

			foreach ($attributeCodes as $attributeCode) {
				$attribute = $this->getEavConfig()->getAttribute($entityType, $attributeCode);

				$this->_priorityAttributes[$attribute->getId()] = $attribute;
			}
		}

		return $this->_priorityAttributes;
	}

	protected function _prepareProductIndexPriority($indexData, $productData, $storeId) {
		$index = array();

		$attributeCode = 'name';
		$entityType = $this->getEavConfig()->getEntityType('catalog_product');
		$attribute = $this->getEavConfig()->getAttribute($entityType, $attributeCode);

		$priorityAttributes = $this->_getPriorityAttributes();

		foreach ($indexData as $attributeData) {
			foreach ($attributeData as $attributeId => $attributeValue) {
				if (is_array($priorityAttributes) && array_key_exists($attributeId, $priorityAttributes)) {
					if ($value = $this->_getAttributeValue($attributeId, $attributeValue, $storeId)) {
						$index[] = $value;
					}
				}
			}
		}
		return join($this->_separator, $index);
	}

	protected function _saveProductIndexesPriority($storeId, $productIndexes, $productIndexesPriority) {
		$values = array();
		$bind   = array();
		foreach ($productIndexes as $productId => &$index) {
			$values[] = sprintf('(%s,%s,%s,%s)',
													$this->_getWriteAdapter()->quoteInto('?', $productId),
													$this->_getWriteAdapter()->quoteInto('?', $storeId),
													'?',
													'?'
													);
			$bind[] = $index;
			$bind[] = isset($productIndexesPriority[$productId]) ? $productIndexesPriority[$productId] : '';
		}

		if ($values) {
			$sql = "REPLACE INTO `{$this->getMainTable()}`(product_id, store_id, data_index, systimesearch_data_index) VALUES"
				. join(',', $values);

			$this->_getWriteAdapter()->query($sql, $bind);
		}

		return $this;
	}
}
