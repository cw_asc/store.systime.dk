<?php
class Systime_Search_Model_System_Config_Source_Attributes_Show {
	public function toOptionArray() {
		$collection = Mage::getResourceModel('eav/entity_attribute_collection')
			->setEntityTypeFilter(Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId())
			->addIsSearchableFilter()
			->addVisibleFilter();

		$options = array();
		foreach ($collection->getItems() as $item) {
			$options[] = array('value' => $item->getAttributeCode(),
												 'label' => $item->getFrontendLabel(),
												 );
		}
		return $options;
	}
}
