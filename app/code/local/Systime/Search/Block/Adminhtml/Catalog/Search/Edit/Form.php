<?php
class Systime_Search_Block_Adminhtml_Catalog_Search_Edit_Form extends Mage_Adminhtml_Block_Catalog_Search_Edit_Form
{
	// Add systime fields to search query editor
	protected function _prepareForm()
	{
		parent::_prepareForm();
		
		$form = $this->getForm();
		$elements = $form->getElements();
		$fieldset = $elements[0];
		
		$model = Mage::registry('current_catalog_search');
        /* @var $model Mage_CatalogSearch_Model_Query */
		
		$fieldset->addField('systime_link', 'text', array(
            'name'      => 'systime_link',
            'label'     => Mage::helper('systimesearch')->__('Systime link'),
            'title'     => Mage::helper('systimesearch')->__('Systime link'),
        ));

		$fieldset->addField('systime_description', 'editor', array(
	        'name'      => 'systime_description',
	        'label'     => Mage::helper('systimesearch')->__('Systime description'),
	        'title'     => Mage::helper('systimesearch')->__('Systime description'),
	    ));
	
		$form->setValues($model->getData());
		
		return $this;
	}
}