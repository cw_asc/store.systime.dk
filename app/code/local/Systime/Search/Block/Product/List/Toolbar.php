<?php
class Systime_Search_Block_Product_List_Toolbar extends TBT_Bss_Block_Catalog_Product_List_Toolbar //Mage_Catalog_Block_Product_List_Toolbar
{
	// disabled 2010-03-29 - anders@crius.dk
	/*public function getCurrentOrder() {
		$order = $this->_orderField;

		return $order;
	}

	public function getCurrentDirection() {
		$dir = 'asc';

		$order = $this->getCurrentOrder();
		switch ($order) {
		case 'publication_year':
			$dir = 'desc';
			break;
		}

		return $dir;
	}*/
	
	// Added 2010-03-29 - anders@crius.dk
	
	protected $_defaultDirections = array(
		'position' => 'asc',
		'title' => 'asc',
		'publication_year' => 'desc',
		);
	
	public function _construct() 
	{
		parent::_construct();
		$this->setDefaultDirection('desc');
	}
	
	public function getCurrentOppositeDirection()
	{
		if ($this->getCurrentDirection() == 'desc') {
			return 'asc';
		}
		return 'desc';
	}
	
	public function getOrderUrl($order, $direction)
	{
		if ($direction=='auto') {
			// For current sort order, link to opposite direction
			if ($this->isOrderCurrent($order)) {
				$direction = $this->getCurrentOppositeDirection();
			// For other sort orders, link to default direction, if they exist, otherwise 'asc'
			} else {
				if (array_key_exists($order, $this->_defaultDirections)) {
					$direction = $this->_defaultDirections[$order];
				} else {
					$direction = 'asc';
				}
			}
		}
		return parent::getOrderUrl($order, $direction);
	}
	
	
	
	// Everything below is copied from TBT_Bss_Block_Catalog_Product_List_Toolbar and mofied to not save in session
	// anders@crius.dk 2010-07-16
	
	
	/**
     * Retrieve current order field
     *
     * @return string
     */
    public function getCurrentOrder()
    {
        $orders = $this->getAvailableOrders();
        $order = $this->getRequest()->getParam($this->getOrderVarName());
        /*if ($order && isset($orders[$order])) {
            Mage::getSingleton('catalog/session')->setSortOrder($order);
        }
        else {
            $order = Mage::getSingleton('catalog/session')->getSortOrder();
        }*/

        // validate session value
        if (!isset($orders[$order])) {
            $order = $this->_orderField;
        }

        // validate has order value
        if (!isset($orders[$order])) {
            $keys = array_keys($orders);
            $order = $keys[0];
        }

        return $order;
    }

    /**
     * Retrieve current direction
     *
     * @return string
     */
    public function getCurrentDirection()
   	{
		//@nelkaake Tuesday April 27, 2010 :
        $directions = array('desc', 'asc');
        $dir = strtolower($this->getRequest()->getParam($this->getDirectionVarName()));
        if ($dir && in_array($dir, $directions)) {
            //Mage::getSingleton('catalog/session')->setSortDirection($dir);
        }
        else {
            //$dir = Mage::getSingleton('catalog/session')->getSortDirection();
			//@nelkaake Tuesday April 27, 2010 :
			if($this->getCurrentOrder() == 'relevance') {
				$dir = empty($dir) ? 'desc' : $dir;
			}
        }

        // validate direction
        if (!$dir || !in_array($dir, $directions)) {
            $dir = $this->_direction;
        }

        return $dir;
    }

    /**
     * Set default Order field
     *
     * @param string $field
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setDefaultOrder($field)
    {
        if (isset($this->_availableOrder[$field])) {
            $this->_orderField = $field;
        }
        return $this;
    }

    /**
     * Set default sort direction
     *
     * @param string $dir
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setDefaultDirection($dir)
    {
        if (in_array(strtolower($dir), array('asc', 'desc'))) {
            $this->_direction = strtolower($dir);
        }
        return $this;
    }
}
