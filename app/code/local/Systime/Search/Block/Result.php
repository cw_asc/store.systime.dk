<?php
class Systime_Search_Block_Result extends Mage_CatalogSearch_Block_Result
{
	/**
     * Set search available list orders
     *
     * @return Systime_Search_Block_Result
     */
    public function setListOrders() {
        $category = Mage::getSingleton('catalog/layer')
            ->getCurrentCategory();
        /* @var $category Mage_Catalog_Model_Category */
        $availableOrders = $category->getAvailableSortByOptions();
        unset($availableOrders['position']);
		// Remove relevance
        /*$availableOrders = array_merge(array(
            'relevance' => $this->__('Relevance')
        ), $availableOrders);*/

        $this->getListBlock()
            ->setAvailableOrders($availableOrders)
            ->setDefaultDirection('desc');
            //->setSortBy('relevance'); // do not sort by relevance

        return $this;
    }
}