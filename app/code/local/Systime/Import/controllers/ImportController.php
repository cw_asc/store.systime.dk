<?php
class Systime_Import_ImportController extends Mage_Core_Controller_Front_Action {
	public function indexAction() {
	}

	private static function checkIP() {
		$allow = false;

		$allowedIPs = trim(Mage::getStoreConfig('systimeimport/general/ip_filter'));
		if ($allowedIPs) {
			$allowedIPs = array_map('trim', explode(',', $allowedIPs));

			foreach ($allowedIPs as $ip) {
				if ((isset($_SERVER['REMOTE_ADDR']) && ($_SERVER['REMOTE_ADDR'] == $ip))
						|| (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], $ip) !== false))) {
					$allow = true;
					break;
				}
			}
		}

		if (!$allow) {
			header('Content-type: text/plain');
			die('Invalid request');
		}
	}

	private function getTempDir() {
// 		$basedir = Mage::getStoreConfig('admin/orderexport/basedir');
// 		if (!empty($basedir) && file_exists($basedir)) {
// 			return $basedir;
// 		} else {
// 			return Mage::getBaseDir();
// 		}
		$basedir = Mage::getBaseDir();
		$path = $basedir.'/systimeimport';
		if (!is_dir($path)) {
			@mkdir($path);
		}
		return $path;
	}

	public function productAction() {
		self::checkIP();

		header('Content-type: text/plain');

		try {
			$post_data = file_get_contents('php://input');

			if ($post_data) {
				$tempDir = $this->getTempDir();
				$importFilename = $tempDir.'/systimeimport-'.strftime('%Y%m%dT%H%M%S').'.csv';

				file_put_contents($importFilename, $post_data);

				$importer = Mage::helper('systimeimport/importer_product');

				$importer->setVerbose($this->getRequest()->getParam('verbose'));

				$importFieldConfig = array(
																	 array('field' => 'sku'),
																	 array('field' => 'edition',
																				 'function' => 'parseInt',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'version',
																				 'function' => 'parseInt',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'name',
																				 // do not update value
																				 'skipUpdate' => true),
																	 array('field' => 'isbn_10'),
																	 array('field' => 'isbn_13'),
																	 array('field' => 'release_date_estimated',
																				 'function' => 'parseDate',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'release_date_confirmed',
																				 'function' => 'parseDate',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'authors',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'projectmanager',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'price',
																				 'function' => 'parseAmount'),
																	 array('field' => 'number_of_pages',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'lab_url',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'subject',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'mm_mediatype'),
																	 array('field' => 'weight'),
																	 array('field' => 'publication_status',
																				 'function' => 'getPublicationStatusCode',
																				 'propagateToSuperEntity' => true),
																	 array('field' => 'publication_year',
																				 'propagateToSuperEntity' => true),
																	 );

				$importer->setImportFieldConfig($importFieldConfig);

				Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

				$res = $importer->importCSV($importFilename);
				if ($res !== false) {
					echo sprintf('%d:%d:%d', $res[0], $res[1], $res[2]);
					exit;
				}
			}
		} catch (Exception $ex) {
			echo $ex;
		}

		echo '0';
		exit;
	}
}
