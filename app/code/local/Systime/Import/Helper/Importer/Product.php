<?php
class Systime_Import_Helper_Importer_Product extends Systime_Import_Helper_Importer {
	/**
	 * @var map from attribute label to id
	 */
	private $publicationStatusCodes = null;


/*
bruges
Få styk
Genoptrykkes - i restordre
I produktion
I tryk
OK
Under genoptryk
Under udgivelse

ikke brugt:
	Aflyst udgivelse
	Afvist I/M
	Ide
	Manus
	Udgået / Makuleret
	Udgået / Ny udgave
	Udgået / Nyt ISBN
	Udgået / Udsolgt
	Udgået/Academic


select
  *
from
  MM_OJ Product
    join MM_KA PublicationStatus on Product.ObjStatus = PublicationStatus.Code and PublicationStatus.KatNr = 80
where
...

select
  KatNr,
  Code,
  Text
from
  MM_KA
where 1=1
  and KatNr = 80


*/

	private $publicationStatusCodesMap = array(
																						 'OK' => 'Udgivet',
																						 'Få styk' => 'Udgivet',

																						 'Under udgivelse' => 'På vej',
																						 'I produktion' => 'På vej',
																						 'I tryk' => 'På vej',

																						 'Genoptrykkes - i restordre' => 'Midlertidigt udsolgt',
																						 'Under genoptryk' => 'Midlertidigt udsolgt',
																						 );

	/**
	 * Get "publication_status" code (int) from name
	 *
	 * @param string $value
	 *
	 * @return int|null publication_status code
	 */
	public function getPublicationStatusCode($value) {
		if (!$this->publicationStatusCodes) {
			$attributeCode = 'publication_status';
			$attribute = Mage::getModel('catalog/product')->getResource()->getAttribute($attributeCode);
			// $attribute->setStoreId($this->getStoreId());
			$collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
				->setPositionOrder('asc')
				->setAttributeFilter($attribute->getId())
				->setStoreFilter($attribute->getStoreId())
				->load();

			$this->publicationStatusCodes = array();
			foreach ($collection as $item) {
				$this->publicationStatusCodes[$item->getValue()] = $item->getId();
			}

			// print_r($this->publicationStatusCodes);
			// die(__METHOD__);
		}

		// Translate value
		if (isset($this->publicationStatusCodesMap[$value])) {
			$value = $this->publicationStatusCodesMap[$value];
		}

		return isset($this->publicationStatusCodes[$value]) ? $this->publicationStatusCodes[$value] : null;
	}

	public function __construct($storeId = 0) {
		$helper = Mage::helper('systimeimport/product');
		parent::__construct($helper);
	}
}
