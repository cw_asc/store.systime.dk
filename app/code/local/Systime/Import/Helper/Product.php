<?php

class Systime_Import_Helper_Product {
	private $db;
	private $storeId = Mage_Core_Model_App::ADMIN_STORE_ID;
	private $entityTypeId = 0;

	public function __construct($storeId = 0) {
		$this->resource = Mage::getSingleton('core/resource');
		$this->db = $this->resource->getConnection('core_write');

// 		print_r(get_class($this->db));
// 		die(__METHOD__);

		$this->entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();

		$this->setStoreId($storeId);
	}

	public function setStoreId($storeId) {
		$this->storeId = $storeId;
	}

	private function select($query, $bindings = array(), $resultType = PDO::FETCH_ASSOC) {
		$res = $this->db->query($query, $bindings);
		return $res->fetch($resultType);
	}

	private function update($query, $bindings = array()) {
		$res = $this->db->query($query, $bindings);
		return $res;
	}

	private function insert($query, $bindings = array()) {
		$res = $this->db->query($query, $bindings);
		return $res;
	}

	private function getAttributeData($attribute_code) {
		$select = $this->db->select()
			->from($this->getTable('eav_attribute'), array('attribute_id', 'backend_type'))
			->where('entity_type_id = ?', $this->entityTypeId)
			->where('attribute_code = ?', $attribute_code);
		$data = $this->db->fetchRow($select);

		return $data;
	}

	public function getAttributeValue($entity_id, $attribute_code) {
		$data = $this->getAttributeData($attribute_code);
		if ($data) {
			$select = $this->db->select()
				->from($this->getTable('catalog_product_entity_'.$data['backend_type']), 'value')
				->where('entity_type_id = ?', $this->entityTypeId)
				->where('attribute_id = ?', $data['attribute_id'])
				->where('entity_id = ?', $entity_id)
				->where('store_id = ?', $this->storeId);
			// var_dump($select->assemble());
			return $this->db->fetchOne($select);

			$query = 'SELECT value from catalog_product_entity_'.$data['backend_type'].' where entity_type_id = ? and attribute_id = ? and entity_id = ? and store_id = ?';
			$result = $this->select($query, array($this->entityTypeId, $data['attribute_id'], $entity_id, $this->storeId));
			return $result ? $result['value'] : null;
		}
	}

	public function hasAttributeValue($entity_id, $attribute_code) {
		$data = $this->getAttributeData($attribute_code);
		if ($data) {
			$select = $this->db->select()
				->from($this->getTable('catalog_product_entity_'.$data['backend_type']), 'value')
				->where('entity_type_id = ?', $this->entityTypeId)
				->where('attribute_id = ?', $data['attribute_id'])
				->where('entity_id = ?', $entity_id)
				->where('store_id = ?', $this->storeId);
			$rows = $this->db->fetchRow($select);

			return $rows ? true : false;
		}
	}

	public function setAttributeValue($entity_id, $attribute_code, $value) {
		$data = $this->getAttributeData($attribute_code);
		// var_dump($data);
		if ($data) {
			$table = $this->getTable('catalog_product_entity_'.$data['backend_type']);
			if (($value === '') || ($value === null)) {
				$rowsAffected = 0;
				if ($this->hasAttributeValue($entity_id, $attribute_code)) {
					// Delete eav attribute
					$rowsAffected = $this->db->delete($table, array($this->db->quoteInto('entity_type_id = ?', $this->entityTypeId),
																													$this->db->quoteInto('attribute_id = ?', $data['attribute_id']),
																													$this->db->quoteInto('store_id = ?', $this->storeId),
																													$this->db->quoteInto('entity_id = ?', $entity_id)));
				}
				return $rowsAffected;
			}
			$values = array('value' => $value);

			if ($this->hasAttributeValue($entity_id, $attribute_code)) {
				$rowsAffected = $this->db->update($table, $values,
																					array($this->db->quoteInto('entity_type_id = ?', $this->entityTypeId),
																								$this->db->quoteInto('attribute_id = ?', $data['attribute_id']),
																								$this->db->quoteInto('store_id = ?', $this->storeId),
																								$this->db->quoteInto('entity_id = ?', $entity_id)));

				return $rowsAffected;

// 				var_dump($update->assemble());

				$query = 'UPDATE catalog_product_entity_'.$data['backend_type'].' set value = ? where entity_type_id = ? and attribute_id = ? and store_id = ? and entity_id = ?';
				$bindings = array($value, $this->entityTypeId, $data['attribute_id'], $this->storeId, $entity_id);
				return $this->update($query, $bindings);
			} else {
				$values['entity_type_id'] = $this->entityTypeId;
				$values['attribute_id'] = $data['attribute_id'];
				$values['store_id'] = $this->storeId;
				$values['entity_id'] = $entity_id;

				$this->db->insert($table, $values);
				if ($this->db->lastInsertId()) {
					return 1;
				}
			}
		}

		return false;
	}

	private function getTable($tableName) {
		return $this->resource->getTableName($tableName);
	}

	public function getEntityId(&$values) {
		// die(__METHOD__);
		$id = 0;
		$sku = null;

		if (isset($values['sku'])) {
			$sku = $values['sku'];
			unset($values['sku']);
		}

		if ($sku) {
			$select = $this->db->select()
				->from($this->getTable('catalog_product_entity'), 'entity_id')
				->where('sku = ?', $sku);

			$id = intval($this->db->fetchOne($select));
		}

		return $id;
	}

	private static $defaultValues = array(
																				'website_ids' => array(1),
																				// 'store' => 'base',
																				'type_id' => 'simple',
																				// 'attribute_set_id' => 0,
																				'status' => 2, // Inactive
																				'visibility' => 1, // Nowhere
																				);

	public function createEntity(&$values) {
		try {
			$defaultValues = array_merge(self::$defaultValues);
			$defaultValues['attribute_set_id'] = $this->getAttributeSet();

			$data = array_merge($defaultValues, $values);

			// print_r($data);

			$product = Mage::getModel('catalog/product')
				->fromArray($data)
				->save();

			return $product->getId();
		} catch (Exception $ex) {
			//print_r($ex->getMessage()); die(__METHOD__);

			Mage::logException($ex);
		}
	}

	const DO_IT_THE_MAGENTO_WAY = false;
	// const DO_IT_THE_MAGENTO_WAY = true;

	public function updateEntity($id, &$values) {
		try {
			if (self::DO_IT_THE_MAGENTO_WAY) {
				$product = Mage::getModel('catalog/product')->load($id);

				foreach ($values as $field => $value) {
					$product->setData($field, $value);
				}

				$product->setIsMassupdate(true);
				$product->save();

				return $product->getId();
			} else {
				// Update DB directly
				foreach ($values as $field => $value) {
					$this->setAttributeValue($id, $field, $value);
				}

				return $id;
			}
		} catch (Exception $ex) {
			Mage::logException($ex);
		}
	}

	/**
	 * Get unique super product id
	 */
	public function getSuperEntityId($id) {
		$select = $this->db->select()
			->from($this->getTable('catalog_product_super_link'), 'parent_id')
			->where('product_id = ?', $id);
		$rows = $this->db->fetchAll($select);
		return (count($rows) == 1) ? intval($rows[0]['parent_id']) : 0;
	}

	public function getAttributeSet($name = 'Default') {
		$select = $this->db->select()
			->from($this->getTable('eav_attribute_set'), 'attribute_set_id')
			->where('entity_type_id = ?', $this->entityTypeId)
			->where('attribute_set_name = ?', $name);
		return $this->db->fetchOne($select);
	}
}
