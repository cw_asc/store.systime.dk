<?php
class Systime_Import_Helper_Importer {
	protected $importFieldConfig;
	protected $helper;

	protected function __construct($helper) {
		$this->helper = $helper;
	}

	public function setImportFieldConfig($importFieldConfig) {
		$this->importFieldConfig = $importFieldConfig;
	}

	/**
	 * Parses a string (yyyymmdd) into a timestamp
	 */
	protected function parseDate($yyyymmdd) {
		preg_match('/([0-9]{4})([0-9]{2})([0-9]{2})/', $yyyymmdd, $matches);
		$timestamp = $matches ? mktime(0, 0, 0, $matches[2], $matches[3], $matches[1]) : 0;
		return $timestamp ? strftime('%F %T', $timestamp) : '';
	}

	protected function parseInt($value) {
		return intval($value);
	}

	protected function parseNumber($value) {
		return floatval($value);
	}

	protected function parseAmount($value) {
		return floatval($value);
	}

	public function importCSV($filename) {
		set_time_limit(0);

		$this->filename = $filename;

		if ($this->importFieldConfig) {
			if ($this->openCSV()) {
				$startTime = microtime(true);
				$this->importFieldFunctions = array();
				$this->propagateToSuperEntityFields = array();
				foreach ($this->importFieldConfig as $item) {
					$this->importFieldFunctions[] = isset($item['function']) ? $item['function'] : null;
					if (isset($item['propagateToSuperEntity']) && $item['propagateToSuperEntity']) {
						$this->propagateToSuperEntityFields[] = $item['field'];
					}
				}

				while (($row = $this->getRow()) !== false) {
					$fieldValues = $this->getImportFieldValues($row);
					// $where = $this->getInsertOrUpdateWhereClause($fieldValues, $row);
					// $this->insertOrUpdate($where, $fieldValues, $row);

					// $sku = null;
					$importValues = array_merge($fieldValues);
					$id = $this->helper->getEntityId($importValues);
 					if ($id) {
						$this->existingEntries++;
					} else {
 						// Create new entity
 						$id = $this->helper->createEntity($fieldValues);
						if ($id) {
							$this->newEntries++;
						}
					}

					if ($id) {
						$this->info("entity_id: %d\t", $id);
						$updateValues = $this->getUpdateValues($importValues);
						/*
						foreach ($updateValues as $field => $value) {
							$res = $this->helper->setAttributeValue($id, $field, $value);
							$this->info("\t%s\t%s\t(res: %s)", $field, $value, ($res === false ? 'false' : $res));
						}
						*/
						$res = $this->helper->updateEntity($id, $updateValues);
						$this->info("\tupdate\t(res: %s)", ($res === false ? 'false' : $res));
						$this->numberOfImportedRows++;

						$superValues = $this->getSuperValues($updateValues);
						if ($superValues) {
							$superEntityId = $this->helper->getSuperEntityId($id);
							if ($superEntityId) {
								$res = $this->helper->updateEntity($superEntityId, $superValues);
								$this->info("\tupdate super\t(res: %s)", ($res === false ? 'false' : $res));
							}
						}
					} else {
						$this->info('Invalid row: ');
					}
				}
				$this->closeCSV();

				$endTime = microtime(true);

				$this->info('Elapsed time: %0.3fs', $endTime-$startTime);

				return array($this->numberOfImportedRows, $this->newEntries, $this->existingEntries);
			}
		}

		return false;
	}

	private $numberOfRecords = 0;
	private $numberOfImportedRows = 0;
	private $newEntries = 0;
	private $existingEntries = 0;

	public function getNumberOfImportedRows() {
		return $this->numberOfImportedRows;
	}

	public function getNumberOfRecords() {
		return $this->numberOfRecords;
	}

	protected function openCSV() {
		if (!$this->filename || !file_exists($this->filename)) {
			throw new Exception('Cannot open file '.$this->filename);
		}

		$this->info('opening '.$this->filename);

		if ($this->handle = fopen($this->filename, 'r')) {
			$numberOfRecords = 0;
			fseek($this->handle, 0);
			while (($row = fgetcsv($this->handle, 0, ',', '"')) !== false) {
				$numberOfRecords++;
			}
			// First row contains field names
			$numberOfRecords--;

			fseek($this->handle, 0);

			$this->csvFieldNames = fgetcsv($this->handle, 0, ',', '"');

			$this->numberOfRecords = $numberOfRecords;
			$this->numberOfImportedRows = 0;
			$this->newEntries = 0;
			$this->existingEntries = 0;

			return true;
		}
	}

	protected function closeCSV() {
		if ($this->handle) {
			fclose($this->handle);
		}
	}

	private function getRow() {
		return fgetcsv($this->handle, 0, ',', '"');
	}

	protected function getImportFieldValues($data) {
		$values = array();
		for ($i = 0; $i < count($data); $i++) {
			if ($i < count($this->importFieldConfig)) {
				$item = &$this->importFieldConfig[$i];
				if (!(isset($item['meta']) && $item['meta'])) {
					$value = $data[$i];
					if ($this->importFieldFunctions[$i]) {
						$value = $this->{$this->importFieldFunctions[$i]}($value, $data);
					}
					$values[$this->importFieldConfig[$i]['field']] = $value;
				}
			}
		}

		return $values;
	}

	protected function getUpdateValues($data) {
		$values = array_merge($data);
		for ($i = 0; $i < count($this->importFieldConfig); $i++) {
			$item = &$this->importFieldConfig[$i];
			if (isset($item['skipUpdate']) && $item['skipUpdate']) {
				unset($values[$item['field']]);
			}
		}
		return $values;
	}

	protected function getSuperValues($data) {
		$values = array();
		foreach ($this->propagateToSuperEntityFields as $field) {
			if (isset($data[$field])) {
				$values[$field] = $data[$field];
			}
		}

		return $values;
	}

	private $verbose = false;

	public function setVerbose($verbose) {
		$this->verbose = $verbose;
	}

	protected function info() {
		if ($this->verbose) {
			if (func_num_args() == 1) {
				echo func_get_arg(0);
			} else {
				$args = func_get_args();
				echo call_user_func_array('sprintf', $args);
			}
			echo "\n";
		}
	}
}