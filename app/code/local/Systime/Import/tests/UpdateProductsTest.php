<?php
require_once 'PHPUnit/Framework.php';

class UpdateProductsTest extends PHPUnit_Framework_TestCase {
	public function testUpdate() {
// 		$data = array(
// 									array('sku', 'version', 'name', 'isbn_10', 'isbn_13', 'release_date_estimated', 'release_date_confirmed', 'authors', 'projectmanager', 'price'),
// 									array('hest-77834518', '187', __FILE__.':'.__LINE__, 'isbn_10', 'isbn_13', '19750523', '20090523', 'Mikkel Ricky', 'Redaktør', '12.85'),
// 									array('hest-12345', '187', __FILE__.':'.__LINE__, 'isbn_10', 'isbn_13', '19750523', '20090523', 'Mikkel Ricky', 'Redaktør', '128.5'),
// 									array('61624918', 1, 'Begrebsapparatet - til AT', '8761624918', '9788761624918', '', '', 'Lars Tonnesen m.fl.', 'Stefan Emkjær', 8.85),
// 									);

// 		$csvLines = array();
// 		foreach ($data as $item) {
// 			$csvLine = '"'.implode('","', $item).'"';
// 			$csvLines[] = $csvLine;
// 		}

// 		print_r(array('csvLines' => $csvLines));

// 		$csv = implode("\r\n", $csvLines)."\r\n";

// 		print_r(array('csv' => $csv));

		$importUrl = 'http://store.mikkelricky.systime.dk/systimeimport/import/product/?verbose=true';
		// $importUrl = 'http://devshop.systime.dk/systimeimport/import/product/?verbose=true';
		// $importUrl = 'http://www.systime.dk/systimeimport/import/product/?verbose=true';

		$importUrl = 'http://new.systime.dk/systimeimport/import/product/?verbose=true';

		// $importUrl .= '/verbose/true';

		echo $importUrl, "\n";

		$importFilenames = array(dirname(__FILE__).'/export-product-from-mm.csv');

// 		$importFilenames = glob('/Users/mri/Documents/remote/mm2magento/*[0-9].csv');
// 		$importFilenames = glob('/Users/mri/Documents/remote/mm2magento/via-publication-export-from-mm.csv');

// 		print_r($importFilenames);
// 		die(__METHOD__);


		foreach ($importFilenames as $importFilename) {
			echo $importFilename, "\n";

			$csv = file_get_contents($importFilename);

			// 		echo $csv;
			// 		die(__METHOD__);



			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $importUrl);
			curl_setopt($ch, CURLOPT_HEADER, false);

			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $csv);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);

			print_r(array('response' => $response));

			break;
		}
	}
}
?>
