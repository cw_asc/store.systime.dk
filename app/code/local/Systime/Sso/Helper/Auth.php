<?php
/**
 * Systime Single Sign-On helper
 *
 * @category   Systime
 * @package    Systime_Sso
 */
class Systime_Sso_Helper_Auth //extends Mage_Core_Helper_Abstract
extends Systime_Sso_Helper_Data
{
	private static $cookieDomain = '.systime.dk';

// 	private static $cookieNames = array(
// 																			'userid' => 'systime-userID',
// 																			'usergroup' => 'systime-usergroup',
// 																			'token' => 'systime-GUID',
// 																			);
// 	private static $paramNames = array(
// 																		 'userid' => 'userID',
// 																		 'token' => 'GUID',
// 																		 'username' => 'username',
// 																		 'password' => 'password',
// 																		 );

	private static $cookieNames = array(
																			'userid' => 'ekeys-userid',
																			'usergroup' => 'ekeys-usergroup',
																			'token' => 'ekeys-token',
																			);
	private static $paramNames = array(
																		 'userid' => 'userid',
																		 'token' => 'token',
																		 'username' => 'username',
																		 'password' => 'password',
																		 );

	public function login($username, $password) {
		$params = array(
										'action' => 'login',
										self::$paramNames['username'] => $username,
										self::$paramNames['password'] => $password,
										);
		$response = $this->sendRequest($params);
		if (strpos($response, '1:') === 0) {
			$this->setLoginCookies();

			// $data = $this->getUserData($response);

			return $this->createOrUpdateCustomer($response);
		}

		return false;
	}

	private function getUserData($info) {
		echo $info, "\n";
		die(__METHOD__);
	}

	/**
	 *
	 * @return Systime_Sso_Model_Customer
	 */
	private function createOrUpdateCustomer($info) {
		$tokens = explode(':', $info);
		if (count($tokens) >= 4) {
			list($ok, $email, $firstname, $lastname) = $tokens;
			if ($ok) {
				$systimesso_userid = $this->cookies[self::$cookieNames['userid']];

				$data = array(
											'email' => $email,
											'firstname' => $firstname,
											'lastname' => $lastname,
											'password' => uniqid(),
											'group_id' => 1,
											'website_id' => 1,
											);

				// Get user group
				$systimesso_usergroups = isset($this->cookies[self::$cookieNames['usergroup']]) ? explode('|', $this->cookies[self::$cookieNames['usergroup']]) : array();
				foreach ($systimesso_usergroups as $usergroup) {
					if (intval($usergroup) > 0) {
						$data['group_id'] = intval($usergroup)+1;
						break;
					}
				}

				// die(__METHOD__.':'.__LINE__.': '.print_r(array($systimesso_userid, $data), true));

				$existingCustomer = $this->getCustomer($systimesso_userid, $email);

				// die(__METHOD__.':'.__LINE__.': '.print_r(array($systimesso_userid, $data), true));

				$customer = Mage::getModel('customer/customer');

				if ($existingCustomer) {
					$customer->load($existingCustomer['entity_id']);
				}

				foreach ($data as $key => $value) {
					$customer->setData($key, $value);
				}

				try {
					$customer->save();
					// echo 'New customer: '.$customer->getId();

					// print_r(array('existingCustomer' => $existingCustomer));

					// Update systimesso_userid in database
					if (!$existingCustomer || ($existingCustomer['systimesso_userid'] != $systimesso_userid)) {
						// Set systimesso_userid
						$table = 'customer_entity';
						$data = array('systimesso_userid' => $systimesso_userid);
						$where = 'entity_id = '.intval($customer->getId());
						$db = Mage::getSingleton('core/resource')->getConnection('core_read');
						$db->update($table, $data, $where);
					}

					return $customer;
				} catch (Exception $ex) {
					$this->sendDeveloperMessage(__METHOD__, $ex);
					// Mage::logException($ex);
					// throw new Exception('Cannot create or update user ('.$email.', '.$systimesso_userid.')', 0);
				}
			}
		}
	}

	/**
	 * Get customer by sso userid or email
	 *
	 * @param string $systimesso_userid
	 * @param string $email
	 *
	 * @return array
	 */
	private function getCustomer($systimesso_userid, $email) {
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('catalog_read');

		// Get customer by systimesso_userid
		$select = $read->select()
			->from($resource->getTableName('customer/entity'))
			->where('systimesso_userid = ?', $systimesso_userid)
			;
		$customer = $read->fetchRow($select);

		if (!$customer) {
			// Get customer by email
			$select = $read->select()
				->from($resource->getTableName('customer/entity'))
				->where('email = ?', $email)
				;
			$customer = $read->fetchRow($select);
		}

		return $customer;
	}

	public function logout() {
		if ($this->hasLoginCookies()) {
			$params = array(
											'action' => 'logout',
											self::$paramNames['userid'] => $_COOKIE[self::$cookieNames['userid']],
											self::$paramNames['token'] => $_COOKIE[self::$cookieNames['token']],
											);
			$response = $this->sendRequest($params);

			if ($response == '1') {
				$this->clearLoginCookies();
				return true;
			}

			return false;
		}
	}

	/**
	 * @return Systime_Sso_Model_Customer
	 */
	public function checkLogin() {
		if ($this->hasLoginCookies()) {
			$params = array(
											'action' => 'checklogin',
											self::$paramNames['userid'] => $_COOKIE[self::$cookieNames['userid']],
											self::$paramNames['token'] => $_COOKIE[self::$cookieNames['token']],
											);


			$response = $this->sendRequest($params);
// 			debug($response, __METHOD__);
// 			debug($this->cookies, __METHOD__);

			if (strpos($response, '1') === 0) {
				return $this->createOrUpdateCustomer($response);
			}
		}
	}

	/**
	 * Check that needed login cookies are set. Also copies them to $this->cookies
	 */
	private function hasLoginCookies() {
		$this->cookies = array();
		foreach (self::$cookieNames as $key => $name) {
			if (!isset($_COOKIE[$name])) {
				return false;
			} else {
				$this->cookies[self::$cookieNames[$key]] = $_COOKIE[$name];
			}
		}

		// debug($this->cookies, __METHOD__);

		return true;
	}

	private function setLoginCookies() {
		foreach (self::$cookieNames as $key => $name) {
			$value = isset($this->cookies[$name]) ? $this->cookies[$name] : '';
			setcookie($name, $value, 0, '/', self::$cookieDomain);
		}
	}

	private function clearLoginCookies() {
		foreach (self::$cookieNames as $name) {
			setcookie($name, '', time()-3600, '/', self::$cookieDomain);
		}
		$this->cookies = array();
	}

	private $cookies = array();
	private $response = null;

	/**
	 * Send a get request and store response cookies for later use
	 */
	private function sendRequest($params) {
		$authUrl = $this->getConfig('auth_url');
		if ($authUrl) {
			$url = $this->buildUrl($authUrl, $params);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$response = curl_exec($ch);
			curl_close($ch);

			$this->log(array('url' => $url, 'response' => $response));

			$lines = explode("\n", trim($response));

			// print_r($lines);

			if (!is_array($this->cookies)) {
				$this->cookies = array();
			}
			$cookiePattern = '@^Set-Cookie:\s*(ekeys-[^=]+)=([^;]+)@';
			foreach ($lines as $line) {
				if (preg_match($cookiePattern, $line, $matches)) {
					$this->cookies[$matches[1]] = urldecode($matches[2]);
				}
			}

			$this->response = $lines[count($lines)-1];

			return $this->response;
		}
	}

// 	public function getCookies() {
// 		return $this->cookies;
// 	}

	// private static $authUrl = 'http://mit.systime.dk/services/AccessControl.aspx';
	// private static $authUrl = 'http://ekeys.mikkelricky.systime.dk/?eID=tx_systimeekeys_auth';

}
