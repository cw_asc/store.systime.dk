<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_GoogleAnalytics
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * GoogleAnalytics data helper
 *
 * @category   Mage
 * @package    Mage_GoogleAnalytics
 */
class Systime_Sso_Helper_Data extends Mage_Core_Helper_Abstract {
	public function getConfig($key, $category = 'general') {
		return Mage::getStoreConfig('systimesso/'.$category.'/'.$key);
	}

	public function buildUrl($url, $params) {
		$query = '';

		foreach ($params as $name => $value) {
			if ($query) {
				$query .= '&';
			}
			$value = utf8_encode($value);
			$query .= urlencode($name).'='.urlencode($value);
		}

		return $url.((strpos($url, '?') !== false) ? '&' : '?').$query;
	}

	public function getContinueUrl($url) {
		$confValue = $this->getConfig($url);
		if ($confValue) {
			$url = $confValue;
		}

		$secure = isset($_SERVER['HTTPS']) || $_SERVER['SERVER_PORT']=='443';
		$scheme = ($secure ? 'https' : 'http') . '://' ;
		$requestUrl = $scheme.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

		// return $this->buildUrl($url, array('continue' => $requestUrl));
		return $this->buildUrl($url, array('redirect_url' => $requestUrl));
	}

	public function log($message, $level=null) {
		if ((bool)$this->getConfig('write_log')) {
			Mage::log($message, $level, 'systime_sso');
		}
	}

	/**
	 * Send developer message by e-mail and write log
	 *
	 * @param Exception $ex
	 * @param string $message
	 */
	public function sendDeveloperMessage($ex, $message=null) {
		$trace = debug_backtrace();
		$frame = $trace[1];
// 		var_dump($frame);
// 		die(__METHOD__);

		$content = implode("\n\n", array(
																	 $message ? $message : '',
																	 implode(':', array($frame['file'], $frame['line'], $frame['function'])),
																	 //$ex->getTraceAsString(),
																	 ));

		$this->log($content, Zend_Log::ERR);

		if ($developerEmail = $this->getConfig('developer_email')) {
			$emails = explode(',', $developerEmail);
			foreach ($emails as $email) {
				// mail(..., ..., ...);
			}
		}
	}
}
