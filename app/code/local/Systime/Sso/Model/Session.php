<?php
	/**
	 * Customer session model
	 *
	 * @category   Systime
	 * @package    Systime_Sso
	 * @author      Mikkel Ricky <mri@systime.dk>
	 */
class Systime_Sso_Model_Session extends Mage_Customer_Model_Session {

// 	public function __construct() {
// 			error_log(__METHOD__);

// 			parent::__construct();
// 	}

	private function isEnabled() {
		// return false;
		return (bool)Mage::helper('systimesso')->getConfig('is_enabled');
	}

	private $isLoggedIn_count = 0;

	public function isLoggedIn() {
// 		$this->getCheckoutSession()->setUseNotice(true);
// 		Mage::throwException(__METHOD__);

		// Authenticate only one time!
		$this->isLoggedIn_count++;
		if ($this->isLoggedIn_count == 1) {
			// Get sso customer
			if ($this->isEnabled()) {
				$helper = Mage::helper('systimesso/auth');
				$customer = $helper->checkLogin();

				if ($customer) {
					if ($customer->getId() != $this->getCustomer()->getId()) {
						$this->setCustomerAsLoggedIn($customer);
					}
				} else {
					// Log out any logged in customer
					if ($this->getCustomer()->getId()) {
						// Verbatim copy from parent::logout to prevent recursion
						Mage::dispatchEvent('customer_logout', array('customer' => $this->getCustomer()));
						$this->setId(null);
					}
				}

				/*
				// var_dump($this->getId());
				$customerId = $customer ? $customer->getId() : 0;
				$customerHasChanged = $this->getId() != $customerId;

				$isLoggedIn = parent::isLoggedIn();
				if ($isLoggedIn && !$customerHasChanged) {
					if (!$customer) {
						// Verbatim copy from parent::logout to prevent recursion
						Mage::dispatchEvent('customer_logout', array('customer' => $this->getCustomer()) );
						$this->setId(null);
					}
				} else {
					if ($customer) {
						// error_log(__METHOD__.': '.$customer->getId());
						$this->setCustomerAsLoggedIn($customer);
					}
				}
				*/
			}
		}

		return parent::isLoggedIn();
	}


	/**
	 * Set customer object and setting customer id in session
	 *
	 * @param   Mage_Customer_Model_Customer $customer
	 * @return  Mage_Customer_Model_Session
	 */
	public function setCustomer(Mage_Customer_Model_Customer $customer) {
		if ($this->isEnabled()) {
			$helper = Mage::helper('systimesso/auth');
			if (!$customer->getId()) {
				$c = $helper->checkLogin();

				if ($c) {
					// print_r($c->getId());

					// Check cookies and log customer in

					$customer->load($c->getId());
					// error_log(__METHOD__.': '.print_r($customer, true));
					// error_log(__METHOD__.': '.$customer->getId());

					$this->setCustomerAsLoggedIn($customer);
				}
			}
		}

		return parent::setCustomer($customer);
	}
}
