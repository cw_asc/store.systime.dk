<?php
/*                                                                        *
 * This script is part of the TypoGento project 						  *
 *                                                                        *
 * TypoGento is free software; you can redistribute it and/or modify it   *
 * under the terms of the GNU General Public License version 2 as         *
 * published by the Free Software Foundation.                             *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */

/**
 * TypoGento Customer Model
 *
 * @version $Id: Customer.php 19 2008-11-25 17:50:44Z weller $
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */
class Systime_Sso_Model_Customer extends Mage_Customer_Model_Customer {
// 	public function __construct() {
// 		error_log(__METHOD__);
// 		parent::__construct();

// 		// echo __METHOD__;
// 	}

	public function authenticate($login, $password) {
		// error_log(__METHOD__);
		// return parent::authenticate($login, $password);

		$helper = Mage::helper('systimesso/auth');

		$customer = $helper->login($login, $password);

		// error_log(__METHOD__.': '.print_r(array($login, $password), true));

		if ($customer) {
			// error_log(__METHOD__.': customer id: '.$customer->getId());

			$this->load($customer->getId());

			return true;
		}

		throw new Exception(Mage::helper('customer')->__('Invalid login or password.'), self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD);

// 		if ($this->systimeAuthenticate($login, $password)) {
//         $this->loadByEmail($login);
// 		}

		return false;
	}

	private function systimeAuthenticate($username, $password) {
//  		$login = 'mri@systime.dk';
//  		$password = 'mikkelricky';

		$helper = Mage::helper('systimesso/auth');

		$customer = $helper->login($username, $password);

		if ($customer) {

		}

		if ($response != 0) {
			$info = explode(':', $response);
			$ok = $info[0];

			if ($ok && isset($this->cookies['systime-userID'])) {
				$personName = $info[1];
			list($firstname, $lastname) = explode(' ', $personName);

			$systimesso_userid = $this->cookies['systime-userID'];

			$email = $username;

			$data = array(
										'email' => $email,
										'firstname' => $firstname,
										'lastname' => $lastname,
										'password' => uniqid(),
										);

			// throw new Exception(print_r(array($systimesso_userid, $data), true), self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD);

			$existingCustomerId = false;

			$db = Mage::getSingleton('core/resource')->getConnection('core_read');
			$result = $db->query('select entity_id from customer_entity where systimesso_userid = :systimesso_userid', array('systimesso_userid' => $systimesso_userid));
			if ($result) {
				$row = $result->fetch(PDO::FETCH_ASSOC);
				$existingCustomerId = intval($row['entity_id']);
				// debug($row, __FILE__.':'.__LINE__);
			}

			$customer = Mage::getModel('customer/customer');

			if ($existingCustomerId) {
				$customer->load($existingCustomerId);
			}
			// echo get_class($customer), "\n";

			// print_r(get_class_methods($customer));

			foreach ($data as $key => $value) {
				$customer->setData($key, $value);
			}
			// $customer->setPlainPassword(uniqid());

// 			$ok = $customer->validate();
// 			if (!$ok) {
// 				debug($ok, __FILE__.':'.__LINE__);
// 			}

			// var_dump($customer);

			try {
				$customer->save();
				// echo 'New customer: '.$customer->getId();

				if (!$existingCustomerId) {
					// Set systimesso_userid
					$table = 'customer_entity';
					$data = array('systimesso_userid' => $systimesso_userid);
					$where = 'entity_id = '.intval($customer->getId());
					$db->update($table, $data, $where);

// 					debug(array(
// 											'table' => $table,
// 											'data' => $data,
// 											'where' => $where,
// 											), __FILE__.':'.__LINE__);

				}

				$helper = Mage::helper('systimesso')->setSsoCookies($this->cookies);

				return true;
			} catch (Exception $ex) {
				throw $ex;
			}
			}
		}

		throw new Exception(__METHOD__.':'.__LINE__, self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD);
		// throw new Exception(Mage::helper('customer')->__('Invalid login or password.'), self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD);
	}
}

?>