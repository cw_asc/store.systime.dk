<?php
$installer = $this;

$installer->startSetup();

$db = $installer->getConnection();

$tableName = 'customer_entity';
$fieldName = 'systimesso_userid';
$definition = 'VARCHAR(100) NOT NULL';
$actualTableName = $installer->getTable($tableName);
if ($db->tableColumnExists($actualTableName, $fieldName)) {
	$sql = 'ALTER TABLE `'.$actualTableName.'` CHANGE `'.$fieldName.'` `'.$fieldName.'` '.$definition;
	$installer->run($sql);
} else {
	$db->addColumn($actualTableName, $fieldName, $definition);
}

$installer->endSetup();
