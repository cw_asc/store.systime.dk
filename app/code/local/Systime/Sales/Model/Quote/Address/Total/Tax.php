<?php
class Systime_Sales_Model_Quote_Address_Total_Tax extends Mage_Sales_Model_Quote_Address_Total_Tax {
	public function collect(Mage_Sales_Model_Quote_Address $address) {
		$store = $address->getQuote()->getStore();
		$addTaxWithTaxvat = (bool)Mage::getStoreConfig('systimesales/general/add_tax_with_taxvat', $store);

		if ($address->getQuote()) {
			$billingAddress = $address->getQuote()->getBillingAddress();

			if (!$addTaxWithTaxvat && $this->hasValidVATNumber($billingAddress)) {
				$address->setTaxAmount(0);
				$address->setBaseTaxAmount(0);
				$address->setAppliedTaxes(array());

				return $this;
			}
		}

		return parent::collect($address);
	}

	protected function hasValidVATNumber($address) {
		$isValid = false;

		if (($address->getAddressType() == 'billing')
				&& trim($address->getData('systimesales_taxvat'))
				&& ($address->getData('country_id') && ($address->getData('country_id') != 'DK'))) {
			$isValid = true;
		}

		return $isValid;
	}
}
