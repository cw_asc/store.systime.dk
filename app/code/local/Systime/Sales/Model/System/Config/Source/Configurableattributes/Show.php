<?php
class Systime_Sales_Model_System_Config_Source_Configurableattributes_Show {
	public function toOptionArray() {
		$collection = Mage::getResourceModel('eav/entity_attribute_collection')
			->setEntityTypeFilter(Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId())
			// ->addVisibleFilter()
			;

		$options = array();

		$options[] = array(
											 'value' => '',
											 'label' => '',
											 );

		foreach ($collection as $attribute) {
			if (($attribute->getIsGlobal() == Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL)
					&& $attribute->getIsVisible()
					&& $attribute->getIsConfigurable()
					&& $attribute->usesSource()) {
				$options[] = array(
													 'value' => $attribute->getAttributeCode(),
													 'label' => $attribute->getFrontendLabel().' ('.$attribute->getAttributeCode().')',
													 );
			}
		}

		return $options;
	}
}