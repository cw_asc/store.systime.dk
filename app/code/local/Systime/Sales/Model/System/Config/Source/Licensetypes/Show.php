<?php
class Systime_Sales_Model_System_Config_Source_Licensetypes_Show {
	private static $attributeCode = 'licens_type';

	public function toOptionArray() {
		$collection = Mage::getModel('eav/entity_attribute_option')->getCollection()
			->setStoreFilter()
			// ->addVisibleFilter()
			->join('attribute', 'attribute.attribute_id=main_table.attribute_id', 'attribute_code')
			;

		$options = array();

		$options[] = array(
											 'value' => 0,
											 'label' => '',
											 );

		foreach ($collection as $attribute) {
			if (self::$attributeCode == $attribute->getAttributeCode()) {
				$name = $attribute->getFrontendLabel() ? $attribute->getFrontendLabel() : $attribute->getValue();
				$options[] = array(
													 'value' => $attribute->getId(),
													 'label' => $name.' ('.$attribute->getValue().')',
													 );
			}
		}

		return $options;
	}
}
