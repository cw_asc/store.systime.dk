<?php
class Systime_Sales_Model_System_Config_Source_Orderstatuses_Show {
	public function toOptionArray() {
		$statuses = Mage::getSingleton('sales/order_config')->getStateStatuses(array(
																																								 Mage_Sales_Model_Order::STATE_NEW,
																																								 Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
																																								 Mage_Sales_Model_Order::STATE_PROCESSING,
																																								 Mage_Sales_Model_Order::STATE_COMPLETE,
																																								 Mage_Sales_Model_Order::STATE_CLOSED,
																																								 Mage_Sales_Model_Order::STATE_CANCELED,
																																								 Mage_Sales_Model_Order::STATE_HOLDED,
																																								 ));
		$options = array();

		$options[] = array(
											 'value' => 0,
											 'label' => '',
											 );

		foreach ($statuses as $value => $label) {
			$options[] = array(
												 'value' => $value,
												 'label' => $label,
												 );
		}

		return $options;
	}
}
