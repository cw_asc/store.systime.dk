<?php
class Systime_Sales_Model_System_Config_Source_Cms_block {
	protected $_options;

	public function toOptionArray() {
		if (!$this->_options) {
			$this->_options = Mage::getResourceModel('cms/block_collection')
				// ->addAttributeToSort('title', 'asc')
				->load()->toOptionArray();
			array_unshift($this->_options, array('value'=>'', 'label' => ''));
		}
		return $this->_options;
	}
}
