<?php
class Systime_Sales_Model_Mysql4_Setup extends Mage_Sales_Model_Mysql4_Setup {
	public function getDefaultEntities() {
		return array(
			'quote_address' => array(
				'entity_model' => 'sales/quote_address',
				'table' => 'sales/quote_address',
        'attributes' => array(
					'systimesales_ean' => array('type' => 'static'),
				),
			),

      'order_address' => array(
        'entity_model' => 'sales/order_address',
        'table'=>'sales/order_entity',
        'attributes' => array(
          'systimesales_ean' => array(),
				),
			),
		);
	}
}