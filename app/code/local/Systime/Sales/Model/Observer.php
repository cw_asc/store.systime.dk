<?php
class Systime_Sales_Model_Observer {
	public function sales_convert_quote_payment_to_order_payment($observer) {
		$order = $observer->getEvent()->getOrderPayment();
    $quote = $observer->getEvent()->getQuotePayment();

		$order->setSystimepaymentInvoiceEan($quote->getSystimepaymentInvoiceEan());
		$order->setSystimepaymentInvoiceAttention($quote->getSystimepaymentInvoiceAttention());
		$order->setSystimepaymentInvoiceOrderref($quote->getSystimepaymentInvoiceOrderref());
		$order->setSystimepaymentInvoiceIko($quote->getSystimepaymentInvoiceIko());

		// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceEan()));
		// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceAttention()));
		// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceOrderref()));
		// 		error_log(__METHOD__.': '.($quote->getSystimepaymentInvoiceIko()));
	}

	public function sales_convert_quote_item_to_order_item($observer) {
		$orderItem = $observer->getOrderItem();
		$item = $observer->getItem();

		// Add Systime Item Number (sku+version)

		// 		error_log(__METHOD__.': '.print_r(array(
		// 																						'item' => $item->getData(),
		// 																						'orderItem' => $orderItem->getData(),
		// 																						), true));

		$product = Mage::getModel('catalog/product')->load($item->getProductId());
		if ($product) {
			// Default version
			$version = 1;
			$sku = $product->getSku();
			$version = intval($product->getVersion());
			// Handle Ajour order in a special way ...
			if (preg_match('/-ajour$/', $sku)) {
				$sku = preg_replace('/-ajour$/', '', $sku);
				$version = 50;
			}
			if ($version < 1) {
				$version = 1;
			}
			$itemNumber = $sku.sprintf('%02d', $version);

			// error_log(__METHOD__.': '.print_r(array('sku' => $sku, 'version' => $version, 'itemNumber' =>  $itemNumber), true));

			$orderItem->setSystimesalesItemnumber($itemNumber);
		}
	}

	public function sales_convert_quote_address_to_order($observer) {
		$address = $observer->getAddress();
		$order = $observer->getOrder();

		// 		error_log(__METHOD__.': '.print_r(array(
		// 																						'address' => $address->getData(),
		// 																						), true));
	}

	private static $customBillingAddressAttributes = array('systimesales_ean', 'systimesales_iko', 'systimesales_oioattention', 'systimesales_oiorequisnumber', 'systimesales_taxvat');
	private static $customShippingAddressAttributes = array();

	public function sales_convert_quote_address_to_order_address($observer) {
		$address = $observer->getAddress();
		$orderAddress = $observer->getOrderAddress();

		if ($address->getAddressType() == 'shipping') {
			$customAttributes = array_diff(self::$customBillingAddressAttributes, self::$customShippingAddressAttributes);
			foreach ($customAttributes as $attributeName) {
				$orderAddress->unsetData($attributeName);
			}
		}

		// 		error_log(__METHOD__.': '.print_r(array(
		// 																						'address' => $address->getData(),
		// 																						'orderAddress' => $orderAddress->getData(),
		// 																						), true));
	}

	public function catalog_product_save_after($observer) {
		$helper = Mage::helper('systimesales');
		if ($attributeList = $helper->getConfig('update_configured_attribute_prices_on_save')) {
			$product = $observer->getEvent()->getProduct();
			$attributeCodes = array_map(create_function('$s', 'return trim($s);'), explode(',', $attributeList));
			foreach ($attributeCodes as $attributeCode) {
				self::updateConfigurablePrices($product, $attributeCode);
			}
		}
	}

	/**
	 * Update configured prices on product from actual prices on child products
	 *
	 * @param Mage_Catalog_Model_Product $product
	 * @param string $attributeCode
	 *
	 * @return ???
	 */
	public static function updateConfigurablePrices(Mage_Catalog_Model_Product $product, /*string*/ $attributeCode) {
		if ($product) {
			$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
			$priceAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'price');

			$resource = Mage::getSingleton('core/resource');
			$db = $resource->getConnection('core_read');

			// Get parent (super) and child products
			$select = $db->select()
				->from(array('super_link' => $resource->getTableName('catalog/product_super_link')))
				->join(array('parent' => $resource->getTableName('catalog/product')),
							 'parent.entity_id = super_link.parent_id',
							 array('parent_sku' => 'parent.sku')
							 )
				->join(array('child' => $resource->getTableName('catalog/product')),
							 'child.entity_id = super_link.product_id',
							 array('child_sku' => 'child.sku')
							 )
				->join(array('catalog_product_super_attribute' => $resource->getTableName('catalog/product_super_attribute')),
							 $db->quoteInto('catalog_product_super_attribute.product_id = parent.entity_id'
															.' and catalog_product_super_attribute.attribute_id = ?',
															$attribute->getId()),
							 'product_super_attribute_id'
							 )
				// join with child price
				->join(array($attributeCode => $resource->getTableName('catalog_product_entity_'.$attribute->getBackendType())),
							 $db->quoteInto($attributeCode.'.entity_id = child.entity_id'
															.' and '.$attributeCode.'.attribute_id = ?',
															$attribute->getAttributeId()),
							 array($attributeCode => $attributeCode.'.value')
							 )
				// join with child price
				->join(array('price' => $resource->getTableName('catalog_product_entity_'.$priceAttribute->getBackendType())),
							 $db->quoteInto('price.entity_id = child.entity_id'
															.' and price.attribute_id = ?',
															$priceAttribute->getId()),
							 array('pricing_value' => 'price.value')
							 )
				->where('parent.entity_id = ?', $product->getId())
				->order(array('pricing_value', $attributeCode))
				;

			// echo $select->assemble(), "\n";

			$childProducts = $db->fetchAll($select);

			// Don't do anything if no child products exist
			if (count($childProducts) == 0) {
				return;
			}

			// Get configured prices on parent (super) product
			$select = $db->select()
				->from(array('product' => $resource->getTableName('catalog/product')), array('sku', 'product_id' => 'product.entity_id'))
				->join(array('product_super_attribute' => $resource->getTableName('catalog/product_super_attribute')),
							 'product_super_attribute.product_id = product.entity_id')
				->join(array('product_super_attribute_pricing' => $resource->getTableName('catalog/product_super_attribute_pricing')),
							 'product_super_attribute_pricing.product_super_attribute_id = product_super_attribute.product_super_attribute_id',
							 array('pricing_value',
										 'value_id',
										 $attributeCode => 'value_index',
										 ))
				->where('product.entity_id = ?', $product->getId())
				->where('product_super_attribute.attribute_id = ?', $attribute->getId())
				->order(array('pricing_value', 'value_index'))
				;

			// echo $select->assemble(), "\n";

			$configurableAttributesData = array(
																					'id' => null,
																					'label' => $attribute->getFrontendLabel(),
																					'position' => 0,
																					'values' => array(),
																					'attribute_id' => $attribute->getId(),
																					'attribute_code' => $attribute->getAttributeCode(),
																					'frontend_label' => $attribute->getFrontendLabel(),
																					);

			// Map from attribute value to 'value_id' used for adding 'value_id' later
			$valueIds = array();

			$rows = $db->fetchAll($select);
			foreach ($rows as $row) {
				$valueIds[$row[$attributeCode]] = $row['value_id'];
			}

			foreach ($childProducts as $row) {
				$configurableAttributesData['id'] = $row['product_super_attribute_id'];
				$value = array(
											 'product_super_attribute_id' => $row['product_super_attribute_id'],
											 'value_index' => $row[$attributeCode],
											 // 'label' => '???', // lærer/klasselicens
											 // 'default_label' => '???', // lærer/klasselicens
											 // 'store_label' => '???', //lærer/klasselicens
											 'is_percent' => 0, // ???
											 'pricing_value' => $row['pricing_value'],
											 'use_default_value' => 1,
											 );
				if (isset($valueIds[$row[$attributeCode]])) {
					$value['value_id'] = $valueIds[$row[$attributeCode]];
				}
				$configurableAttributesData['values'][] = $value;
			}

			try {
				$id = isset($configurableAttributesData['id']) ? $configurableAttributesData['id'] : null;
				Mage::getModel('catalog/product_type_configurable_attribute')
					->setData($configurableAttributesData)
					->setId($id)
					->setStoreId($product->getStoreId())
					->setProductId($product->getId())
					->save()
					;
			} catch (Exception $ex) {
				Mage::logException($ex);
			}
		}
	}
}
