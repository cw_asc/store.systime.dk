<?php

class Systime_Sales_Helper_Cart extends Mage_Sales_Helper_Data {
	public static $licenseTypeAttributeCode = 'licens_type';
	public static $publicationStatusAttributeCode = 'publication_status';

	public function getConfig($key, $storeId=null, $category = 'general') {
		return Mage::getStoreConfig('systimesales/'.$category.'/'.$key, $storeId);
	}

	/**
	 * Get warnings about items in cart that cannot be bought by "private" customers
	 */
	public function getInvalidItemsPrivate($items) {
		$warnings = array();


		// License types
		$invalidAttributeCodes = $this->getIntArray($this->getConfig('invalid_license_types_private', null, 'checkout'));

		$attributeCode = self::$licenseTypeAttributeCode;
		foreach ($items as $item) {
			$product = $item->getProduct();
			if ($product->isConfigurable()) {
				$child = $this->getChildProduct($item);

				if ($child && in_array($child->getData($attributeCode), $invalidAttributeCodes)) {
					if (!isset($warnings[$attributeCode])) {
						$messages = array();
						$warnings[$attributeCode] = &$messages;
					} else {
						$messages = &$warnings[$attributeCode];
					}

					$obj = new Varien_Object();
					$obj->setProduct($product);
					$obj->setChildProduct($child);
					$obj->setData($attributeCode, $child->getData($attributeCode));

					// var_dump(array(__METHOD__, $this->getProductAttributes($product)));

					$messages[] = $obj;
				}

				// echo implode(', ', array(
				// 												 $_product->getSku(),
				// 												 $_child->getSku(),
				// 												 $_child->getData($_attributeCode),
				// 												 )),
				// 	'<br/>';
				// print_r(array_keys($_child->getData()));
			}
		}


		// Publication statuses
		$invalidAttributeCodes = $this->getIntArray($this->getConfig('invalid_publication_statuses_private', null, 'checkout'));

		$attributeCode = self::$publicationStatusAttributeCode;
		foreach ($items as $item) {
			$product = $item->getProduct();
			if ($product) {
				$product->load($product->getId());
				if (in_array($product->getData($attributeCode), $invalidAttributeCodes)) {
					if (!isset($warnings[$attributeCode])) {
						$messages = array();
						$warnings[$attributeCode] = &$messages;
					} else {
						$messages = &$warnings[$attributeCode];
					}

					$obj = new Varien_Object();
					$obj->setProduct($product);
					// $obj->setChildProduct($product);
					$obj->setData($attributeCode, $child->getData($attributeCode));

					// var_dump(array(__METHOD__, $this->getProductAttributes($product)));

					$messages[] = $obj;
				}
			}
		}

		return $warnings;
	}


	public function getProductAttributes($product) {
		$attributes = $product->getTypeInstance(true)
			->getSelectedAttributesInfo($product);
		return $attributes;
	}

	/**
	 * Get configurable (product) item option
	 */
	public function getChildProduct($item) {
		$child = null;

		if ($item->getProduct()->isConfigurable()) {
			if ($option = $item->getOptionByCode('simple_product')) {
				$child = $option->getProduct();
				// Load product to make all attributes available
				$child->load($child->getId());
			}
		}

		return $child;
	}

	private function getIntArray($list) {
		$value = array();

		if ($list) {
			foreach (explode(',', $list) as $token) {
				$value[] = intval($token);
			}
		}

		return $value;
	}
}
