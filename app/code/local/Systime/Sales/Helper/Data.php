<?php

class Systime_Sales_Helper_Data extends Mage_Sales_Helper_Data {
	public static $licenseTypeAttributeCode = 'licens_type';
	public static $publicationStatusAttributeCode = 'publication_status';

	public function getConfig($key, $storeId=null, $category = 'general') {
		return Mage::getStoreConfig('systimesales/'.$category.'/'.$key, $storeId);
	}

	public function isSchoolOnlyProduct($product, $item=null) {
		if ($product) {
			// Load product to make all attributes available
			$product->load($product->getId());

			// Check license types
			$attributeCode = self::$licenseTypeAttributeCode;
			$schoolOnlyAttributeCodes = $this->getIntArray($this->getConfig('school_only_license_types', null, 'checkout'));

			if ($product->isConfigurable()) {
				$child = $this->getItemChildProduct($item);

				if ($child && in_array($child->getData($attributeCode), $schoolOnlyAttributeCodes)) {
					return $attributeCode;
				}
			}

			// Check publication status
			$attributeCode = self::$publicationStatusAttributeCode;
			$schoolOnlyAttributeCodes = $this->getIntArray($this->getConfig('school_only_publication_statuses', null, 'checkout'));

			if (in_array($product->getData($attributeCode), $schoolOnlyAttributeCodes)) {
				return $attributeCode;
			}
		}

		return null;
	}

	/**
	 * Decide if an item's product can only be purchased by schools
	 *
	 * @param Mage_Sales_Model_Quote_Item $item
	 *
	 * @return string
	 *   null or the attribute code that makes the item product a school only product
	 */
	public function isSchoolOnlyItem($item) {
		$product = $item->getProduct();
		return $this->isSchoolOnlyProduct($product, $item);
	}

	/**
	 *
	 */
	public function containsSchoolOnlyItems($items) {
		foreach ($items as $item) {
			$attributeCode = $this->isSchoolOnlyItem($item);
			if ($attributeCode) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get warnings about items in cart that cannot be bought by "private" customers
	 */
	public function getSchoolOnlyItems($items) {
		$warnings = array();

		foreach ($items as $item) {
			$attributeCode = $this->isSchoolOnlyItem($item);
			if ($attributeCode) {

			echo '<pre>';
			print_r(array(__METHOD__, $attributeCode, $warnings));
			echo '</pre>';

				// if (!isset($warnings[$attributeCode])) {
				// 	$warnings[$attributeCode] = array();
				// } else {
				// 	$messages = $warnings[$attributeCode];
				// }


				// $product = $this->getSchoolOnlyProduct($item);
				$product = $item->getProduct();

				// echo __METHOD__, ' ', $attributeCode, ' ', $product->getSku(), '<br/>';

				$obj = new Varien_Object();
				$obj->setAttributeCode($attributeCode);
				// $obj->setAttributeCode($count++);
				// $obj->setProduct($product);
				// $obj->setData($attributeCode, $child->getData($attributeCode));

				// array_push(&$warnings[$attributeCode], $obj);


				$warnings[] = $obj;

			// echo '<pre>';
			// print_r(array(__METHOD__, $attributeCode, $warnings));
			// echo '</pre>';

			}
		}

		return $warnings;
	}


	/**
	 * Get actual school only product, e.g. for a configurable product the actual product option and otherwise the product itself
	 *
	 * @param Mage_Sales_Model_Quote_Item $item
	 *
	 * @return Mage_Catalog_Model_Product
	 */
	public function getSchoolOnlyProduct($item) {
		$product = $item->getProduct();

		if ($product) {
			$child = $this->getItemChildProduct($item);
			if ($child) {
				return $child;
			}
		}

		return $product;
	}

	/**
	 * Get configurable product option
	 *
	 * @param Mage_Sales_Model_Quote_Item $item
	 *
	 * @return Mage_Catalog_Model_Product
	 */
	private function getItemChildProduct($item) {
		$child = null;

		if ($item) {
			$product = $item->getProduct();
			if ($product) {
				if ($product->isConfigurable()) {
					if ($option = $item->getOptionByCode('simple_product')) {
						$child = $option->getProduct();
						// Load product to make all attributes available
						$child->load($child->getId());
					}
				}
			}
		}

		return $child;
	}


	/**
	 * Explode a string represention of a list of integeres into an array of integers
	 *
	 * @param string $list
	 *
	 * @return array of int
	 */
	public function getIntArray($list, $delimiter = ',') {
		$value = array();

		if ($list) {
			foreach (explode($delimiter, $list) as $token) {
				$value[] = intval($token);
			}
		}

		return $value;
	}

	/**
	 * Explode a string represention of a list into an array of trimmed values
	 *
	 * @param string $list
	 *
	 * @return array of int
	 */
	public function trimExplode($list, $delimiter = ',') {
		$value = array();

		if ($list) {
			foreach (explode($delimiter, $list) as $token) {
				$value[] = trim($token);
			}
		}

		return $value;
	}


	/**
	 * Get formatted release date and label.
	 *
	 * @param Mage_Catalog_Model_Product $product
	 *
	 * @return null|array()
	 */
	public function getReleaseDateInfo(Mage_Catalog_Model_Product $product) {
		$publication_status = $product->getData('publication_status');
		$showReleaseDatePublicationStatuses = $this->getIntArray($this->getConfig('publication_statuses_with_release_date'));

		if (!in_array($publication_status, $showReleaseDatePublicationStatuses)) {
			return null;
		}

		$release_date_estimated = $product->getData('release_date_estimated');
		if ($release_date_estimated) {
			$release_date_estimated = new Zend_Date($release_date_estimated, Zend_Date::ISO_8601);
		}
		$release_date_confirmed = $product->getData('release_date_confirmed');
		if ($release_date_confirmed) {
			$release_date_confirmed = new Zend_Date($release_date_confirmed, Zend_Date::ISO_8601);
		}
		$now = new Zend_Date(Mage::getModel('core/date')->Timestamp(time()));

		// Use estimated release date if it after confirmed release date
		if ($release_date_confirmed && $release_date_estimated
				&& ($release_date_estimated->isLater($release_date_confirmed))
				&& ($release_date_confirmed->isEarlier($now))) {
			$release_date_confirmed = null;
		}

		$label = null;
		$formattedValue = null;

		if ($release_date_confirmed) {
			if ($release_date_confirmed->isLater($now)) {
				// Show confirmed release date
				$attribute = $product->getResource()->getAttribute('release_date_confirmed');

				// Why doesn't this format using the locale?!
				// $formattedValue = Mage::helper('core/data')->formatDate($release_date_confirmed, Mage_Core_Model_Locale::FORMAT_TYPE_LONG);
				$locale = Mage::app()->getLocale();
				$formattedValue = $release_date_confirmed->toString('d. MMMM yyyy', $locale->getLocale());

				$label = $this->__('Confirmed publication date');
			}
		} else if ($release_date_estimated) {
			if ($release_date_estimated->isLater($now)) {
				// Show estimated release date
				$attribute = $product->getResource()->getAttribute('release_date_estimated');
				$month = intval($release_date_estimated->get(Zend_Date::MONTH_SHORT));
				$quarter = intval(($month-1)/3)+1;
				$formattedValue = sprintf($this->__('Quarter %1$d, %2$d'), $quarter, $release_date_estimated->get(Zend_Date::YEAR));

				$label = $this->__('Estimated publication date');
			}
		}

		if ($label && $formattedValue) {
			if ($publication_status == 40) { // "Midlertidigt udsolgt"
				$label = $product->getAttributeText('publication_status').', '.strtolower($label);
			}

			return array(
									 'label' => $label,
									 'value' => $formattedValue,
									 );
		}
	}
}
