<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Sales
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Systime_Sales_Block_Order_History extends Mage_Sales_Block_Order_History {
	public function __construct() {
		parent::__construct();

		// Exclude select order statuses from order history
		$helper = Mage::helper('systimesales');
		$excludeStatuses = $helper->trimExplode($helper->getConfig('order_history_exclude_statuses'));

		if ($excludeStatuses) {
			//TODO: add full name logic
			$orders = Mage::getResourceModel('sales/order_collection')
				->addAttributeToSelect('*')
				->joinAttribute('shipping_firstname', 'order_address/firstname', 'shipping_address_id', null, 'left')
				->joinAttribute('shipping_lastname', 'order_address/lastname', 'shipping_address_id', null, 'left')
				->addAttributeToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
				->addAttributeToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))

				// mri@systime.dk 2009-09-22
				->addAttributeToFilter('status', array('nin' => $excludeStatuses))

				->addAttributeToSort('created_at', 'desc')
				;


			$this->setOrders($orders);
		}
	}
}
