<?php
$installer = $this;

$installer->startSetup();

$customAttributes = array();
$customAttributes['systimesales_ean'] = array(
																							// Entity type (ids) to add to
																							'entityTypes' => array('customer_address', 'order_address'),
																							// Attribute attributes
																							'config' => array(
																																'type'          => 'varchar',
																																'backend'       => '',
																																'frontend'      => '',
																																'label'         => 'Systime Sales EAN',
																																'global'        => true,
																																'visible'       => true,
																																'required'      => false,
																																'visible_on_front' => true,
																																),

																							// DB tables to add to
																							'tables' => 'sales_flat_quote_address',
																							// DB column definition
																							'definition' => 'VARCHAR(13) NOT NULL',
																							);

// $customAttributes['systimesales_iko'] = $customAttributes['systimesales_ean'];
// $customAttributes['systimesales_iko']['definition'] = 'VARCHAR(30) NOT NULL';
// $customAttributes['systimesales_iko']['config']['label'] = 'Systime Sales IKO';

// $customAttributes['systimesales_oioattention'] = $customAttributes['systimesales_iko'];
// $customAttributes['systimesales_oioattention']['config']['label'] = 'Systime Sales OIO Attention';

// $customAttributes['systimesales_oiorequisnumber'] = $customAttributes['systimesales_iko'];
// $customAttributes['systimesales_oiorequisnumber']['config']['label'] = 'Systime Sales OIO Requis Number';

$customAttributes['systimesales_taxvat'] = $customAttributes['systimesales_ean'];
$customAttributes['systimesales_taxvat']['definition'] = 'VARCHAR(30) NOT NULL';
$customAttributes['systimesales_taxvat']['config']['label'] = 'Systime Sales Tax/VAT Number';

$customAttributes['systimesales_companynumber'] = $customAttributes['systimesales_ean'];
$customAttributes['systimesales_companynumber']['config']['label'] = 'Systime Sales Company Number';

$customAttributes['systimesales_itemnumber'] = array(
																										 'entityTypes' => 'order_item',
																										 'config' => array(
																																			 'type' => 'varchar',
																																			 ),
																										 // DB tables to add to
																										 'tables' => 'sales_flat_order_item',
																										 // DB column definition
																										 'definition' => 'VARCHAR(100) NOT NULL',
																										 );

foreach ($customAttributes as $attributeName => $spec) {
	// Add attributes to entities
	if (!is_array($spec['entityTypes'])) {
		$spec['entityTypes'] = array($spec['entityTypes']);
	}
	$config = $spec['config'];
	foreach ($spec['entityTypes'] as $entityType) {
		$installer->addAttribute($entityType, $attributeName, $config);
	}

	if (isset($spec['tables'])) {
		// Update DB tables
		$db = $installer->getConnection();
		if (!is_array($spec['tables'])) {
			$spec['tables'] = array($spec['tables']);
		}
		$fieldName = $attributeName;
		$definition = $spec['definition'];
		foreach ($spec['tables'] as $tableName) {
			$actualTableName = $installer->getTable($tableName);
			if ($db->tableColumnExists($actualTableName, $fieldName)) {
				$sql = 'ALTER TABLE `'.$actualTableName.'` CHANGE `'.$fieldName.'` `'.$fieldName.'` '.$definition;
				$installer->run($sql);
			} else {
				$db->addColumn($actualTableName, $fieldName, $definition);
			}
		}
	}
}

// $installer->getConnection()->addColumn($this->getTable('sales_flat_quote_payment'), 'systimepayment_invoice_ean', 'VARCHAR(100) NOT NULL');

// $installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote_address')}` ADD `systimesales_ean` varchar(100) NULL;");

// $installer->addAttribute('quote_item', 'systimesales_itemnumber', array('type' => 'varchar'));
// $installer->addAttribute('order_item', 'systimesales_itemnumber', array('type' => 'varchar'));

$installer->endSetup();
