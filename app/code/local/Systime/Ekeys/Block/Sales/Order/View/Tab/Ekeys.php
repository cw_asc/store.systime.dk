<?php

class Systime_Ekeys_Block_Sales_Order_View_Tab_Ekeys extends Mage_Adminhtml_Block_Template
implements Mage_Adminhtml_Block_Widget_Tab_Interface {
	protected function _construct() {
		parent::_construct();
		$this->setTemplate('sales/order/view/tab/systimeekeys.phtml');
	}

	/**
	 * Retrieve order model instance
	 *
	 * @return Mage_Sales_Model_Order
	 */
	public function getOrder() {
		return Mage::registry('current_order');
	}

	private $ekeysInfo = null;

	public function getEkeysInfo() {
		if ($this->ekeysInfo === null) {
			$contents = $this->getOrder()->getSystimeekeysContents();
			$this->ekeysInfo = @unserialize($contents);
		}
		return $this->ekeysInfo;
	}

	public function getEKeyActivationUrl($key) {
		$helper = Mage::helper('systimeekeys');
		$url = $helper->getConfig('ekey_activation_url', $this->getStoreId());
		return str_replace('%KEY%', urlencode($key), $url);
	}

	/**
	 * TAB settings
	 */
	public function getTabLabel() {
		return Mage::helper('systimeekeys')->__('Order E-keys');
	}

	public function getTabTitle() {
		return Mage::helper('systimeekeys')->__('Order E-keys');
	}

	public function canShowTab() {
		return $this->getEkeysInfo();
	}

	public function isHidden() {
		return false;
	}
}