<?php

class Systime_Ekeys_Block_Email_Ekeys extends Mage_Core_Block_Template {
	public function getEKeyActivationUrl($key) {
		$helper = Mage::helper('systimeekeys');
		$url = $helper->getConfig('ekey_activation_url', $this->getStoreId());
		return str_replace('%KEY%', urlencode($key), $url);
	}
}
