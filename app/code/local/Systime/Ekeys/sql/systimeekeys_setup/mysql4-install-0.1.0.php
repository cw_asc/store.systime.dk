<?php
$installer = $this;

$installer->startSetup();

$db = $installer->getConnection();

$tableName = 'sales_order';
$fieldName = 'systimeekeys_contents';
$definition = 'TEXT DEFAULT \'\' NOT NULL';
$actualTableName = $installer->getTable($tableName);
if ($db->tableColumnExists($actualTableName, $fieldName)) {
	$sql = 'ALTER TABLE `'.$actualTableName.'` CHANGE `'.$fieldName.'` `'.$fieldName.'` '.$definition;
	$installer->run($sql);
} else {
	$db->addColumn($actualTableName, $fieldName, $definition);
}

$installer->endSetup();
