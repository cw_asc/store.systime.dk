<?php
	/**
	 * Generate E-keys module observer
	 *
	 * @category   Mage
	 * @package    Mage_GenerateEKeys
	 */
class Systime_EKeys_Model_Observer {
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $observer
	 */
	public function checkout_type_onepage_save_order_after($observer) {
		return;
		/*
		 try {
		 $order = $observer->getOrder();
		 $this->sendEKeys($order);
		 } catch (Exception $ex) {
		 Mage::logException($ex);
		 }
		*/
	}

	public function checkout_onepage_controller_success_action($observer) {
		try {
			$order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
			$this->sendEKeys($order);
		} catch (Exception $ex) {
			Mage::logException($ex);
		}
	}

	public function sales_order_place_after($observer) {
		try {
			$order = $observer->getOrder();
			$this->sendEKeys($order);
		} catch (Exception $ex) {
			Mage::logException($ex);
		}
	}

	/**
	 * Iterate order items, call e-keys service, and store response in database
	 */
	/*private*/ function createEKeys($order) {
		if ($order) {
			if ($order->getData('systimeekeys_contents')) {
				$info = @unserialize($order->getData('systimeekeys_contents'));
				if ($info) {
					return $info;
				}
			}

			try {
				$successes = array();
				$failures = array();

				$helper = Mage::helper('systimeekeys');

				$items = $order->getAllItems();

				$formater = new Varien_Filter_Template();
				$infoFormat = $helper->getConfig('orderinfo_format');
				if (!trim($infoFormat)) {
					$infoFormat = 'Order Id: {{var increment_id}} || Customer: {{var customer_email}}';
				}

				foreach ($items as $item) {
					$product = Mage::getModel('catalog/product')->load($item->getProductId());

					$ekeyType = $helper->getEkeyType($product);

					if ($ekeyType) {
						$identifier = $product->getSku();
						$quantity  = intval($item->getQtyOrdered());
						$expire_at = 0;
						$activation_period = $helper->getEkeyActivationPeriod($product);

						$formater->setVariables(array_merge($order->getData(), $item->getData()));

						$info = $formater->filter($infoFormat);

						$xml = $this->createEKeysWS($identifier, $ekeyType, $quantity, $info, $expire_at, $activation_period);

						$obj = new StdClass();
						$obj->sku = $product->getSku();
						$obj->title = $product->getName();
						$obj->mediaTypeName = $product->getAttributeText('mediatype');
						$obj->websiteUrl = $product->getWebsiteUrl();

						$obj->quantity = $quantity;
						$obj->activation_period = $activation_period;

						if ($xml) {
							if ((string)$xml->status == 'OK') {
								$codes = array();
								foreach ($xml->code as $node) {
									$codes[] = array(
																	 'code' => (string)$node,
																	 'count' => intval($node->attributes()->count),
																	 );
								}
								$obj->codes = $codes;
								if (!isset($successes[$ekeyType])) {
									$successes[$ekeyType] = array();
								}
								$successes[$ekeyType][$product->getSku()] = $obj;
							} else {
								$obj->error = (string)$xml->description;
								$failures[] = $obj;
							}
						} else {
							$obj->error = 'Error calling e-keys webservice';
							$failures[$product->getSku()] = $obj;
						}
					}
				}

				$info = array('successes' => $successes,
											'failures' => $failures,
											);

				// Store (serialized) in database
				$table = 'sales_order';
				$data = array('systimeekeys_contents' => serialize($info));
				$where = 'entity_id = '.intval($order->getId());
				$db = Mage::getSingleton('core/resource')->getConnection('core_write');
				$db->update($table, $data, $where);

				// echo '<pre>', print_r(array('successes' => $successes,
				// 														'failures' => $failures,
				// 														), true), '</pre>';

				return $info;
			} catch (Exception $ex) {
				Mage::logException($ex);
			}
		}
	}

	/*private*/ function sendEKeys($order) {
		if ($order) {
			try {
				$info = $this->createEKeys($order);
				if ($info) {
					$successes = isset($info['successes']) ? $info['successes'] : null;
					$failures = isset($info['failures']) ? $info['failures'] : null;
					$helper = Mage::helper('systimeekeys');

					$translate = Mage::getSingleton('core/translate');
					$translate->setTranslateInline(false);

					$mailTemplate = Mage::getModel('core/email_template');

					$sendTo = array(
													array(
																'name'  => $order->getCustomerName(),
																'email' => $order->getCustomerEmail(),
																),
													);

					$bcc = $helper->getConfig('ekeys_email_bcc', $order->getStoreId());
					if ($bcc) {
						foreach (explode(',', $bcc) as $email) {
							$mailTemplate->addBcc($email);
						}
					}

					if ($successes) {
						foreach ($successes as $type => $data) {
							// Attach PDF
							$pdf = null;
							$pdfFilename = 'enoegler';
							switch ($type) {
								case 'user':
									$pdf = Mage::getModel('systimeekeys/pdf_user')->getPdf($order, $data);
									break;
								case 'company':
									$pdf = Mage::getModel('systimeekeys/pdf_company')->getPdf($order, $data);
									$pdfFilename = 'skolelicens';
									break;
								default:
									break;
							}
							if ($pdf) {
								$mailTemplate = Mage::helper('emailattachments')->addAttachment($pdf,$mailTemplate,$pdfFilename);
							}

							foreach ($sendTo as $recipient) {
								$variables = array(
																	 'order'  => $order,
																	 'ekeys'=> $data,
																	 );

								$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $order->getStoreId()))
									->sendTransactional($helper->getConfig('email_template_'.$type),
																			$helper->getConfig('email_identity', $order->getStoreId()),
																			$recipient['email'],
																			$recipient['name'],
																			$variables
																			);
							}
						}
					}

					if ($failures) {
						foreach ($failures as $failure) {
							$failures_html = '';
							$failures_html .= '<dl>';
							$failures_html .= '<dt>'.htmlspecialchars(sprintf('%s (%s)', $failure->title, $failure->sku)).'</dt>';
							$failures_html .= "\n";
							$failures_html .= '<dd>'.$failure->error.'</dd>';
							$failures_html .= "\n";
							$failures_html .= '</dl>';

							if ($bcc) {
								foreach (explode(',', $bcc) as $email) {
									$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $order->getStoreId()))
										->sendTransactional($helper->getConfig('email_failure_template'),
																				$helper->getConfig('email_identity', $order->getStoreId()),
																				$email,
																				$email,
																				array(
																							'order'  => $order,
																							'failures'=> $failures,
																							'failures_html'=> $failures_html,
																							)
																				);
								}
							}
						}
					}

					$translate->setTranslateInline(true);
				}
			} catch (Exception $ex) {
				Mage::logException($ex);
			}
		}
	}

	/**
	 * Wrapper for calling createEKeys webservice
	 *
	 * return SimpleXMLElement
	 */
	private function createEKeysWS($sku, $type, $count, $info, $expire_at=0, $activation_period=0) {
		try {
			// debug(func_get_args(), __METHOD__);
			// 		return ;

			$helper = Mage::helper('systimeekeys');
			$url = $helper->getConfig('ws_url');
			$hashKey = $helper->getConfig('hash_key');

			$hash = md5($sku.$type.$count.$expire_at.$activation_period.$hashKey);

			$client = new SoapClient(null, array(
																					 'location' => $url,
																					 'uri' => 'ekeys.systime.dk',
																					 ));

			$response = $client->createEKeys($sku, $type, $count, $expire_at, $activation_period, $info, $hash);

			$this->log(array('url' => $url, 'args' => func_get_args(), 'response' => $response));

			return new SimpleXMLElement($response);
		} catch (Exception $ex) {
			Mage::logException($ex);
		}
	}

	public function catalog_product_save_after($observer) {
		$helper = Mage::helper('systimeekeys');

		if ($helper->getConfig('update_epublication_on_save')) {
			$product = $observer->getEvent()->getProduct();

			if ($helper->getEkeyType($product)) {
				$url = $helper->getConfig('ws_url');
				$hashKey = $helper->getConfig('hash_key');

				$identifier = $product->getSku();
				$title = $product->getName();
				$url = $product->getWebsite_url();
				$mediatype = $product->getMediatype();
				$subjects = $product->getSubject();
				$children = null;
				$hash = md5($identifier.$title.$url.$mediatype.$subjects.$hashKey);

				$this->log(array(
												 'method' => __METHOD__,
												 'identifier' => $identifier,
												 'title' => $title,
												 'url' => $url,
												 'mediatype' => $mediatype,
												 'subjects' => $subjects,
												 ));

				$client = new SoapClient(null, array(
																						 'location' => $url,
																						 'uri' => 'ekeys.systime.dk',
																						 ));

				$response = $client->updatePublication($identifier, $title, $url, $mediatype, $subjects, $children, $hash);

				$xml = new SimpleXMLElement($response);

				if ($xml->status == 'OK') {
					$this->log($xml->status.' '.$xml->description);
				} else {
					$this->log($xml->status.' '.$xml->description);
					$this->log($response);
				}
			}
		}
	}

	private function log($value, $level=null) {
		Mage::helper('systimeekeys')->log($value, $level);
	}
	}
