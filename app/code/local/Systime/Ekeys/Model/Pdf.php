<?php
class Systime_EKeys_Model_Pdf
{
	public function getPdf($order, $ekeys)
	{
		$helper = Mage::helper('pdfcustomiser/pdf');//new Fooman_PdfCustomiser_Order();
		$pdf = new Fooman_PdfCustomiser_Model_Mypdf('P', 'mm',  'A4', true, 'UTF-8', false);
		$pdf->SetStandard($helper);
		$pdf->SetPrintFooter(false);
		$pdf->AddPage();
		$pdf->SetX($helper->getPdfMargins('sides'));
		$this->_outputTop($pdf, $order);
		$this->_outputEkeys($pdf, $order, $ekeys);
		$this->_outputBottom($pdf, $order);
		$pdf->setPdfAnyOutput(true);
		$pdf->lastPage();
		return $pdf;
	}

	// Override these functions in subclasses
	protected function _outputTop($pdf, $order) { }
	protected function _outputBottom($pdf, $order) { }

	protected function _outputEkeys($pdf, $order, $ekeys)
	{
		$helper = Mage::helper('pdfcustomiser/pdf');
		$ekeyshelper = Mage::helper('systimeekeys/data');
		if (is_array($ekeys) && (count($ekeys) > 0))
		{
			foreach ($ekeys as $key)
			{
				$pdf->SetX($helper->getPdfMargins('sides'));
				$pdf->Cell(0, $pdf->getLastH(), $ekeyshelper->__('Publication Title'), 0, 1);
				$pdf->SetX($helper->getPdfMargins('sides')+10);
				$pdf->Cell(0, $pdf->getLastH(), $key->title.((isset($key->mediaTypeName) && $key->mediaTypeName) ? ' ('.$key->mediaTypeName.')' : ''), 0, 1);

				if (isset($key->activation_period) && ($key->activation_period > 0)) {
					$pdf->SetX($helper->getPdfMargins('sides'));
					$pdf->Cell(0, $pdf->getLastH(), $ekeyshelper->__('Activation period'), 0, 1);
					$pdf->SetX($helper->getPdfMargins('sides')+10);
					$pdf->Cell(0, $pdf->getLastH(), $ekeyshelper->formatActivationPeriod($key->activation_period), 0, 1);
				}

				$pdf->SetX($helper->getPdfMargins('sides'));
				$pdf->Cell(0, $pdf->getLastH(), (count($key->codes) == 1) ? $ekeyshelper->__('E-key code') : $ekeyshelper->__('E-key codes'), 0, 1);
				foreach ($key->codes as $code)
				{
					$pdf->SetX($helper->getPdfMargins('sides')+10);
					$keycode = $code['code'];
					$activationCount = intval($code['count']);
					$pdf->addHtmlLink($this->getEKeyActivationUrl($keycode), $keycode);
					$pdf->Ln(6);
					if ($activationCount > 1) {
						$pdf->SetX($helper->getPdfMargins('sides')+10);
						$pdf->Cell(0, sprintf($ekeyshelper->__('Number of allowed activations: %1$d'), $activationCount), $key->title, 0, 1);
					}
				}
				$pdf->SetX($helper->getPdfMargins('sides'));
			}
			$pdf->Ln(6);
		}
	}

	public function getEKeyActivationUrl($key) {
		$helper = Mage::helper('systimeekeys');
		$url = $helper->getConfig('ekey_activation_url');
		return str_replace('%KEY%', urlencode($key), $url);
	}
}