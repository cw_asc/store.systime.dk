<?php
require_once(BP . DS . 'app/code/local/Systime/Ekeys/Model/Pdf.php');
class Systime_EKeys_Model_Pdf_User extends Systime_EKeys_Model_Pdf
{
	protected function _outputTop($pdf, $order)
	{
		$helper = Mage::helper('pdfcustomiser/pdf');
		$pdf->printHeader($helper, 'Aktivering af eNøgler (ordrenr. ' . $order->getIncrementId() . ')');
		$pdf->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());
		$pdf->Cell(0, 0, 'Hej ' . $order->getCustomerName(), 0, 1);
		$pdf->Ln(6);
		$pdf->Cell(0, $pdf->getLastH(), 'Her er dine eNøgler fra ordrenr. ' . $order->getIncrementId(), 0, 1);
		$pdf->Ln(6);
	}

	protected function _outputBottom($pdf, $order)
	{
		$pdf->MultiCell(0, $pdf->getLastH(), 'Du aktiverer eNøglen ved at klikke på den og derefter følge aktiveringsprocessen. Hvis linket ikke virker, kan du logge ind på MinKonto og aktivere din eNøgle i menuen til venstre.

Har du spørgsmål, så skriv til os på systime@systime.dk eller ring på 70 12 11 00.

Vi håber, du får glæde af materialet.

Med venlig hilsen
Systime', 0, 'L');
	}
}