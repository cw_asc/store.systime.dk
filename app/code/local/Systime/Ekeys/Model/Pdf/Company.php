<?php
require_once(BP . DS . 'app/code/local/Systime/Ekeys/Model/Pdf.php');
class Systime_EKeys_Model_Pdf_Company extends Systime_EKeys_Model_Pdf
{
	protected function _outputTop($pdf, $order)
	{
		$helper = Mage::helper('pdfcustomiser/pdf');
		$pdf->printHeader($helper, 'Aktivering af skolelicens (ordrenr. ' . $order->getIncrementId() . ')');
		$pdf->SetFont($helper->getPdfFont(), '', $helper->getPdfFontsize());
		$pdf->Cell(0, 0, 'Hej ' . $order->getCustomerName(), 0, 1);
		$pdf->Ln();
	}
	
	protected function _outputBottom($pdf, $order)
	{
		$pdf->Cell(0, $pdf->getLastH(), 'Dette er en kvittering for din skolelicens fra ordrenr. ' . $order->getIncrementId(), 0, 1);
		$pdf->Ln();
		$pdf->Cell(0, $pdf->getLastH(), 'Systime åbner for skolens adgang til materialet inden for 1-3 hverdage.', 0, 1);
		$pdf->Ln();
		$pdf->Cell(0, $pdf->getLastH(), 'Har du spørgsmål, så skriv til os på systime@systime.dk eller ring på 70 12 11 00.', 0, 1);
		$pdf->Ln();
		$pdf->Cell(0, $pdf->getLastH(), 'Vi håber, du får glæde af materialet.', 0, 1);
		$pdf->Ln();
		$pdf->Cell(0, $pdf->getLastH(), 'Med venlig hilsen', 0, 1);
		$pdf->Cell(0, $pdf->getLastH(), 'Systime', 0, 1);
	}
	
	protected function _outputEkeys($pdf, $order, $ekeys)
	{
		// Don't output any ekeys
	}
}