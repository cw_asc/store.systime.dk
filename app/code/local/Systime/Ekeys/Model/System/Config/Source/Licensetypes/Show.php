<?php
class Systime_Ekeys_Model_System_Config_Source_Licensetypes_Show
{
    public function toOptionArray()
    {

			$collection = Mage::getModel('eav/entity_attribute_option')->getCollection()
				->setStoreFilter()
				// ->addVisibleFilter()
				->join('attribute','attribute.attribute_id=main_table.attribute_id', 'attribute_code')
				;

			$options = array();

			foreach ($collection as $item) {
				if ('licens_type' == $item->getAttributeCode()) {
					$options[] = array(
														 'value' => $item->getId(),
														 // 'label' => $item->getAttributeCode(),
 														 // 'label' => $item->getValue(),
														 'label' => $item->getFrontendLabel().' ('.$item->getValue().')',
														 );
				}
			}

			return $options;
   }
}