<?php
class Systime_Ekeys_Helper_Data extends Mage_Core_Helper_Abstract {
	public function getConfig($key, $storeId=null, $category = 'general') {
		return Mage::getStoreConfig('systimeekeys/'.$category.'/'.$key, $storeId);
	}

	private static $licenseTypeAttributeCode = 'licens_type';

	/**
	 * Determine if product can be delivered using an e-key
	 *
	 * @param Mage_Catalog_Model_Product $product
	 *
	 * @return boolean
	 */
	public function isEKeyProduct(Mage_Catalog_Model_Product $product) {
		$productLicenseType = $product->getData(self::$licenseTypeAttributeCode);
		$list = $this->getConfig('license_types');

		$result = false;

		if ($list) {
			foreach (explode(',', $list) as $token) {
				if ($productLicenseType == intval(trim($token))) {
					$result = true;
					break;
				}
			}
		}

		return $result;
	}

	/**
	 * Get type of e-key to generate for product. Returns null if the product is not an e-key product.
	 *
	 * @param Mage_Catalog_Model_Product $product
	 *
	 * @return string null, "private", "company"
	 */
	public function getEkeyType($product) {
		$productLicenseType = intval($product->getData(self::$licenseTypeAttributeCode));

		if ($productLicenseType) {
			foreach (array('user', 'company') as $type) {
				$list = $this->getConfig('license_types_'.$type);
				if ($list) {
					foreach (explode(',', $list) as $token) {
						if ($productLicenseType == intval(trim($token))) {
							return $type;
						}
					}
				}
			}
		}
	}

	public function getEkeyActivationPeriod($product) {
		$productLicenseType = intval($product->getData(self::$licenseTypeAttributeCode));

		if ($productLicenseType) {
			// Check all configurable activation periods
			for ($i = 0; $i < 8; $i++) {
				$period = intval($this->getConfig('activation_period_'.$i, null, 'activation_periods'));
				if ($period) {
					// Chekc if product license type matches acivation period
					$list = $this->getConfig('activation_period_'.$i.'_license_types', null, 'activation_periods');
					if ($list) {
						foreach (explode(',', $list) as $token) {
							if ($productLicenseType == intval(trim($token))) {
								return $period;
							}
						}
					}
				}
			}
		}

		return 0;
	}

	public function formatActivationPeriod($activationPeriod) {
		if ($activationPeriod > 0) {
			$formatted = '';
			$exact = false;

			$secondsPerYear = 31556926;
			$secondsPerDay = 24*60*60;

			if (abs($activationPeriod-0.5*$secondsPerYear) < 14*$secondsPerDay) {
				$exact = ($activationPeriod == 0.5*$secondsPerYear);
				$formatted = $this->__('½ year');
			} else if (abs($activationPeriod-1.0*$secondsPerYear) < 14*$secondsPerDay) {
				$exact = ($activationPeriod == 1.0*$secondsPerYear);
				$formatted = $this->__('1 year');
			} else if (abs($activationPeriod-2.0*$secondsPerYear) < 14*$secondsPerDay) {
				$exact = ($activationPeriod == 2.0*$secondsPerYear);
				$formatted = $this->__('2 years');
			} else if (abs($activationPeriod-3.0*$secondsPerYear) < 14*$secondsPerDay) {
				$exact = ($activationPeriod == 3.0*$secondsPerYear);
				$formatted = $this->__('3 years');
			} else if (abs($activationPeriod-4.0*$secondsPerYear) < 14*$secondsPerDay) {
				$exact = ($activationPeriod == 4.0*$secondsPerYear);
				$formatted = $this->__('4 years');
			} else {
				// @see http://knowledge.drun.net/viewtopic.php?id=96
				$periods = array(
												 'year'     => 31556926,
												 'month'    => 2629743,
												 'week'     => 604800,
												 'day'      => 86400,
												 'hour'     => 3600,
												 'minute'   => 60,
												 'second'   => 1
												 );
				$values = array();
				$seconds = $activationPeriod;
				foreach ($periods as $period => $value) {
					$count = floor($seconds / $value);

					if ($count == 0) {
						continue;
					}

					$values[$period] = $count;
					$seconds = $seconds % $value;
				}

				foreach ($values as $key => $value) {
					if ($formatted) {
						$formatted .= ', ';
					}
					$formatted .= $value.' '.(($value == 1) ? $this->__($key) : $this->__($key.'s'));
				}
				$exact = true;
			}

			return ($exact ? '' : '~ ').$formatted;
		}

	}

	public function log($message, $level=null) {
		if ((bool)$this->getConfig('write_log')) {
			Mage::log($message, $level, 'systime_ekeys');
		}
	}
}
?>