<?php
class Systime_Misc_Helper_Data extends Mage_Core_Helper_Abstract {
	public function getConfig($key, $storeId=null) {
		return Mage::getStoreConfig('systimemisc/general/'.$key, $storeId);
	}

	/**
	 * Decide if product is virtual, i.e. it has only virtual media types
	 *
	 * @param Mage_Catalog_Model_Product $product
	 *
	 * @return boolean
	 */
	public function isVirtualProduct($product) {
		if ($product) {
			$virtualMediaTypes = explode(',', $this->getConfig('virtual_mediatypes'));
				// Load product again to make sure mediatype is included
			$product->load($product->getId());
			$productMediaTypes = explode(',', $product->getData('mediatype'));
			// Product is virtual if it has only virtual media types
			$isVirtual = array_diff($productMediaTypes, $virtualMediaTypes) ? false : true;
			return $isVirtual;
		}
	}
	
	/**
	 * Decide if product should display an alternative
	 * product link button instead of add-to-cart button
	 * on the product list page
	 *
	 * @param Mage_Catalog_Model_Product $product
	 *
	 * @return boolean
	 */
	public function useAlternativeAddtocartButton($product) {
		if ($product) {
			$alternativeMediaTypes = explode(',', $this->getConfig('alternative_addtocart_mediatypes'));
			// Load product again to make sure mediatype is included
			//$product = Mage::getModel('catalog/product')->load($product->getId());
			$productMediaTypes = explode(',', $product->getData('mediatype'));
			// Use alternative button if product has any alternative mediatypes
			return (array_intersect($productMediaTypes, $alternativeMediaTypes)) ? true : false;
		}
	}

	protected $checkoutAttributeList = null;

	public function getCheckoutField($code, &$caller) {
		if ($this->checkoutAttributeList === null) {
			$this->checkoutAttributeList = array();
			if ($caller && method_exists($caller, 'getCustomFieldList')) {
				foreach (array(1, 2) as $pos) {
					$fieldList = $caller->getCustomFieldList($pos);
					foreach ($fieldList as $field) {
						$this->checkoutAttributeList[$field['attribute_code']] = $field;
					}
				}
			}
		}
		if ($this->checkoutAttributeList && isset($this->checkoutAttributeList[$code])) {
			return $this->checkoutAttributeList[$code];
		}
	}

	/**
	 * Render a static block
	 *
	 * @param string $blockId the block to render
	 * @param string cssClass
	 *
	 * @return string
	 */
	public function renderBlock($blockId, $cssClass = null) {
		$block = Mage::app()->getLayout()->createBlock('cms/block');
		if ($block) {
			$block->setBlockId($blockId);
			$content = $block->toHtml();

			if ($content) {
				return '<div class="systime-misc-block '.($cssClass ? ' '.$cssClass : '').'">'.$content.'</div>';
			}
		}
	}

	/**
	 * Print a static block
	 *
	 * @param string $blockId the block to render
	 * @param string cssClass
	 */
	public function printBlock($blockId, $cssClass = null) {
		$content = $this->renderBlock($blockId, $cssClass);
		echo $content;
	}
}
