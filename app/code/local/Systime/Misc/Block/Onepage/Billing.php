<?php

// $parentClass = 'Mage_Checkout_Block_Onepage_Billing';
// if (class_exists('Aitoc_Aitcheckattrib_Block_Checkout_Onepage_Billing')) {
// 	$parentClass = 'Aitoc_Aitcheckattrib_Block_Checkout_Onepage_Billing';
// }

class Systime_Misc_Block_Onepage_Billing extends Aitoc_Aitcheckattrib_Block_Checkout_Onepage_Billing {
	public function isUseBillingAddressForShipping() {
		$res = parent::isUseBillingAddressForShipping();

		if ((bool)Mage::helper('systimemisc')->getConfig('flip_isUseBillingAddressForShipping')) {
			$res = !$res;
		}

		return $res;
	}
}
