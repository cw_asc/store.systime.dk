<?php
class Systime_Export_ExportController extends Mage_Core_Controller_Front_Action {
	public function indexAction() {}

	private function getTempDir() {
		$basedir = Mage::getBaseDir();
		$path = $basedir.'/systimeexport';
		if (!is_dir($path)) {
			@mkdir($path);
		}
		return $path;
	}

	private $productFields = array(
																 'isbn_10' => array('header' => 'isbn10'),
																 'isbn_13' => array('header' => 'isbn13'),
																 // 'mediatype' => array('header' => 'medietype'),
																 'name' => array('header' => 'titel'),
																 'authors' => array('header' => 'forfatter'),
																 'number_of_pages' => array('header' => 'sidetal'),
																 'edition' => array('header' => 'udgave'),
																 'publication_year' => array('header' => 'udgivelsesår'),
																 'price' => array('header' => 'pris'),
																 // 'leveringstid' => array('header' => 'leveringstid'),
																 'url' => array(),

																 'description' => array(),
																 'image' => array(),
																 );

	private static $attributeSetAttributeCode = 'attribute_set_id';
	private $attributeSets = array(
																 26 => 'Website',
																 27 => 'Bog',
																 28 => 'E-bog',
																 29 => 'Cd-audio',
																 30 => 'Cd-rom',
																 31 => 'DVD',
																 );

	// Flip of $attributeSets. Will be set when needed
	private $attributeSetNames = null;

	private function getProductFieldHeader($fieldName) {
		return isset($this->productFields[$fieldName]['header'])
			? $this->productFields[$fieldName]['header'] : $fieldName;
	}

	private function getProductValue($fieldName, $product) {
		$value = $product->getData($fieldName);
		switch ($fieldName) {
		case 'authors':
			$value = trim($value, ' ,');
			break;
		case 'leveringstid':
			switch ($product->getData(self::$attributeSetAttributeCode)) {
			case $this->attributeSetNames['Website']:
			case $this->attributeSetNames['E-bog']:
				$value = 'med det samme';
				break;
			default:
				$value = '2-3 dage';
				break;
			}
			break;
		case 'mediatype':
			$value = $this->attributeSets[$product->getData(self::$attributeSetAttributeCode)];
			break;
		case 'url':
			$value = $product->getProductUrl();
			break;
		case 'image':
			$value = $product->getImageUrl();
			break;
		}
		return $value;
	}

	// private static $csvDelimiter = ',';
	// private static $csvEnclosure = '"';
	private static $csvDelimiter = '|';
	private static $csvEnclosure = '';

	private function fputcsv($handle, $fields) {
		if ((self::$csvDelimiter == '|') && (self::$csvEnclosure == '')) {
			$line = '';
			$index = 0;
			foreach ($fields as $value) {
				if ($index++ > 0) {
					$line .= self::$csvDelimiter;
				}
				// replace line breaks with spaces and remove all delimiters in value
				$value = str_replace(array("\x0D\x0A", "\x0A", self::$csvDelimiter),
														 array(' ', ' ', ''),
														 $value);
				$line .= $value;
			}
			fputs($handle, $line);
			fputs($handle, "\x0D\x0A");
		} else {
			fputcsv($handle, $fields, self::$csvDelimiter, self::$csvEnclosure);
		}
	}

	private function getProductsExport($format = 'csv') {
		$fieldNames = array_keys($this->productFields);

		$collection = Mage::getModel('catalog/product')
			->setStoreId($this->storeId)
			->getCollection()
			->addAttributeToSelect($fieldNames);
		// status = aktiv
		$collection->addFieldToFilter('status', 1)
			// ->addFieldToFilter('type_id', array('configurable'))
			->addFieldToFilter('visibility', array(Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
																						 Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
																						 Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG))
			->addFieldToFilter('attribute_set_id', array($this->attributeSetNames['Bog']))
			;

		// Include all "simple" products that are not "website" plus "configurable" products
		$collection->getSelect()->where('
(type_id = \'simple\' and attribute_set_id <> '.$this->attributeSetNames['Website'].')
 or (type_id = \'configurable\')
');

		$collection->load();

		// print_r(get_class_methods($collection->getSelect()));

		// print_r($collection->getSelect()->assemble());
		// echo "\n\n";

		// echo count($collection), "\n";
		// die();

		$filename = 'so.txt';
		$fp = fopen($filename, 'w');

		// Write header field names
		$headers = array();
		foreach ($fieldNames as $name) {
			$header = $this->getProductFieldHeader($name);
			$headers[] = $header;
		}
		$this->fputcsv($fp, $headers);

		foreach ($collection as $product) {
			// Why is this needed?!
			$product->setStoreId($this->storeId);

			$values = array();
			foreach ($fieldNames as $name) {
				$value = self::getProductValue($name, $product);
				$values[$name] = $value;
			}
			$this->fputcsv($fp, $values);
		}

		fclose($fp);

		$csv = file_get_contents($filename);

		return $csv;
	}

	private function getStoreId($code) {
		$storeId = 0;

		$storeCollection = Mage::getModel('core/store')->getCollection()
			// ->initCache($this->getCache(), 'app', array(Mage_Core_Model_Store::CACHE_TAG))
			// ->setLoadDefault(true)
			;

		foreach ($storeCollection as $store) {
			if ((is_numeric($code) && ($code == $store->getId()))
					|| ($code == $store->getCode())) {
				$storeId = $store->getId();
			}
		}

		return $storeId;
	}

	private $storeId = 0;
	private $format = 'csv';

	private function init() {
		$this->attributeSetNames = array_flip($this->attributeSets);

		if ($value = $this->getRequest()->getParam('store')) {
			$this->storeId = $this->getStoreId($value);
		}
		if ($value = $this->getRequest()->getParam('format')) {
			if (in_array($value, array('csv'))) {
				$this->format = $value;
			}
		}
	}

	public function productAction() {
		$this->init();

		header('Content-type: text/plain; charset=utf-8');

		if (!($this->storeId && $this->format)) {
			die('Invalid request');
		}

		set_time_limit(0);
		$csv = $this->getProductsExport();

		echo $csv;
		exit;
	}
}
