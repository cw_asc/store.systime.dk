<?php
/**
* Systime modifications for AITOC Shop by brands module
*/

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('aitmanufacturers')} ADD COLUMN `linkedin_url` VARCHAR(255) NOT NULL AFTER `update_time`;

ALTER TABLE {$this->getTable('aitmanufacturers')} ADD COLUMN `systime_lab_url` VARCHAR(255) NOT NULL AFTER `linkedin_url`;

");

$installer->endSetup();