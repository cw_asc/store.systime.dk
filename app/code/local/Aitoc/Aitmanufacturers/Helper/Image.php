<?php
/**
* @copyright  Copyright (c) 2009 AITOC, Inc. 
*/

// Rewritten by anders@crius.dk 2010-03-02 to support auto-resize with caching
class Aitoc_Aitmanufacturers_Helper_Image extends Mage_Core_Helper_Abstract
{
	protected $_dir = '';
	protected $_width;
	
    public function getUrl($image)
    {
		if (!file_exists($this->_getCachedPath($image))) {
			$this->_saveCached($image);
		}
        return $this->_getCachedUrl($image);
    }

	protected function _getCachedPath($image)
	{
		return Mage::getBaseDir('media') . DS . 'aitmanufacturers' . DS . $this->_dir . $image;
	}
	
	protected function _getCachedUrl($image)
	{
		return Mage::getBaseUrl('media') . 'aitmanufacturers' . DS . $this->_dir . $image;
	}
    
	public function resizeWidth($width)
	{
		$this->_dir = $width . DS;
		$this->_width = $width;
		return $this;
	}

	private function _saveCached($image)
	{
		$originalFile = Mage::getBaseDir('media') . DS . 'aitmanufacturers' . DS . $image;
		$newFile = $this->_getCachedPath($image);
		$adapter = Varien_Image_Adapter::factory(Varien_Image_Adapter::ADAPTER_GD2);
		$adapter->open($originalFile);
		if ($this->_width) {
			$adapter->resize($this->_width);
		}
		$adapter->save($newFile);
	}
}
