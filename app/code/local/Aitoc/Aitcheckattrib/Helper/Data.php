<?php

class Aitoc_Aitcheckattrib_Helper_Data extends Mage_Core_Helper_Abstract
{
    function getStepData($sType, $sMode = 'full')
    {
        if (!$sType) return false;
        
        switch ($sType)
        {
            case 'onepage':
                $aStepData = array
                (
                    array
                    (
                        'value' => '',
                        'label' => $this->__('None')
                    ),
                    array
                    (
                        'value' => 1,
                        'label' => $this->__('1. Billing Info')
                    ),
                    array
                    (
                        'value' => 2,
                        'label' => $this->__('2. Shipping Info')
                    ),
                    array
                    (
                        'value' => 3,
                        'label' => $this->__('3. Shipping Method')
                    ),
                    array
                    (
                        'value' => 4,
                        'label' => $this->__('4. Payment Info')
                    ),
                    array
                    (
                        'value' => 5,
                        'label' => $this->__('5. Order Review')
                    ),
                );
            break;    
            
            case 'multipage':
                $aStepData = array
                (
                    array
                    (
                        'value' => '',
                        'label' => $this->__('None')
                    ),
                    array
                    (
                        'value' => 1,
                        'label' => $this->__('1. Select Addresses')
                    ),
                    array
                    (
                        'value' => 2,
                        'label' => $this->__('2. Shipping Info')
                    ),
                    array
                    (
                        'value' => 3,
                        'label' => $this->__('3. Billing Info')
                    ),
                    array
                    (
                        'value' => 4,
                        'label' => $this->__('4. Place Order')
                    ),
                );
            break;    
        }
        
        if ($sMode == 'hash')
        {
            $aStepHash = array();
            
            foreach ($aStepData as $aItem)
            {
                if ($aItem['value'])
                {
                    $aStepHash[$aItem['value']] =$aItem['label'];
                }
            }
            
            $aStepData = $aStepHash;   
        }
        
        
        return $aStepData;
    }
    
    function getStepId($sStepType)
    {
        if (!$sStepType) return false;
        
        $aStepIdHash = array
        (
            'billing'       => '1',
            'shippinfo'     => '2',
            'shippmethod'   => '3',
            'payment'       => '4',
            'review'        => '5',

            'mult_addresses'    => '1',
            'mult_shippinfo'    => '2',
            'mult_billing'      => '3',
            'mult_overview'     => '4',
        );
        
        if (isset($aStepIdHash[$sStepType]))
        {
            return $aStepIdHash[$sStepType];
        }
        
        return 0;
    }
    
}

?>