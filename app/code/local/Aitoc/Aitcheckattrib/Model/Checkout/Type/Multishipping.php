<?php
/**
 * Magento
 *
 */


class Aitoc_Aitcheckattrib_Model_Checkout_Type_Multishipping extends Mage_Checkout_Model_Type_Multishipping
{
    
    public function createOrders()
    {
        
        parent::createOrders();

        $oReq = Mage::app()->getFrontController()->getRequest();
        
        $sKey  = 'multi';
        
        $data = $oReq->getPost($sKey);

        if ($data)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');
            
            foreach ($data as $sKey => $sVal)
            {
                $oAttribute->setCustomValue($sKey, $sVal, 'multishipping');
            }
        }
        
        
        $aOrderIdHash = Mage::getSingleton('core/session')->getOrderIds(true);

        if ($aOrderIdHash)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');
            
            foreach ($aOrderIdHash as $iOrderId => $sVal)
            {
                $oAttribute->saveCustomOrderData($iOrderId, 'multishipping');
            }
            
            $oAttribute->clearCheckoutSession('multishipping');
        }
        
        return $this;
    }    
}
