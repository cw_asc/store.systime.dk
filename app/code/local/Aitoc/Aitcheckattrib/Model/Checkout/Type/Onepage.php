<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Aitoc_Aitcheckattrib_Model_Checkout_Type_Onepage extends Mage_Checkout_Model_Type_Onepage
{
    // overwright parent
    public function saveBilling($data, $customerAddressId)
    {
        if ($data)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');
            
            foreach ($data as $sKey => $sVal)
            {
                $oAttribute->setCustomValue($sKey, $sVal, 'onepage');
            }
        }

        return parent::saveBilling($data, $customerAddressId);
    }

    // overwright parent
    public function saveShipping($data, $customerAddressId)
    {
        if ($data)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');
            
            foreach ($data as $sKey => $sVal)
            {
                $oAttribute->setCustomValue($sKey, $sVal, 'onepage');
            }
        }

        return parent::saveShipping($data, $customerAddressId);
    }

    // overwright parent
    public function saveShippingMethod($shippingMethod)
    {
        $oReq = Mage::app()->getFrontController()->getRequest();
        
        $data = $oReq->getPost('shippmethod');
        
        if ($data)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');
            
            foreach ($data as $sKey => $sVal)
            {
                $oAttribute->setCustomValue($sKey, $sVal, 'onepage');
            }
        }

        return parent::saveShippingMethod($shippingMethod);
    }
    
    // overwright parent
    public function savePayment($data)
    {
        parent::savePayment($data);
        
        if ($data)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');
            
            foreach ($data as $sKey => $sVal)
            {
                $oAttribute->setCustomValue($sKey, $sVal, 'onepage');
            }
        }

        return array();
    }

    
    
    // overwright parent
    public function saveOrder()
    {
        parent::saveOrder();

        // set review attributes data
        
        $oReq = Mage::app()->getFrontController()->getRequest();
        
        $data = $oReq->getPost('customreview');
        
        if ($data)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');
            
            foreach ($data as $sKey => $sVal)
            {
                $oAttribute->setCustomValue($sKey, $sVal, 'onepage');
            }
        }
       
        // save attribute data to DB
        
        $order = Mage::getModel('sales/order');
        $order->load($this->getCheckout()->getLastOrderId());
        
        $iOrderId = $this->getCheckout()->getLastOrderId();
        
        if ($iOrderId)
        {
            $oAttribute = Mage::getModel('aitcheckattrib/aitcheckattrib');

            $oAttribute->saveCustomOrderData($iOrderId, 'onepage');
            $oAttribute->clearCheckoutSession('onepage');
        }
        
        return true;
    }    
}
