<?php
/**
 * Magento
 *
 */

class Aitoc_Aitcheckattrib_Block_Checkout_Onepage_Billing extends Mage_Checkout_Block_Onepage_Billing
{
    
    protected function _construct()
    {
        Mage::getModel('aitcheckattrib/aitcheckattrib')->checkDatabaseInstall();
        
        parent::_construct();
    }
    
    public function getFieldHtml($aField)
    {
        $sSetName = 'billing';
        
        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getAttributeHtml($aField, $sSetName, 'onepage');
    }
    
    public function getCustomFieldList($iTplPlaceId)
    {
        $iStepId = Mage::helper('aitcheckattrib')->getStepId('billing');
        
        if (!$iStepId) return false;

        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getCheckoutAtrributeList($iStepId, $iTplPlaceId, 'onepage');
    } 
    
    public function checkStepHasRequired()
    {
        $iStepId = Mage::helper('aitcheckattrib')->getStepId('shippinfo');
        
        if (!$iStepId) return false;

        return Mage::getModel('aitcheckattrib/aitcheckattrib')->checkStepHasRequired($iStepId, 'onepage');
    } 
}