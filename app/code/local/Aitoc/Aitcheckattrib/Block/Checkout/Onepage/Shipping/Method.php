<?php
/**
 * Magento
 *
 */

class Aitoc_Aitcheckattrib_Block_Checkout_Onepage_Shipping_Method extends Mage_Checkout_Block_Onepage_Shipping_Method
{
    
    protected function _construct()
    {
#        print '<h1>DO NOT WORRY - IT IS DEBUG BY RASTORGUEV!</h1><br>';

        // to do - check for date type
        
#        if ($head = $this->getLayout()->getBlock('head')) 
        {
#            $head->setCanLoadCalendarJs(true);
        }
        parent::_construct();
    }
    
    public function _prepareLayout()
    {
        /*
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setCanLoadCalendarJs(true);
        }
        */
        return parent::_prepareLayout();
    }
    
    
    public function getFieldHtml($aField)
    {
        $sSetName = 'shippmethod';
        
        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getAttributeHtml($aField, $sSetName, 'onepage');
    }
    
    public function getCustomFieldList($iTplPlaceId)
    {
        $iStepId = Mage::helper('aitcheckattrib')->getStepId('shippmethod');
        
        if (!$iStepId) return false;

        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getCheckoutAtrributeList($iStepId, $iTplPlaceId, 'onepage');
    } 
}