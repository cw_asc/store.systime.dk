<?php
/**
 * Magento
 *
 */

class Aitoc_Aitcheckattrib_Block_Checkout_Onepage_Payment extends Mage_Checkout_Block_Onepage_Payment
{
    
    protected function _construct()
    {
        parent::_construct();
    }
    
    
    public function getFieldHtml($aField)
    {
        $sSetName = 'payment';
        
        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getAttributeHtml($aField, $sSetName, 'onepage');
    }
    
    public function getCustomFieldList($iTplPlaceId)
    {
        $iStepId = Mage::helper('aitcheckattrib')->getStepId('payment');
        
        if (!$iStepId) return false;

        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getCheckoutAtrributeList($iStepId, $iTplPlaceId, 'onepage');
    }
    
    public function getAttributeEnableHtml($aField)
    {
        $sSetName = 'payment';
        
        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getAttributeEnableHtml($aField, $sSetName);
    }
    
     
}