<?php
/**
 * Magento
 *
 */

class Aitoc_Aitcheckattrib_Block_Checkout_Multishipping_Overview extends Mage_Checkout_Block_Multishipping_Overview
{
    
    // overwright parent
    protected function _construct()
    {
        parent::_construct();
    }
    
    public function getFieldHtml($aField)
    {
        $sSetName = 'multi';
        
        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getAttributeHtml($aField, $sSetName, 'multishipping');
    }
    
    public function getAttributeEnableHtml($aField)
    {
        $sSetName = 'multi';
        
        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getAttributeEnableHtml($aField, $sSetName);
    }
    
    public function getCustomFieldList($iTplPlaceId)
    {
        $iStepId = Mage::helper('aitcheckattrib')->getStepId('mult_overview');
        
        if (!$iStepId) return false;

        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getCheckoutAtrributeList($iStepId, $iTplPlaceId, 'multishipping');
    } 
}