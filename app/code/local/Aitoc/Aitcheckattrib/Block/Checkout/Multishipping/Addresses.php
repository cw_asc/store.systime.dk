<?php
/**
 * Magento
 *
 */

class Aitoc_Aitcheckattrib_Block_Checkout_Multishipping_Addresses extends Mage_Checkout_Block_Multishipping_Addresses
{
    
    protected function _construct()
    {
        Mage::getModel('aitcheckattrib/aitcheckattrib')->checkDatabaseInstall();	    
        parent::_construct();
    }
    
    public function _prepareLayout()
    {
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setCanLoadCalendarJs(true);
        }
        
        return parent::_prepareLayout();
    }
    
    
    public function getFieldHtml($aField)
    {
        $sSetName = 'multi';
        
        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getAttributeHtml($aField, $sSetName, 'multishipping');
    }
    
    public function getCustomFieldList($iTplPlaceId)
    {
        $iStepId = Mage::helper('aitcheckattrib')->getStepId('mult_addresses');
        
        if (!$iStepId) return false;

        return Mage::getModel('aitcheckattrib/aitcheckattrib')->getCheckoutAtrributeList($iStepId, $iTplPlaceId, 'multishipping');
    } 
}