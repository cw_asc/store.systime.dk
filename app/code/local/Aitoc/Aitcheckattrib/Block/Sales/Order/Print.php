<?php

class Aitoc_Aitcheckattrib_Block_Sales_Order_Print  extends Mage_Sales_Block_Order_Print  {

    public function getOrderCustomData()
    {
        $iStoreId = $this->getOrder()->getStoreId();

        $oFront = Mage::app()->getFrontController();
        
        $iOrderId = $oFront->getRequest()->getParam('order_id');
        
        $oAitcheckattrib  = Mage::getModel('aitcheckattrib/aitcheckattrib');

        $aCustomAtrrList = $oAitcheckattrib->getOrderCustomData($iOrderId, $iStoreId, false);
        
        return $aCustomAtrrList;
    }
    

}

?>