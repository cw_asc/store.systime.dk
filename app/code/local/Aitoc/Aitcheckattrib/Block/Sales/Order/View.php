<?php

class Aitoc_Aitcheckattrib_Block_Sales_Order_View  extends Mage_Sales_Block_Order_View  {

	public function _construct()
    {
        Mage::getModel('aitcheckattrib/aitcheckattrib')->checkDatabaseInstall();	    
        
    	parent::_construct();
        $this->setTemplate('aitcheckattrib/sales/order/view.phtml');
    }
    
        
    public function getOrderCustomData()
    {
        $iStoreId = $this->getOrder()->getStoreId();

        $oFront = Mage::app()->getFrontController();
        
        $iOrderId = $oFront->getRequest()->getParam('order_id');
        
        $oAitcheckattrib  = Mage::getModel('aitcheckattrib/aitcheckattrib');

        $aCustomAtrrList = $oAitcheckattrib->getOrderCustomData($iOrderId, $iStoreId, false);
        
        return $aCustomAtrrList;
    }

}

?>