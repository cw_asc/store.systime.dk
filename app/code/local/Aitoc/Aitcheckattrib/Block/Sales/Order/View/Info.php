<?php

class Aitoc_Aitcheckattrib_Block_Sales_Order_View_Info  extends Mage_Adminhtml_Block_Sales_Order_View_Info  {

	public function _construct()
    {
        Mage::getModel('aitcheckattrib/aitcheckattrib')->checkDatabaseInstall();	    
    	parent::_construct();
    }
    
        
    public function getOrderCustomData()
    {
        $iStoreId = $this->getOrder()->getStoreId();

        $oFront = Mage::app()->getFrontController();
        
        $iOrderId = $oFront->getRequest()->getParam('order_id');
        
        $oAitcheckattrib  = Mage::getModel('aitcheckattrib/aitcheckattrib');

        $aCustomAtrrList = $oAitcheckattrib->getOrderCustomData($iOrderId, $iStoreId, true);
        
        return $aCustomAtrrList;
    }

}

?>