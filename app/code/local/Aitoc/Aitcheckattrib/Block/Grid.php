<?php

class Aitoc_Aitcheckattrib_Block_Grid  extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      
      $this->setId('aitcheckattribgrid');
      $this->setDefaultSort('attribute_code');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
      $this->setTemplate('aitcheckattrib/grid.phtml');
  }

  protected function _prepareCollection()
  {
      $type='aitoc_checkout';
      
      $this->type=$type;
      $collection = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setEntityTypeFilter( Mage::getModel('eav/entity')->setType($type)->getTypeId() )
            ->addVisibleFilter();
      
      $this->setCollection($collection);
      return parent::_prepareCollection();
      
  }

  protected function _prepareColumns()
  {
      $this->addColumn('attribute_code', array(
            'header'=>Mage::helper('catalog')->__('Attribute Code'),
            'sortable'=>true,
            'index'=>'attribute_code'
        ));

        $this->addColumn('frontend_label', array(
            'header'=>Mage::helper('catalog')->__('Attribute Label'),
            'sortable'=>true,
            'index'=>'frontend_label'
        ));

        $this->addColumn('frontend_input', array(
            'header'=>Mage::helper('catalog')->__('Input Type'),
            'sortable'=>true,
            'index'=>'frontend_input',
            'type' => 'options',
            'options' => array(
                'text'          => Mage::helper('catalog')->__('Text Field'),
                'textarea'      => Mage::helper('catalog')->__('Text Area'),
                'date'          => Mage::helper('catalog')->__('Date'),
                'boolean'       => Mage::helper('catalog')->__('Yes/No'),
                'multiselect'   => Mage::helper('catalog')->__('Multiple Select'),
                'select'        => Mage::helper('catalog')->__('Dropdown'),
            ),
        ));
/*
        $this->addColumn('is_visible', array(
            'header'=>Mage::helper('catalog')->__('Visible'),
            'sortable'=>true,
            'index'=>'is_visible_on_front',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('Yes'),
                '0' => Mage::helper('catalog')->__('No'),
            ),
            'align' => 'center',
        ));

        $this->addColumn('is_global', array(
            'header'=>Mage::helper('catalog')->__('Scope'),
            'sortable'=>true,
            'index'=>'is_global',
            'type' => 'options',
            'options' => array(
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE =>Mage::helper('catalog')->__('Store View'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE =>Mage::helper('catalog')->__('Website'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL =>Mage::helper('catalog')->__('Global'),
            ),
            'align' => 'center',
        ));
*/
        $this->addColumn('is_filterable', array(
            'header'=>Mage::helper('catalog')->__('Attribute Placeholder'),
            'sortable'=>true,
            'index'=>'is_filterable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('On Top'),
                '2' => Mage::helper('catalog')->__('At the bottom'),
            ),
            'align' => 'left',
        ));
        
        $this->addColumn('is_required', array(
            'header'=>Mage::helper('catalog')->__('Required'),
            'sortable'=>true,
            'index'=>'is_required',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('Yes'),
                '0' => Mage::helper('catalog')->__('No'),
            ),
            'align' => 'center',
        ));
        
        
/*
        $this->addColumn('is_user_defined', array(
            'header'=>Mage::helper('catalog')->__('System'),
            'sortable'=>true,
            'index'=>'is_user_defined',
            'type' => 'options',
            'align' => 'center',
            'options' => array(
                '0' => Mage::helper('catalog')->__('Yes'),   // intended reverted use
                '1' => Mage::helper('catalog')->__('No'),    // intended reverted use
            ),
        ));
*/
        
 /************    START AITOC CHECKOUT ATTRIBUTES          ************/

        $this->addColumn('is_searchable', array(
            'header'=>Mage::helper('catalog')->__('Step (for one page)'),
            'sortable'=>true,
            'index'=>'is_searchable',
            'type' => 'options',
            'options' => Mage::helper('aitcheckattrib')->getStepData('onepage', 'hash'),
#            'align' => 'center',
        ));
 
        $this->addColumn('is_comparable', array(
            'header'=>Mage::helper('catalog')->__('Step (for multi-address)'),
            'sortable'=>true,
            'index'=>'is_comparable',
            'type' => 'options',
            'options' => Mage::helper('aitcheckattrib')->getStepData('multipage', 'hash'),
#            'align' => 'center',
        ));
 
 
 
//        print_r(Mage::helper('aitcheckattrib')->getStepHash('aaa'));

 /************    FINISH AITOC CHECKOUT ATTRIBUTES          ************/
        
/*
        $this->addColumn('is_filterable', array(
            'header'=>Mage::helper('catalog')->__('Use In Layered Navigation'),
            'sortable'=>true,
            'index'=>'is_filterable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('Filterable (with results)'),
                '2' => Mage::helper('catalog')->__('Filterable (no results)'),
                '0' => Mage::helper('catalog')->__('No'),
            ),
            'align' => 'center',
        ));
        $this->addColumn('is_comparable', array(
            'header'=>Mage::helper('catalog')->__('Comparable'),
            'sortable'=>true,
            'index'=>'is_comparable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('Yes'),
                '0' => Mage::helper('catalog')->__('No'),
            ),
            'align' => 'center',
        ));
*/		
#		$this->addExportType('*/*/exportCsv', Mage::helper('aitcheckattrib')->__('CSV'));
#		$this->addExportType('*/*/exportXml', Mage::helper('aitcheckattrib')->__('XML'));
	  
      return parent::_prepareColumns();
  }

  public function addNewButton(){
  	return $this->getButtonHtml(
  		Mage::helper('aitcheckattrib')->__('New Attribute'), //label
  		"setLocation('".$this->getUrl('*/*/new', array('attribute_id'=>0))."')", //url
  		"scalable add" //classe css
  		);
  }
  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('attribute_id' => $row->getAttributeId()));
  }
}

?>