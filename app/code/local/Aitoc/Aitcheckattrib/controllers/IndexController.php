<?php

class Aitoc_Aitcheckattrib_IndexController extends Mage_Adminhtml_Controller_Action
{

    protected $_checkoutTypeId;
	
	protected $_type;

    public function preDispatch()
    {
        parent::preDispatch();
        Mage::getModel('aitcheckattrib/aitcheckattrib')->checkDatabaseInstall();	    
        
        $this->_checkoutTypeId = Mage::getModel('eav/entity')->setType('aitoc_checkout')->getTypeId();
        $this->_type = 'checkout';
    }
    
	protected function _initAction($ids=null) {
		$this->loadLayout($ids);
		
		return $this;
	}
	
	public function indexAction()
	{
        Mage::getModel('aitcheckattrib/aitcheckattrib')->checkDatabaseInstall();	    
        
		$this->_initAction()
			->_setActiveMenu('system/aitoc')
			->_addContent($this->getLayout()->createBlock('aitcheckattrib/Grid'));
		$this->renderLayout();
	}

    public function editAction() {
        
        Mage::getModel('aitcheckattrib/aitcheckattrib')->checkDatabaseInstall();	    
        
		$id     = (int)$this->getRequest()->getParam('attribute_id');
		$model  = Mage::getModel('eav/entity_attribute');
		$model->load($id);
		if(0!==$id){
//			$db = Mage::getSingleton('core/resource')->getConnection('core_write');
//		$model->setData("sort_order",$db->fetchOne("select sort_order from eav_entity_attribute where attribute_id=$id"));
		}
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('aitcheckattrib_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('system/aitoc');

#			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
#			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this
				->_addContent($this->getLayout()->createBlock('aitcheckattrib/edit'))
				->_addLeft($this->getLayout()->createBlock('aitcheckattrib/edit_tabs'))
				;

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('aitcheckattrib')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
	
	public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);

        $attributeCode  = $this->getRequest()->getParam('attribute_code');
        $attributeId    = $this->getRequest()->getParam('attribute_id');
        $this->_entityTypeId=$this->_checkoutTypeId;
        
        $attribute = Mage::getModel('eav/entity_attribute')
            ->loadByCode($this->_entityTypeId, $attributeCode);

        if ($attribute->getId() && !$attributeId) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('catalog')->__('Attribute with the same code already exists'));
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }
        $this->getResponse()->setBody($response->toJson());
    }
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('catalog/entity_attribute');
            /* @var $model Mage_Catalog_Model_Entity_Attribute */

            if ($id = $this->getRequest()->getParam('attribute_id')) {

                $model->load($id);

                $data['attribute_code'] = $model->getAttributeCode();
                $data['frontend_input'] = $model->getFrontendInput();
            }
			
            $defaultValueField = $model->getDefaultValueByInput($data['frontend_input']);
            if ($defaultValueField) {
                $data['default_value'] = $this->getRequest()->getParam($defaultValueField);
            }
			
			// process website/store assign

			if (!isset($data['is_visible_in_advanced_search']))
			{
			    $data['is_visible_in_advanced_search'] = 0;
			}
			
			if (!$data['is_visible_in_advanced_search']) // is not global
			{
    			if (isset($data['assign_website']) AND $data['assign_website'])
    			{
    			    $data['apply_to'] = implode(',', array_keys($data['assign_website']));
    			}
    			else 
    			{
    			    $data['apply_to'] = '';
    			}
    			
    			if (isset($data['assign_store']) AND $data['assign_store'])
    			{
    			    $sCommonData = '';
    			    
    			    $aStoreHash = array();
    			    
    			    foreach ($data['assign_store'] as $iWebsiteKey => $aStoreData)
    			    {
    			        $aStoreHash[] = implode(',', $aStoreData);
    			    }
    			    
    			    $data['note'] = implode(',', $aStoreHash);
    			}
    			else 
    			{
    			    $data['note'] = '';
    			}
			}
			
			$model->addData($data);
			
			try {
				
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}
				
				$model->save();
				$id=$model->getId();
				
                /**
                 * Clear translation cache because attribute labels are stored in translation
                 */
//                Mage::app()->cleanCache(array(Mage_Core_Model_Translate::CACHE_TAG));
                Mage::getSingleton('adminhtml/session')->setAttributeData(false);
				
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('aitcheckattrib')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('attribute_id' => $id));
					return;
				}
				
				$this->_redirect('*/*/index/filter//');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('attribute_id' => $this->getRequest()->getParam('attribute_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('aitcheckattrib')->__('Unable to find item to save'));
        $this->_redirect('*/*/index/filter//');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('attribute_id') > 0 ) {
			try {
				$model = Mage::getModel('eav/entity_attribute');
				 
				$model->setId($this->getRequest()->getParam('attribute_id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/index/filter//');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('attribute_id' => $this->getRequest()->getParam('attribute_id')));
			}
		}
		$this->_redirect('*/*/index/filter//');
	}

    public function massDeleteAction() {
        $categoriesattributesIds = $this->getRequest()->getParam('aitcheckattrib');
        if(!is_array($categoriesattributesIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($categoriesattributesIds as $categoriesattributesId) {
                    $categoriesattributes = Mage::getModel('eav/entity_attribute')->load($categoriesattributesId);
                    $categoriesattributes->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($categoriesattributesIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index/filter//');
    }
	
    public function massStatusAction()
    {
//        $categoriesattributesIds = $this->getRequest()->getParam('aitcheckattrib');
//        if(!is_array($categoriesattributesIds)) {
//            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
//        } else {
//            try {
//                foreach ($categoriesattributesIds as $categoriesattributesId) {
//                    $categoriesattributes = Mage::getSingleton('categoriesattributes/categoriesattributes')
//                        ->load($categoriesattributesId)
//                        ->setStatus($this->getRequest()->getParam('status'))
//                        ->setIsMassupdate(true)
//                        ->save();
//                }
//                $this->_getSession()->addSuccess(
//                    $this->__('Total of %d record(s) were successfully updated', count($categoriesattributesIds))
//                );
//            } catch (Exception $e) {
//                $this->_getSession()->addError($e->getMessage());
//            }
//        }
//        $this->_redirect('*/*/index');
    }
  
/*    
    public function exportCsvAction()
    {
        $fileName   = $this->_type.'attributes.csv';
        $content    = $this->getLayout()->createBlock('aitcheckattrib/grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = $this->_type.'attributes.xml';
        $content    = $this->getLayout()->createBlock('aitcheckattrib/grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }
*/
/*
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
*/    
}

?>