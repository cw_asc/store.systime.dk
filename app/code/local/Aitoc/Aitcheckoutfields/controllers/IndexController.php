<?php

class Aitoc_Aitcheckoutfields_IndexController extends Mage_Adminhtml_Controller_Action
{

    protected $_checkoutTypeId;
	
	protected $_type;

    public function preDispatch()
    {
        parent::preDispatch();
        Mage::getModel('aitcheckoutfields/aitcheckoutfields')->checkDatabaseInstall();	    
        
        $this->_checkoutTypeId = Mage::getModel('eav/entity')->setType('aitoc_checkout')->getTypeId();
        $this->_type = 'checkout';
    }
    
	protected function _initAction($ids=null) {
		$this->loadLayout($ids);
		
		return $this;
	}
	
	public function indexAction()
	{
        Mage::getModel('aitcheckoutfields/aitcheckoutfields')->checkDatabaseInstall();	    
        
		$this->_initAction()
			->_setActiveMenu('system/aitoc')
			->_addContent($this->getLayout()->createBlock('aitcheckoutfields/Grid'));
		$this->renderLayout();
	}

    public function editAction() {
        
        Mage::getModel('aitcheckoutfields/aitcheckoutfields')->checkDatabaseInstall();	    
        
		$id     = (int)$this->getRequest()->getParam('attribute_id');
		$model  = Mage::getModel('eav/entity_attribute');
		$model->load($id);
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('aitcheckoutfields_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('system/aitoc');

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this
				->_addContent($this->getLayout()->createBlock('aitcheckoutfields/edit'))
				->_addLeft($this->getLayout()->createBlock('aitcheckoutfields/edit_tabs'))
				;

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('aitcheckoutfields')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
	
	public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);

        $attributeCode  = $this->getRequest()->getParam('attribute_code');
        $attributeId    = $this->getRequest()->getParam('attribute_id');
        $this->_entityTypeId=$this->_checkoutTypeId;
        
        $attribute = Mage::getModel('eav/entity_attribute')
            ->loadByCode($this->_entityTypeId, $attributeCode);

        if ($attribute->getId() && !$attributeId) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('catalog')->__('Attribute with the same code already exists'));
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }
        $this->getResponse()->setBody($response->toJson());
    }
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('catalog/entity_attribute');
            /* @var $model Mage_Catalog_Model_Entity_Attribute */

            if ($id = $this->getRequest()->getParam('attribute_id')) {

                $model->load($id);

                $data['attribute_code'] = $model->getAttributeCode();
                $data['frontend_input'] = $model->getFrontendInput();
            }
			
            $sRealInput = $data['frontend_input'];
            
            if ($data['frontend_input'] == 'checkbox')
            {
                $data['frontend_input'] = 'multiselect';
            }
            
            if ($data['frontend_input'] == 'radio')
            {
                $data['frontend_input'] = 'select';
            }
            
            $defaultValueField = $model->getDefaultValueByInput($data['frontend_input']);
            if ($defaultValueField) {
                $data['default_value'] = $this->getRequest()->getParam($defaultValueField);
            }
			
			// process website/store assign

			if (!isset($data['is_visible_in_advanced_search']))
			{
			    $data['is_visible_in_advanced_search'] = 0;
			}
			
			if (!$data['is_visible_in_advanced_search']) // is not global
			{
    			if (isset($data['assign_website']) AND $data['assign_website'])
    			{
    			    $data['apply_to'] = implode(',', array_keys($data['assign_website']));
    			}
    			else 
    			{
    			    $data['apply_to'] = '';
    			}
    			
    			if (isset($data['assign_store']) AND $data['assign_store'])
    			{
    			    $sCommonData = '';
    			    
    			    $aStoreHash = array();
    			    
    			    foreach ($data['assign_store'] as $iWebsiteKey => $aStoreData)
    			    {
    			        $aStoreHash[] = implode(',', $aStoreData);
    			    }
    			    
    			    $data['note'] = implode(',', $aStoreHash);
    			}
    			else 
    			{
    			    $data['note'] = '';
    			}
			}
			
			$model->addData($data);
			
			try {
				
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}
				
				$model->setAitocflag(true);
				
				$model->save();
				$id=$model->getId();
				
				// save descs
				
				$aDescription = array();
				
				if (isset($data['frontend_desc']) AND $data['frontend_desc'])
				{
				    $aDescription = $data['frontend_desc'];
				}
				
				Mage::getModel('aitcheckoutfields/aitcheckoutfields')->saveAttributeDescription($id, $aDescription);
				
				if ($data['frontend_input'] != $sRealInput)
				{
				    $oUpdateModel = Mage::getModel('catalog/entity_attribute');
				    $oUpdateModel->load($id);
				    
				    $oUpdateModel->addData(array('frontend_input' => $sRealInput));
    				$oUpdateModel->setAitocflag(true);
				    $oUpdateModel->save();
				}
				
                /**
                 * Clear translation cache because attribute labels are stored in translation
                 */
                Mage::getSingleton('adminhtml/session')->setAttributeData(false);
				
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('aitcheckoutfields')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('attribute_id' => $id));
					return;
				}
				
				$this->_redirect('*/*/index/filter//');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('attribute_id' => $this->getRequest()->getParam('attribute_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('aitcheckoutfields')->__('Unable to find item to save'));
        $this->_redirect('*/*/index/filter//');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('attribute_id') > 0 ) {
			try {
				$model = Mage::getModel('eav/entity_attribute');
				 
				$model->setId($this->getRequest()->getParam('attribute_id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/index/filter//');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('attribute_id' => $this->getRequest()->getParam('attribute_id')));
			}
		}
		$this->_redirect('*/*/index/filter//');
	}

    public function massDeleteAction() {
        $categoriesattributesIds = $this->getRequest()->getParam('aitcheckoutfields');
        if(!is_array($categoriesattributesIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($categoriesattributesIds as $categoriesattributesId) {
                    $categoriesattributes = Mage::getModel('eav/entity_attribute')->load($categoriesattributesId);
                    $categoriesattributes->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($categoriesattributesIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index/filter//');
    }
}

?>