<?php

class Aitoc_Aitcheckoutfields_Model_Aitcheckoutfields extends Mage_Eav_Model_Entity_Attribute
{
    
    protected $_aCheckoutAtrrList;
    protected $_sEntityTypeCode     = 'aitoc_checkout';
    protected $_sCustomAttrTable    = 'aitoc_order_entity_custom';
    protected $_sDescAttrTable      = 'aitoc_custom_attribute_description';
    
    
    public function _construct()
    {
        parent::_construct();
    }
    
    public function getAtrributeLabel($iAttributeId, $iStoreId = 0)
    {
        if (!$iAttributeId) return false;
        
		$oAttribute  = Mage::getModel('eav/entity_attribute');
		$oAttribute->load($iAttributeId);

		if (!$oAttribute->getData()) return false;
		
		if (!$iStoreId)
		{
		    $iStoreId = Mage::app()->getStore()->getId();
		}
		
        $values = array();
        $values[0] = $oAttribute->getFrontend()->getLabel();
        // it can be array and cause bug
        
        $frontendLabel = $oAttribute->getFrontend()->getLabel();
        if (is_array($frontendLabel)) {
            $frontendLabel = array_shift($frontendLabel);
        }
        $translations = Mage::getModel('core/translate_string')
           ->load(Mage_Catalog_Model_Entity_Attribute::MODULE_NAME.Mage_Core_Model_Translate::SCOPE_SEPARATOR.$frontendLabel)
           ->getStoreTranslations();
           
        foreach ($this->getStores() as $store) {
            if ($store->getId() != 0) {
                $values[$store->getId()] = isset($translations[$store->getId()]) ? $translations[$store->getId()] : '';
            }
        }
        
        if (isset($values[$iStoreId]) AND $values[$iStoreId])
        {
            $sLabel = $values[$iStoreId];
        }
        else 
        {
            $sLabel = $values[0];
        }
        
        return $sLabel;
    }    
    
    
    public function getStores()
    {
        $stores = $this->getData('stores');
        if (is_null($stores)) {
            $stores = Mage::getModel('core/store')
                ->getResourceCollection()
                ->setLoadDefault(true)
                ->load();
            $this->setData('stores', $stores);
        }
        return $stores;
    }

    public function getAttributeOptionValues($sFieldId, $iStoreId, $aOptionIdList)
    {
        if (!$sFieldId OR !$iStoreId OR !$aOptionIdList) return false;
        
        $valuesCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setAttributeFilter($sFieldId)
            ->setStoreFilter($iStoreId, true)
            ->load();
            
        if (!is_array($aOptionIdList))
        {
            $aOptionIdList = array($aOptionIdList);
        }
            
        $aValueList = array();
        
        foreach ($valuesCollection as $item) 
        {
            if (in_array($item->getId(), $aOptionIdList)) 
            {
                $aValueList[] = $item->getValue();
            }
        }
        
        return $aValueList;
    }
    
    public function getAttributeHtml($aField, $sSetName, $sPageType)
    {
        $oView = new Zend_View();

        $iItemId = $aField['attribute_id'];
        $sPrefix = 'aitoc_checkout_';

        $sFieldId = $sSetName . ':' . $sPrefix . $iItemId;
        
        $sLabel = $this->getAtrributeLabel($iItemId);
        
        $sHtml = '<label for="' . $sFieldId . '">' . $sLabel . '';
        
        if ($aField['is_required'])
        {
            $sHtml .= '<span class="required">*</span>';
        }
        
        $sHtml .= '</label><br /> ';

        $sFieldName     = $sSetName . '[' . $sPrefix . $iItemId . ']';
        $sFieldValue    = $this->getCustomValue($aField, $sPageType);
        
        $sFieldClass = '';
        
        if ($aField['frontend_class'])
        {
            $sFieldClass .= $aField['frontend_class'];
        }
        
        if ($aField['is_required'])
        {
            $sFieldClass .= ' required-entry';
        }
        
        $aParams = array
        (
            'id' => $sFieldId,
#                    'class' => 'validate-zip-international required-entry input-text', // to do - check
            'class' => $sFieldClass, // to do - check
            'title' => $sLabel,
        );
                
        switch ($aField['frontend_input'])
        {
            case 'text':
                $aParams['class'] .= ' input-text';
                $sHtml .= $oView->formText($sFieldName, $sFieldValue, $aParams);
            break;    
            
            case 'textarea':
                $aParams['class'] .= ' input-text';
//                $aParams['style'] = 'height:50px; width:100%';
                $aParams['style'] = 'height:50px;';
				// anders@crius.dk 2010-03-09 - maxlength added
				$aParams['maxlength'] = '254';
				$aParams['onkeyup'] = 'return ismaxlength(this)';
                $sHtml .= $oView->formTextarea($sFieldName, $sFieldValue, $aParams);
				// anders@crius.dk 2010-03-09 - script added
				$sHtml .= '
				<script language="javascript">
				function ismaxlength(obj){
				var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
				if (obj.getAttribute && obj.value.length>mlength)
				obj.value=obj.value.substring(0,mlength)
				}
				</script>
				';
            break;    
            
            case 'select':
                $select = Mage::getModel('core/layout')->createBlock('core/html_select')
                    ->setName($sFieldName)
                    ->setId($sFieldId)
                    ->setTitle($sLabel)
#                    ->setClass('validate-select')
                    ->setClass($sFieldClass)
                    ->setValue($sFieldValue)
                    ->setOptions($this->getOptionValues($iItemId));
                
                    $sHtml .= $select->getHtml();
            break;    
            
            case 'multiselect':
                $select = Mage::getModel('core/layout')->createBlock('core/html_select')
                    ->setName($sFieldName . '[]')
                    ->setId($sFieldId)
                    ->setTitle($sLabel)
//                    ->setClass('validate-select')
                    ->setClass($sFieldClass)
                    ->setValue($sFieldValue)
                    ->setExtraParams('multiple')
                    ->setOptions($this->getOptionValues($iItemId));
                
                    $sHtml .= $select->getHtml();
            break;    
            
            case 'checkbox':
                
#            $selectHtml = '<ul id="options-'.$_option->getId().'-list" class="options-list">';
            $selectHtml = '<ul id="options-'.$sFieldId.'-list" class="options-list">';
            $require = ($aField['is_required']) ? ' validate-one-required-by-name' : '';
            $arraySign = '';
            /*
            switch ($_option->getType()) {
                case Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO:
                    $type = 'radio';
                    $class = 'radio';
                    if (!$_option->getIsRequire()) {
                        $selectHtml .= '<li><input type="radio" id="options_'.$_option->getId().'" class="'.$class.' product-custom-option" name="options['.$_option->getId().']" onclick="opConfig.reloadPrice()" value="" checked="checked" /><span class="label"><label for="options_'.$_option->getId().'">' . $this->__('None') . '</label></span></li>';
                    }
                    break;
                case Mage_Catalog_Model_Product_Option::OPTION_TYPE_CHECKBOX:
                    $type = 'checkbox';
                    $class = 'checkbox';
                    $arraySign = '[]';
                    break;
            }
            */
                    $type = 'checkbox';
                    $class = 'checkbox';
                    $arraySign = '[]';
                    
            $count = 0;
            
            if ($aOptionHash = $this->getOptionValues($iItemId))
            {
                foreach ($aOptionHash as $iKey => $sValue) 
                {
                    $count++;
                    
                    $sChecked = '';
                    
                    if ($sFieldValue AND in_array($iKey, $sFieldValue))
                    {
                        $sChecked = 'checked';
                    }
                    
                    $selectHtml .= '<li>' .
                                   '<input type="'.$type.'" class="'.$class.' '.$require.' product-custom-option" name="'.$sFieldName.''.$arraySign.'" id="'.$sFieldId.'_'.$count.'" value="'.$iKey.'" '.$sChecked.' />' .
                                   '<span class="label"><label for="'.$sFieldId.'_'.$count.'">'.$sValue.'</label></span>';
    /*                               
                    if ($aField['is_required'] AND 1 == 2) {
                        $selectHtml .= '<script type="text/javascript">' .
                                        '$(\''.$sFieldId.'_'.$count.'\').advaiceContainer = \''.$sFieldId.'-container\';' .
                                        '$(\''.$sFieldId.'_'.$count.'\').callbackFunction = \'validateOptionsCallback\';' .
                                       '</script>';
                    }
    */                
                    $selectHtml .= '</li>';
                }
            }
            $selectHtml .= '</ul>';
                
                    $sHtml .= $selectHtml;
            break;    
            
            case 'radio':
                
            $selectHtml = '<ul id="options-'.$sFieldId.'-list" class="options-list">';
            $require = ($aField['is_required']) ? ' validate-one-required-by-name' : '';
            
                    $type = 'radio';
                    $class = 'radio';
                    if (!$aField['is_required']) {
                        $selectHtml .= '<li><input type="radio" id="'.$sFieldId.'" class="'.$class.' product-custom-option" name="'.$sFieldName.'" value="" checked="checked" /><span class="label"><label for="options_'.$sFieldId.'">' . Mage::helper('catalog')->__('None') . '</label></span></li>';
                    }
                    
            $count = 0;
            
            if ($aOptionHash = $this->getOptionValues($iItemId))
            {
                foreach ($aOptionHash as $iKey => $sValue) 
                {
                    $count++;
                    
                    $sChecked = '';
                    
                    if ($iKey == $sFieldValue)
                    {
                        $sChecked = 'checked';
                    }
                    
                    $selectHtml .= '<li>' .
                                   '<input type="'.$type.'" class="'.$class.' '.$require.' product-custom-option" name="'.$sFieldName.''.'" id="'.$sFieldId.'_'.$count.'" value="'.$iKey.'" '.$sChecked.' />' .
                                   '<span class="label"><label for="'.$sFieldId.'_'.$count.'">'.$sValue.'</label></span>';
                                   
                    $selectHtml .= '</li>';
                }
            }
            $selectHtml .= '</ul>';
                
                    $sHtml .= $selectHtml;
            break;    
            
#    <ul id="options-2-list" class="options-list"><li><input type="checkbox" class="checkbox  validate-one-required-by-name product-custom-option" onclick="opConfig.reloadPrice()" name="options[2][]" id="options_2_2" value="3" /><span class="label"><label for="options_2_2">1111 <span class="price-notice">+<span class="price">$111.00</span></span></label></span><script type="text/javascript">$('options_2_2').advaiceContainer = 'options-2-container';$('options_2_2').callbackFunction = 'validateOptionsCallback';</script></li><li><input type="checkbox" class="checkbox  validate-one-required-by-name product-custom-option" onclick="opConfig.reloadPrice()" name="options[2][]" id="options_2_3" value="4" /><span class="label"><label for="options_2_3">33333 <span class="price-notice">+<span class="price">$1,499.85</span></span></label></span><script type="text/javascript">$('options_2_3').advaiceContainer = 'options-2-container';$('options_2_3').callbackFunction = 'validateOptionsCallback';</script></li></ul>                        <span id="options-2-container"></span>

            
#    <ul id="options-1-list" class="options-list"><li>
#<input type="radio" class="radio  validate-one-required-by-name product-custom-option" onclick="opConfig.reloadPrice()" name="options[1]" id="options_1_2" value="1" />
#<span class="label"><label for="options_1_2">ddddddddddddd <span class="price-notice">+<span class="price">$8,888.00</span></span></label></span><script type="text/javascript">$('options_1_2').advaiceContainer = 'options-1-container';$('options_1_2').callbackFunction = 'validateOptionsCallback';</script></li><li><input type="radio" class="radio  validate-one-required-by-name product-custom-option" onclick="opConfig.reloadPrice()" name="options[1]" id="options_1_3" value="2" /><span class="label"><label for="options_1_3">2222222 <span class="price-notice">+<span class="price">$100,000,000.00</span></span></label></span><script type="text/javascript">$('options_1_3').advaiceContainer = 'options-1-container';$('options_1_3').callbackFunction = 'validateOptionsCallback';</script></li></ul>                        <span id="options-1-container"></span>

            
            case 'boolean':
                
                $yesno = array(
                    array(
                        'value' => 0,
                        'label' => Mage::helper('catalog')->__('No')
                    ),
                    array(
                        'value' => 1,
                        'label' => Mage::helper('catalog')->__('Yes')
                    ));
                
                $select = Mage::getModel('core/layout')->createBlock('core/html_select')
                    ->setName($sFieldName)
                    ->setId($sFieldId)
                    ->setTitle($sLabel) 
                    ->setClass('validate-select')
                    ->setValue($sFieldValue)
                    ->setOptions($yesno);
                
                    $sHtml .= $select->getHtml();
            break;    
            
            case 'date':
                $calendar = Mage::getModel('core/layout')
                    ->createBlock('core/html_date')
                    ->setName($sFieldName)
                    ->setId($sFieldId)
                    ->setTitle($sLabel) 
                    ->setClass($sFieldClass)
                    ->setValue($sFieldValue)
#                    ->setClass('input-text'.$require)
                    ->setImage(Mage::getDesign()->getSkinUrl('images/grid-cal.gif'))
                    ->setFormat(Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT));
        
                $sHtml .= $calendar->getHtml();
            break;    
        }

        $aDescHash = $this->getAttributeDescription($iItemId);
        
	    $iStoreId = Mage::app()->getStore()->getId();
        
        if ($aDescHash AND isset($aDescHash[$iStoreId]))
        {
            $sHtml .= '<br>' . $aDescHash[$iStoreId];
        }
        
        return $sHtml;        
    }

    public function getAttributeEnableHtml($aField, $sSetName)
    {
        $iItemId = $aField['attribute_id'];
        $sPrefix = 'aitoc_checkout_';
        
        $sFieldId = $sSetName . ':' . $sPrefix . $iItemId;
            
        if ($aField['frontend_input'] == 'radio' OR $aField['frontend_input'] == 'checkbox')
        {
            $sHtml = ''; 
            
            $aOptionHash = $this->getOptionValues($iItemId);            
            
            $count = 0;
            
            if ($aOptionHash)
            {
                foreach ($aOptionHash as $sVal)
                {
                    $count++;
                    $sHtml .= ' $("' . $sFieldId.'_'.$count . '").disabled = false; ';
                }
            }
        }
        else 
        {
            $sHtml = ' $("' . $sFieldId . '").disabled = false; ';
        }
        
        return $sHtml;        
    }

    public function getCustomValue($aField, $sPageType)
    {
        if (!$aField) return false;
        
        if ($aField['frontend_input'] == 'multiselect' OR $aField['frontend_input'] == 'checkbox')
        {
            $sValue = explode(',', $aField['default_value']);
        }
        else 
        {
            $sValue = $aField['default_value'];            
        }
        
        if (isset($_SESSION['aitoc_checkout_used'][$sPageType][$aField['attribute_id']]))
        {
            return $_SESSION['aitoc_checkout_used'][$sPageType][$aField['attribute_id']];
        }
        
        return $sValue;
    }
    
    public function getOptionValues($sFieldId)
    {
        if (!$sFieldId) return false;

        $valuesCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setAttributeFilter($sFieldId)
            ->setStoreFilter()
            ->load();
            
        $aOptionHash    = array();
        $aRawOptionHash = array();
        $aSortHash      = array();
        
        foreach ($valuesCollection as $item) 
        {
            $aSortHash[$item->getId()] = $item->getData('sort_order');
            $aRawOptionHash[$item->getId()] = $item->getValue();
        }
        
        if ($aSortHash)
        {
            asort($aSortHash);
            
            foreach ($aSortHash as $iKey => $sVal)
            {
                $aOptionHash[$iKey] = $aRawOptionHash[$iKey];
            }
        }
        
        return $aOptionHash;
    }    
    
    public function getCheckoutAtrributeList($iStepId, $iTplPlaceId, $sPageType)
    {
        if ($this->_aCheckoutAtrrList === NULL)
        {
            if (!isset($_SESSION['aitoc_checkout_used']))
            {
                $_SESSION['aitoc_checkout_used'] = array();
            }
            
            switch ($sPageType)
            {
                case 'onepage':
                    $sStepField = 'is_searchable'; // hook for input source (one page)
                break;    
                
                case 'multishipping':
                    $sStepField = 'is_comparable'; // hook for input source (multi shipping)
                break;    
            }
            
		    $iStoreId = Mage::app()->getStore()->getId();
		    $iSiteId  = Mage::app()->getWebsite()->getId();
		    
            $sWhereScope = '(is_visible_in_advanced_search = 1 OR (find_in_set("' . $iStoreId . '", note) OR find_in_set("' . $iSiteId . '", apply_to)))';
            
            $collection = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setEntityTypeFilter( Mage::getModel('eav/entity')->setType($this->_sEntityTypeCode)->getTypeId() );
                    
            $collection->getSelect()->where('main_table.' . $sStepField . ' > 0');                
            $collection->getSelect()->where($sWhereScope);                
            $collection->getSelect()->order('position ASC');                
                    
            $aAttributeList = $collection->getData();
            
            $this->_aCheckoutAtrrList = array();
       
            if ($aAttributeList)
            {
                foreach ($aAttributeList as $aItem)
                {
                    $this->_aCheckoutAtrrList[$aItem[$sStepField]][$aItem['is_filterable']][$aItem['attribute_id']] = $aItem;
                }
            }
        }
        
        if (isset($this->_aCheckoutAtrrList[$iStepId][$iTplPlaceId]))
        {
            return $this->_aCheckoutAtrrList[$iStepId][$iTplPlaceId];
        }
        else 
        {
            return false;
        }
    }    
    
    public function checkStepHasRequired($iStepId, $sPageType)
    {
        switch ($sPageType)
        {
            case 'onepage':
                $sStepField = 'is_searchable'; // hook for input source (one page)
            break;    
            
            case 'multishipping':
                $sStepField = 'is_comparable'; // hook for input source (multi shipping)
            break;    
        }
        
        $collection = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setEntityTypeFilter( Mage::getModel('eav/entity')->setType($this->_sEntityTypeCode)->getTypeId() );
                
        $collection->getSelect()->where('main_table.' . $sStepField . ' = ' . $iStepId);                
        $collection->getSelect()->where('is_required = 1');                
        $collection->getSelect()->order('position ASC');                
                
        $aAttributeList = $collection->getData();
#d($collection->getSelect()->__toString());        
        if ($aAttributeList)
        {
            return true;
        }
        else 
        {
            return false;
        }
        
    }    
    
    public function setCustomValue($sFieldName, $sFieldValue, $sPageType)
    {
        if (!$sFieldName OR !$sPageType) return false;

        if (strpos($sFieldName, 'itoc_checkout_'))
        {
            $aNameParts = explode('_', $sFieldName);
            
            $sFieldId = $aNameParts[2];
            
            $_SESSION['aitoc_checkout_used'][$sPageType][$sFieldId] = $sFieldValue;
        }
        
        return true;
    }       
    
    public function saveCustomOrderData($iOrderId, $sPageType)
    {
        if (!$iOrderId OR !$sPageType) return false;

        if (isset($_SESSION['aitoc_checkout_used'][$sPageType]) AND $_SESSION['aitoc_checkout_used'][$sPageType])
        {
            $oDb = Mage::getSingleton('core/resource')->getConnection('core_write');
            
            foreach ($_SESSION['aitoc_checkout_used'][$sPageType] as $sFieldId => $sValue)
            {
                if (is_array($sValue))
                {
                    $sValue = implode(',', $sValue);                    
                }
                
                $aDBInfo = array
                (
                    'entity_id'     => $iOrderId,
                    'attribute_id'  => $sFieldId,
                    'value'         => $sValue,
                );
        
                $oDb->insert($this->_sCustomAttrTable, $aDBInfo);
            }
        }
        
        return true;
    }      
    
    public function clearCheckoutSession($sPageType)
    {
        $_SESSION['aitoc_checkout_used'][$sPageType] = array();
    }
        
    public function checkDatabaseInstall()
    {
        if (isset($_SESSION['aitoc_checkout_database_install']))
        {
            return true;
        }        
        else 
        {
            // check main table install

            $oDb = Mage::getSingleton('core/resource')->getConnection('core_write');
            
            $sSql = 'show tables like "' . $this->_sCustomAttrTable . '" ';
            
            if (!$oDb->fetchAll($sSql)) // table does not exist
            {
                $sSql = '
CREATE TABLE IF NOT EXISTS `' . $this->_sCustomAttrTable . '` (
  `value_id` int(11) NOT NULL auto_increment,
  `attribute_id` smallint(5) unsigned NOT NULL default "0",
  `entity_id` int(10) unsigned NOT NULL default "0",
  `value` text NOT NULL,
  PRIMARY KEY  (`value_id`),
  UNIQUE KEY `UNQ_AITOC_ENTITY_ATTRIBUTE` (`entity_id`,`attribute_id`),
  KEY `FK_aitoc_order_entity_custom_attribute` (`attribute_id`),
  KEY `FK_aitoc_order_entity_custom` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;                
                ';
                
                $oDb->query($sSql);
            }

            // check desc table install

            $oDb = Mage::getSingleton('core/resource')->getConnection('core_write');
            
            $sSql = 'show tables like "' . $this->_sDescAttrTable . '" ';
            
            if (!$oDb->fetchAll($sSql)) // table does not exist
            {
                $sSql = '
CREATE TABLE IF NOT EXISTS `' . $this->_sDescAttrTable . '` (
  `attribute_id` int(10) unsigned NOT NULL default "0",
  `store_id` int(10) unsigned NOT NULL default "0",
  `value` text NOT NULL,
  UNIQUE KEY `UNQ_AITOC_DESC_ATTRIBUTE` (`store_id`,`attribute_id`),
  KEY `FK_aitoc_attr_custom_attribute` (`attribute_id`),
  KEY `FK_aitoc_desc_custom_attribute` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;                
                ';
                
                $oDb->query($sSql);
            }
            
            
            // check atribute group field install

            $oE = Mage::getModel('eav/mysql4_entity_type');
            
            $oSelect = $oDb->select()  ->from($oE->getMainTable())
                                        ->where('entity_type_code = ?', $this->_sEntityTypeCode);
            
            if (!$oDb->fetchAll($oSelect)) // record does not exist
            {
                $aDBInfo = array
                (
                    'entity_type_code' => $this->_sEntityTypeCode,
                );
                
                $oDb->insert($oE->getMainTable(), $aDBInfo);
            }
                
            $_SESSION['aitoc_checkout_database_install'] = true;
                       
            return true;
        }
        
    }
    
    public function getOrderCustomData($iOrderId, $iStoreId, $bForAdmin)
    {
        if (!$iOrderId) return false;
        
        $aCustomAtrrList = array();
        
        $oDb = Mage::getSingleton('core/resource')->getConnection('core_write');

        $select = $oDb->select()
            ->from(array('c' => $this->_sCustomAttrTable), '*')
#            ->joinInner(array('p' => $this->getTable('catalog/product')), 'o.product_id=p.entity_id', array())
            ->where('c.entity_id=?', $iOrderId)
        ;
        
        $oAttribute = Mage::getModel('eav/entity_attribute');

        $aItemList = $oDb->fetchAll($select);
        
        if ($aItemList)
        {
            foreach ($aItemList as $aItem)
            {
                $oAttribute->load($aItem['attribute_id']);
                
                $aAttrData = $oAttribute->getData();
                
                if ($aAttrData)
                {
                    if ($bForAdmin)
                    {
                        $bShowAttribute = $aAttrData['is_used_for_price_rules']; // fix for admin
                    }
                    else 
                    {
                        $bShowAttribute = $aAttrData['is_filterable_in_search']; // fix for member
                    }
                }
                else 
                {
                    $bShowAttribute = false;
                }
                
                if ($bShowAttribute)
                {
                    $sValue = '';
                    
                    switch ($aAttrData['frontend_input'])
                    {
                        case 'text':
                        case 'date': // to check?
                        case 'textarea':
                            $sValue = $aItem['value'];
                        break;
                            
                        case 'boolean':
                            
                            if ($aItem['value'])
                            {
                                $sValue = Mage::helper('catalog')->__('Yes');
                            }
                            else 
                            {
                                $sValue = Mage::helper('catalog')->__('No');
                            }
                            
                        break;
                            
                        case 'select':
                        case 'radio':
                            
                            $aValueList = $this->getAttributeOptionValues($aItem['attribute_id'], $iStoreId, $aItem['value']);
                            if ($aValueList)
                            {
                                $sValue = $aValueList[0];
                            }
                        break;    
                        
                        case 'multiselect':
                        case 'checkbox':
                            $aValueList = $this->getAttributeOptionValues($aItem['attribute_id'], $iStoreId, explode(',', $aItem['value']));
                            if ($aValueList)
                            {
                                $sValue = implode(', ', $aValueList);
                            }
                        break;    
                    }
                    
                    $aCustomData = array
                    (
                        'label' => $this->getAtrributeLabel($aItem['attribute_id'], $iStoreId),
                        'value' => $sValue,
                    );
                            
                    $aCustomAtrrList[] = $aCustomData;
                }
            }
        }        
        
        return $aCustomAtrrList;
    }    
    
    public function getSessionCustomData($sPageType, $iStoreId, $bForAdmin)
    {
        if (!$sPageType) return false;
        
        $aCustomAtrrList = array();
        
        $oAttribute = Mage::getModel('eav/entity_attribute');
        
        if (isset($_SESSION['aitoc_checkout_used'][$sPageType]) AND $_SESSION['aitoc_checkout_used'][$sPageType])
        {
            
            foreach ($_SESSION['aitoc_checkout_used'][$sPageType] as $sFieldId => $sValue)
            {
                
                $oAttribute->load($sFieldId);
                
                $aAttrData = $oAttribute->getData();
                
                if ($aAttrData)
                {
                    $bShowAttribute = true;
                    
                    if ($bForAdmin)
                    {
#                        $bShowAttribute = $aAttrData['is_used_for_price_rules']; // fix for admin
                    }
                    else 
                    {
#                        $bShowAttribute = $aAttrData['is_filterable_in_search']; // fix for member
                    }
                }
                else 
                {
                    $bShowAttribute = false;
                }
                
                if ($bShowAttribute)
                {
#                    $sValue = '';
                    
                    switch ($aAttrData['frontend_input'])
                    {
                        case 'text':
                        case 'date': // to check?
                        case 'textarea':
                            $sValue = $sValue;
                        break;
                            
                        case 'boolean':
                            
                            if ($sValue)
                            {
                                $sValue = Mage::helper('catalog')->__('Yes');
                            }
                            else 
                            {
                                $sValue = Mage::helper('catalog')->__('No');
                            }
                            
                        break;
                            
                        case 'select':
                        case 'radio':
                            
                            $aValueList = $this->getAttributeOptionValues($sFieldId, $iStoreId, $sValue);
                            if ($aValueList)
                            {
                                $sValue = $aValueList[0];
                            }
                        break;    
                        
                        case 'multiselect':
                        case 'checkbox':
                            $aValueList = $this->getAttributeOptionValues($sFieldId, $iStoreId, $sValue);
                            if ($aValueList)
                            {
                                $sValue = implode(', ', $aValueList);
                            }
                        break;    
                    }
                    
                    $aCustomData = array
                    (
                        'label' => $this->getAtrributeLabel($sFieldId, $iStoreId),
                        'value' => $sValue,
                    );
                            
                    $aCustomAtrrList[] = $aCustomData;
                }
            }
        }
        
        return $aCustomAtrrList;
    }    
    
    public function saveAttributeDescription($iAttributeId, $aDescriptionData)
    {
        $oDb = Mage::getSingleton('core/resource')->getConnection('core_write');
        
        $oDb->delete($this->_sDescAttrTable, 'attribute_id = ' . $iAttributeId);
        
        if ($aDescriptionData)
        {
            foreach ($aDescriptionData as $iStoreId => $sValue)
            {
                $aDBInfo = array
                (
                    'attribute_id'  => $iAttributeId,
                    'store_id'     => $iStoreId,
                    'value'         => $sValue,
                );
        
                $oDb->insert($this->_sDescAttrTable, $aDBInfo);
            }
        }
        
        return true;
    }
    
    public function getAttributeDescription($iAttributeId)
    {
        if (!$iAttributeId) return false;
        
        $oDb = Mage::getSingleton('core/resource')->getConnection('core_write');

        $select = $oDb->select()
            ->from(array('c' => $this->_sDescAttrTable), array('store_id', 'value'))
#            ->joinInner(array('p' => $this->getTable('catalog/product')), 'o.product_id=p.entity_id', array())
            ->where('c.attribute_id=?', $iAttributeId)
        ;
        
        $aItemList = $oDb->fetchPairs($select);
        
        return $aItemList;
    }
    
    
}
?>