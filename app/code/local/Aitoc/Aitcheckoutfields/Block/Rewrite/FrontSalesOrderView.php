<?php

class Aitoc_Aitcheckoutfields_Block_Rewrite_FrontSalesOrderView  extends Mage_Sales_Block_Order_View  {

	public function _construct()
    {
        Mage::getModel('aitcheckoutfields/aitcheckoutfields')->checkDatabaseInstall();	    
        
    	parent::_construct();
        $this->setTemplate('aitcommonfiles/design--frontend--default--default--template--sales--order--view.phtml');
    }
    
        
    public function getOrderCustomData()
    {
        $iStoreId = $this->getOrder()->getStoreId();

        $oFront = Mage::app()->getFrontController();
        
        $iOrderId = $oFront->getRequest()->getParam('order_id');
        
        $oAitcheckoutfields  = Mage::getModel('aitcheckoutfields/aitcheckoutfields');

        $aCustomAtrrList = $oAitcheckoutfields->getOrderCustomData($iOrderId, $iStoreId, false);
        
        return $aCustomAtrrList;
    }

}

?>