<?php

class Aitoc_Aitcheckoutfields_Block_Rewrite_FrontSalesOrderPrint  extends Mage_Sales_Block_Order_Print  {

    public function getOrderCustomData()
    {
        $iStoreId = $this->getOrder()->getStoreId();

        $oFront = Mage::app()->getFrontController();
        
        $iOrderId = $oFront->getRequest()->getParam('order_id');
        
        $oAitcheckoutfields  = Mage::getModel('aitcheckoutfields/aitcheckoutfields');

        $aCustomAtrrList = $oAitcheckoutfields->getOrderCustomData($iOrderId, $iStoreId, false);
        
        return $aCustomAtrrList;
    }

}

?>