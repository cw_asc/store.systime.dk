<?php

class Aitoc_Aitcheckoutfields_Block_Rewrite_AdminSalesOrderViewInfo  extends Mage_Adminhtml_Block_Sales_Order_View_Info  {

	public function _construct()
    {
        Mage::getModel('aitcheckoutfields/aitcheckoutfields')->checkDatabaseInstall();	    
    	parent::_construct();
    }
    
        
    public function getOrderCustomData()
    {
        $iStoreId = $this->getOrder()->getStoreId();

        $oFront = Mage::app()->getFrontController();
        
        $iOrderId = $oFront->getRequest()->getParam('order_id');
        
        $oAitcheckoutfields  = Mage::getModel('aitcheckoutfields/aitcheckoutfields');

        $aCustomAtrrList = $oAitcheckoutfields->getOrderCustomData($iOrderId, $iStoreId, true);
        
        return $aCustomAtrrList;
    }

}

?>