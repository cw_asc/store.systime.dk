<?php

class Aitoc_Aitcheckoutfields_Block_Rewrite_FrontSalesOrderEmailItems extends Mage_Sales_Block_Order_Email_Items
{
    public function _toHtml()
    {
        $sContent = '';
        
        $aCustomAtrrList = $this->getOrderCustomData();
        
        if ($aCustomAtrrList)
        {
            $sContent .= '<br><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
              <THEAD>
              <TR>
                <TH 
                style="BORDER-RIGHT: #bebcb7 1px solid; PADDING-RIGHT: 9px; BORDER-TOP: #bebcb7 1px solid; PADDING-LEFT: 9px; PADDING-BOTTOM: 6px; BORDER-LEFT: #bebcb7 1px solid; LINE-HEIGHT: 1em; PADDING-TOP: 5px; BORDER-BOTTOM: medium none" 
                align=left width="100%" bgColor=#d9e5ee>' . Mage::helper('sales')->__('Order Custom Data') . '</TH></TR></THEAD>
              <TBODY>
              <TR>
                <TD 
                style="BORDER-RIGHT: #bebcb7 1px solid; PADDING-RIGHT: 9px; BORDER-TOP: 0px; PADDING-LEFT: 9px; BACKGROUND: #f8f7f5; PADDING-BOTTOM: 9px; BORDER-LEFT: #bebcb7 1px solid; PADDING-TOP: 7px; BORDER-BOTTOM: #bebcb7 1px solid" 
                vAlign=top>';
            
            foreach ($aCustomAtrrList as $aItem)
            {
                $sContent .= '<b>' . $aItem['label'] . ':</b> ' . $aItem['value'] . '<br>';
            }
            
            $sContent .= '</TD></TR></TBODY></TABLE><BR>';
            
        }
        
        $sContent .= parent::_toHtml();
        
        return $sContent;
    }

    public function getOrderCustomData()
    {
        $iStoreId = $this->getOrder()->getStoreId();

        $oFront = Mage::app()->getFrontController();

        $sPathInfo = $oFront->getRequest()->getPathInfo();
        
        if ($sPathInfo AND strpos($sPathInfo, '/multishipping/'))
        {
            $sPageType = 'multishipping';
        }
        else 
        {
            $sPageType = 'onepage';
        }
 
       $oAitcheckoutfields  = Mage::getModel('aitcheckoutfields/aitcheckoutfields');

         $aCustomAtrrList = $oAitcheckoutfields->getSessionCustomData($sPageType, $iStoreId, true);

					// mri@systime.dk 2010-08-14
				// Session data is cleared when saving order [@see Aitoc_Aitcheckoutfields_Model_Rewrite_FrontCheckoutTypeOnepage::saveOrder and Aitoc_Aitcheckoutfields_Model_Aitcheckoutfields::clearCheckoutSession], and thus we cannot use session data when coming back from credit card authorization page.
				// However, the custom data has been saved to the order and we can get it from there.
				$orderId = $this->getOrder()->getId();
				$aCustomAtrrList = $oAitcheckoutfields->getOrderCustomData($orderId, $iStoreId, true);

				// Order hasn't been saved when not using credit card payment, so we get data from session when it cannot be retrieved from last order
				if (!$aCustomAtrrList) {
					$aCustomAtrrList = $oAitcheckoutfields->getSessionCustomData($sPageType, $iStoreId, true);
				}

        return $aCustomAtrrList;
    }
    
    
}
