<?php

class Aitoc_Aitcheckoutfields_Block_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        Mage::getModel('aitcheckoutfields/aitcheckoutfields')->checkDatabaseInstall();
        
        $iTypeId = Mage::getModel('eav/entity')->setType('aitoc_checkout')->getTypeId();
        
        $model = Mage::registry('aitcheckoutfields_data');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend'=>Mage::helper('catalog')->__('Attribute Properties'))
        );
        if ($model->getId()) {
            $fieldset->addField('attribute_id', 'hidden', array(
                'name' => 'attribute_id',
            ));
        }

        $this->_addElementTypes($fieldset);

        $yesno = array(
            array(
                'value' => 0,
                'label' => Mage::helper('catalog')->__('No')
            ),
            array(
                'value' => 1,
                'label' => Mage::helper('catalog')->__('Yes')
            ));

        $fieldset->addField('attribute_code', 'text', array(
            'name'  => 'attribute_code',
            'label' => Mage::helper('catalog')->__('Attribute Code'),
            'title' => Mage::helper('catalog')->__('Attribute Code'),
            'note'  => Mage::helper('catalog')->__('For internal use. Must be unique with no spaces'),
            'class' => 'validate-code',
            'required' => true,
        ));
/*
        $scopes = array(
            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE =>Mage::helper('catalog')->__('Store View'),
            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE =>Mage::helper('catalog')->__('Website'),
            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL =>Mage::helper('catalog')->__('Global'),
        );

        if ($model->getAttributeCode() == 'status' || $model->getAttributeCode() == 'tax_class_id') {
            unset($scopes[Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE]);
        }

        $fieldset->addField('is_global', 'select', array(
            'name'  => 'is_global',
            'label' => Mage::helper('catalog')->__('Scope'),
            'title' => Mage::helper('catalog')->__('Scope'),
            'note'  => Mage::helper('catalog')->__('Declare attribute value saving scope'),
            'values'=> $scopes
        ));
*/
        $inputTypes = array(
            array(
                'value' => 'text',
                'label' => Mage::helper('catalog')->__('Text Field')
            ),
            array(
                'value' => 'textarea',
                'label' => Mage::helper('catalog')->__('Text Area')
            ),
            array(
                'value' => 'date',
                'label' => Mage::helper('catalog')->__('Date')
            ),
            array(
                'value' => 'boolean',
                'label' => Mage::helper('catalog')->__('Yes/No')
            ),
            array(
                'value' => 'multiselect',
                'label' => Mage::helper('catalog')->__('Multiple Select')
            ),
            array(
                'value' => 'select',
                'label' => Mage::helper('catalog')->__('Dropdown')
            ),
            array(
                'value' => 'checkbox',
                'label' => Mage::helper('catalog')->__('Checkbox')
            ),
            array(
                'value' => 'radio',
                'label' => Mage::helper('catalog')->__('Radiobutton')
            ),
        );
        /*
        if($type==="catalog_category"){
         $inputTypes[]=   array(
                'value' => 'image',
                'label' => Mage::helper('catalog')->__('Image')

        );
        }
*/
        $response = new Varien_Object();
        $response->setTypes(array());
        //Mage::dispatchEvent('adminhtml_product_attribute_types', array('response'=>$response));

        $_disabledTypes = array();
        $_hiddenFields = array();
        foreach ($response->getTypes() as $iTypeId) {
            $inputTypes[] = $iTypeId;
            if (isset($iTypeId['hide_fields'])) {
                $_hiddenFields[$iTypeId['value']] = $iTypeId['hide_fields'];
            }
            if (isset($iTypeId['disabled_types'])) {
                $_disabledTypes[$iTypeId['value']] = $iTypeId['disabled_types'];
            }
        }
        Mage::register('attribute_type_hidden_fields', $_hiddenFields);
        Mage::register('attribute_type_disabled_types', $_disabledTypes);


        $fieldset->addField('frontend_input', 'select', array(
            'name' => 'frontend_input',
#            'label' => Mage::helper('catalog')->__('Catalog Input Type for Store Owner'),
            'label' => Mage::helper('catalog')->__('Input Type'),
            'title' => Mage::helper('catalog')->__('Input Type'),
            'value' => 'text',
            'values'=> $inputTypes
        ));
        
        $fieldset->addField('frontend_class', 'select', array(
            'name'  => 'frontend_class',
#            'label' => Mage::helper('catalog')->__('Input Validation for Store Owner'),
            'label' => Mage::helper('catalog')->__('Input Validation'),
            'title' => Mage::helper('catalog')->__('Input Validation'),
            'values'=>  array(
                array(
                    'value' => '',
                    'label' => Mage::helper('catalog')->__('None')
                ),
                array(
                    'value' => 'validate-number',
                    'label' => Mage::helper('catalog')->__('Decimal Number')
                ),
                array(
                    'value' => 'validate-digits',
                    'label' => Mage::helper('catalog')->__('Integer Number')
                ),
                array(
                    'value' => 'validate-email',
                    'label' => Mage::helper('catalog')->__('Email')
                ),
                array(
                    'value' => 'validate-url',
                    'label' => Mage::helper('catalog')->__('Url')
                ),
                array(
                    'value' => 'validate-alpha',
                    'label' => Mage::helper('catalog')->__('Letters')
                ),
                array(
                    'value' => 'validate-alphanum',
                    'label' => Mage::helper('catalog')->__('Letters(a-zA-Z) or Numbers(0-9)')
                ),
            )
        ));

        $fieldset->addField('is_filterable', 'select', array(
            'name'  => 'is_filterable',
            'label' => Mage::helper('catalog')->__('Attribute Placeholder'),
            'title' => Mage::helper('catalog')->__('Attribute Placeholder'),
            'note'  => Mage::helper('catalog')->__('If you choose "On Top", the attribute will be displayed in the top placeholder of the checkout step and vice versa if you choose "At the Bottom"'),
            'required' => true,
            'values'=>  array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('catalog')->__('On Top')
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('catalog')->__('At the bottom')
                ),
            )
        ));

        $fieldset->addField('position', 'text', array(
            'name'  => 'position',
            'label' => Mage::helper('catalog')->__('Position in Placeholder'),
            'title' => Mage::helper('catalog')->__('Position in Placeholder'),
            'note' => Mage::helper('catalog')->__('Can be used to manage attributes\' positions when there are more than one attribute in one placeholder'),
            'class' => 'validate-digits',
        ));
        
        
        
    /****** champs cach�s dans le formulaire **********/
        $fieldset->addField('entity_type_id', 'hidden', array(
            'name' => 'entity_type_id',
            'value' => $iTypeId
        ));
        
        
        
        $fieldset->addField('is_user_defined', 'hidden', array(
            'name' => 'is_user_defined',
            'value' => 1
        ));
        /*
        $fieldset->addField('attribute_set_id', 'hidden', array(
            'name' => 'attribute_set_id',
            'value' => Mage::getModel('eav/entity')->setType($iTypeId)->getTypeId()
        ));
        */
        /*
        $fieldset->addField('attribute_group_id', 'hidden', array(
            'name' => 'attribute_group_id',
            'value' => Mage::getModel('eav/entity')->setType($iTypeId)->getTypeId()
        ));
        */
         
 /*******************************************************/
 
 /************    START AITOC CHECKOUT ATTRIBUTES          ************/
/* 
         $fieldset->addField('default_value', 'text', array(
            'name' => 'default_value',
            'label' => Mage::helper('catalog')->__('Default Value'),
            'title' => Mage::helper('catalog')->__('Default Value'),
#            'class' => 'validate-text',
        ));
*/
        $fieldset->addField('default_value_text', 'text', array(
            'name' => 'default_value_text',
            'label' => Mage::helper('catalog')->__('Default value'),
            'title' => Mage::helper('catalog')->__('Default value'),
            'value' => $model->getDefaultValue(),
        ));

        $fieldset->addField('default_value_yesno', 'select', array(
            'name' => 'default_value_yesno',
            'label' => Mage::helper('catalog')->__('Default value'),
            'title' => Mage::helper('catalog')->__('Default value'),
            'values' => $yesno,
            'value' => $model->getDefaultValue(),
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('default_value_date', 'date', array(
            'name'   => 'default_value_date',
            'label'  => Mage::helper('catalog')->__('Default value'),
            'title'  => Mage::helper('catalog')->__('Default value'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'value'  => $model->getDefaultValue(),
            'format'       => $dateFormatIso
        ));

        $fieldset->addField('default_value_textarea', 'textarea', array(
            'name' => 'default_value_textarea',
            'label' => Mage::helper('catalog')->__('Default value'),
            'title' => Mage::helper('catalog')->__('Default value'),
            'value' => $model->getDefaultValue(),
        ));
        
        
        
        // for one page checkout
        
        $fieldset->addField('is_searchable', 'select', array(
            'name'  => 'is_searchable',
            'label' => Mage::helper('catalog')->__('Step (for one page)'),
            'title' => Mage::helper('catalog')->__('Step (for one page)'),
            'note'  => Mage::helper('catalog')->__('Add the attribute to a step of the one page checkout'),
            'values'=> Mage::helper('aitcheckoutfields')->getStepData('onepage'),
        ));
        
        // for multi-address checkout
        
        $fieldset->addField('is_comparable', 'select', array(
            'name'  => 'is_comparable',
            'label' => Mage::helper('catalog')->__('Step (for multi-address)'),
            'title' => Mage::helper('catalog')->__('Step (for multi-address)'),
            'note'  => Mage::helper('catalog')->__('Add the attribute to a step of the multi-address checkout'),
            'values'=> Mage::helper('aitcheckoutfields')->getStepData('multipage'),
        ));
        
        
        
 /************    FINISH AITOC CHECKOUT ATTRIBUTES          ************/
 
 /*
        $fieldset->addField('is_unique', 'select', array(
            'name' => 'is_unique',
            'label' => Mage::helper('catalog')->__('Unique Value'),
            'title' => Mage::helper('catalog')->__('Unique Value (not shared with other products)'),
            'note'  => Mage::helper('catalog')->__('Not shared with other products'),
            'values' => $yesno,
        ));
*/
        $fieldset->addField('is_required', 'select', array(
            'name' => 'is_required',
            'label' => Mage::helper('catalog')->__('Values Required'),
            'title' => Mage::helper('catalog')->__('Values Required'),
            'values' => $yesno,
        ));

        $fieldset->addField('is_used_for_price_rules', 'select', array(
            'name' => 'is_used_for_price_rules',
            'label' => Mage::helper('catalog')->__('Display on Order Page in Admin Area'),
            'title' => Mage::helper('catalog')->__('Display on Order Page in Admin Area'),
            'value' => 1,
            'values' => $yesno,
        ));

        $fieldset->addField('is_filterable_in_search', 'select', array(
            'name' => 'is_filterable_in_search',
            'label' => Mage::helper('catalog')->__('Display on Order Page in Member Area'),
            'title' => Mage::helper('catalog')->__('Display on Order Page in Member Area'),
            'value' => 1,
            'values' => $yesno,
        ));

        // -----

/*
        // frontend properties fieldset
        $fieldset = $form->addFieldset('front_fieldset',
        	array('legend'=>Mage::helper('catalog')->__('Frontend Properties')));

     

        $fieldset->addField('position', 'text', array(
            'name' => 'position',
            'label' => Mage::helper('catalog')->__('Position'),
            'title' => Mage::helper('catalog')->__('Position In Layered Navigation'),
            'note' => Mage::helper('catalog')->__('Position of attribute in layered navigation block'),
            'class' => 'validate-digits',
        ));

        if ($model->getIsUserDefined() || !$model->getId()) {
            $fieldset->addField('is_visible_on_front', 'select', array(
                'name' => 'is_visible_on_front',
                'label' => Mage::helper('catalog')->__('Visible on Catalog Pages on Front-end'),
                'title' => Mage::helper('catalog')->__('Visible on Catalog Pages on Front-end'),
                'values' => $yesno,
            ));
        }
        
        $fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => Mage::helper('catalog')->__('Order'),
            'title' => Mage::helper('catalog')->__('Order in form'),
            'note' => Mage::helper('catalog')->__('order of attribute in form edit/create. Leave blank for form bottom.'),
            'class' => 'validate-digits',
        	'value' => $model->getAttributeSetInfo()
        ));
        
*/
        if ($model->getId()) {
            $form->getElement('attribute_code')->setDisabled(1);
            $form->getElement('frontend_input')->setDisabled(1);

            if (isset($disableAttributeFields[$model->getAttributeCode()])) {
                foreach ($disableAttributeFields[$model->getAttributeCode()] as $field) {
                    $form->getElement($field)->setDisabled(1);
                }
            }
        }
       /* if (!$model->getIsUserDefined() && $model->getId()) {
            $form->getElement('is_unique')->setDisabled(1);
        }
		*/
        //var_dump($model->getData());exit;
        $form->addValues($model->getData());
        
                

        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _getAdditionalElementTypes()
    {
        return array(
            'apply' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_apply')
        );
    }

}
