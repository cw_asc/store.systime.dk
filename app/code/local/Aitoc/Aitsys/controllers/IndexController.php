<?php
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */

class Aitoc_Aitsys_IndexController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
        $this->loadLayout()
			->_setActiveMenu('system/aitsys')
            ->_addContent($this->getLayout()->createBlock('aitsys/edit')->initForm());
		$this->renderLayout();
	}

	public function newAction() {
		$this->_forward('edit');
	}
	
	public function saveAction() {
	    
		if ($data = $this->getRequest()->getPost('enable')) 
		{
		    if ($aErrorList = Mage::getModel('aitsys/aitsys')->saveData($data))
		    {
		        foreach ($aErrorList as $aError)
		        {
                    $this->_getSession()->addError($aError);
		        }
		    }
		    else 
		    {
                $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Modules settings were saved successfully'));
		    }
		}
	    
        $this->_redirect('*/*');
	}

}

?>