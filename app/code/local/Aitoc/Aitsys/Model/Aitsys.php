<?php
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */


class Aitoc_Aitsys_Model_Aitsys extends Mage_Eav_Model_Entity_Attribute
{
    
    protected $_sAitocModulePrefix      = 'Aitoc_';
    protected $_aModuleIgnoreName       = array('Aitoc_Aitinstall', 'Aitoc_Aitsys', 'Aitoc_Aitprepare');    
    protected $_sModuleRewriteNameOld   = 'Aitoc_Aitcheckattrib';    
    protected $_sModuleRewriteNameNew   = 'Aitoc_Aitcheckoutfields';    
    
    protected $_aModuleClassRewrite     = array();    
    protected $_aModulePriority         = array();    
    protected $_aModuleConfig           = array();    
    protected $_aConfigReplace          = array();    
    protected $_aFileMerge          = array();    
    protected $_aFileWriteContent       = array();    
    protected $_aErrorList              = array();    
    
    public function _construct()
    {
        parent::_construct();
    }

    protected function _getModuleOrder()
    {
        if ($this->_aModulePriority)
        {
            arsort($this->_aModulePriority);
            return array_keys($this->_aModulePriority);
        }
        else 
        {
            return false;
        }
    }

    protected function _getEtcDir()
    {
        $oConfig = Mage::getConfig();
        
        $sEtcDir = $oConfig->getOptions()->getEtcDir();
        
        if (!$sEtcDir) die('No config directory!');

        $sModuleEtcDir = $sEtcDir . '/modules';
        
        return $sModuleEtcDir;
    }

    protected function _getLocalDir()
    {
        $oConfig = Mage::getConfig();
        
        $sCodeDir = $oConfig->getOptions()->getCodeDir();

        $sLocalDir = $sCodeDir . '/local/';
        
        return $sLocalDir;
    }

    protected function _getDiffFilePath($sModuleDir, $sArea, $sFileKey)
    {
        if (!$sModuleDir OR !$sArea OR !$sFileKey) return false;
        
        $sDiffFilePath = $sModuleDir . '/data/template/' . $sArea . '/' . $sFileKey . '.diff';
        
        return $sDiffFilePath;
    }

    protected function _getModuleDir($sModuleName)
    {
        if (!$sModuleName) return false;
        
        $sModuleDir = $this->_getLocalDir() . str_replace('_', '/', $sModuleName);
        
        return $sModuleDir;
    }

    protected function _getModuleHash()
    {
        $aModuleHash = array();
        
        $sModuleEtcDir = $this->_getEtcDir();
        
        if ($handle = opendir($sModuleEtcDir)) 
        {
            while (false !== ($sFile = readdir($handle))) 
            {
                if ($sFile AND (strlen($sFile) > 3) AND strpos($sFile, $this->_sAitocModulePrefix) === 0)
                {
                    $sModuleName = substr($sFile, 0, strpos($sFile, '.'));
                }
                else 
                {
                    $sModuleName = '';
                }
                
                if ($sModuleName AND !in_array($sModuleName, $this->_aModuleIgnoreName))
                {
                    $sFullPath = $sModuleEtcDir . '/' . $sFile;
                    
                    $aModuleHash[$sFile] = $sFullPath;
                }
            }
            closedir($handle);
        }        

        return $aModuleHash;
    }

    public function getAitocModuleList()
    {
        $aModuleList = array();
        
        $aModuleHash    = $this->_getModuleHash();
        $sModuleEtcDir  = $this->_getEtcDir();
        
        if ($aModuleHash)
        {
            foreach ($aModuleHash as $sFile => $sFullPath)
            {
                $aFileParts = explode('.', $sFile);
                
                $sModuleKey = $aFileParts[0];
                
                $oModuleMainConfig = simplexml_load_file($sFullPath);
                
                $bIsActive = (bool)('true' == $oModuleMainConfig->modules->$sModuleKey->active);
                
                if ($oModuleMainConfig->modules->$sModuleKey->self_name)
                {
                    $sModulelabel = $oModuleMainConfig->modules->$sModuleKey->self_name;
                }
                else 
                {
                    $sModulelabel = $sModuleKey;
                }

                if ($this->_checkFileWritePermissions($sFullPath))
                {
                    $bFileAccess = true;
                }
                else 
                {
                    $bFileAccess = false;
                }
                
                $aModuleList[] = array
                (
                    'label' => $sModulelabel,
                    'key'   => $sModuleKey,
                    'value' => $bIsActive,
                    'access'=> $bFileAccess,
                    'file'  => $sFullPath,
                );
            }
        }
        
        return $aModuleList;
    }
    
    protected function _getModuleConfig($sModuleDir, $bCustom = false)
    {
        if (!$sModuleDir) return false;
        
        if ($bCustom)
        {
            $sConfigFile = $sModuleDir . '/etc/custom.data.xml';
        }
        else 
        {
            $sConfigFile = $sModuleDir . '/etc/config.data.xml';
        }
        
        if (file_exists($sConfigFile))
        {
            return simplexml_load_file($sConfigFile);
        }
        else 
        {
            return false;
        }
    }

    protected function _checkFileWritePermissions($sFilePath)
    {
    	if (!$sFilePath) return false;
    	
        $oFileSys = Mage::getModel('aitsys/aitfilesystem');

        if (file_exists($sFilePath))
        {
            if (!$oFileSys->isWriteable($sFilePath))
            {
                if (is_file($sFilePath))
                {
                    $this->_addError($sFilePath, 'no_access_file');
                }
                else 
                {
                    $this->_addError($sFilePath, 'no_access_dir');
                }
                return false;
            }
        }
        else 
        {
            if (!$oFileSys->isWriteable($sFilePath))
            {
                $this->_addError(dirname($sFilePath), 'no_access_dir');
                return false;
            }
        }
        
        return true;
    }

    protected function _addModuleClassRewrite($oModuleMainConfig, $sModuleFile)
    {
        if (!$oModuleMainConfig OR !$sModuleFile) return false;

        if ($oModuleMainConfig->global)
        {
            $aKeyHash = array('blocks', 'models');
            
            foreach ($aKeyHash as $sTypeKey)
            {
                if ($oModuleMainConfig->global->$sTypeKey)
                {
                    foreach ($oModuleMainConfig->global->$sTypeKey->children() as $sClassName => $oChildren)
                    {
                        if ($oChildren)
                        {
                            foreach ($oChildren->children() as $sChildName => $oData)
                            {
                                if ($oData AND $sChildName == 'rewrite')
                                {
                                    foreach ($oData->children() as $sDataName => $oDataValue)
                                    {
                                        $sKey = $sTypeKey . '___' . $sClassName . '___' . $sDataName;
                                        
                                        if (isset($this->_aModuleClassRewrite[$sKey]))
                                        {
                                            $this->_aModuleClassRewrite[$sKey]['replace'][$sModuleFile] = (string)$oDataValue;
                                            $this->_aModuleClassRewrite[$sKey]['count']++;
                                        }
                                        else 
                                        {
                                            $this->_aModuleClassRewrite[$sKey] = array
                                            (
                                                'replace'   => array($sModuleFile => (string)$oDataValue), 
                                                'count'     => 0, 
                                                'type'      => $sTypeKey, 
                                            );
                                        }
                                    }                                
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return true;
    }

    protected function _setRewriteClass($sKey, $aData, $aModuleOrder)
    {
        if (!$sKey OR !$aData OR !$aModuleOrder) return false;
        
        $aReplaceHash = array();
        
        $aKeyParts = explode('___', $sKey);
        
        $sClassType = '';
        
        switch ($aKeyParts[0])
        {
            case 'blocks':
                $sClassType = 'Block';
            break;    
            
            case 'models':
                $sClassType = 'Model';
            break;    
        }
        
        $sClassOrig = 'Mage '. ucwords($aKeyParts[1] . ' ' . $sClassType . ' ' . str_replace('_', ' ', $aKeyParts[2]));
        
        $sClassOrig = str_replace(' ', '_', $sClassOrig);
        
        $sConfigName    = '';
        $sPrevName      = '';
        
        $aReplaceHash = array();

        foreach ($aModuleOrder as $sModKey => $sModuleName)
        {
            if (!isset($aData['replace'][$sModuleName]))
            {
                unset($aModuleOrder[$sModKey]);
            }
        }   
        if ($aModuleOrder)
        {
            $aModuleOrderCopy = $aModuleOrder;
            
            if (sizeof($aModuleOrder) == 1)
            {
                $sConfigName = $aData['replace'][array_shift($aModuleOrderCopy)];
            }
            else 
            {
                $sConfigName = $aData['replace'][array_shift($aModuleOrderCopy)];
            }
            
            // get name of class for config
            
            $aModuleOrder = array_reverse($aModuleOrder);
            
            foreach ($aModuleOrder as $sModuleName)
            {
                $aReplaceHash[$sModuleName] = array
                (
                    'extends'   => $sPrevName,
                );
                
                $sFilePath = $this->_getLocalDir() . str_replace('_', '/', $aData['replace'][$sModuleName]);
                
                $aReplaceHash[$sModuleName]['path'] = $sFilePath;
                
                $aReplaceHash[$sModuleName]['original'] = $sClassOrig;
                
                $aReplaceHash[$sModuleName]['module'] = $aData['replace'][$sModuleName];

                $aReplaceHash[$sModuleName]['config'] = $sConfigName;
                
                $sPrevName = $aData['replace'][$sModuleName];
            }
        }
        
        return $aReplaceHash;
    }

    protected function _setFileMergeData($oModuleCustomConfig, $sModuleDir)
    {
        if (!$oModuleCustomConfig) return false;
        
        $oConfig     = Mage::getConfig();
        
        $aDestFileHash = array();
        
        if ($oModuleCustomConfig->template AND $oModuleCustomConfig->template->children())
        {
        	foreach ($oModuleCustomConfig->template->children() as $sAreaName => $aAreaConfig)
        	{
                if ($aAreaConfig AND $aAreaConfig->children())
                {
                	foreach ($aAreaConfig->children() as $sNodeKey => $aFileConfig)
                	{
                	    $aFilePathKey = $aFileConfig->attributes()->path;
                	    $bOptional = $aFileConfig->attributes()->optional ? (int)$aFileConfig->attributes()->optional : false;
                            $bOptional = $bOptional ? true : false;

                	    $sTplKey = $aFilePathKey . '.phtml.patch';
                	
                	    if (isset($this->_aFileMerge[$sTplKey]['optional']))
                	    {
                	        if($this->_aFileMerge[$sTplKey]['optional'])
                	        {
                	            $this->_aFileMerge[$sTplKey]['optional'] = $bOptional;
                	        }
                	    }
                	    else
                	    {
                	        $this->_aFileMerge[$sTplKey]['optional'] = $bOptional;
                	    }
                	    
                	    $sDiffFile = $sModuleDir . '/data/' .  $sTplKey;
                	    
                	    if (!file_exists($sDiffFile))
                	    {
                	        $this->_addError($sDiffFile, 'no_module_file');
                	    }

                	    $sFileDest = 'design/' . $sAreaName . '/default/default/template/aitcommonfiles/' . $aFilePathKey . '.phtml';
                	    
                	    $sDestFilePath = $oConfig->getOptions()->getAppDir() . '/' . $sFileDest;
                	    
                	    $aDestFileHash[$sDestFilePath] = 1;
                	    
                	    if (isset($this->_aFileMerge[$sTplKey]))
                	    {
                	        $this->_aFileMerge[$sTplKey]['modules'][$sModuleDir] = $sFileDest;
                	    }
                	    else 
                	    {
                	        $this->_aFileMerge[$sTplKey] = array(
                    	        'modules' => array(
                	                $sModuleDir => $sFileDest
                        	    )
                	        );
                	    }
                	}
                }
        	}
            
        }
        if ($oModuleCustomConfig->layout AND $oModuleCustomConfig->layout->children())
        {
        	foreach ($oModuleCustomConfig->layout->children() as $sAreaName => $aAreaConfig)
        	{
                if ($aAreaConfig AND $aAreaConfig->children())
                {
                	foreach ($aAreaConfig->children() as $sNodeKey => $aFileConfig)
                	{
                	    $aFilePathKey = $aFileConfig->attributes()->path;
                	    $bOptional = $aFileConfig->attributes()->optional ? (int)$aFileConfig->attributes()->optional : false;
                            $bOptional = $bOptional ? true : false;

                	    $sTplKey = $aFilePathKey . '.xml.patch';
                	
                	    if (isset($this->_aFileMerge[$sTplKey]['optional']))
                	    {
                	        if($this->_aFileMerge[$sTplKey]['optional'])
                	        {
                	            $this->_aFileMerge[$sTplKey]['optional'] = $bOptional;
                	        }
                	    }
                	    else
                	    {
                	        $this->_aFileMerge[$sTplKey]['optional'] = $bOptional;
                	    }
                	    
                	    $sDiffFile = $sModuleDir . '/data/' .  $sTplKey;
                	    
                	    if (!file_exists($sDiffFile))
                	    {
                	        $this->_addError($sDiffFile, 'no_module_file');
                	    }

                	    $sLayoutDestName   = 'aitoc' . substr($aFilePathKey, strrpos($aFilePathKey, '--') + 2);
                	    
                	    $sFileDest     = 'design/' . $sAreaName . '/default/default/layout/' . $sLayoutDestName . '.xml';
                	    
                	    $sDestFilePath = $oConfig->getOptions()->getAppDir() . '/' . $sFileDest;
                	    
                	    $aDestFileHash[$sDestFilePath] = 1;
                	    
                	    if (isset($this->_aFileMerge[$sTplKey]))
                	    {
                	        $this->_aFileMerge[$sTplKey]['modules'][$sModuleDir] = $sFileDest;
                	    }
                	    else 
                	    {
                	        $this->_aFileMerge[$sTplKey] = array(
                    	        'modules' => array(
                	                $sModuleDir => $sFileDest
                        	    )
                	        );
                	    }
                	}
                }
        	}
        }

        if ($aDestFileHash)
        {
            foreach ($aDestFileHash as $sFile => $sVal)
            {
                $this->_checkFileWritePermissions($sFile);
            }
        }
        
        return true;
    }

    public function getModulesStatusHash()
    {
    	$aModuleList = Mage::getModel('aitsys/aitsys')->getAitocModuleList();
    	$aModHash = array();
    	foreach ($aModuleList as $aMod)
    	{
    		$aModHash[$aMod['key']] = $aMod['value'];
    	}
    	return $aModHash;
    }
    
    public function saveData($aData, $aModuleHash = array())
    {
        if (!$aData) return false;
        
        if (!$aModuleHash)
        {
            $aModuleHash    = $this->_getModuleHash();
        }
        $sModuleEtcDir  = $this->_getEtcDir();
        
        
        // dispatching pre-disable event
        $aStatusHash = $this->getModulesStatusHash();
        foreach ($aStatusHash as $sModule => $bIsActive)
        {
        	if (!isset($aData[$sModule]) or !$aData[$sModule])
        	{
        		$aEventParams = array(
        		    'object'             => &$this, // modules will put errors to the current instance
        		    'aitocmodulename'    => $sModule,
        		);
        		Mage::dispatchEvent('aitoc_module_disable_before', $aEventParams);
        	}
        }
        // checking if we got any error from events
        $aErrors = $this->_getErrorList();
        if (!empty($aErrors))
        {
        	return $aErrors;
        }
    
        if ($aModuleHash)
        {
            foreach ($aModuleHash as $sFile => $sFullPath)
            {
                $sModuleName = substr($sFile, 0, strpos($sFile, '.'));
                
                $sModuleDir = $this->_getModuleDir($sModuleName);
                
                // get main module config
                
                $oModuleMainConfig = $this->_getModuleConfig($sModuleDir);
                
                // set module status
                if (isset($aData[$sModuleName]) AND $aData[$sModuleName])
                {
                    $sModuleActive = 'true';
                    $this->_addModuleClassRewrite($oModuleMainConfig, $sModuleName);
                    
                    // get custom module config
                    
                    $oModuleCustomConfig = $this->_getModuleConfig($sModuleDir, true);
                    
                    $this->_setFileMergeData($oModuleCustomConfig, $sModuleDir);
                }    
                else            
                {
                    $sModuleActive = 'false';
                }                
                // get base module config
                
                $oModuleBaseConfig = simplexml_load_file($sFullPath);
                
                $oModuleBaseConfig->modules->$sModuleName->active = $sModuleActive;
                
                if ($iPriority = $oModuleBaseConfig->modules->$sModuleName->priority)
                {
                    $this->_aModulePriority[$sModuleName] = (integer)$iPriority;
                }
                else 
                {
                    $this->_aModulePriority[$sModuleName] = 0;
                }
                
                $sFileContent = $oModuleBaseConfig->asXML();
                
                $this->_checkFileSaveWithContent($sFullPath, $sFileContent);
                
            }
            
            // set new php class inheritance
           
            if ($this->_aModuleClassRewrite)
            {
                $aModuleOrder = $this->_getModuleOrder();
               
                foreach ($this->_aModuleClassRewrite as $sKey => $aData)
                {
                    $aReplaceHash = $this->_setRewriteClass($sKey, $aData, $aModuleOrder);
                    
                    if ($aReplaceHash)
                    {
                        foreach ($aReplaceHash as $sModuleName => $aReplace)
                        {
                            if ($aReplace['extends']) // must change inheritance
                            {
                                // modify class inheritance
                                
                                $sExtends = ' extends ';
                                
                                $aReplaceData = array
                                (
                                    'search'    => array($sExtends . $aReplace['original']),
                                    'replace'   => array($sExtends . $aReplace['extends']),
                                );   
                                
                            }
                            else 
                            {
                                $aReplaceData = array();
                            }
                            
                            $this->_checkFileSaveWithBackup($aReplace['path'], 'php', $aReplaceData);
                            
                            $this->_aConfigReplace[$sModuleName][$aReplace['module']] = $aReplace['config'];         
                        }
                    }
                }
            }
            
            // save updated config modules files
    
            if ($this->_aConfigReplace)
            {
                foreach ($this->_aConfigReplace as $sModuleName => $aData)
                {
                    $aReplaceData = array();
                    
                    foreach ($aData as $sKey => $sVal)
                    {
                        $aReplaceData['search'][]  = $sKey;
                        $aReplaceData['replace'][] = $sVal;
                    }
                    
                    $sModuleDir = $this->_getModuleDir($sModuleName);
    
                    $sConfigFilePath = $sModuleDir . '/etc/config';
                    
                    $this->_checkFileSaveWithBackup($sConfigFilePath, 'xml', $aReplaceData);
                }
            }
            if (!$this->_aErrorList)
            {
                if ($this->_aFileMerge)
                {
                    $oPatch = Mage::getModel('aitsys/aitpatch');
                    
                    $oPatch->setPatchFiles($this->_aFileMerge);
                    
                    $aErrorList = $oPatch->applyPatchDryrun();
                    
                    if ($aErrorList)
                    {
                        foreach ($aErrorList as $aError)
                        {
                            $this->_addError($aError['file'], $aError['type']);
                        }
                    }
                    else 
                    {
                        $oPatch->applyPatch();
                    }
                }
                
                if (!$this->_aErrorList)
                {
                    $this->_saveAllFileContent();
                }
            }
        }
        
        Mage::app()->cleanCache();

        return $this->_getErrorList();
    }
    
    protected function _getErrorList()
    {
        if (!$this->_aErrorList) return false;
        
        return array_unique($this->_aErrorList);
    }
    
    
    protected function _saveAllFileContent()
    {
        if (!$this->_aFileWriteContent) return false;
        
        foreach ($this->_aFileWriteContent as $sFilePath => $sFileContent)
        {
            $fh = fopen($sFilePath, 'w');
            fwrite($fh, $sFileContent);
            fclose($fh);
        }
                    
        return true;
    }
    
    protected function _checkFileSaveWithBackup($sFileName, $sExtenstion, $aReplaceData)
    {
        if (!$sFileName OR !$sExtenstion) return false;
        
        $sOrigFilePath  = $sFileName . '.data.' . $sExtenstion;
        $sDestFilePath  = $sFileName . '.' . $sExtenstion;
        
        if (!file_exists($sOrigFilePath))
        {
            $this->_addError($sOrigFilePath, 'no_file');
        }
    
        if (file_exists($sDestFilePath))
        {
            // check for file permissions 

            if (!$this->_checkFileWritePermissions($sDestFilePath))
            {
#                $this->_addError($sDestFilePath, 'no_access_file');
            }
        }
        else 
        {
            // check for dir permissions 
            
            $sDirPath = substr($sFileName, 0, strrpos($sFileName, '/') + 1);
            
            if (!$this->_checkFileWritePermissions($sDirPath))
            {
#                $this->_addError($sDirPath, 'no_access_dir');
            }
        }
        
        if ($this->_aErrorList) return false;
        
        $fh = fopen($sOrigFilePath, 'r');
        $sFileContent = fread($fh, filesize($sOrigFilePath));                

        if ($aReplaceData) // must change inheritance
        {
            $sFileContent = str_ireplace($aReplaceData['search'], $aReplaceData['replace'], $sFileContent);
        }
                    
        $this->_aFileWriteContent[$sDestFilePath] = $sFileContent;
        
        return true;
    }
    
    protected function _checkFileSaveWithContent($sFilePath, $sFileContent)
    {
        if (!$sFilePath OR !$sFileContent) return false;
        
        if (!file_exists($sFilePath))
        {
            $this->_addError($sFilePath, 'no_file');
        }
        else 
        {
            // check for file permissions 
    
            if (!$this->_checkFileWritePermissions($sFilePath))
            {
#                $this->_addError($sFilePath, 'no_access_file');
            }
        }
    
        if ($this->_aErrorList) return false;
        
        $this->_aFileWriteContent[$sFilePath] = $sFileContent;
        
        return true;
    }
    
    protected function _addError($sFilePath, $sType)
    {
        if (!$sFilePath OR !$sType) return false;
        
        switch ($sType)
        {
            case 'no_file':
                $sMessage = 'File does not exist: %s';
            break;   
             
            case 'no_access_file':
                $sMessage = 'File does not have write permissions: %s';
            break;    
            
            case 'no_access_dir':
                $sMessage = 'Folder does not have write permissions: %s';
            break;    
            
            case 'no_module_file':
                $sMessage = 'Module installation file is absent: %s';
            break;    
            
            case 'file_uncompatible':
                $sMessage = 'Magento file is corrupted or changed: %s';
            break;    
        }
        
        $this->_aErrorList[] = Mage::helper('adminhtml')->__($sMessage, $sFilePath);
        
        return true;
    }
    
    public function addCustomError($sErrorMessage)
    {
    	$this->_aErrorList[] = Mage::helper('adminhtml')->__($sErrorMessage);
    }
    
    public function getAllowInstallErrors()
    {
        $aAitocModuleList = $this->getAitocModuleList();
        
        $aErrorList = array();
       
        if ($aAitocModuleList)
        {
            $sHasRewritePathOld = '';
            $sHasRewritePathNew = '';
            
            foreach ($aAitocModuleList as $aModule)
            {
                if ($aModule['key'] == $this->_sModuleRewriteNameOld)
                {
                    $sHasRewritePathOld = $aModule['file'];
                }
                
                if ($aModule['key'] == $this->_sModuleRewriteNameNew)
                {
                    $sHasRewritePathNew = $aModule['file'];
                }
            }
            
            if ($sHasRewritePathOld) // has old version
            {
                if (!$sHasRewritePathNew) // no new version
                {
                    $sErrorMsg = Mage::helper('adminhtml')->__('No Aitoc module can be installed because you have outdated version of Checkout Fields Manager installed. Please contact AITOC for updated version of Checkout Fields Manager to resolve this issue.');
                    
                    $aErrorList[] = $sErrorMsg;
                    return $aErrorList;
                }
                
                if (is_writable($sHasRewritePathOld))
                {
                    $aPostData = array($aModule['key'] => 1);
                    
                    $aModuleHashStrict = array($this->_sModuleRewriteNameNew . '.php' => $sHasRewritePathNew);
                    
                    $aErrorList = $this->saveData($aPostData, $aModuleHashStrict);
                    
                    if (!$aErrorList)
                    {
                        unlink($sHasRewritePathOld); // kill old version config file
                    }
                    
                    return $aErrorList;
                }
                else 
                {
                    $sErrorMsg = Mage::helper('adminhtml')->__('File does not have write permissions: %s', $sHasRewritePathOld);
                    $aErrorList[] = $sErrorMsg;
                    return $aErrorList;
                }
            }
        }
        
        return false;
    }
    
}
?>