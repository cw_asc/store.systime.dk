<?php
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */



class Aitoc_Aitsys_Model_Aitpatch extends Mage_Eav_Model_Entity_Attribute
{
	private $_aPatch = array();
	
	public function applyPatchDryrun()
	{
		return $this->applyPatch(true);
	}
	
    public function applyPatch($bDryRun = false)
    {
    	$oConfig     = Mage::getConfig();
        $aPatchFiles = $this->_getPatchFiles();
        $oPatcher    = Mage::getModel('aitsys/aitfilepatcher');
        $oFileSys    = Mage::getModel('aitsys/aitfilesystem');
        
        $aErrors = array();
        
        foreach ($aPatchFiles as $sFile => $aParams)
        {
        	$sPatch = '';
        	$aModules = $aParams['modules'];
        	foreach ($aModules as $sMod => $sModPathToFile)
        	{
#        		$sPatch .= file_get_contents($oConfig->getOptions()->getAppDir() . '/code/local/Aitoc/' . $sMod . '/' . $sFile);
        		$sPatch .= file_get_contents($sMod . '/data/' . $sFile);
        	}
        	$oPatcher->parsePatch($sPatch);
        	
        	// now will check if we can patch current file
        	#$sFileToPatch = $oConfig->getOptions()->getAppDir() . '/' . substr(str_replace('_', '/', $sFile), 0, strpos($sFile, '.patch'));
#        	$sFileToPatch = $oConfig->getOptions()->getAppDir() . '/' . substr(str_replace('_', '/', $sFile), 0, strpos($sFile, '.patch'));
			$sFileNameToPatch = str_replace('--', '/', $sFile);
			
			$sFileToPatch = $oConfig->getOptions()->getAppDir() . '/' . substr($sFileNameToPatch, 0, strpos($sFileNameToPatch, '.patch'));
        	if (!file_exists($sFileToPatch) && $aParams['optional'])
        	{
        	    continue;
        	}

        	if (!$oPatcher->canApplyChanges($sFileToPatch))
        	{
        		$aErrors[] = array('file' => $sFileToPatch, 'type' => 'file_uncompatible');
        	}
        	
        	if (!$bDryRun)
        	{
        		// apply patch
        		$sApplyPatchTo = $oFileSys->makeTemporary($sFileToPatch);
        		$oPatcher->applyPatch($sApplyPatchTo);
	        	foreach ($aModules as $sMod => $sModPathToFile)
	            {
#	            	$oFileSys->cpFile($sApplyPatchTo, $oConfig->getOptions()->getAppDir() . '/' . str_replace('_', '/', $sModPathToFile));
	            	$oFileSys->cpFile($sApplyPatchTo, $oConfig->getOptions()->getAppDir() . '/' . $sModPathToFile);
	            }
        		$oFileSys->rmFile($sApplyPatchTo);
        	}
        }
        
        if ($bDryRun)
        {
            return $aErrors;
        }
        
        return true;
    }
    
    public function setPatchFiles($aPatch)
    {
    	$this->_aPatch = $aPatch;
    }
    
    final private function _getPatchFiles()
    {
    	return $this->_aPatch;
    }
}