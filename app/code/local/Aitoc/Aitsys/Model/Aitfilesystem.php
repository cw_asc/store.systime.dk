<?php
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */


class Aitoc_Aitsys_Model_Aitfilesystem
{
	/**
	 * Makes temporary file in var/ folder
	 *
	 * @param string $sFromFile
	 * @return string file path
	 */
	function makeTemporary($sFromFile)
	{
		$oConfig = Mage::getConfig();
		$sFileType = substr($sFromFile, strrpos($sFromFile, '.'));
		$sTemp = $oConfig->getOptions()->getVarDir() . '/' . uniqid(time()) . $sFileType;
		copy($sFromFile, $sTemp);
		return $sTemp;
	}
	
	function mkDir($sPath)
	{
		$this->makeDirStructure($sPath);
	}
	
	function makeDirsDiff($sOrigDir, $sChangedDir, $sPatchFilePath)
	{
		$sCmd = 'diff -aurBb ' . $sOrigDir . ' ' . $sChangedDir . ' > ' . $sPatchFilePath;
		exec($sCmd);
		@chmod($sPatchFilePath, 0777);
	}
	
	/**
	 * Removes file
	 *
	 * @param string $sPath
	 */
	function rmFile($sPath)
	{
		if (file_exists($sPath) && is_file($sPath) && is_writable($sPath))
		{
			@unlink($sPath);
		}
	}
	
	/**
	 * Copy file. Makes directory structure if not exists.
	 *
	 * @param string $sSource
	 * @param string $sDestination
	 */
	function cpFile($sSource, $sDestination)
	{
		$this->makeDirStructure($sDestination);
		copy($sSource, $sDestination);
	}
	
	/**
	 * Makes directory structure and sets permissions
	 *
	 * @param string $sPath
	 */
	function makeDirStructure($sPath)
	{
		$sCreatePath = '/';
		$aPath = explode('/', $sPath);
		array_pop($aPath);
		foreach ($aPath as $sDir)
		{
			if (!$sDir) continue;
			$sCreatePath .= $sDir . '/';
			if (!@file_exists($sCreatePath) or (@file_exists($sCreatePath) and !@is_dir($sCreatePath)))
			{
				@mkdir($sCreatePath, 0777);
				@chmod($sCreatePath, 0777);
			}
		}
	}
	
	function isWriteable($sPath, $bCheckParentDirIfNotExists = true)
	{
		clearstatcache();
		if (file_exists($sPath) and is_file($sPath))
		{
			return $this->isFileWritable($sPath);
		}
		if (file_exists($sPath) and is_dir($sPath))
		{
			return $this->isDirWritable($sPath);
		}
		if (!file_exists($sPath))
		{
			if (!$bCheckParentDirIfNotExists)
			{
				return false;
			}
			$sDirname = dirname($sPath);
			while (strlen($sDirname) > 0 AND !file_exists($sDirname))
			{
				$sDirname = dirname($sDirname);
			}
			return $this->isDirWritable($sDirname);
		}
		return false;
	}
	
	function isFileWritable($sPath)
	{
	    if (!$sPath)
        {
            return false;
        }
        if (stristr(PHP_OS, "win"))
        {
        	// trying to append
        	$fp = @fopen($sPath, 'a+');
            if (!$fp)
            {
            	return false;
            }
            fclose($fp);
            return true;
        } else 
        {
        	return is_writable($sPath);
        }
	}
	
	function isDirWritable($sPath)
	{
		if (!$sPath)
		{
			return false;
		}
		if ('/' != $sPath[strlen($sPath)-1])
		{
			$sPath .= DIRECTORY_SEPARATOR;
		}
		if (stristr(PHP_OS, "win"))
		{
			/**
             * Trying to create a new file
             */
			$sFilename = uniqid(time());
            $fp = @fopen($sPath . $sFilename, 'w');
            if (!$fp) 
            {
                return false;
            }
            if (!@fwrite($fp, 'test'))
            {
                return false;
            }
            fclose($fp);
            /**
             * clean up after ourselves
             */
            unlink($sPath . $sFilename);
            return true;
		} else 
		{
			return is_writable($sPath);
		}
	}
}
