<?php
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */



class Aitoc_Aitsys_Block_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function initForm()
    {
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('module_list', array(
            'legend' => Mage::helper('adminhtml')->__('Enable/Disable Module List')
        ));

        $aModuleList = Mage::getModel('aitsys/aitsys')->getAitocModuleList();

        if ($aModuleList)
        {
            foreach ($aModuleList as $aModule) 
            {
                if ($aModule['access'])
                {
                    $fieldset->addField('hidden_enable_'.$aModule['key'], 'hidden', array(
                        'name'=>'enable['.$aModule['key'].']',
                        'value'=>0,
                    ));
                    
                    $fieldset->addField('enable_'.$aModule['key'], 'checkbox', array(
                        'name'=>'enable['.$aModule['key'].']',
                        'label'=>$aModule['label'],
                        'value'=>1,
                        'checked'=>$aModule['value'],
                    ));
                }
                else 
                {
                    $sMessage = 'File does not have write permissions: %s';
                    
                    $fieldset->addField('ignore_'.$aModule['key'], 'note', array(
                        'name'=>'ignore['.$aModule['key'].']',
                        'label'=>$aModule['label'],
                        'text'=> '<ul class="messages"><li class="error-msg"><ul><li>' . Mage::helper('adminhtml')->__($sMessage, $aModule['file']) . '</li></ul></li></ul>',
#                        'checked'=>$aModule['value'],
                    ));
                }
            }
        }

        $this->setForm($form);

        return $this;
    }
    
}


