<?php
/**
 * Payment method description
 */
class Dandomain_Block_Standard_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('dandomain/standard/description.phtml');
        parent::_construct();
    }
}