<?php
/**
 * Block for error page
 */
class Dandomain_Block_Standard_Error extends Mage_Core_Block_Template
{
	protected function _construct()
    {
        $this->setTemplate('dandomain/standard/error.phtml');
        parent::_construct();
    }
    
    public function getRedirectForm()
    {
    	$standard = Mage::getModel('dandomain/standard');
		return $standard->getRedirectForm()->toHtml();
    }
    
    public function getErrorCode()
    {
    	return $this->getRequest()->getParam('errorcode', false);
    }
    
    public function getErrorString()
    {
    	return Mage::getModel('dandomain/standard')->getErrorString($this->getErrorCode());
    }
    
    public function getActionCode()
    {
    	return $this->getRequest()->getParam('ActionCode', false);
    }
}