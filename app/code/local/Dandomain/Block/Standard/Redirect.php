<?php
/**
 * Redirect to payment form
 * Displayed after order is placed
 */
class Dandomain_Block_Standard_Redirect extends Mage_Core_Block_Template
{
	protected function _construct()
    {
        $this->setTemplate('dandomain/standard/redirect.phtml');
        parent::_construct();
    }
    
    public function getRedirectForm()
    {
    	$standard = Mage::getModel('dandomain/standard');
		return $standard->getRedirectForm()->toHtml();
    }
}
