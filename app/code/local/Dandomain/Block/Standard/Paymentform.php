<?php
/**
 * Payment form page (either through secure tunnel or local)
 */
class Dandomain_Block_Standard_Paymentform extends Mage_Core_Block_Template
{
	protected $_order;
	
    public function getHtml()
    {
    	return $this->_toHtml();
    }
    
    public function getAuthUrl()
    {
    	return Mage::getModel('dandomain/standard')->getAuthUrl();
    }
    
    public function getSuccessUrl($secure = null)
    {
    	return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, $secure) . 'dandomain/standard/success/';
    }
    
    public function getFailUrl($secure = null)
    {
    	return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, $secure) . 'dandomain/standard/error/';
    }
    
    public function getInstantCapture()
    {
    	return (int)$this->_getConfigData('instantcapture');
    }
    
    public function getMerchantNumber()
    {
    	return $this->_getConfigData('merchantnumber');
    }
    
    public function getTestMode()
    {
    	return (int)$this->_getConfigData('testmode');
    }
    
	public function getOrder()
	{
		if (!$this->_order)
		{
			$this->_order = Mage::getModel('sales/order');
			$this->_order->loadByIncrementId($_GET['OrderID']);
		}
		return $this->_order;
	}

    public function getRawAmount()
    {
        return $this->_order->getTotalDue();
    }
    
    public function getAmountForDD()
    {
        return number_format($this->getRawAmount(), 2, ',', '');
    }
    
    public function getAmountForView($amount)
    {
        $amount = str_replace(array('.', ','), array('', '.'), $amount);
    	return $this->helper('core')->currency($amount, true, false);
    }
    
    public function getOrderId()
    {
		return $this->getOrder()->getIncrementId();
    }
    
    public function getCurrencyCode()
    {
//return 208;
        $payment = Mage::getModel('dandomain/standard');
        return $payment->currencyStr2Num($this->getOrder()->getOrderCurrency());
    }

	public function getChecksum()
	{
		if ($this->_getConfigData('md5secret')) {
			return md5($this->getOrderId() . '+' . $this->getAmountForDD() . '+' . $this->_getConfigData('md5secret') . '+' . $this->getCurrencyCode());
		} else {
			return '';
		}
	}
    
    public function getMonthSelect()
    {
    	$form = new Varien_Data_Form();
    	$attributes = array();
    	$attributes['html_id'] = 'ExpireMonth';
    	$attributes['name'] = 'ExpireMonth';
    	$months = array('01','02','03','04','05','06','07','08','09','10','11','12');
    	$attributes['values'] = array();
    	foreach ($months as $month) {
    		$attributes['values'][$month] = $month;
    	}
    	$select = new Varien_Data_Form_Element_Select($attributes);
    	$form->addElement($select);
    	return $select->getHtml();
    }
    
    public function getYearSelect()
    {
    	$form = new Varien_Data_Form();
    	$attributes = array();
    	$attributes['html_id'] = 'ExpireYear';
    	$attributes['name'] = 'ExpireYear';
    	$attributes['values'] = array();
    	for ($i = date("y"); $i < date("y")+20; $i++ ) {
    		$attributes['values'][$i] = $i;
    	}
    	$select = new Varien_Data_Form_Element_Select($attributes);
    	$form->addElement($select);
    	return $select->getHtml();
    }
    
    protected function _getConfigData($field)
    {
    	$payment = Mage::getModel('dandomain/standard');
		return $payment->getConfigData($field);
    }
}
