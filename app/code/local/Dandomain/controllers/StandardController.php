<?php

/**
 * Dandomain Standard Checkout Controller
 *
 */
class Dandomain_StandardController extends Mage_Core_Controller_Front_Action
{

    /**
     * When a customer chooses Dandomain on Checkout/Payment page
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setDandomainStandardQuoteId($session->getQuoteId());
        
      	$payment = Mage::getModel('dandomain/standard');

		$order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
		
		// Do not allow setting payment to pending again if it has already been authorized (back button problem)
		if (!$this->_paymentAuthorized($order->getIncrementId())) {
			$order->setStatus('pending_dandomain');
			$order->save();
		} else {
			// mri@systime.dk 2011-09-15
			$order->setStatus('pending_dandomain');
			$order->save();

Mage::log(array(
								'location' => __FILE__.':'.__LINE__,
								'_redirect' => 'checkout/cart',
								), null, 'dandomain.log');

			$this->_redirect('checkout/cart');
		    return;
		}
      	
      	$this->loadLayout();
      	
     	// Secure tunnel - redirect to gateway
      	if (((int)$payment->getConfigData('paymentformtype')) == 1) {
        	$this->getLayout()->getBlock('content')->append($this->getLayout()->createBlock('dandomain/standard_redirect'));
        	
       	// Render local payment form
        } else {
        	$form = $this->getLayout()->createBlock('dandomain/standard_paymentform');
        	$form->setTemplate('dandomain/standard/paymentform_local.phtml');
        	$this->getLayout()->getBlock('content')->append($form);
        }
        
        $this->renderLayout();
    }
    
    /**
     * The standalone payment form page for the secure tunnel
     */
    public function paymentformAction()
    {
    	$page = $this->getLayout()->createBlock('dandomain/standard_paymentform');
        $page->setTemplate('dandomain/standard/paymentform.phtml');
    	$this->getResponse()->setBody($page->getHtml());
    }
    
    /**
     * when an error occurs
     */
    public function errorAction()
    {
    	$this->loadLayout();
        $this->getLayout()->getBlock('content')->append($this->getLayout()->createBlock('dandomain/standard_error'));
        $this->renderLayout();
    }

    /**
     * when dandomain returns
     */
    public function successAction()
    {
        
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getDandomainStandardQuoteId(true));
        
        $order = Mage::getModel('sales/order');
        $payment = Mage::getModel('dandomain/standard');
        
        // Load the order number
        if (Mage::getSingleton('checkout/session')->getLastOrderId()) {
          $order->load(Mage::getSingleton('checkout/session')->getLastOrderId());
        } else {
          if (isset($_REQUEST["orderid"])) {
            $order->loadByIncrementId((int)$_REQUEST["orderid"]);
          } else {
            echo "<h1>An error occured!</h1>";
            echo "No orderid was supplied to the system!";
            exit();
          }
        }

		// Check if payment is authorized through Dandomain
		if (!$this->_paymentAuthorized($order->getIncrementId())) {
			echo "<h1>An error occured!</h1>";
            echo "Payment was not found!";
            exit();
		}
        
        // Validate the order and send email confirmation if enabled
        if($order->getStatus() == 'pending_dandomain'){
            if (((int)$payment->getConfigData('sendmailorderconfirmation')) == 1) {
              $order->sendNewOrderEmail();
            }
        
	        // Write comment to order
	        $msg = $this->__("The order has been payed through DanDomain. Please log on to DanDomain to capture the payment:") . " https://pay.dandomain.dk/";
	        $order->addStatusToHistory($payment->getConfigData('order_status'),$msg);
	        $order->save();
        } else {
          echo "<h1>An error occured!</h1>";
          echo "The order id was not known to the system";
          exit();
        }

        $this->_redirect('checkout/onepage/success');
    }

/*
	private function _paymentAuthorized($orderid) {
		$client = new Varien_Http_Client('https://pay.dandomain.dk/PayApi.asp');
		$client->setMethod('GET');
		$payment = Mage::getModel('dandomain/standard');
		$client->setParameterGet('username', $payment->getConfigData('username'));
		$client->setParameterGet('password', $payment->getConfigData('password'));
		$client->setParameterGet('Checkstatus', '1');
		$client->setParameterGet('OrderID', $orderid);
		$response = null;
		try {
			$response = $client->request();

			// @see http://help.dandomain.dk/paynet/DK/
			$body = $response->getBody();
			$status = substr($body, 0, 3);

			switch ($status) {
			case 200: // 200 - Transaction exists. Not captured.
			case 201:	// 201 - Transaction exists. Captured.
			case 202:	// 202 - Transaction exists. Rejected.
			case 203:	// 203 - Transaction exists. Credited.
				return true;
				break;
			}
		} catch (Zend_Http_Client_Exception $e) {}

		return false;
	}
*/

	private function _paymentAuthorized($orderid)
	{
		$client = new Varien_Http_Client('https://pay.dandomain.dk/PayApi.asp');
		$client->setMethod('GET');
		$payment = Mage::getModel('dandomain/standard');
		$client->setParameterGet('username', $payment->getConfigData('username'));
		$client->setParameterGet('password', $payment->getConfigData('password'));
		$client->setParameterGet('Checkstatus', '1');
		$client->setParameterGet('OrderID', $orderid);
		$response = null;
		try {
			$response = $client->request();
		} catch (Zend_Http_Client_Exception $e) {
		Mage::log(array(
										'location' => __FILE__.':'.__LINE__,
										'orderid' => $orderid,
										'message' => $e->getMessage(),
										), null, 'dandomain.log');

			return false;
		}	


	Mage::log(array(
									'location' => __FILE__.':'.__LINE__,
									'orderid' => $orderid,
									'body' => $response->getBody(),
									'check' => (substr($response->getBody(),0,3) != '404'),
									), null, 'dandomain.log');

		return (substr($response->getBody(),0,3) != '404');
	}
}
