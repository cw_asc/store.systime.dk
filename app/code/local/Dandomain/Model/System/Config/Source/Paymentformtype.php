<?php
class Dandomain_Model_System_Config_Source_Paymentformtype
{

    public function toOptionArray()
    {
        return array(
            array('value'=>1, 'label'=>Mage::helper('dandomain')->__("Load through DanDomain's secure tunnel")),
            array('value'=>2, 'label'=>Mage::helper('dandomain')->__('Use local form (requires SSL)')),
        );
    }

}
