<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php
class AW_Advancedreports_ChartController extends Mage_Adminhtml_Controller_Action
{
    public function ajaxBlockAction()
    {
        $output = 'fucjk';
        $width  = $this->getRequest()->getParam('width');
        $block  = $this->getRequest()->getParam('block');
        $option = $this->getRequest()->getParam('option');
        $type   = $this->getRequest()->getParam('type');
        $output = $this->getLayout()->createBlock( 'advancedreports/chart' )
                       ->setType( $type )
                       ->setOption( $option )
                       ->setWidth( $width )
                       ->setRouteOption( $block )
                       ->setHeight( Mage::helper('advancedreports')->getChartHeight() )
                       ->toHtml();
        $this->getResponse()->setBody($output);
        return;
    }
}
