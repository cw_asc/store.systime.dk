<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php
class AW_Advancedreports_HoursController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('report/advancedreports/hours')
            ->_addBreadcrumb(Mage::helper('advancedreports')->__('Advanced'), Mage::helper('advancedreports')->__('Advanced'))
            ->_addBreadcrumb(Mage::helper('advancedreports')->__('Sales by Hour'), Mage::helper('advancedreports')->__('Sales by Hour'))
            ->_addContent( $this->getLayout()->createBlock('advancedreports/advanced_hours') )
            ->renderLayout();
    }

    public function exportOrderedCsvAction()
    {
        $fileName   = 'hours.csv';
        $content    = $this->getLayout()->createBlock('advancedreports/advanced_hours_grid')
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportOrderedExcelAction()
    {
        $fileName   = 'hours.xml';
        $content    = $this->getLayout()->createBlock('advancedreports/advanced_hours_grid')
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }

}
