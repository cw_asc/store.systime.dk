<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php


class AW_Advancedreports_Model_System_Config_Source_Statuses
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'complete','label'=>'Complete'),
            array('value'=>'pending','label'=>'Pending'),
            array('value'=>'processing','label'=>'Processing'),
            array('value'=>'closed','label'=>'Closed'),
            array('value'=>'canceled','label'=>'Canceled'),
            array('value'=>'holded','label'=>'Holded'),
        );
    }
}