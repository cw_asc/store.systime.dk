<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php
class AW_Advancedreports_Block_Report_Sales_Sales_Grid extends Mage_Adminhtml_Block_Report_Sales_Sales_Grid
{
    protected $_routeOption = AW_Advancedreports_Helper_Data::ROUTE_SALES_SALES;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate( Mage::helper('advancedreports')->getGridTemplate() );
    }

    public function getRoute()
    {
        return $this->_routeOption;
    }

    public function hasRecords()
    {
        return (count( $this->getCollection()->getIntervals() ) > 1)
               && Mage::helper('advancedreports')->getChartParams( $this->_routeOption )
               && count( Mage::helper('advancedreports')->getChartParams( $this->_routeOption ) );
    }

    public function getChartParams()
    {
        return Mage::helper('advancedreports')->getChartParams( $this->_routeOption );
    }

    protected function _toHtml()
    {
        $html = parent::_toHtml();
        $this->_prepareData();
        return $html;
    }

    protected function _prepareData()
    {
        //Remember available keys
        $keys = array();
        foreach ( $this->getChartParams() as $param )
        {
            $keys[] = $param['value'];
        }

        $dataKeys = array();
        foreach ( $this->_columns as $column )
        {
            if ( !$column->getIsSystem() && in_array($column->getIndex(), $keys) )
            {
                $dataKeys[] = $column->getIndex();
            }
        }
        //Get data
        $data = array();
        foreach ($this->getCollection()->getIntervals() as $_index=>$_item)
        {
            $report = $this->getReport($_item['start'], $_item['end']);
            $row = array();
            foreach ($report as $_subIndex=>$_subItem)
            {
                $row = array();
                foreach ($this->_columns as $column)
                {
                    if ( in_array( $column->getIndex(), $dataKeys ) )
                    {
                        $row[$column->getIndex()] = $_subItem->getData( $column->getIndex() );
                    }
                }          
            }            
            $row['period'] = $_index;
            $data[] = $row;
        }

        if ($data)
        {
            Mage::helper('advancedreports')->setChartData( $data, Mage::helper('advancedreports')->getDataKey( $this->_routeOption ) );
        }
    }

    public function getChartType()
    {
        return AW_Advancedreports_Block_Chart::CHART_TYPE_LINE;
    }
}
