<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php
class AW_Advancedreports_Block_Advanced_Country_Grid extends AW_Advancedreports_Block_Advanced_Grid
{   
    protected $_routeOption = AW_Advancedreports_Helper_Data::ROUTE_ADVANCED_COUNTRY;
    
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate( Mage::helper('advancedreports')->getGridTemplate() );
        $this->setExportVisibility(true);
        $this->setStoreSwitcherVisibility(true);
        $this->setId('gridCountry');
    }

    public function hasRecords()
    {
        return (count( $this->_customData ))
               && Mage::helper('advancedreports')->getChartParams( $this->_routeOption )
               && count( Mage::helper('advancedreports')->getChartParams( $this->_routeOption ) );
    }

    public  function addOrderItemsCount()
    {
	$itemTable = $this->getCollection()->getTable('sales_flat_order_item');
	$this->getCollection()->getSelect()
			->join( array('item'=>$itemTable), "(item.order_id = e.entity_id AND item.parent_item_id IS NULL)", array( 'sum_qty' => 'SUM(item.qty_ordered)',  'sum_total' => 'SUM(item.row_total)'))
			->where("e.entity_id = item.order_id");
	return $this;

    }

    public function addAddress()
    {
	$entityValues = $this->getCollection()->getTable('sales_order_entity_varchar');
	$entityAtribute = $this->getCollection()->getTable('eav_attribute');
	$entityType = $this->getCollection()->getTable('eav_entity_type');
	$entityOrder = $this->getCollection()->getTable('sales_order_entity');
//	$flatAddress = $this->getCollection()->getTable('sales_flat_quote_address');
	$this->getCollection()->getSelect()
//				    ->join(array('addr'=>$flatAddress), "addr.quote_id = e.entity_id AND addr.address_type = 'billing'", array('country_id'=>'addr.country_id'))
				    ->join(array('a_type'=>$entityType), "a_type.entity_type_code='order_address'", array())
				    ->join(array('a_attr'=>$entityAtribute), "a_type.entity_type_id=a_attr.entity_type_id AND a_attr.attribute_code = 'country_id'", array())
				    ->join(array('a_entity'=>$entityOrder), "a_entity.parent_id=e.entity_id AND a_entity.entity_type_id = a_type.entity_type_id AND a_entity.is_active = '1'", array())
				    ->join(array('a_value'=>$entityValues), "a_value.attribute_id=a_attr.attribute_id AND a_value.entity_id = a_entity.entity_id", array('country_id'=>'value'))
				    ->join(array('a_attr_bil'=>$entityAtribute), "a_type.entity_type_id = a_attr_bil.entity_type_id AND a_attr_bil.attribute_code = 'address_type'", array())
				    ->join(array('a_value_bil'=>$entityValues), "a_value_bil.attribute_id = a_attr_bil.attribute_id AND a_value_bil.entity_id = a_entity.entity_id AND a_value_bil.value = 'billing'", array())
				    ->group('country_id')
				    ;
	return $this;
    }

    protected function _prepareCollection()
    {
	parent::_prepareCollection();

	$this->addAddress()->addOrderItemsCount();


	$this->_prepareData();
	return $this;
    }
    
    protected function _addCustomData($row)
    {
        if ( count( $this->_customData ) )
        {
            foreach ( $this->_customData as &$d )
            {
                if ( $d['country_id'] === $row['country_id'] )
                {
                    $qty = $d['qty_ordered'];
                    $total = $d['total'];
                    unset($d['total']);
                    unset($d['qty_ordered']);
                    $d['total'] = $row['total'] + $total;
                    $d['qty_ordered'] = $row['qty_ordered'] + $qty;
                    return $this;
                }
            }
        }
        $this->_customData[] = $row;
        return $this;
    }

    /*
     * Need to sort bestsellers array
     */
    protected function _compareTotalElements($a, $b)
    {
        if ($a['total'] == $b['total'])
        {
            return 0;
        }
        return ($a['total'] > $b['total']) ? -1 : 1;
    }
    /*
     * Need to sort bestsellers array
     */
    protected function _compareQtyElements($a, $b)
    {
        if ($a['qty_ordered'] == $b['qty_ordered'])
        {
            return 0;
        }
        return ($a['qty_ordered'] > $b['qty_ordered']) ? -1 : 1;
    }

    /*
     * Prepare data array for Pie and Grid     
     */
    protected function _prepareData()
    {
//	echo $this->getCollection()->getSelect()->__toString();
        foreach ( $this->getCollection() as $order )
        {
            $row = array();
            foreach ($this->_columns as $column)
            {
                    if ( !$column->getIsSystem() )
                {
                    $row[ $column->getIndex() ] = $order->getData( $column->getIndex() );
                }
            }
	    if ($order->getCountryId())
	    {
		$row['country_id']  = $order->getCountryId();
		$row['qty_ordered'] = $order->getSumQty();
		$row['total']       = $order->getSumTotal();
		$this->_addCustomData($row);
	    }
        }

        if ( ! count( $this->_customData ) )
        {
            return $this;
        }

        $key = $this->getFilter('reload_key');
        if ( $key === 'qty' )
        {
            //Sort data
            usort($this->_customData, array(&$this, "_compareQtyElements") );

            //All qty
            $qty = 0;
            foreach ( $this->_customData as $d )
            {
                $qty += $d['qty_ordered'];
            }
            foreach ( $this->_customData as $i=>&$d )
            {
                $d['order'] = $i + 1;
                $d['percent'] = round( $d['qty_ordered'] * 100 / $qty ).' %'; 
                $d['percent_data'] = round( $d['qty_ordered'] * 100 / $qty );
                //Add title
                $d['country_name'] = Mage::getSingleton('directory/country')->loadByCode( $d['country_id'] )->getName();;
            }
        }
        elseif ($key === 'total')
        {
            //Sort data
            usort($this->_customData, array(&$this, "_compareTotalElements") );

            //All qty
            $total = 0;
            foreach ( $this->_customData as $d )
            {
                $total += $d['total'];
            }
            foreach ( $this->_customData as $i=>&$d )
            {
                $d['order'] = $i + 1;
                $d['percent'] = round( $d['total'] * 100 / $total ).' %';
                $d['percent_data'] = round( $d['total'] * 100 / $total );
                //Add title
                $d['country_name'] = Mage::getSingleton('directory/country')->loadByCode( $d['country_id'] )->getName();;
            }
        }
        else
        {
            return $this;
        }

        Mage::helper('advancedreports')->setChartData( $this->_customData, Mage::helper('advancedreports')->getDataKey( $this->_routeOption ) );
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('order', array(
            'header'    =>Mage::helper('reports')->__('N'),
            'width'     =>'60px',
            'align'     =>'right',
            'index'     =>'order',
            'type'      =>'number'
        ));

        $this->addColumn('country_name', array(
            'header'    =>Mage::helper('reports')->__('Country'),
            'index'     =>'country_name'
        ));

        $this->addColumn('percent', array(
            'header'    =>Mage::helper('advancedreports')->__('Percent'),
            'width'     =>'60px',
            'align'     =>'right',
            'index'     =>'percent',
            'type'      =>'text'
        ));

        $this->addColumn('qty_ordered', array(
            'header'    =>Mage::helper('advancedreports')->__('Quantity'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'qty_ordered',
            'total'     =>'sum',
            'type'      =>'number'
        ));

        $this->addColumn('total', array(
            'header'    =>Mage::helper('reports')->__('Total'),
            'width'     =>'120px',
            'currency_code' => $this->getCurrentCurrencyCode(),
            'index' => 'total',
            'type'  => 'currency',

        ));
//
//        $this->addColumn('action',
//            array(
//                'header'    => Mage::helper('catalog')->__('Action'),
//                'width'     => '50px',
//                'type'      => 'action',
//                'align'     =>'right',
//                'getter'    => 'getId',
//                'actions'   => array(
//                    array(
//                        'caption' => Mage::helper('advancedreports')->__('View'),
//                        'url'     => array(
//                            'base'=>'adminhtml/catalog_product/edit',
//                            'params'=>array()
//                        ),
//                        'field'   => 'id'
//                    )
//                ),
//                'filter'    => false,
//                'sortable'  => false,
//                'index'     => 'stores',
//        ));

        $this->addExportType('*/*/exportOrderedCsv', Mage::helper('advancedreports')->__('CSV'));
        $this->addExportType('*/*/exportOrderedExcel', Mage::helper('advancedreports')->__('Excel'));

        return $this;
    }

    public function getChartType()
    {
        return AW_Advancedreports_Block_Chart::CHART_TYPE_MAP;
    }


}
