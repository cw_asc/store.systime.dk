<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php
class AW_Advancedreports_Block_Advanced_Sales_Grid extends AW_Advancedreports_Block_Advanced_Grid
{
    protected $_routeOption = AW_Advancedreports_Helper_Data::ROUTE_ADVANCED_SALES;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate( Mage::helper('advancedreports')->getGridTemplate() );
        $this->setExportVisibility(true);
        $this->setStoreSwitcherVisibility(true);
        $this->setId('gridAdvancedSales');
    }

    public function getHideShowBy()
    {
        return true;
    }

    protected function _addCustomData($row)
    {
	$this->_customData[] = $row;
	return $this;
    }

    public function addOrderItems()
    {
	$itemTable = $this->getCollection()->getTable('sales_flat_order_item');
	$orderTable = $this->getCollection()->getTable('sales_order');
	$this->getCollection()->getSelect()
			->join( array('item'=>$itemTable), "(item.order_id = e.entity_id AND item.parent_item_id IS NULL)" )
			->order('e.created_at DESC')
			;
	return $this;
    }

    public function _prepareCollection()
    {
        parent::_prepareCollection();

	$collection = $this->getCollection();
	$orderTable = $collection->getTable('sales_order');
	$collection->getSelect()->reset();
	$collection->getSelect()->from(array('e'=>$orderTable), array('order_created_at' => 'created_at', 'order_id' => 'entity_id'));
	
        $date_from = $this->_getMysqlFromFormat($this->getFilter('report_from'));
        $date_to = $this->_getMysqlToFormat($this->getFilter('report_to'));



        $this->setDateFilter($date_from, $date_to)->setState();

        if ($this->getRequest()->getParam('store')) {
            $storeIds = array($this->getParam('store'));
        } else if ($this->getRequest()->getParam('website')){
            $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
        } else if ($this->getRequest()->getParam('group')){
            $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
        }
        if (isset($storeIds))
        {
	    $this->setStoreFilter($storeIds);
        }
	$this->addOrderItems();
        $this->_prepareData();
    }

    protected function _prepareData()
    {
//	echo $this->getCollection()->getSelect()->__toString();
	foreach ($this->getCollection() as $row)
	{
	    $this->_addCustomData($row->getData());
	}
	
//	echo '<pre>';
//	var_dump($chartLabels);
//	var_dump($this->_customData);
//	echo '</pre>';		
//
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('created_at', array(
            'header' => Mage::helper('reports')->__('Purchased On'),
            'index' => 'order_created_at',
            'type' => 'datetime',
            'width' => '140px',
        ));

        $this->addColumn('sku', array(
            'header'    =>Mage::helper('reports')->__('SKU'),
            'width'     =>'120px',
            'index'     =>'sku',
            'type'      =>'text'
        ));

        $this->addColumn('name', array(
            'header'    =>Mage::helper('reports')->__('Product Name'),
            'index'     =>'name',
	    'type'      =>'text'
        ));

        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('advancedreports')->__('Quantity'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'qty_ordered',
            'total'     =>'sum',
            'type'      =>'number'
        ));

        $this->addColumn('total', array(
            'header'    =>Mage::helper('reports')->__('Total'),
            'width'     =>'120px',
            'type'      =>'currency',
            'currency_code' => $this->getCurrentCurrencyCode(),
            'total'     =>'sum',
            'index'     =>'row_total'
        ));

        $this->addColumn('view_order',
            array(
                'header'    => Mage::helper('advancedreports')->__('View Order'),
                'width'     => '70px',
                'type'      => 'action',
                'align'     =>'left',
                'getter'    => 'getOrderId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('advancedreports')->__('View'),
                        'url'     => array(
                            'base'=>'adminhtml/sales_order/view',
                            'params'=>array()
                        ),
                        'field'   => 'order_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
        ));
        $this->addColumn('view_product',
            array(
                'header'    => Mage::helper('advancedreports')->__('View Product'),
                'width'     => '70px',
                'type'      => 'action',
                'align'     =>'left',
                'getter'    => 'getProductId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('advancedreports')->__('View'),
                        'url'     => array(
                            'base'=>'adminhtml/catalog_product/edit',
                            'params'=>array()
                        ),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
        ));

        $this->addExportType('*/*/exportOrderedCsv', Mage::helper('advancedreports')->__('CSV'));
        $this->addExportType('*/*/exportOrderedExcel', Mage::helper('advancedreports')->__('Excel'));

        return $this;
    }

    public function getChartType()
    {
        return 'none';
    }

    public function hasRecords()
    {
	return false;
    }

    public function getPeriods()
    {
        return array();
    }

}
