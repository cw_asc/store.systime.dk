<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php
class AW_Advancedreports_Block_Advanced_Grid extends Mage_Adminhtml_Block_Report_Grid
{
    protected $_customData = array();
    protected $_customVarData;
    protected $_timeDiff;
    protected $_ctz;


    protected function _setTimezone()
    {
        $this->_ctz = date_default_timezone_get();
        $mtz =  Mage::app()->getStore()->getConfig('general/locale/timezone');
        @date_default_timezone_set( $mtz );
    }
    
    protected function _unsetTimezone()
    {
        @date_default_timezone_set( $this->_ctz );
    }

    public function getNeedReload()
    {
        return Mage::helper('advancedreports')->getNeedReload( $this->_routeOption );
    }

    public function getRoute()
    {
        return $this->_routeOption;
    }

    public function getTimeDiff()
    {
        if (!$this->_timeDiff)
        {
            $this->_timeDiff = Mage::helper('advancedreports')->getTimeDiff();
        }
        return $this->_timeDiff;
    }

    protected function _getMysqlFromFormat($date)
    {
        $format = $this->getLocale()->getDateStrFormat( Mage_Core_Model_Locale::FORMAT_TYPE_SHORT );
        $arr = strptime($date, $format);
        $tme = mktime( 0,0,0,$arr['tm_mon']+1,$arr['tm_mday'],$arr['tm_year'] + 1900 );
        $tme += 3600 * $this->getTimeDiff();
        return date('Y-m-d', $tme );
    }

    protected function _getMysqlToFormat($date)
    {        
        $format = $this->getLocale()->getDateStrFormat( Mage_Core_Model_Locale::FORMAT_TYPE_SHORT );
        $arr = strptime($date, $format);
        $tme = mktime( 24,0,0,$arr['tm_mon']+1,$arr['tm_mday'],$arr['tm_year'] + 1900 );
        $tme += 3600 * $this->getTimeDiff();
        return date('Y-m-d', $tme );
    }

    /*
     * Filter collection by Date
     */
    public function setDateFilter($from, $to)
    {
	$this->getCollection()->getSelect()
                        ->where("e.created_at >= ?", $from)
                        ->where("e.created_at <= ?", $to);		
	return $this;
    }

    /*
     * Filter collection by Store Ids
     */
    public function setStoreFilter($storeIds)
    {
	$this->getCollection()->getSelect()
			->where('e.store_id in (?)', $this->getStoreIds($storeIds));
	return $this;
    }

//    public function addOrderItems($limit = 10)
//    {
//	$itemTable = $this->getCollection()->getTable('sales_flat_order_item');
//
//	$this->getCollection()->getSelect()
//			->join( array('item'=>$itemTable), "e.entity_id = item.order_id", array( 'i_count' => 'COUNT( item.product_id )', 'sum_qty' => 'SUM(item.qty_ordered)',  'sum_total' => 'SUM(item.row_total)', 'name' => 'name', 'sku'=>'sku' ) )
//			->group('item.product_id')
//			->limit( $limit );
//	return $this;
//    }

    public function orderByTotal()
    {
	$this->getCollection()->getSelect()
			->order('sum_total DESC');
	return $this;
    }
    
    public function orderByQty()
    {
	$this->getCollection()->getSelect()
			->order('sum_qty DESC');

	return $this;
    }

    public function setHourFilter()
    {
	$itemTable = $this->getCollection()->getTable('sales_flat_order_item');

	$this->getCollection()->getSelect()
			->join( array('item'=>$itemTable), "e.entity_id = item.order_id AND item.parent_item_id IS NULL", array( 'hour' => 'HOUR( e.created_at )', 'sum_qty' => 'SUM(item.qty_ordered)',  'sum_total' => 'SUM(item.row_total)', 'name' => 'name', 'sku'=>'sku' ) )
			->group('hour');
	return $this;
    }
//    public function setHourFilter($hour)
//    {
//	$this->getCollection()->getSelect()
//				    ->where('HOUR(e.created_at) = ?', $hour);
//	return $this;
//    }
    
    /*
     * $dayofweek = 1,2,3,4,5,6,7
     */
    public function setDayOfWeekFilter()
    {
	$itemTable = $this->getCollection()->getTable('sales_flat_order_item');

	$this->getCollection()->getSelect()
			->join( array('item'=>$itemTable), "e.entity_id = item.order_id AND item.parent_item_id IS NULL", array( 'day_of_week' => 'DAYOFWEEK(e.created_at)', 'sum_qty' => 'SUM(item.qty_ordered)',  'sum_total' => 'SUM(item.row_total)', 'name' => 'name', 'sku'=>'sku' ) )
			->group('day_of_week');
	return $this;	
    }

    /*
     * Filter collction by order status
     */
    protected function _getProcessStates()
    {
	$states = explode( ",", Mage::helper('advancedreports')->confProcessOrders() );
	$is_first = true;
	$filter = "";
	foreach ($states as $state)
	{
	    if (!$is_first)
	    {
		$filter .= " OR ";
	    }
	    $filter .= "val.value = '".$state."'";
	    $is_first = false;
	}
	return "(".$filter.")";
	return "val.value = '".Mage_Sales_Model_Order::STATE_COMPLETE."'";
    }
    
    public function setState()
    {
	$entityValues = $this->getCollection()->getTable('sales_order_varchar');
	$entityAtribute = $this->getCollection()->getTable('eav_attribute');
	$this->getCollection()->getSelect()
			->join( array('attr'=>$entityAtribute), "attr.attribute_code = 'status'", array())
			->join( array('val'=>$entityValues), "attr.attribute_id = val.attribute_id AND ".$this->_getProcessStates()." AND e.entity_id = val.entity_id", array())
			;
	return $this;
    }

    protected  function _prepareCollection()
    {
        parent::_prepareCollection();
        
	$this->setCollection( Mage::getModel('sales/order')->getCollection() );
        $date_from = $this->_getMysqlFromFormat($this->getFilter('report_from'));
        $date_to = $this->_getMysqlToFormat($this->getFilter('report_to'));
	


        $this->setDateFilter($date_from, $date_to)->setState();

        if ($this->getRequest()->getParam('store')) {
            $storeIds = array($this->getParam('store'));
        } else if ($this->getRequest()->getParam('website')){
            $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
        } else if ($this->getRequest()->getParam('group')){
            $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
        }
        if (isset($storeIds))
        {
	    $this->setStoreFilter($storeIds);
        }
        
        //$this->_prepareData();
        return $this;
    }
    
    protected function _prepareOlderCollection()
    {
        parent::_prepareCollection();
        return $this;
    }

    public function getStoreIds($ids = array())
    {
        if (count($ids))
        {
            $res = '';
            $is_first = true;
            foreach ($ids as $id)
            {
                $res .= $is_first ? $id : ",".$id;
                $is_first = false;
            }
        }
        return $res;
    }

    public function getChartParams()
    {
        return Mage::helper('advancedreports')->getChartParams( $this->_routeOption );
    }

    public function hasRecords()
    {
        return (count( $this->_customData ) > 1)
               && Mage::helper('advancedreports')->getChartParams( $this->_routeOption )
               && count( Mage::helper('advancedreports')->getChartParams( $this->_routeOption ) );
    }

    public function getShowCustomGrid()
    {
        return true;
    }

    public function getHideNativeGrid()
    {
        return true;
    }

    public function getHideShowBy()
    {
        return true;
    }

    public function getCustomVarData()
    {
        //return $this->getCollection();

        if ($this->_customVarData)
        {
            return $this->_customVarData;
        }
        foreach ($this->_customData as $d)
        {
            $obj = new Varien_Object();
            $obj->setData( $d );
            $this->_customVarData[] = $obj;
        }
        return $this->_customVarData;
    }

    public function getPeriods()
    {
        return array();
    }

    protected function _getOlderPeriods()
    {
        return parent::getPeriods();
    }

    public function getOldExcel($filename = '')
    {
        return parent::getExcel($filename);
    }

    public function getOldCsv()
    {
        return parent::getCsv();
    }

    public function getExcel($filename = '')
    {
        $this->_prepareGrid();

        $data = array();
        foreach ($this->_columns as $column)
        {
            if (!$column->getIsSystem() && $column->getIndex() != 'stores')
            {
                $row[] = $column->getHeader();
            }
        }
        $data[] = $row;
        foreach ($this->getCustomVarData() as $obj)
        {
            $row = array();
            foreach ($this->_columns as $column)
            {
                if (!$column->getIsSystem() && $column->getIndex() != 'stores')
                {
                    $row[] = $column->getRowField($obj);
                }
            }
            $data[] = $row;
        }
        $xmlObj = new Varien_Convert_Parser_Xml_Excel();
        $xmlObj->setVar('single_sheet', $filename);
        $xmlObj->setData($data);
        $xmlObj->unparse();

        return $xmlObj->getData();
    }

    public function getCsv($filename = '')
    {

        $csv = '';
        $this->_prepareGrid();
        foreach ($this->_columns as $column) {
            if (!$column->getIsSystem() && $column->getIndex() != 'stores') {
                $data[] = '"'.$column->getHeader().'"';
            }
        }
        $csv.= implode(',', $data)."\n";

        foreach ($this->getCustomVarData() as $obj)
        {
            $data = array();
            foreach ($this->_columns as $column) {
                if (!$column->getIsSystem() && $column->getIndex() != 'stores') {
                    $data[] = '"'.str_replace(array('"', '\\'), array('""', '\\\\'), $column->getRowField($obj)).'"';
                }
            }
            $csv.= implode(',', $data)."\n";
        }

        return $csv;
    }
}
