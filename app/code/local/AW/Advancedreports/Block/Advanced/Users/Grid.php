<?php
	/**
	 * aheadWorks Co.
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the EULA
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 *
	 * @category   AW
	 * @package    AW_Advancedreports
	 * @copyright  Copyright (c) 2009 aheadWorks Co. (http://www.aheadworks.com)
	 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
	 */
?>
<?php
class AW_Advancedreports_Block_Advanced_Users_Grid extends AW_Advancedreports_Block_Advanced_Grid
{
    protected $_routeOption = AW_Advancedreports_Helper_Data::ROUTE_ADVANCED_USERS;


    public function __construct()
    {
        parent::__construct();
        $this->setTemplate( Mage::helper('advancedreports')->getGridTemplate() );
        $this->setExportVisibility(true);
        $this->setStoreSwitcherVisibility(true);
        $this->setId('gridUsers');
    }

    public function getHideShowBy()
    {
        return false;
    }

    protected function _addCustomData($row)
    {
	$this->_customData[] = $row;
	return $this;
    }

    public function _prepareCollection()
    {
        parent::_prepareOlderCollection();
	# This calculate collection of intervals
        $this->getCollection()
            ->initReport('reports/product_ordered_collection');
        $this->_prepareData();
        return $this;
    }

    /*
     *  This part build collection for every period filter by entered skus.
     *  It's more optimal variant for oprimisation
     */
    protected function _setOrderStateFilter($collection)
    {
	$entityValues = $collection->getTable('sales_order_varchar');
	$entityAtribute = $collection->getTable('eav_attribute');
	$collection->getSelect()
			->join( array('attr'=>$entityAtribute), "attr.attribute_code = 'status'", array())
			->join( array('val'=>$entityValues), "attr.attribute_id = val.attribute_id AND ".$this->_getProcessStates()." AND e.entity_id = val.entity_id", array())
			;
	return $this;
    }

    protected function _setStoreFilter($collection, $storeIds)
    {
	$collection->getSelect()
			->where('e.store_id in (?)', $this->getStoreIds($storeIds));
	return $this;
    }

    protected function _getOrdersCount($from, $to)
    {
	$collection = Mage::getModel('sales/order')->getCollection();

	#set data filter
	$collection->getSelect()
                        ->where("e.created_at >= ?", $from)
                        ->where("e.created_at <= ?", $to);
			;

	#set order state filter
	$this->_setOrderStateFilter($collection);

	# check Store Filter
	if ($this->getRequest()->getParam('store')) {
	    $storeIds = array($this->getParam('store'));
	} else if ($this->getRequest()->getParam('website')){
	    $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
	} else if ($this->getRequest()->getParam('group')){
	    $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
	}
	if (isset($storeIds))
	{
   	    $this->_setStoreFilter($collection, $storeIds);
	}
	return $collection->getSize();
    }
    
    protected function _getAccountsCount($from, $to)
    {
	$collection = Mage::getModel('customer/customer')->getCollection();

	#set data filter
	$collection->getSelect()
                        ->where("e.created_at >= ?", $from)
                        ->where("e.created_at <= ?", $to);
			;

	#set order state filter
//	$this->_setOrderStateFilter($collection);

	# check Store Filter
	if ($this->getRequest()->getParam('store')) {
	    $storeIds = array($this->getParam('store'));
	} else if ($this->getRequest()->getParam('website')){
	    $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
	} else if ($this->getRequest()->getParam('group')){
	    $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
	}
	if (isset($storeIds))
	{
   	    $this->_setStoreFilter($collection, $storeIds);
	}
	
//	echo $collection->getSelect()->__toString().'<hr>';
	return $collection->getSize();
    }

    protected function _getReviewsCount($from, $to)
    {
	$collection = Mage::getModel('review/review')->getCollection();

	#set data filter
	$collection->getSelect()
                        ->where("main_table.created_at >= ?", $from)
                        ->where("main_table.created_at <= ?", $to);
			;

	# check Store Filter
	if ($this->getRequest()->getParam('store')) {
	    $storeIds = array($this->getParam('store'));
	} else if ($this->getRequest()->getParam('website')){
	    $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
	} else if ($this->getRequest()->getParam('group')){
	    $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
	}
	if (isset($storeIds))
	{
	    $collection->addStoreFilter($this->getStoreIds($storeIds));
	}
	return $collection->getSize();
    }

    protected function _prepareData()
    {

	# primary analise
	foreach ( $this->getCollection()->getIntervals() as $_index=>$_item )
	{
	    $row['period'] = $_item['title'];

	    $row['accounts'] = $this->_getAccountsCount($_item['start'], $_item['end']);
	    $row['orders'] = $this->_getOrdersCount($_item['start'], $_item['end']);
	    $row['reviews'] = $this->_getReviewsCount($_item['start'], $_item['end']);
	    $this->_addCustomData($row);
	}

	$chartLabels = array('accounts' => Mage::helper('advancedreports')->__('New Accounts'),
			     'orders'	=> Mage::helper('advancedreports')->__('Orders'),
			     'reviews'	=> Mage::helper('advancedreports')->__('Reviews') );
	$keys = array();
	foreach ($chartLabels as $key=>$value)
	{
	    $keys[] = $key;
	}
	
//	echo '<pre>';
//	var_dump($chartLabels);
//	var_dump($this->_customData);
//	echo '</pre>';		
	
        Mage::helper('advancedreports')->setChartData( $this->_customData, Mage::helper('advancedreports')->getDataKey( $this->_routeOption ) );
        Mage::helper('advancedreports')->setChartKeys( $keys, Mage::helper('advancedreports')->getDataKey( $this->_routeOption )  );
        Mage::helper('advancedreports')->setChartLabels( $chartLabels, Mage::helper('advancedreports')->getDataKey( $this->_routeOption )  );
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('periods', array(
            'header'    =>$this->getPeriodText(),
            'width'     =>'120px',
            'index'     =>'period',
            'type'      =>'text'
        ));

        $this->addColumn('accounts', array(
            'header'    =>Mage::helper('advancedreports')->__('New Accounts'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'accounts',
            'type'      =>'number',
	    'default'   =>'0'
        ));

        $this->addColumn('orders', array(
            'header'    =>Mage::helper('advancedreports')->__('Orders'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'orders',
            'type'      =>'number',
	    'default'   =>'0'
        ));

        $this->addColumn('reviews', array(
            'header'    =>Mage::helper('advancedreports')->__('Reviews'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'reviews',
            'type'      =>'number',
	    'default'   =>'0'
        ));

        $this->addExportType('*/*/exportOrderedCsv', Mage::helper('advancedreports')->__('CSV'));
        $this->addExportType('*/*/exportOrderedExcel', Mage::helper('advancedreports')->__('Excel'));

        return $this;
    }

    public function getChartType()
    {
        return AW_Advancedreports_Block_Chart::CHART_TYPE_MULTY_LINE;
    }

    public function getPeriods()
    {
        return parent::_getOlderPeriods();
    }

}
