<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php

class AW_Zblocks_Adminhtml_ZblocksController extends Mage_Adminhtml_Controller_action
{

    protected function _initAction() 
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/zblocks')
            ->_addBreadcrumb(Mage::helper('zblocks')->__('Z-Blocks'), Mage::helper('zblocks')->__('Block Manager'));
        return $this;
    }
 
    public function indexAction() 
    {
        $this->_initAction()->renderLayout();
    }

    public function editAction() 
    {
        $id     = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('zblocks/zblocks')->load($id);

        if ($model->getId() || $id == 0) 
        {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('zblocks_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('cms/zblocks');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Block Manager'), Mage::helper('zblocks')->__('Block Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Block News'), Mage::helper('zblocks')->__('Block News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit'))
                ->_addLeft($this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('zblocks')->__('Block does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() 
    {
        $this->_forward('edit');
    }

    private function _timeParseErrors($time)
    {
        $data = date_parse($time);
        return $data['error_count'];
    }

    public function saveAction() 
    {
        if ($data = $this->getRequest()->getPost())
        {
            $model = Mage::getModel('zblocks/zblocks');
            $model->setData($data)->setId($this->getRequest()->getParam('id'));

            try
            {
                if($model->getBlockPosition()=='custom' && !$model->getBlockPositionCustom())
                {
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('zblocks')->__('Please set block Custom Position identifier'));
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }

                $model->setCategoryIds(implode(',', array_unique(explode(',', $model->getCategoryIds()))));
                $model->setStoreIds(implode(',', $model->getStoreIds()));
                if ($model->getCreationTime == NULL) $model->setCreationTime(now());
                $model->setUpdateTime(now());

                // check if schedule date was entered correctly
                $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

                if ($date = $model->getScheduleFromDate()) 
                {
                    $date = Mage::app()->getLocale()->date($date, $format, null, false);
                    $model->setScheduleFromDate($date->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));
                } 
                else $model->setScheduleFromDate(null);

                if ($date = $model->getScheduleToDate()) 
                {
                    $date = Mage::app()->getLocale()->date($date, $format, null, false);
                    $model->setScheduleToDate($date->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));
                } 
                else $model->setScheduleToDate(null);

                (!is_null($model->getScheduleFromDate()) && !is_null($model->getScheduleToDate()) && 
                    strtotime($model->getScheduleFromDate()) > strtotime($model->getScheduleToDate()))
                ? $dateError = Mage::helper('zblocks')->__('Start date can\'t be greater than end date')
                : $dateError = false;

                if (is_null($model->getScheduleFromDate())) $model->setScheduleFromDate(new Zend_Db_Expr('null'));
                if (is_null($model->getScheduleToDate())) $model->setScheduleToDate(new Zend_Db_Expr('null'));

                // check if schedule time was entered correctly
                if($data['schedule_from_time'] && $this->_timeParseErrors($data['schedule_from_time'])) 
                    $parseError = Mage::helper('zblocks')->__('Schedule From Time');
                elseif($data['schedule_to_time'] && $this->_timeParseErrors($data['schedule_to_time'])) 
                    $parseError = Mage::helper('zblocks')->__('Schedule To Time');
                else $parseError = false;

                if($parseError || $dateError)
                {
                    if($parseError) Mage::getSingleton('adminhtml/session')->addError(Mage::helper('zblocks')->__('Error in "%s" field', $parseError));
                    if($dateError) Mage::getSingleton('adminhtml/session')->addError($dateError);
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab' => 'schedule'));
                    return;
                }

                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('zblocks')->__('Block %s was successfully saved', '&quot;'.$model->getBlockTitle().'&quot;'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab' => $this->getRequest()->getParam('tab')));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('zblocks')->__('Unable to find block to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() 
    {
        if ($id = $this->getRequest()->getParam('id'))
        {
            try 
            {
                $model = Mage::getModel('zblocks/zblocks');

                $title = $model->getTitle();

                $model->setId($id)->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Block %s was successfully deleted', '&quot;'.$title.'&quot;'));
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

// = = = = = = = = = = = = = = = = = = = = = content actions section = = = = = = = = = = = = = = = = = = = =

    public function editContentAction()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id'); // here we mean 'content_id' on 'id'
        // inside content grid we use 'id' and 'block_id' parameters
        $model = Mage::getModel('zblocks/content');

        // 2. Initial checking
        if ($id) 
        {
            $model->load($id);
            if (!$model->getId()) 
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This content block no longer exists'));
                $this->_redirect('*/*/'); // !!!!
                return;
            }
        }

        // Set entered data if an error occurred during saving
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) $model->setData($data);

        // set block_id if we add new record to table
        if($this->getRequest()->getParam('block_id')) $model->setZblockId($this->getRequest()->getParam('block_id'));

        // 4. Register model to use later in blocks
        Mage::register('zblocks_content', $model);

        // 5. Build edit form
        $this->_initAction()
            ->_addBreadcrumb($id ? Mage::helper('zblocks')->__('Edit Block') : Mage::helper('zblocks')->__('New Block'), $id ? Mage::helper('zblocks')->__('Edit Block') : Mage::helper('zblocks')->__('New Block'))
            ->_addContent($this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tab_content_edit')->setData('action', $this->getUrl('*/*/saveContent')))
            ->renderLayout();
    }

    public function saveContentAction() 
    {
        if ($data = $this->getRequest()->getPost())
        {
            $blockModel = Mage::getModel('zblocks/zblocks')->load($data['zblock_id']);
            if(!$blockModel->getId())
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('zblocks')->__('This block no longer exists'));
                $this->_redirect('*/*/'); 
                return;
            }

            $model = Mage::getModel('zblocks/content');
            $model->setData($data)->setId($this->getRequest()->getParam('id'));
            try
            {
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('zblocks')->__('Block %s was successfully saved', '&quot;'.$model->getTitle().'&quot;'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) 
                    $this->_redirect('*/*/editContent', array('id' => $model->getId()));
                else
                    $this->_redirect('*/*/edit', array('id' => $data['zblock_id'], 'tab' => 'content'));

                return;
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('zblocks')->__('Unable to find block to save'));
        $this->_redirect('*/*/');
    }

    public function deleteContentAction() 
    {
        if ($id = $this->getRequest()->getParam('id'))
        {
            try 
            {
                $model = Mage::getModel('zblocks/content')->load($id);

                $title = $model->getTitle();
                $zblockId = $model->getZblockId();

                $model->setId($id)->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Block "%s" was successfully deleted', $title));
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        if(isset($zblockId)) $this->_redirect('*/*/edit', array('id' => $zblockId, 'tab' => 'content'));
        else $this->_redirect('*/*/');
    }

// = = = = = = = = = = = = = = = = = = = = = AJAX section = = = = = = = = = = = = = = = = = = = =

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('zblocks/adminhtml_zblocks_grid')->toHtml()
        );
    }

    public function editGridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tab_content_grid')->toHtml()
        );
    }

    public function categoriesAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tab_categories')->toHtml()
        );
    }

    public function categoriesJsonAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tab_categories')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

}
