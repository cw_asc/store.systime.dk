<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php

class AW_Zblocks_Helper_Data extends Mage_Core_Helper_Abstract
{

private $schedulePatterns = array(
            'every day'     => 'Every day',
            'odd days'      => 'Odd days of the month',
            'even days'     => 'Even days of the month',
            '1'             => 'On 1',
            '2'             => 'On 2',
            '3'             => 'On 3',
            '4'             => 'On 4',
            '5'             => 'On 5',
            '6'             => 'On 6',
            '7'             => 'On 7',
            '8'             => 'On 8',
            '9'             => 'On 9',
            '10'            => 'On 10',
            '11'            => 'On 11',
            '12'            => 'On 12',
            '13'            => 'On 13',
            '14'            => 'On 14',
            '15'            => 'On 15',
            '16'            => 'On 16',
            '17'            => 'On 17',
            '18'            => 'On 18',
            '19'            => 'On 19',
            '20'            => 'On 20',
            '21'            => 'On 21',
            '22'            => 'On 22',
            '23'            => 'On 23',
            '24'            => 'On 24',
            '25'            => 'On 25',
            '26'            => 'On 26',
            '27'            => 'On 27',
            '28'            => 'On 28',
            '29'            => 'On 29',
            '30'            => 'On 30',
            '31'            => 'On 31',
            '1,11,21'       => 'On 1, 11, and 21st of the month',
            '1,11,21,31'    => 'On 1, 11, 21, and 31st of the month',
            '10,20,30'      => 'On 10, 20, and 30th of the month',
            'su'            => 'On Sundays',
            'mo'            => 'On Mondays',
            'tu'            => 'On Tuesdays',
            'we'            => 'On Wednesdays',
            'th'            => 'On Thursdays',
            'fr'            => 'On Fridays',
            'sa'            => 'On Saturdays',
            'mon-fri'       => 'From Monday to Friday',
            'sat-sun'       => 'On Saturdays and Sundays',
            'tue-fri'       => 'From Tuesday to Friday',
            'mon-sat'       => 'From Monday to Saturday',
            'mon,wed,fri'   => 'On Mondays, Wednesdays, and Fridays',
            'tue,thu,sat'   => 'On Tuesdays, Thursdays, and Saturdays',
    );                           

private $blockIds = array(
    'Default for using in CMS page template' => array(
        'custom', 
    ),
    'General blocks (will be disaplyed on all pages)' => array(
        'sidebar-right-top', 
        'sidebar-right-bottom',
        'sidebar-left-top',
        'sidebar-left-bottom',
        'content-top',
        'menu-top',
        'menu-bottom',
        'page-bottom',
    ),
    'Catalog and product blocks' => array(
        'catalog-sidebar-right-top',
        'catalog-sidebar-right-bottom',
        'catalog-sidebar-left-top',
        'catalog-sidebar-left-bottom',
        'catalog-content-top',
        'catalog-menu-top',
        'catalog-menu-bottom',
        'catalog-page-bottom',
    ),
    'Product only blocks' => array(
        'product-sidebar-right-top',
        'product-sidebar-right-bottom',
        'product-sidebar-left-top',
        'product-content-top',
        'product-menu-top',
        'product-menu-bottom',
        'product-page-bottom',
        'product-sidebar-left-bottom',
    ),
    'Special blocks' => array(
        'customer-content-top',
        'cart-content-top',
    ),
);

private $rotatorModes = array(
    0 => 'Show all',
    1 => 'Rotate one by one',
    2 => 'Show random',
);

    public function getPatternsToOptionsArray()
    {
        $res = array();

        foreach($this->schedulePatterns as $k=>$v) 
            $res[$k] = $this->__($v);

        return $res;
    }

    public function getBlockIdsToOptionsArray()
    {
        $res = array();
        $counter=0;

        foreach($this->blockIds as $header=>$blocks)
            foreach($blocks as $k) $res[$k] = $this->__(ucwords(str_replace('-', ' ', $k)));

        return $res;
    }

    public function getRotatorModesToOptionsArray()
    {
        $res = array();
        foreach($this->rotatorModes as $k=>$v) $res[$k] = Mage::helper('zblocks')->__($v);
        return $res;
    }

    protected function _getDaySeconds($hours, $minutes, $seconds)
    {
        return $hours*3600 + $minutes*60 + $seconds;
    }

    protected function _isValid($data)
    {

        $time = getdate(time()); 
        $timeZoneOffset = Mage::getSingleton('core/date')->calculateOffset(
                Mage::app()->getStore()->getConfig('general/locale/timezone'));
        $time['hours'] += $timeZoneOffset/3600;

        $daySecondsNow = $this->_getDaySeconds($time['hours'], $time['minutes'], $time['seconds']);
        $today = (int)floor((time()+$timeZoneOffset-$daySecondsNow)); // in seconds

        if(($data['schedule_from_date'] && $today < strtotime($data['schedule_from_date']))
            ||
           ($data['schedule_to_date'] && $today > 1+strtotime($data['schedule_to_date']))) return false; // add 1 second for _very_ slow servers

        switch ($data['schedule_pattern'])
        {
            case 'every day'   : break;
            case 'odd days'    : if(!($time['mday']%2)) return false; break;
            case 'even days'   : if($time['mday']%2) return false; break;
            case '1'           :
            case '2'           :
            case '3'           :
            case '4'           :
            case '5'           :
            case '6'           :
            case '7'           :
            case '8'           :
            case '9'           :
            case '10'          :
            case '11'          :
            case '12'          :
            case '13'          :
            case '14'          :
            case '15'          :
            case '16'          :
            case '17'          :
            case '18'          :
            case '19'          :
            case '20'          :
            case '21'          :
            case '22'          :
            case '23'          :
            case '24'          :
            case '25'          :
            case '26'          :
            case '27'          :
            case '28'          :
            case '29'          :
            case '30'          :
            case '31'          : if((string)$time['mday'] !== $data['schedule_pattern']) return false; break;
            case '1,11,21'     : if(($time['mday']%10)-1 || $time['mday']=='31') return false; break;
            case '1,11,21,31'  : if(($time['mday']%10)-1) return false; break;
            case '10,20,30'    : if($time['mday']%10) return false; break;
            case 'mo'          :
            case 'tu'          :
            case 'we'          :
            case 'th'          :
            case 'fr'          :
            case 'sa'          :
            case 'su'          : if(strtolower(substr($time['weekday'], 0, 2)) !== $data['schedule_pattern']) return false; break;
            case 'mon-fri'     : if(!$time['wday'] || $time['wday'] == 6) return false; break;
            case 'sat-sun'     : if($time['wday'] || $time['wday'] !== 6) return false; break;
            case 'tue-fri'     : if($time['wday'] < 2 || $time['wday'] > 5) return false; break;
            case 'mon-sat'     : if(!$time['wday']) return false; break;
            case 'mon,wed,fri' : if($time['wday'] !== 1 && $time['wday'] !== 3 && $time['wday'] !== 5) return false; break;
            case 'tue,thu,sat' : if($time['wday'] !== 2 && $time['wday'] !== 4 && $time['wday'] !== 6) return false; break;
            default : return false; // unknown pattern? that's incredible!!!
        }

        if($data['schedule_from_time'])
        {
            $time = date_parse($data['schedule_from_time']);
            $timeFrom = $this->_getDaySeconds($time['hour'], $time['minute'], $time['second']);
        }
        else $timeFrom = 0;

        if($data['schedule_to_time'])
        {
            $time = date_parse($data['schedule_to_time']);
            $timeTo = $this->_getDaySeconds($time['hour'], $time['minute'], $time['second']);
        }
        else $timeTo = 0;

        if ($timeFrom && $timeTo)
            if($timeFrom < $timeTo) {
                if ($daySecondsNow < $timeFrom || $daySecondsNow > $timeTo) return false;
            }
            else {
                if (!($daySecondsNow > $timeFrom && $daySecondsNow < $timeTo)) return false;
            }
        else {
            if ($timeFrom && $daySecondsNow < $timeFrom) return false;
            if ($timeTo && $daySecondsNow > $timeTo) return false;
        }

        return true;
    }

    public function getBlocks($customPosition, $blockPosition, $storeId, $categoryPath, $currentCategoryId)
    {
        $blocks=array();

        $collection = Mage::getResourceModel('zblocks/zblocks_collection');
        $collection->getSelect()->where('block_is_active=1');

        if($customPosition) 
                $collection->getSelect()
                    ->where('block_position_custom=?', $customPosition)
                    ->order('block_sort_order');
        elseif($blockPosition)
        {
            if(count($categoryPath)) // if it's category or product page
            {
                $categorySelect = '(show_in_subcategories=0 and find_in_set('.(string)$currentCategoryId.', category_ids)) OR (';

                // if(strpos($blockPosition, 'catalog.')!==false || strpos($blockPosition, 'product.')!==false) 
                    $categorySelect .= "show_in_subcategories=1 AND (category_ids='' OR ";

                foreach($categoryPath as $c)
                    $categorySelect .= 'find_in_set('.$c.', category_ids) OR ';

                $categorySelect = substr($categorySelect, 0, strlen($categorySelect)-4);
                $categorySelect .= '))';
            }
            else
                $categorySelect = "category_ids=''";

            $collection->getSelect()
                ->where('block_position=?', $blockPosition)
                ->where('find_in_set(0, store_ids) OR find_in_set(?, store_ids)', (int)$storeId)
                ->where($categorySelect)
                ->order('block_sort_order');
        }
        else return $blocks; // neither blockId nor block_position specified, illegal call probably from CMS page layout

        $collection->load();

        foreach($collection->getItems() as $item => $val) 
        {
            if(!$this->_isValid($val->getData())) continue;

            $contentColl = Mage::getResourceModel('zblocks/content_collection');
            $contentColl->getSelect() 
                            ->from('', array())
                            ->where('zblock_id=?', $val['zblock_id'])
                            ->where('is_active=?', 1)
                            ->order('sort_order');
            $contentColl->load();

            switch ($val['rotator_mode'])
            {
                case 0: //all
                        foreach($contentColl->getItems() as $k => $v)
                            $blocks[] = $v->getContent();
                        break;

                case 1: // one by one
                        $session = Mage::getSingleton('zblocks/session');
                        $session->start();
                        $oneByOneBlockIds = $session->getOneByOneBlockIds() ? $session->getOneByOneBlockIds() : array();
                        $zblockId = $val['zblock_id'];

                        $takeFirstBlock = true;
                        if(array_key_exists($zblockId, $oneByOneBlockIds))
                        {
                            $currentBlockId = $oneByOneBlockIds[$zblockId];
                            $foundCurrent = false;
                            foreach($contentColl->getItems() as $k => $v)
                                if($foundCurrent)
                                {    // if we got here then the current block is not at the end of the collection
                                    $takeFirstBlock = false; // so, we don't have to take first block
                                    break;
                                }
                                elseif($k == $currentBlockId) $foundCurrent = true;
                        }
                        if($takeFirstBlock)
                            foreach($contentColl->getItems() as $k => $v) break; //we need only first record

                        if(isset($k))
                        {
                            $oneByOneBlockIds[$zblockId] = $k;
                            $session->setOneByOneBlockIds($oneByOneBlockIds);
                            $blocks[] = $v['content'];
                        }
                        break;

                case 2: // random
                        if(!count($contentColl->getItems())) break;
                        $randomBlock = rand(1, count($contentColl->getItems()));
                        $i = 1;
                        foreach($contentColl->getItems() as $k => $v) 
                            if($i == $randomBlock)
                            {
                                $blocks[] = $v['content'];
                                break;
                            }
                            else $i++;
                        break;

                default: // none - we take only first block to show
                        foreach($contentColl->getItems() as $k => $v)
                        {
                            $blocks[] = $v['content'];
                            break; //we need only first record
                        }
            }
        }
        $processor = Mage::getModel('core/email_template_filter');
        foreach($blocks as $k=>$v) $blocks[$k] = $processor->filter($blocks[$k]);

        return $blocks;
    }
}