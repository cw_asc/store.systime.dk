<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php

class AW_Zblocks_Block_Adminhtml_Zblocks_Edit_Tab_Schedule extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('zblocks_activateOn', array('legend'=>Mage::helper('zblocks')->__('Display block:')));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField('schedule_from_date', 'date', array(
            'name'   => 'schedule_from_date',
            'label'  => Mage::helper('zblocks')->__('From Date'),
            'title'  => Mage::helper('zblocks')->__('From Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso,
        ));

        $fieldset->addField('schedule_to_date', 'date', array(
            'name'   => 'schedule_to_date',
            'label'  => Mage::helper('zblocks')->__('To Date'),
            'title'  => Mage::helper('zblocks')->__('To Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso,
        ));

        $fieldset = $form->addFieldset('zblocks_schedule', array('legend'=>Mage::helper('zblocks')->__('Schedule Pattern')));

        $fieldset->addField('schedule_pattern', 'select', array(
            'name'      => 'schedule_pattern',
            'label'     => Mage::helper('zblocks')->__('Show'),
            'title'     => Mage::helper('zblocks')->__('Show'),
            'default'   => current(Mage::helper('zblocks')->getPatternsToOptionsArray()),
            'options'   => Mage::helper('zblocks')->getPatternsToOptionsArray(),
        ));

        $fieldset->addField('schedule_from_time', 'text', array(
            'name'      => 'schedule_from_time',
            'label'     => Mage::helper('zblocks')->__('From Time'),
            'title'     => Mage::helper('zblocks')->__('From Time'),
            'note' => Mage::helper('zblocks')->__('As Hours:Minutes:Seconds'),
        ));

        $fieldset->addField('schedule_to_time', 'text', array(
            'name'      => 'schedule_to_time',
            'label'     => Mage::helper('zblocks')->__('To Time'),
            'title'     => Mage::helper('zblocks')->__('To Time'),
            'note' => Mage::helper('zblocks')->__('As Hours:Minutes:Seconds'),
        ));

        if ( Mage::getSingleton('adminhtml/session')->getZblocksData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getZblocksData());
            Mage::getSingleton('adminhtml/session')->setZblocksData(null);
        } elseif ( Mage::registry('zblocks_data') ) {
            $form->setValues(Mage::registry('zblocks_data')->getData());
        }
        return parent::_prepareForm();
    }
}