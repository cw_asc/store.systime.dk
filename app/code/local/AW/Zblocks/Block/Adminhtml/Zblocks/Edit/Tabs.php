<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php

class AW_Zblocks_Block_Adminhtml_Zblocks_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('zblocks_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('zblocks')->__('Block Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general_section', array(
            'label'     => Mage::helper('zblocks')->__('General Information'),
            'title'     => Mage::helper('zblocks')->__('General Information'),
            'content'   => $this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tab_general')->toHtml(),
        ));

        $this->addTab('content_section', array(
            'label'     => Mage::helper('zblocks')->__('Content'),
            'title'     => Mage::helper('zblocks')->__('Content'),
            'content'   => $this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tab_content')->toHtml(),
        ));

        $this->addTab('schedule_section', array(
            'label'     => Mage::helper('zblocks')->__('Schedule'),
            'title'     => Mage::helper('zblocks')->__('Schedule'),
            'content'   => $this->getLayout()->createBlock('zblocks/adminhtml_zblocks_edit_tab_schedule')->toHtml(),
        ));

        $this->addTab('categories', array(
                'label'     => Mage::helper('catalog')->__('Categories'),
                'url'       => $this->getUrl('*/*/categories', array('_current' => true)),
                'class'     => 'ajax',
            ));

        if($tabName = $this->getRequest()->getParam('tab'))
        {
            $tabName = (strpos($tabName, 'zblocks_tabs_')!==false) // zblocks_tabs_general_section
                        ? substr($tabName, 13)
                        : $tabName.'_section';

            if(isset($this->_tabs[$tabName])) $this->setActiveTab($tabName);
        }

        return parent::_beforeToHtml();
    }
}