<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php

class AW_Zblocks_Block_Adminhtml_Zblocks_Edit_Tab_Content_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('zblocks_content_edit_form'); 
        $this->setTitle(Mage::helper('zblocks')->__('Block Information'));
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('zblocks_content');

        $form = new Varien_Data_Form(array(
                                        'id' => 'edit_form',
                                        'action' => $this->getUrl('*/*/saveContent', array('id' => $this->getRequest()->getParam('id'))),
                                        'method' => 'post',
                                        'enctype' => 'multipart/form-data'
                                    ));

        $fieldset = $form->addFieldset('zblocks_content', array('legend'=>Mage::helper('zblocks')->__('Content Item Information'), 'class' => 'fieldset-wide'));

        $fieldset->addField('zblock_id', 'hidden', array(
            'name'      => 'zblock_id',
            'value'     => $model->getZblockId(),
        ));

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('zblocks')->__('Title'),
            'title'     => Mage::helper('zblocks')->__('Title'),
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('zblocks')->__('Status'),
            'title'     => Mage::helper('zblocks')->__('Status'),
            'name'      => 'is_active',
            'options'   => array(
                '1' => Mage::helper('zblocks')->__('Enabled'),
                '0' => Mage::helper('zblocks')->__('Disabled'),
            ),
        ));

        $fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => Mage::helper('zblocks')->__('Sort Order'),
        ));

        $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label'     => Mage::helper('zblocks')->__('Content'),
            'title'     => Mage::helper('zblocks')->__('Content'),
            'style'     => 'height:36em',
            'wysiwyg'   => false,
            'required'  => true,
        ));

        if ( Mage::getSingleton('adminhtml/session')->getZblocksData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getZblocksData());
            Mage::getSingleton('adminhtml/session')->setZblocksData(null);
        } elseif ( Mage::registry('zblocks_content') ) {
            $form->setValues(Mage::registry('zblocks_content')->getData());
        }

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}