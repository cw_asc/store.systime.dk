<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php

class AW_Zblocks_Block_Adminhtml_Zblocks_Edit_Tab_Content_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'zblocks';
        $this->_controller = 'adminhtml_zblocks_edit_tab_content';

        $zblockId = ($this->getRequest()->getParam('block_id'))?($this->getRequest()->getParam('block_id')):(Mage::registry('zblocks_content')->getZblockId());

        $this->_updateButton('back', 'onclick', "setLocation('".$this->getUrl('*/*/edit', array('id'=>$zblockId, 'tab' => 'content'))."')");

        $this->_updateButton('delete', 'label', Mage::helper('zblocks')->__('Delete Item'));
        $this->_updateButton('delete', 'onclick', "setLocation('".$this->getUrl('*/*/deleteContent', array('id'=>$this->getRequest()->getParam('id')))."')");

        $this->_updateButton('save', 'label', Mage::helper('zblocks')->__('Save Item'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('zblocks')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('zblocks_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'zblocks_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'zblocks_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('zblocks_content') && Mage::registry('zblocks_content')->getId() ) 
            return Mage::helper('zblocks')->__("Edit Content Item '%s'", $this->htmlEscape(Mage::registry('zblocks_content')->getTitle()));
        else 
            return Mage::helper('zblocks')->__('Add Content Item');
    }
}