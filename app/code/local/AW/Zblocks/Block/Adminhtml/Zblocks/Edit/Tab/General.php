<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php

class AW_Zblocks_Block_Adminhtml_Zblocks_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('zblocks_schedule', array('legend'=>Mage::helper('zblocks')->__('General Information')));

        $fieldset->addField('block_title', 'text', array(
            'name'      => 'block_title',
            'label'     => Mage::helper('zblocks')->__('Block Title'),
            'title'     => Mage::helper('zblocks')->__('Block Title'),
        ));

        $model=Mage::registry('zblocks_data');
        $fieldset->addField('creation_time', 'hidden', array(
                'name'      => 'creation_time',
                'value'     => $model['creation_time'],
            ));

        if (!Mage::app()->isSingleStoreMode()) 
        {
            $fieldset->addField('store_ids', 'multiselect', array(
                'name'      => 'store_ids[]',
                'label'     => Mage::helper('zblocks')->__('Store View'),
                'title'     => Mage::helper('zblocks')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        }
        else {
            $fieldset->addField('store_ids', 'hidden', array(
                'name'      => 'store_ids[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreIds(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('block_is_active', 'select', array(
            'label'     => Mage::helper('zblocks')->__('Status'),
            'title'     => Mage::helper('zblocks')->__('Status'),
            'name'      => 'block_is_active',
            'options'   => array(
                '1' => Mage::helper('zblocks')->__('Enabled'),
                '0' => Mage::helper('zblocks')->__('Disabled'),
            ),
        ));

        $fieldset->addField('block_position', 'select', array(
            'label'     => Mage::helper('zblocks')->__('Block Position'),
            'title'     => Mage::helper('zblocks')->__('Block Position'),
            'name'      => 'block_position',
            'options'   => Mage::helper('zblocks')->getBlockIdsToOptionsArray(),
        ));

        $fieldset->addField('block_position_custom', 'text', array(
            'name'      => 'block_position_custom',
            'label'     => Mage::helper('zblocks')->__('Custom Position'),
            'title'     => Mage::helper('zblocks')->__('Custom Position'),
            'note' => Mage::helper('zblocks')->__("Required if Block Position is set to 'Custom'"),
        ));

        $fieldset->addField('show_in_subcategories', 'select', array(
            'label'     => Mage::helper('zblocks')->__('Show in subcategories'),
            'title'     => Mage::helper('zblocks')->__('Show in subcategories'),
            'name'      => 'show_in_subcategories',
            'note'      => Mage::helper('zblocks')->__('Show in subcategories and products'),
            'options'   => array(
                '1' => Mage::helper('zblocks')->__('Enabled'),
                '0' => Mage::helper('zblocks')->__('Disabled'),
            ),
        ));

        $fieldset->addField('rotator_mode', 'select', array(
            'label'     => Mage::helper('zblocks')->__('Mode'),
            'title'     => Mage::helper('zblocks')->__('Mode'),
            'name'      => 'rotator_mode',
            'options'   => Mage::helper('zblocks')->getRotatorModesToOptionsArray(),
        ));

        $fieldset->addField('block_sort_order', 'text', array(
            'name' => 'block_sort_order',
            'label' => Mage::helper('zblocks')->__('Sort Order'),
            'title' => Mage::helper('zblocks')->__('Sort Order'),
        ));

        if ( Mage::getSingleton('adminhtml/session')->getZblocksData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getZblocksData());
            Mage::getSingleton('adminhtml/session')->setZblocksData(null);
        } elseif ( Mage::registry('zblocks_data') ) {
            $form->setValues(Mage::registry('zblocks_data')->getData());
        }
        return parent::_prepareForm();
    }
}
