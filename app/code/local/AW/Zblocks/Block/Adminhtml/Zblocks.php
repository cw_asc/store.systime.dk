<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */
?>
<?php
class AW_Zblocks_Block_Adminhtml_Zblocks extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_zblocks';
    $this->_blockGroup = 'zblocks';
    $this->_headerText = Mage::helper('zblocks')->__('Z-Blocks');
    $this->_addButtonLabel = Mage::helper('zblocks')->__('Add Block');
    parent::__construct();
  }
}
