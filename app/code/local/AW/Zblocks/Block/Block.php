<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Zblocks
 * @copyright  Copyright (c) 2008-2009 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */

class AW_Zblocks_Block_Block extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        $categoryPath = array();
        $currentCategoryId = 0;

        if ($this->getRequest()->getControllerName()=='category' && $c = $this->getRequest()->getParam('cat'))
        {
            $categoryPath[] = $c;
            $currentCategoryId = $c;
        }

        if($c = Mage::registry("current_product")) 
        {
            $data = $c->getData();

            if(is_array($data['category_ids'])) $categoryIds = $data['category_ids'];
            else $categoryIds = explode(',', $data['category_ids']);

            $categoryPath = array_merge($categoryPath, $categoryIds);
            if(!$currentCategoryId) $currentCategoryId = reset($categoryIds);
        }

        if ($c = Mage::registry("current_category")) 
        {
            $data = $c->getData();
            if(!$currentCategoryId) $currentCategoryId = $data['entity_id'];
            $categoryPath = array_merge($categoryPath, explode('/', $data['path']));
        }
        $categoryPath = array_unique($categoryPath);
        foreach($categoryPath as $k => $v) if(!$v) unset($categoryPath[$k]);

        $position = $this->getPosition() ? $this->getPosition() : '';
        $blockPosition = $this->getBlockPosition() ? $this->getBlockPosition() : '';

        return implode('', 
            Mage::helper('zblocks')->getBlocks(
                $position,
                $blockPosition,
                Mage::app()->getStore(true)->getId(),
                $categoryPath,
                $currentCategoryId
            ));
    }
}