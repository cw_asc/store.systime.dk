<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
		<title>Frontpage</title>
		<style type="text/css">
@import "http://ajax.googleapis.com/ajax/libs/yui/2.8.0r4/build/reset-fonts/reset-fonts.css";
@import "http://ajax.googleapis.com/ajax/libs/yui/2.8.0r4/build/base/base-min.css";

object {
	width: 100%;
	height: 200px;
	border: solid 1px #000;
}
		</style>
	</head>
	<body>
<?php
$sourceUrl = 'http://udvikling.systime.dk/index.php?id=1560';
$targetDir = basename(dirname(__FILE__));
$baseUrl = 'http://'.$_SERVER['SERVER_NAME'].'/'.$targetDir.'/';
$htmlFilename = 'index.html';
$targetUrl = $baseUrl.'/'.$htmlFilename;
?>

<h1>Source (<?php echo htmlspecialchars($sourceUrl) ?>)</h1>
<object data="<?php echo htmlspecialchars($sourceUrl) ?>" type="text/html"></object>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
function execute($cmd) {
if (is_array($cmd)) {
$cmd = implode(' ', $cmd);
}

echo $cmd;

exec($cmd.' 2>&1', $output, $result);

if ($result !== 0) {
// debug(array($cmd, $result, $output), __METHOD__);
echo "\n";
echo 'Command failed: '.$cmd.' (exit code: '.$result.'):', "\n";
echo implode("\n", $output), "\n";
}
echo "\n";
flush();
}

function info() {
for ($i = 0; $i < func_num_args(); $i++) {
echo htmlspecialchars(func_get_arg($i));
echo ' ';
}
echo '<br/>';
flush();
}

// header('Content-type: text/plain');

// Guess location of "wget"
$wget = 'wget';
foreach (array('/usr/bin', '/opt/local/bin') as $dir) {
$path = realpath($dir.'/wget');
if (is_executable($path)) {
$wget = $path;
break;
}
}

// info('sourceUrl:', $sourceUrl);
// info('baseUrl:', $baseUrl);

chdir(dirname(__FILE__));

info();
info('Fetching resources (CSS and images)');
$cmd = array($wget,
'--no-verbose',
'--no-host-directories',
'--page-requisites',
'--convert-links',
$sourceUrl);
execute($cmd);

info();
info('Fetching frontpage');
$cmd = array($wget,
'--no-verbose',
'--output-document='.$htmlFilename,
$sourceUrl);
execute($cmd);

info();
info('Adding <base/>');

$contents = file_get_contents($htmlFilename);

$contents = preg_replace('@(<head[^>]*>)@', '\1<base href="'.htmlspecialchars($baseUrl).'"/>', $contents);
// info(array('contents' => $contents));
file_put_contents($htmlFilename, $contents);

info('Done!');
}
?>

<h1>Target (<?php echo htmlspecialchars($targetUrl) ?>)</h1>
<object data="<?php echo htmlspecialchars($targetUrl) ?>" type="text/html"></object>

<form action="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']) ?>" method="post">
<button>Refresh</button>
<span class="info">Click to refresh local copy (target)</span>
</form>

</body>
</html>
